var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractCSS = new ExtractTextPlugin('styles/[name]/[name].css');
const CleanCSSPlugin = require("less-plugin-clean-css");
const ModernizrWebpackPlugin = require('modernizr-webpack-plugin');


const extractCommons = new webpack.optimize.CommonsChunkPlugin({
  names: ["common", "vendor"],
	minChunks: 2,
   //children: true,
  // async: true,

});


let config = {
  context: path.resolve(__dirname, 'src'),
  entry: {
    dudiscovery: './apps/dudiscovery',
   // dudiscovery_ar: './apps/dudiscovery_ar',
    eshop: './apps/eshop',
   selfcare: './apps/selfcare',
   // selfcare_ar: './apps/selfcare_ar',
    vendor: [ 'jquery', 'hammerjs' ]
  },
  output: {
    path: path.join(__dirname, 'dist'),
    publicPath: "/dist/",
    filename: 'js/[name]/[name].js',
    chunkFilename: "js/[name].chunk.js"
    
  },
  devtool: "source-map",
  module: {
    
    rules: [{
      test: /\.css$/,
      loader: extractCSS.extract(['css-loader'])
    },
    {
      test: /\.scss$/,
      loader: extractCSS.extract(['css-loader', 'sass-loader'])
    },
    {
      test: /\.less$/,
      loader: extractCSS.extract([{
        loader: 'css-loader', options: {
          sourceMap: true
        }
      }, {
        loader: 'less-loader', options: {
          sourceMap: true,
          compress: true,

          plugins: [
            new CleanCSSPlugin({ advanced: true })
          ]
        }
      }])
    },
    {
      test: /\.js$/,
      exclude: /(node_modules)/,
      use: [{
        loader: 'babel-loader',
        options: {
          presets: [['es2015', { modules: false }]],
          plugins: ['syntax-dynamic-import']
        }
      }]
    },
    {
      test: /\.(jpe?g|png|gif|svg)$/i,
      use: [
        {
          loader: "file-loader",
          options: {
            hash: 'sha512&digest',
            digest: 'hex',
            name: 'images/[name].[ext]'
          }
        },
        {
          loader: "image-webpack-loader",
          options: {

            optipng: {
              optimizationLevel: 7,
            },
            gifsicle: {
              interlaced: true,
            }
          }
        }
      ]
    },
    { test: /\.svg$/, loader: 'url-loader?limit=65000&mimetype=image/svg+xml&name=fonts/[name].[ext]' },
    { test: /\.woff$/, loader: 'url-loader?limit=65000&mimetype=application/font-woff&name=fonts/[name].[ext]' },
    { test: /\.woff2$/, loader: 'url-loader?limit=65000&mimetype=application/font-woff2&name=fonts/[name].[ext]' },
    { test: /\.[ot]tf$/, loader: 'url-loader?limit=65000&mimetype=application/octet-stream&name=fonts/[name].[ext]' },
    { test: /\.eot$/, loader: 'url-loader?limit=65000&mimetype=application/vnd.ms-fontobject&name=fonts/[name].[ext]' },
     // HTML: htm, html
       {
         test: /\.html?$/,
         loader: "file?name=[name].[ext]"
       },



    ]
  },
  plugins: [
    //new ExtractTextPlugin('styles.css'),
   extractCommons,
    extractCSS,
    /*new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        drop_console: false,
      }
    }),*/
    new ModernizrWebpackPlugin(),
    new webpack.optimize.ModuleConcatenationPlugin(),
    
    new webpack.ProvidePlugin({
           $: 'jquery',
            jQuery: 'jquery',
            "window.jQuery": 'jquery',
            "Hammer" : 'hammerjs',
  
      }),
      new webpack.SourceMapDevToolPlugin({
        filename: '[name].js.map',
        exclude: ['common.js','vendor.js']
      })
  ],

  resolve: {
    extensions: ['.js', '.css'],
    // add alias for application code directory
    alias:{
      core: path.resolve( __dirname, './src/core' ),
  //    'masonry/masonry': path.resolve(__dirname, './src/node_modules/masonry-layout/masonry.js'),
      //hammerjs: path.resolve( __dirname, './src/node_modules/hammerjs/hammer.min' ),
      'bridget': 'jquery-bridget',
      'masonry': 'masonry-layout',
      'isotope': 'isotope-layout'
    }
  }

};
module.exports = config;
