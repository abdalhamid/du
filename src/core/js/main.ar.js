//localStorage.clear();

requirejs.config({
    "baseUrl": "/du/common/scripts",
    "paths": {
        // the left side is the module ID,
        // the right side is the path to
        // the jQuery file, relative to baseUrl.
        // Also, the path should NOT include
        // the '.js' file extension. This example
        // is using jQuery 1.9.0 located at
        // js/lib/jquery-1.9.0.js, relative to
        // the HTML page.
       // 'jquery': 'jquery-1.11.2.min',
       'jquery': 'jquery-3.0.0.min',
          'datatables': 'prototype/jquery.dataTables.min',
        'icheck':'prototype/icheck',
        isotope: 'jquery.isotope.min',
        bridget: 'jquery-bridget.min'
    },
    shim: {
        "icheck": ['jquery'],
         "isotope": ['jquery']
    }
});

// Load the main app module to start the app
//requirejs(["app"]);

require(['jquery', 'datatables','icheck','isotope', 'bridget'], function ($,datatables, icheck,isotope,base) {
    isotope.LayoutMode.create('none');
    require([
        'modernizr-2.6.2.min',
        'jquery.imagesloaded.min',
        'hammer.min',
        'jquery.hammer.min',
        'tabulous.min',
        'jquery.reject.min',
        'respond.min',
		'jquery.barrating',
        'jquery.cookie.min',
		'motion-ui.min',
		'countrySelect.min',
        'jquery.bxslider',
        'du/duTabs.min',
        'du/duDetect.min',
        'du/duColumns.min',
        'du/duScroll.min',
        'du/duSlide.min',
        'du/duBuy.min',
        'du/duProductScroll.min',
        'du/duScrollToTop.min',
        'du/duCoverflow.min',
        'du/layoutRpr.min',
        'prototype/jquery.reveal.min',
        'prototype/jquery.alert.min',
        'prototype/bootstrap.popover.min',
        'prototype/jquery.knob.min',
        'prototype/dropzone.min',
        'prototype/jquery.dial.ar.min',
        'prototype/jquery.timer.min',
        'prototype/jquery.validator.min',
        'prototype/protoSlide.min',
        'prototype/jquery.selectric.min',
        'prototype/d3.min',
        'prototype/jquery.charter.ar.min',
        'prototype/jquery.datetimepicker.min',
        'prototype/jquery.dateRangeFilter.min',
        'prototype/jquery.dateSort.min',
        'prototype/jquery.matchHeight.min',
	 'du/duLoginTabs',
        'du/duSupportTickets.min',
        'du/duQuickRecharge.Autocomplete.min',
    ], function () {
        require([
            'du/init_global.min',
            'du/ie_ar.min',
            'du/init_ar.min',
            /*'all.du.ar.min'*/
            'custom/custom.min'
        ])
    });
});