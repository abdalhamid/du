	function changeLocale(){
		$("form#changeLanguageForm").submit();
	}

	function redirectOnDuPage(index){
		$("form#menu"+index).submit();
	}

	function submitLogoutForm() {
       $("form#logoutForm").submit();
    }
	
	function removeAlert(id){ 
    	$("#"+id).remove();  
    }
	
	function submitProductDetail() {
       $("form.productDetailForm").submit();
    }
	
	$(function(){
			var list = $('.breadcrumbs');
			var listItems = list.children('a.main');
			list.append(listItems.get().reverse());
	});
	
	function submitForm(idForm) {
		if(idForm ==='formDashboard'){
			window.location.href = $("#"+idForm+" #urlDashboard").val();
		} else{
			$("form#"+idForm).submit();
		}
		
	}
	
	function showMoreLess(index) {
        if(index != undefined){
        	$("#overhead-details-resp"+index).toggleClass("content-active");
        }else{
        	$("#overhead-details-resp").toggleClass("content-active");
        }
    }
	
	var validators = {
	    "credit-card-number": /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|3[47][0-9]{13})$/,
	    "cvv-number": /^[0-9]{3}$/,
	    "number": /^[0-9]*$/,
	    "alphabetic": /^[a-zA-Z ]*$/,
//	    "amount": /(^[0-9]*[1-9]+[0-9]*\.[0-9]*$)|(^[0-9]*\.[0-9]*[1-9]+[0-9]*$)|(^[0-9]*[1-9]+[0-9]*$)/,
	    //min 1.00 max 30000.00
	    "account": /^(([0-9][.]([0-9]|[0-9][.][0-9])+)|(((971[2|3|4|6|7|9]))[0-9]{7})|(((9715))[0-9]{8}))$/,
	    "amount": /^(([1-9][0-9]{0,3}(\.[0-9][0-9]?)?)|([1-2][0-9][0-9][0-9][0-9](\.[0-9][0-9]?)?|[1-3][0][0][0][0](\.[0][0]?)?))$/,
	    "password": /(?=.*[0-9])(?=.*[a-zA-Z])^([a-zA-Z0-9]){8,12}$/,
	    "email": /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$|(^$)/,
	    "primaryContact": /^([+]?971[0-9]{9})$/,
	    "prefered-number": /^(971[0-9]{9})$/,
	    "nickname": /^([a-zA-Z0-9]{2,9})$/,
	    "percent": /^(\d?[1-9]|[1-9]0)$/,
        "phoneNumber": /^((((971[2|3|4|6|7|9]))[0-9]{7})|(((9715))[0-9]{8}))$|(^$)/,
        "nickname": /^([a-zA-Z0-9]{2,12})$/,
        "mobile": /^(((9715))[0-9]{8})$/,
        "landline": /^(((971[2|3|4|6|7|9]))[0-9]{7})$/,
        "voucher": /^([0-9]{15})$/,
        "recharge" : /^([1-4][0-9][0-9]|(500)|[2-9][0-9])$/
	} 
	function validateForm(formId) {
		var _this = $("#"+formId);
		var formID=formId; 
		var foundError = false;
		$("#"+formID+" "+".control").each(function () { 
			var $this = $(this);
			if (!$this.is(":hidden") || !(!$this.closest(".selectricWrapper").length || $this.closest(".selectricWrapper").is(":hidden"))) {
				if ($this.data("required") && $this.val() == "") {
					foundError = true;
				} else {
					switch ($this.data("valid")) {
					default:
						if (!$this.val().match(validators[$this.data("valid")])) {
							foundError = true;
						}
					break;
					case "match":
						if ($this.val() != $($this.data("match-with")).val()) {
							foundError = true;
						}
						break;
					}
				}
				if($('.usage-alert').length && $('.usage-alert input.control:eq(0)').val() == $('.usage-alert input.control:eq(1)').val()){
					foundError = true;
                }
			}
		});
		if (foundError) {
			return false;
		}
		else{
			return true;
		}
	}
	
	function submitPromotion(){
 		$("form#promotion").submit();
 	} 
	
	/* Closa alert function in dashboard */
//	function closeAlert(){  
//    	$("div.alert").removeClass("active");  
//    };
	
	function closeAlert(a){
		if(a!=null) {
			$(a).parent().removeClass("active");
		} 
	};
    
	function callerTunes(){
		window.location.href = "/DU/init.jsp";
	}
	function submitFormSettings(idForm){
		/*form validation */
		$("form#"+idForm).submit();
	}
	function submitUpgradeDowngrade(indexContract,indexMenu){
		/*form validation */
		$("#upgradeDowngrade"+indexContract+"-"+indexMenu).submit();
	}

	function isDowngrade(currentSpeed, newSpeed){
		var result = false;
		var downloadCurrentSpeedMeasure= currentSpeed.substring(0, currentSpeed.indexOf("/")).replace(/[0-9]/g, "");
		var uploadCurrentSpeedMeasure= currentSpeed.substring(currentSpeed.indexOf("/")+1).replace(/[0-9]/g, "");
		var downloadNewSpeedMeasure= newSpeed.substring(0, newSpeed.indexOf("/")).replace(/[0-9]/g, "");
		var uploadNewSpeedMeasure= newSpeed.substring(newSpeed.indexOf("/")+1).replace(/[0-9]/g, "");
		
		var downloadCurrentSpeed = parseInt(currentSpeed.substring(0, currentSpeed.indexOf("/")).trim().replace(" ", "").replace("Mbps", "").replace("Kbps", ""));
		var uploadCurrentSpeed = parseInt(currentSpeed.substring(currentSpeed.indexOf("/")+1).trim().replace(" ", "").replace("Mbps", "").replace("Kbps", ""));
		var downloadNewSpeed = parseInt(newSpeed.substring(0, newSpeed.indexOf("/")).trim().replace(" ", "").replace("Mbps", "").replace("Kbps", ""));
		var uploadNewSpeed = parseInt(newSpeed.substring(newSpeed.indexOf("/")+1).trim().replace(" ", "").replace("Mbps", "").replace("Kbps", ""));
		
		if((downloadCurrentSpeedMeasure === "Mbps")&&(downloadNewSpeedMeasure === "Kbps")){
			result=true;
		} else if(downloadCurrentSpeedMeasure === downloadNewSpeedMeasure){
			if((uploadCurrentSpeedMeasure === "Mbps")&&(uploadNewSpeedMeasure  === "Kbps")){
				result=true;
			}else if(uploadCurrentSpeedMeasure === uploadNewSpeedMeasure){
				if(downloadCurrentSpeed>downloadNewSpeed){
					result=true;
				}else if((downloadCurrentSpeed==downloadNewSpeed)&&(uploadCurrentSpeed>uploadNewSpeed)){
					result=true;
				}
			}
		}
		return result;
	}
	
	function submitLastFormMenu(index){
		/*form validation */
		$("#LastformMenu"+index).submit();
	}
	
	$(document).ready(function(){
		$("#hrefLastBill").click(function() {
			$("#UsageHrefForm").submit();
		});
	});
	
	function goToPayment(){
		$("form#formStartPayment").submit();
	}
	
	function goToAutopayment() {
		$("#formMenuAutoPayment").submit();
	}
	
	function sortContractAddonsDashboard(index){
		var mylist = $('#contractAddons'+index+" ul"); // ul DOM element
		var listitems = mylist.children('li').get();
		listitems.sort(function(a, b) {
		   return $(a).children('div.text').children('h4.regular').text().toUpperCase().localeCompare($(b).children('div.text').children('h4.regular').text().toUpperCase());
		})
		mylist.empty().append(listitems);
	}
	
	function submitMenu(index,isAutopayment,contractIndex){
		var url= $("#address"+index).val();
		if(isAutopayment == true){
			url = url.replace(/\?+\S*/, "");
			$("#address"+index).val(url+"?index="+contractIndex);
		}
		$("#formMenu"+index).submit();
	}
    
	function goToRechargeWallet(wallet,index){
		var url= $("#sAddressWallet"+index).val();
		url = url.replace(/\?+\S*/, "");
		$("#sAddressWallet"+index).val(url + "?oldPrepaid="+wallet+"|"+index+"|wallet");
		$("#formRechargeWallet"+index).submit();
	}
	
	function submitManageForm(){
	   /*form validation */
		$("form#formManagement").submit();
	}
	 
	$(document).ready(function(){
		$('#view-data-pool').DataTable({
            "bFilter": false,
            "info": false
        }).draw();
        
        $('#add-data-pool').DataTable({
            "bFilter": false,
            "info": false
        }).draw();
	});
