/*
 *
 * duProductScroll JS class used to manage horizontal scrolling
 *
 * Requirements: jQuery, Hammer.js, Modernizr.js
 *
 * @param: object { 'target': HTMLObject, 'enableTouch': boolean, 'snap': boolean, 'direction': string }
 * @return: void
 *
 *
 */
 
	function duProductScroll($options) {
		/* Properties */
		this.options = { 'target': null, 'enableTouch': false, 'snap': false, 'direction': 'ltr' };

		/* Methods declarations */
		this.goToZero = goToZero;
		this.getWidth = getWidth;
		this.preventClick = preventClick;
		this.getPercentage = getPercentage;
		this.useTransforms = useTransforms;
		this.fixPosition = fixPosition;
		this.unsetButtonsActions = unsetButtonsActions;
		this.setButtonsActions = setButtonsActions;
		this.setItemsPosition = setItemsPosition;

		this.fromItemsMoveDraggedElement = fromItemsMoveDraggedElement;

		this.windowResize = windowResize;
		this.setWidthAndHeight = setWidthAndHeight;
		this.init = init;

		/* Internal variables */
		this.currentWidth = 0; // Used to understand if there is the need to recalculate everything or not on screen resize
		this.currentHeight = 0; // Used to understand if there is the need to recalculate everything or not on screen resize
		this.items = null;
		this.mask = null;
		this.itemCollection = null;
		this.stepWidth = 0;
		this.scrollArea = null;
		this.scrollBar = null;
		this.scrollObj = null;
		this.buttons = null;
		this.slideDistance = 0;
		this.itemsDistance = 0;
		this.currentItemsPosition = 0;
		this.useTransformsCache = ($('html').hasClass('csstransforms3d')) ? true :Modernizr.csstransforms3d;
		this.isTouch = ($('body').hasClass('mobile')) ? true : Modernizr.touch;
		this.buttonsVisible = false;
		this.buttonRight = undefined;
		this.buttonLeft = undefined;


		/* Methods */

		function goToZero() {
			var du = this;
			if (du.options.direction == "rtl") {

				du.currentItemsPosition = (du.mask.width() - du.items.width());
				du.currentItemsPercentage = 100;
				du.buttonLeft.removeClass('inactive');
				du.buttonRight.addClass('inactive');
				du.options.target.removeClass('at-the-beginning');
				du.options.target.addClass('at-the-end');

			} else {

				du.currentItemsPosition = 0;
				du.currentItemsPercentage = 0;
				du.buttonLeft.addClass('inactive');
				du.buttonRight.removeClass('inactive');
				du.options.target.addClass('at-the-beginning');
				du.options.target.removeClass('at-the-end');

			}
			du.setItemsPosition();
		}

		function getWidth() {
			var du = this,
				w = 0;
			if ((du.itemCollection.css('float') == 'left') || (du.itemCollection.css('float') == 'right')) {
				du.itemCollection.each(
					function () {
						w += $(this).outerWidth(true);
					}
				);
			} else {
				w = du.itemCollection.outerWidth(true);
			}
			return w;
		}

		/*
		 *
		 * preventClick
		 * @param: bool
		 * @return: void
		 *
		 */
		function preventClick(action) {
			var du = this;
			if (action) {
				du.items.find('a').bind('click', function (e) { e.preventDefault(); });
			} else {
				du.items.find('a').unbind('click');
			}
		}

		/*
		 *
		 * getPercentage
		 * @param: float, float
		 * @return: float
		 *
		 */
		function getPercentage(total, part) {
			var du = this;
			return (100 * part) / total;
		}

		/*
		 *
		 * useTransforms
		 * @param: void
		 * @return: boolean
		 *
		 */
		function useTransforms() {
			var du = this;
			return du.useTransformsCache;
		}

		/*
		 *
		 * fixPosition
		 * @param: string, int, string
		 * @return: void
		 *
		 */

		function fixPosition() {
			var du = this;
			if (du.options.snap) {
				var elementWidth = du.itemCollection.outerWidth(true),
					newPosition = Math.round(du.currentItemsPosition / elementWidth) * elementWidth;
				du.currentItemsPosition = newPosition;
				du.setItemsPosition();
			}
		}

		/*
		 *
		 * unsetButtonsActions
		 * @param: void
		 * @return: void
		 *
		 */
		function unsetButtonsActions() {
			var du = this;
			du.buttons.unbind('click');
		}

		/*
		 *
		 * setButtonsActions
		 * @param: void
		 * @return: void
		 *
		 */
		function setButtonsActions() {
			var du = this;
			du.buttons
				.unbind('click')
				.bind(
				'click',
				function (e) {
					e.preventDefault();
					du.unsetButtonsActions();
					var minPos = du.mask.width() - (du.items.width()),
						// minPos = (Math.floor((du.mask.width() - du.getWidth()) / du.stepWidth) * du.stepWidth),
						maxPos = 0,
						elementWidth = du.itemCollection.outerWidth(true);
					if ($(this).hasClass('moveLeft')) {

						if (!$(this).hasClass('inactive')) {
							if ((du.currentItemsPosition + elementWidth) <= maxPos) {
								du.currentItemsPosition = du.currentItemsPosition + elementWidth;
								du.buttonRight.removeClass('inactive');
								du.options.target.removeClass('at-the-end');
								if (du.currentItemsPosition >= maxPos) {
									du.buttonLeft.addClass('inactive');
									du.options.target.addClass('at-the-beginning');
								} else {
									du.buttonLeft.removeClass('inactive');
								}
								du.setItemsPosition();
							}
						}

					} else {

						if (!$(this).hasClass('inactive')) {
							if ((du.currentItemsPosition - elementWidth - minPos) >= -10) {
								du.currentItemsPosition = du.currentItemsPosition - elementWidth;
								du.buttonLeft.removeClass('inactive');
								du.options.target.removeClass('at-the-beginning');
								if ((du.currentItemsPosition - elementWidth - minPos) <= -10) {
									du.buttonRight.addClass('inactive');
									du.options.target.addClass('at-the-end');
								} else {
									du.buttonRight.removeClass('inactive');
								}
								du.setItemsPosition();
							}
						}
					}
					du.setButtonsActions();
				}

				);
		}

		/*
		 *
		 * setItemsPosition
		 * @param: void
		 * @return: void
		 *
		 */
		function setItemsPosition() {
			var du = this,
				minPos = du.mask.width() - (du.getWidth()),
				maxPos = 0;
			if (du.useTransforms()) {
				du.items.css(
					{
						"-webkit-transform": "translate3d(" + du.currentItemsPosition + "px,0,0)",
						"-moz-transform": "translate3d(" + du.currentItemsPosition + "px,0,0)",
						"-o-transform": "translate3d(" + du.currentItemsPosition + "px,0,0)",
						"-ms-transform": "translate3d(" + du.currentItemsPosition + "px,0,0)",
						"transform": "translate3d(" + du.currentItemsPosition + "px,0,0)"
					}
				);
			} else {
				// du.items.css( { 'left' : du.currentItemsPosition + 'px' } );
				du.items.animate(
					{
						'left': du.currentItemsPosition + 'px'
					}
				);
			}
		}

		/*
		 * windowResize
		 * @param: string, int, string
		 * @return: void
		 *
		 */
		function fromItemsMoveDraggedElement(target, distance, direction) {
			var du = this;
			du.currentItemsPosition = function computeNewPosition() {
				var newPosition = distance - du.itemsDistance,
					minPos = du.mask.width() - (du.items.width()),
					maxPos = 0;
				du.itemsDistance = distance;
				if (direction == 'left') {
					newPosition = -1 * newPosition;
				}
				if ((du.currentItemsPosition + newPosition) >= minPos && (du.currentItemsPosition + newPosition) <= maxPos) {
					newPosition = (du.currentItemsPosition + newPosition);
				} else {
					if ((du.currentItemsPosition + newPosition) < minPos) newPosition = minPos;
					else newPosition = maxPos;
				}
				return newPosition;
			}();
			du.currentItemsPercentage = getPercentage((du.mask.width() - du.items.width()), du.currentItemsPosition);
			du.setItemsPosition();
		}

		/*
		 * windowResize
		 * @param: void
		 * @return: void
		 * Called on window resize
		 *
		 */
		function windowResize() {
			var du = this,
				// Give the browser some time to apply media-queries and resize elements
				timeout = setTimeout(
					function () {
						clearTimeout(timeout);
						du.setWidthAndHeight();
						// Reset Positions
						du.goToZero();
					},
					250
				);
		}

		/*
		 * setWidthAndHeight
		 * @param: void
		 * @return: void
		 * Called on class initialization and or resize, sets the width of the items' container
		 *
		 */
		function setWidthAndHeight() {
			var du = this,
				newWidth = du.getWidth(),
				newHeight = du.itemCollection.outerHeight(true),
				maskWidth = du.mask.width(), handlerWidth = 50;
			du.stepWidth = du.itemCollection.outerWidth(true);
			// Width of the items
			if (newWidth != du.currentWidth) {
				du.items.css({ 'width': newWidth + 'px' });
				du.currentWidth = newWidth;
			}
			// Height of the entire box
			if (newHeight != du.currentHeight) {
				du.mask.css({ 'height': newHeight + 'px' });
				du.currentHeight = newHeight;
				// Fix buttons position
				// on screen resize
				$(du.buttons).css({ 'top': ((du.mask.outerHeight(true) / 2) - ($('.buttonScroll').outerHeight(true) / 2)) + 'px' });
			}
			// Width of the handler
			if (newWidth <= maskWidth) {
				du.buttonsVisible = false;
			} else {
				du.buttonsVisible = true;
			}
			if (!du.buttonsVisible) du.buttons.css({ 'display': 'none' });
			else du.buttons.css({ 'display': 'block' });

			if ($('body').hasClass('mobile')) du.buttons.css({ 'display': 'none' });
			if ($('body').hasClass('half-touch') && du.buttonsVisible) du.buttons.css({ 'display': 'block' });
		}

		/*
		 *
		 * init
		 * @param: void
		 * @return: void
		 * Called on class initialization
		 *
		 */
		function init() {
			var du = this, tmo = undefined;
			du.items = du.options.target.find('.items');
			du.itemCollection = du.options.target.find('.item');
			du.mask = du.options.target.find('.itemsW');
			du.stepWidth = du.itemCollection.outerWidth(true);
			du.scrollArea = du.options.target.find('.slider');
			du.scrollBar = du.options.target.find('.handle');
			du.buttons = du.options.target.find('.buttonScroll');
			du.buttonLeft = du.options.target.find('.buttonScroll.moveLeft');
			du.buttonRight = du.options.target.find('.buttonScroll.moveRight');

			$(du.itemCollection.get((du.itemCollection.length - 1))).addClass('last-item');

			/* Add listener to the buttons */
			du.setButtonsActions();

			/* Add listeners on swipe if needed */
			if (du.options.enableTouch && du.isTouch) {
				du.items.hammer(
					{
						"prevent_default": false,
						"drag": true,
						"drag_vertical": false,
						"drag_horizontal": true,
						"drag_min_distance": 0,
						"transform": false,
						"tap": false,
						"tap_double": false,
						"hold": false
					}
				)
					.unbind("dragstart drag dragend")
					.bind(
					"dragstart drag dragend",
					function (e) {
						switch (e.type) {
							case 'drag':
								du.fromItemsMoveDraggedElement(e.target, e.distance, e.direction);
								break;
							case 'dragend':
								// du.preventClick(false);
								du.fixPosition(e.target);
								break;
							case 'dragstart':
								// du.preventClick(true);
								du.itemsDistance = 0;
								break;
						}
					}
					);
			}
			du.buttonLeft.addClass('inactive');
			du.buttonRight.addClass('inactive');
			du.options.target.addClass('at-the-beginning');
			du.options.target.addClass('at-the-end');
			/* Add class for IE7 & 8 */
			du.options.target.hover(
				function (e) {
					$(this).addClass('buttonsHover');
				},
				function (e) { $(this).removeClass('buttonsHover'); }
			);

			/* Add listener on screen resize */
			$(window).resize(function () { du.windowResize(); });

			/* Init positions */
			du.windowResize();
			tmo = setTimeout(function () { clearTimeout(tmo); du.windowResize(); /* $(du.options.target).css( { 'opacity' : 1 } ); */ }, 1000);
		}

		this.options = $.extend(this.options, $options);
		// $(this.options.target).css( { 'opacity' : 0 } );
		this.init();
	}
 