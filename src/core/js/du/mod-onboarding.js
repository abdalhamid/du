// On Boarding routines
(function($){
$(function(){

    // material design fx input fields
    $('.md-fx1').each(function(){
        var $me = $(this), $label = $me.find('label'), $input = $me.find('.txt1');

        $input.each(function(){
            var $t = $(this), $p = $t.parent();
            if($t.val().replace(/\s+/g,'').length > 0) $p.addClass('expanded');

            // fix chrome overlapping text
            if($t.is(':-webkit-autofill')){
                $p.addClass('expanded');
            }

            $t.on('blur', function(){
                if($t.val().replace(/\s+/g,'') == '') {
                    $p.removeClass('expanded');
                }
                else {
                    $p.addClass('expanded');
                }
            });
        });

        $label.on('click', function(){
            $input.focus();
        });

    });

    // helpful videos carousel
    $('.helpful-videos').each(function(){
        var $me = $(this), $vidTitle = $me.find('h4');

        // load fitVids lib first
        require(['lib/jquery.fitvids.min'], function(){
            $me.find('.the-carousel').bxSlider({
                nextSelector: '.next',
                prevSelector: '.prev',
                video: true,
                onSlideAfter: updateTitle
            });
        });

        function updateTitle($item){
            $vidTitle.text($item.data('video-title'));
        }
    });

    // On Boarding Forms
    $('#loginForm, #otpForm, #createForm, #ccForm, #welcomeConfirm').each(function(){
        var $me = $(this), $txt1 = $me.find('.txt1'), $submit = $me.find('[type=submit]');
        var opts = {};

        // invalid form handler
        $.extend(opts, {
            onInvalidForm: function(){
                $me.find('.error .txt1').eq(0).focus();
            }
        });
        if ($me.is('#ccForm')) {
            $.extend(opts, {group: ".md-fx1"});
            $me.find('.txt1').on('change', function(){
                // console.log($(this).val());
            });
            $me.find('.txt-MM').on('change', function(){
                var $t = $(this);
                if($t.val() < 1 || $t.val() > 12) {
                    $t.val('');
                }
            });
        }
        if ($me.is('#welcomeConfirm')) {
            $.extend(opts, {group: ".detail-item"});

            $me.find('.txt1').on('change', function(){
                var _origText = $submit.val();
                $submit.val($submit.data('alt-text'));
            });
        }

        // init prototype validator
        $me.protoValidator(opts);
        
        $txt1.on('blur', function(){
            if (isFilledIn()) {
                $submit.removeClass('btn-disabled');
            } else {
                $submit.addClass('btn-disabled');
            }
        });
        $txt1.on('keydown', function(){
            if (isFilledIn()) {
                $submit.removeClass('btn-disabled');
            } else {
                $submit.addClass('btn-disabled');
            }
        });

        function isFilledIn(){
            var _retVal = true;
            $txt1.each(function(){
                if($(this).val().length < 1) _retVal = false;
            });

            return _retVal;
        }

        // detect Card Type
        $me.find('.txt-CC').on('keydown', function(){
            showCardType($(this));
        }).on('blur', function(){
            var $t = $(this);
            setTimeout(function(){
                showCardType($t);
            },300);
        });

        // show card type
        var showCardType = function($el){
            var $t = $el;
            var _cardType = GetCardType($t.val());
            var $label = $t.parent().find('.card-type');
            if ($t.val().replace(/\s+/g,'').length > 0) {
                $label.removeClass('card-visa card-master card-amex card-other');
                if (_cardType == 'Visa') {
                    $label.addClass('card-visa');
                }
                else if (_cardType == 'Mastercard') {
                    $label.addClass('card-master');
                }
                /*else if (_cardType == 'AMEX') {
                    $label.addClass('card-amex');
                }*/
                else {
                    $label.addClass('card-other');
                }
            }
            else {
                $label.removeClass('card-visa card-master card-amex card-other');
            }
            
        }

        // load masked-input plugin
        require([
            'lib/jquery.maskedinput.min'
        ], function(){

            $me.find('.txt-CC').mask('9999 9999 9999 99?99', {placeholder: '', autoclear: false});
            $me.find('.txt-CVV').mask('999', {placeholder: ''});
            $me.find('.txt-MM').mask('99', {placeholder: ''});
            $me.find('.txt-YY').mask('99', {placeholder: ''});
            $me.find('.txt-OTP').mask('9999?99', {placeholder: ''});
            
            $.mask.definitions['9'] = "";
            $.mask.definitions['h'] = "[0-9]";
            $me.find('.txt-mobile').mask('+9715hhhhhhhh', {placeholder: '', autoclear: false});
            
        });

        // Slide form submission
        var formExit = false;
        $me.on('submit', function(){
            if ($me.find('.error').length) return false;
            if ($me.data('exit') == 'slide') {
                var $box = $me.closest('.msg-box-content');
                if (!formExit) {
                    $box.addClass('slide-out');
                    setTimeout(function(){
                        formExit = true;
                        $me.submit();
                    },600);
                }
            }
            else {
                formExit = true;
            }
            return formExit;
        });
    });

    function GetCardType(number)
    {
        // visa
        var re = new RegExp("^4");
        if (number.match(re) != null)
            return "Visa";

        // Mastercard
        re = new RegExp("^5[1-5]");
        if (number.match(re) != null)
            return "Mastercard";

        // AMEX
        re = new RegExp("^3[47]");
        if (number.match(re) != null)
            return "AMEX";

        // Discover
        re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
        if (number.match(re) != null)
            return "Discover";

        // Diners
        re = new RegExp("^36");
        if (number.match(re) != null)
            return "Diners";

        // Diners - Carte Blanche
        re = new RegExp("^30[0-5]");
        if (number.match(re) != null)
            return "Diners - Carte Blanche";

        // JCB
        re = new RegExp("^35(2[89]|[3-8][0-9])");
        if (number.match(re) != null)
            return "JCB";

        // Visa Electron
        re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
        if (number.match(re) != null)
            return "Visa Electron";

        return "";
    }


});
})(jQuery);


/* pullUpMsgBox Plugin  */
(function($) {
    $.extend($.fn, {
        pullUpMsgBox: function(options) {
            options = $.extend({
                // Options Defaults
                escape_selector: '.escape',
                close_selector: '.close',
                box_selector: '.msg-box-content',
                btn_continue: '.msg-box-btn-continue',
                btn_skip: '.msg-box-btn-skip',
            }, options);

            this.each(function() {
                // Operations for each DOM element
                var $me = $(this),
                    $escape = $me.find(options.escape_selector),
                    $close = $me.find(options.close_selector),
                    $box = $me.find(options.box_selector),
                    $btnContinue = $me.find(options.btn_continue),
                    $btnSkip = $me.find(options.btn_skip);

                $escape.on('click', function(){
                    if ($me.data('modal') == true) return false;
                    slideDown();
                    return false;
                });

                $close.on('click', function(){
                    slideDown();
                    return false;
                });
                $btnSkip.on('click', function(){
                    slideDown();
                    return false;
                });

                function slideDown() {
                    $box.addClass('close')
                    $me.delay(300).fadeOut(100);
                }
                
                function init(){
                }


                $btnContinue.on('click', function(){
                    var $t = $(this), _href = $t.attr('href'), $exit = $t.data('exit');
                    if ($exit == 'slide') {
                        $box.addClass('slide-out');
                        setTimeout(function(){
                            window.location = _href;
                        },500);
                        return false;
                    } 
                });

                init();

            }).data('pullUpMsgBox', {
                // Plugin interface object
            });
        
            return this;
        }
    });

    /* apply only to On Boarding templates */
    $('.onboarding-box').pullUpMsgBox();

})(jQuery);