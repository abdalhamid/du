$(function(){
 setTimeout(function(){
  if($('body').hasClass('ie8')) {

   ie8Dials();

  }
 },1000);

 //bug in ie 11 that localStorage is undefined so it stops the code execution
 // http://stackoverflow.com/a/21156133/1157196
if(localStorage)
  localStorage.clear();
})

var ie8Dials = function(){
 var $mb = data_progress($('.radial-mb').attr('data-progress'));
 var $mins = data_progress($('.radial-calls').attr('data-progress'));
 var $sms = data_progress($('.radial-text').attr('data-progress'));

 //alert($mb);

 var $mb_dial = $('<span class="ie8-circle"><img src="/themes/du/resources/images/eshop/dial-mb-'+$mb+'.gif" /></span>');
 var $mins_dial = $('<span class="ie8-circle"><img src="/themes/du/resources/images/eshop/dial-mins-'+$mins+'.gif" /></span>');
 var $sms_dial = $('<span class="ie8-circle"><img src="/themes/du/resources/images/eshop/dial-sms-'+$sms+'.gif" /></span>');

 $('.radial-progress').find('.ie8-circle').fadeOut(500, function(){
  $(this).remove();
 })
 $mb_dial.prependTo('.radial-mb');
 $mins_dial.prependTo('.radial-calls');
 $sms_dial.prependTo('.radial-text');

 //alert(data_progress(75));
}



function data_progress($value){
 var $num = parseInt($value);
 //alert($num);
 if($num >= 1 && $num <= 25){
  $num = 25;
 }
 else if($num > 25 && $num <= 50){
  $num = 50;
 }
 else if($num > 50 && $num <= 75){
  $num = 75;
 }
 else if($num > 75 && $num <= 100){
  $num = 100;
 }
 else{
  $num = 0;
 }
 return $num;
}

$.reject({
 reject: {all:!1,msie5:!0,msie6:!0,msie7:!0,msie8:!0}, // Reject all renderers for demo
 //closeCookie: true, // Set cookie to remmember close for this session
 browserInfo: { // Settings for which browsers to display
  chrome: {
   // Text below the icon
   text: 'Google Chrome',
   // URL For icon/text link
   url: 'http://www.google.com/chrome/',
   // (Optional) Use "allow" to customized when to show this option
   // Example: to show chrome only for IE users
   // allow: { all: false, msie: true }
  },
  firefox: {
   text: 'Mozilla Firefox',
   url: 'http://www.mozilla.com/firefox/'
  },
  safari: {
   text: 'Safari 5+',
   url: 'http://www.apple.com/safari/download/'
  },
  opera: {
   text: 'Opera',
   url: 'http://www.opera.com/download/'
  },
  msie: {
   text: 'Internet Explorer 9+',
   url: 'http://www.microsoft.com/windows/Internet-explorer/'
  }
 },
 header: 'Did you know that your Internet Browser is out of date?',
 paragraph1: 'We have upgraded our website using the latest technologies in order to offer a cutting edge experience across a variety of devices. Your browser is out of date, and may not be compatible with our website.' +
 '<span><img src="../common/images/ico/devices.gif" alt="" /></span>',
 paragraph2: 'A list of the most popular web browsers can be found below. Just click on the icons to get to the download pages.',
 close: false,
 display: ['chrome', 'firefox', 'safari', 'msie'],
 imagePath: '../common/images/ico/', // Path where images are located
});


// Destroys the localStorage copy of CSS that less.js creates
 
function destroyLessCache(pathToCss) { // e.g. '/css/' or '/stylesheets/'
 
  if (!window.localStorage || !less || less.env !== 'development') {
    return;
  }
  var host = window.location.host;
  var protocol = window.location.protocol;
  var keyPrefix = protocol + '//' + host + pathToCss;
  
  for (var key in window.localStorage) {
    if (key.indexOf(keyPrefix) === 0) {
      delete window.localStorage[key];
    }
  }
}