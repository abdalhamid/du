$(document).ready(
    function () {
        var init = function () {

            $('.modTextual .navigation .item a').unbind('click').bind(
			    'click',
			    function (e) {
			        e.preventDefault();
			        var showElement = function () {
			            lis.removeClass('selected');
			            cont.removeClass('selected');
			            p.addClass('selected');
			            $(cont.get(idx)).addClass('selected');
			        },
					    a = $(this),
					    p = $(this).parent('.item'),
					    idx = p.index(),
					    ul = p.parent('.items'),
					    lis = p.parent('.items').children('.item'),
					    cont = p.parent('.items').parent('.navigation').siblings('.contents').children('.items').children('.item');

			        if ($('body').width() >= 768) {
			            if (!$(p).hasClass('selected')) {
			                showElement();
			            }
			        } else {
			            if ($(ul).hasClass('open')) {
			                $(ul).removeClass('open');
			                if (!$(p).hasClass('selected')) showElement();
			            } else {
			                if ($('.modTextual .navigation .item.selected').length == 0) {
			                    showElement();
			                } else {
			                    $(ul).addClass('open');
			                }
			            }
			        }
			    }
		    );

            if ($('.modTextual .navigation .item.selected').length == 0) {
                $('.modTextual .navigation .item:first a').trigger('click');
            }

        },
        fixTables = function () {
            $('.modTextual table').each(
                function () {
                    $(this).wrap('<div class="table-container" />');
                }
            );
        },
        // TODO
        // Add a class to the container ONLY if the content is larger than the available space.
        resizeWindow = function () {
            console.log('resize');
            $('.modTextual .table-container').each(
                function () {
                    console.log('resize - each');
                    console.log($(this).width());
                    console.log($(this).children('table').width());
                    if ($(this).width() > $(this).children('table').width()) {
                        $(this).addClass('scroll');
                    } else {
                        $(this).remove('scroll');
                    }
                }
            );
        };

        if ($('.modTextual').length > 0) init();
        if ($('.modTextual table').length > 0) {
            fixTables();
            // resizeWindow();
        }
        // $(window).resize(resizeWindow);
    }
);