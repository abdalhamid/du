/*
 *
 * duDetect JS class used to manage horizontal slideshows
 *
 * Requirements: jQuery, Hammer.js, Modernizr.js
 *
 * @param: object { 'target': HTMLObject }
 * @return: void
 *
 *
 */

 
export default function duDetect($options) {
		/* Properties */
		this.options = { 'target': $('body') };

		/* Methods declarations */
		this.getInternetExplorerVersion = getInternetExplorerVersion;
		this.init = init;

		/* Internal variables */

		/* Methods */


		/*
		 *
		 * getInternetExplorerVersion
		 * @param: void
		 * @return: void
		 *
		 */
		function getInternetExplorerVersion() {
			var rv = -1; // Return value assumes failure.
			if (navigator.appName == 'Microsoft Internet Explorer') {
				var ua = navigator.userAgent;
				var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
				if (re.exec(ua) != null)
					rv = parseFloat(RegExp.$1);
			}
			return rv;
		}

		/*
		 *
		 * init
		 * @param: void
		 * @return: void
		 * Called on class initialization
		 *
		 */
		function init() {
			var du = this,
				systemInfo = navigator.appCodeName + " " + navigator.appName + " " + navigator.appVersion + " " + navigator.cookieEnabled + " " + navigator.platform + " " + navigator.userAgent;
			if ((systemInfo.toLowerCase().indexOf('ipad') > -1)) du.options.target.addClass('mobile ipad tablet');
			else if ((systemInfo.toLowerCase().indexOf('iphone') > -1)) du.options.target.addClass('mobile iphone');
			else if ((systemInfo.toLowerCase().indexOf('ipod') > -1)) du.options.target.addClass('mobile ipod');
			else if ((systemInfo.toLowerCase().indexOf('android') > -1)) du.options.target.addClass('mobile android');
			else if ((systemInfo.toLowerCase().indexOf('windows phone os') > -1)) du.options.target.addClass('mobile windows-phone');
			else if ((systemInfo.toLowerCase().indexOf('mobile') > -1)) du.options.target.addClass('mobile generic');
			else du.options.target.addClass('desktop');
			if ($('html').hasClass('no-touch')) {
				if (du.options.target.hasClass('windows-phone')) {
					du.options.target.addClass('half-touch');
				} else {
					du.options.target.addClass('no-touch');
				}
			} else {
				du.options.target.addClass('touch');
			}

			if (navigator.appName == 'Microsoft Internet Explorer') {
				du.options.target.addClass('ie');
				du.options.target.addClass('ie' + Math.floor(du.getInternetExplorerVersion()));
			}

			// temporary fix for content images resize
			$('.modProdOverview .pic img').attr('width', 'auto').attr('height', 'auto');

		}

		this.options = $.extend(this.options, $options);
		this.init();
	}
 
 //module.exports = duDetect;