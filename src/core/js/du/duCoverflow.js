/*
 *
 * duCoverflow JS class used to manage coverflow views
 *
 * Requirements: jQuery, Hammer.js, Modernizr.js, contentflow.js
 *
 * @param: object { 'target': string, 'loop': boolean, 'reflection': float }
 * @return: void
 *
 *
 */
 
	function duCoverflow($options) {
		/* Properties */
		this.options = { 'target': null, 'loop': true, 'reflection': .3 };
		/* Methods */
		this.changeSelection = changeSelection;
		this.enableLegend = enableLegend;
		this.addLegend = addLegend;
		this.init = init;
		/* Variables */
		this.flow = undefined;
		this.pagination = '<div class="pagination"><div class="paginationInt" ><ul></ul></div></div>';
		this.pageTemplate = '<li><a href="#[%PAGE%]" title="[%PAGE%]">[%PAGE%]</a></li>';
		this.buttonsHolder = undefined;
		this.buttons = undefined;

		/*
		 *
		 * changeSelection
		 * @param: object, object
		 * @return: void
		 *
		 */
		function changeSelection(obj, du) {
			du.buttons.removeClass('sel');
			$(du.buttons.get(obj.index - 1)).addClass('sel');
		}

		/*
		 *
		 * enableLegend
		 * @param: void
		 * @return: void
		 *
		 */
		function enableLegend() {
			var du = this;
			du.buttons.each(
				function () {
					$(this).children('a')
						.unbind('click')
						.bind(
						'click',
						function clickOnLegendElement(e) {
							e.preventDefault();
							var selectedElement = parseInt($(this).attr('href').split('#')[1]);
							du.flow.moveTo(selectedElement);
						}
						);
				}
			);
		}

		/*
		 *
		 * addLegend
		 * @param: void
		 * @return: void
		 *
		 */
		function addLegend() {
			var du = this;
			$('#' + du.options.target).after(du.pagination);
			du.buttonsHolder = $('#' + du.options.target).siblings('.pagination').find('ul');
			var itms = ($('#' + du.options.target).find('.item').length);
			for (var i = 0; i < itms; i++) {
				du.buttonsHolder.append(du.pageTemplate.replace(/\[\%PAGE\%\]/ig, (i + 1)));
			}
			du.buttons = du.buttonsHolder.find('li');
			w = (itms * du.buttons.outerWidth(true));
			du.buttonsHolder.css({ 'width': w + 'px' });
			du.buttonsHolder.parent().css({ 'width': w + 'px' });
		}

		function init() {
			var du = this,
				scale = ($('body').hasClass('mobile')) ? .3 : 1;
			du.addLegend();
			du.enableLegend();
			du.flow = new ContentFlow(du.options.target, { 'curcularFlow': du.options.loop, 'reflectionHeight': du.options.reflection, 'scaleFactor': scale, 'onMakeActive': function (obj) { du.changeSelection(obj, du) } });
		}

		this.options = $.extend(this.options, $options);
		this.init();

	}
 