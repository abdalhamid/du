define(['jquery'], function ($) {

        var init = function () {
        
             $('.login-register2 .tabs a, .new-dashboard .tabs a').on('click', function (event) {
              
                event.preventDefault();
                var tabName = $(this).attr('tab-name');
                $('#'+tabName).show().siblings('.tab-container').hide();
                $("."+tabName).show().siblings('.tab-container').hide();
                $(this).addClass('tab-active').siblings('.btn').removeClass('tab-active');

             });

             if(window.location.hash) {
                   $("a[tab-name="+window.location.hash.substring(1)+"]").click();
              }

        };

        if ($('.login-register2, .usage-details').length > 0)
        { 
            init();
        }
          
    
});