$(document).ready(function () {

    /* ENTERTAINMENT TILES isotope

    $.Isotope.prototype._positionAbs = function (x, y) {
        return {right: x, top: y};
    };

    var $container = $('.modTiles .items');
    if ($container.length > 0) {
        var tmo = setTimeout(function () {
            clearTimeout(tmo);
            $container.imagesLoaded(function () {
                //$container.isotope(
                //{
                // itemSelector: '.item',
                // layoutMode: 'masonry',
                // resizable: true
                //}
                require(['isotope.pkgd.min'],
                    function (Isotope) {
                        var iso = new Isotope(
                            $container[0], {
                                itemSelector: '.item',
                                layoutMode: 'masonry',
                                resizable: true
                            });
                    });
            });
        }, 250);
    }

     */


    /* Enable scrollable contents */
    $('.scrollable').each(
        function () {
            if (!$(this).hasClass('du-ready')) {
                new duScroll({'target': $(this), 'enableTouch': true, 'direction': 'rtl'});
                $(this).addClass('du-ready');
            }
        }
    );
    /* Enable product scrolls */
    $('.productScrollable').each(
        function () {
            if (!$(this).hasClass('du-ready')) {
                new duProductScroll({
                    'target': $(this),
                    'enableTouch': true,
                    'direction': 'rtl'
                });
                $(this).addClass('du-ready');
            }
        }
    );
    /* Enable simple sliders **/
    /*$('.slideable').each(
        function () {
            if ($(this).hasClass('modDials')) return;
            if (!$(this).hasClass('du-ready')) {
                new duSlide({'target': $(this), 'enableTouch': true, 'timer': 5000, 'direction': 'rtl'});
                $(this).addClass('du-ready');
            }
        }
    );*/
    /* Enable advanced sliders */
    $('.slideableAdvanced').each(
        function () {
            if (!$(this).hasClass('du-ready')) {
                new duSlide({
                    'target': $(this),
                    'enableTouch': true,
                    'timer': 0,
                    'customPagination': true,
                    'direction': 'rtl'
                });
                $(this).addClass('du-ready');
            }
        }
    );

    /* Enable Coverflow */
    $('.coverflowable').each(
        function () {
            if (!$(this).hasClass('du-ready')) {
                new duCoverflow({
                    'target': $(this).attr('id')
                });
                $(this).addClass('du-ready');
            }
        }
    );


    // document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
    // console.log($('html').attr('class'));
    // console.log($('body').attr('class'));
    // alert($('body').attr('class'));

        // payment history table & filters
        var payment_history = $('#payment-history').DataTable({
            "aoColumns": [{
                "sType": "custom_euro_date"
            }, {}, {}],
            "aaSorting": [
                [0, "desc"]
            ], // Sort by first column descending
            "info": false,
            "paging": false,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "اكتب الكلمة المفتاحية"
            },
            initComplete: function () {
                var api = this.api();
                var i = 1;
                var column = api.column(i);
                var select = $(
                    '<select class="hidden-sm"><option value="">Filter use type</option></select>'
                ).appendTo($('.table-filters')).on('change',
                    function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val());
                        column.search(val ? '^' + val + '$' :
                            '', true, false).draw();
                    });
                column.data().unique().sort().each(function (d,
                                                             j) {
                    select.append('<option value="' + d +
                        '">' + d + '</option>');
				});
            }
        });

        // recharge history table & filters
        var recharge_history = $('#recharge-history').DataTable({
            "aoColumns": [{
                "sType": "custom_euro_date"
            }, {}, {}, {}],
            "aaSorting": [
                [0, "desc"]
            ], // Sort by first column descending
            "info": false,
            "paging": false,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "اكتب الكلمة المفتاحية"
            },
            initComplete: function () {
                var api = this.api();
                var i = 1;
                var column = api.column(i);
                var select = $(
                    '<select class="visible-xs"><option value="">Filter use type</option></select>'
                ).appendTo($('.table-filters')).on('change',
                    function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val());
                        column.search(val ? '^' + val + '$' :
                            '', true, false).draw();
                    });
                column.data().unique().sort().each(function (d,
                                                             j) {
                    select.append('<option value="' + d +
                        '">' + d + '</option>');
                });
            }
        });
        
        // usage history table & filters
        var usage_table = $('#usage').DataTable({
            "aoColumns": [{
                "sType": "custom_euro_date"
            }, {}, {}, {}, {}, {}],
            "aaSorting": [
                [0, "desc"]
            ], // Sort by first column descending
            "info": false,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "اكتب الكلمة المفتاحية"
            },
            initComplete: function () {
                var api = this.api();
                var i = 1;
                var column = api.column(i);
                var select = $(
                    '<select class="visible-xs"><option value="">Filter use type</option></select>'
                ).appendTo($('.table-filters')).on('change',
                    function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val());
                        column.search(val ? '^' + val + '$' :
                            '', true, false).draw();
                    });
                column.data().unique().sort().each(function (d,
                                                             j) {
                    select.append('<option value="' + d +
                        '">' + d + '</option>');
                });
                $('.table-filters .btn-group .btn').on('click',
                    function (e) {
                        e.preventDefault();
                        $('.table-filters .btn-group .btn')
                            .removeClass('is-checked');
                        $(this).addClass('is-checked');
                        var val = $(this).attr(
                            'data-use-type');
                        column.search(val ? '^' + val + '$' :
                            '', true, false).draw();
                    });
            }
        });

        // init date time picker
        $('#startdate').datetimepicker({
            format: 'd/m/Y',
            timepicker: false,
            closeOnDateSelect: true,
            onChangeDateTime: function (ct, $i) {
                usage_table.draw();
                payment_history.draw();
                recharge_history.draw();
            }
        });

        // init date time picker
        $('#enddate').datetimepicker({
            format: 'd/m/Y',
            timepicker: false,
            closeOnDateSelect: true,
            onChangeDateTime: function (ct, $i) {
                usage_table.draw();
                payment_history.draw();
                recharge_history.draw();
            }
        });

    /*$('.contentIn > .hero span').text('اربح عند استخدام بطاقة ماستركارد');
    $('.contentIn > .hero .action a').text('اعرف المزيد');
    $('.contentIn > .hero .action a').attr('href','http://www.du.ae/ar/personal/mobile/specialoffers/pay-online-and-win');*/

    $('.contentIn > .hero').remove();

});

function showGraph() {
    $("#graphLink a").text("إخفاء الرسم البياني");
    $(".graph").show();
}

function graphF() {
    var display = $(".graph").css('display');
    if (display == "block") {
        $(".graph").hide();
        $("#graphLink a").text("إظهار الرسم البياني");
        $(".graphButton").hide();
    } else {
        $(".graph").show();
        $("#graphLink a").text("إخفاء الرسم البياني");
        $(".graphButton").show();
    }
}