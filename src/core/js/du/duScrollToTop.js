﻿/*
 *
 * duScrollToTop JS class used to manage scroll to top functionality
 *
 * Requirements: jQuery, Modernizr.js
 *
 * @param: void
 * @return: void
 *
 *
 */
 
    function duScrollToTop() {
        /* Properties */

        /* Methods definitions */
        this.windowScroll = windowScroll;

        /* Methods */

        /*
        *
        * windowScroll
        * @param: void
        * @return: void
        * Called window scroll
        *
        */
        function windowScroll() {
            if ($('.inpagenavigation').offset().top < $(window).scrollTop() - 100)
                $('.go-to-top').show();
            else
                $('.go-to-top').hide();
        }

        var du = this;
        $(window).scroll(function () { du.windowScroll(); });
    }

   