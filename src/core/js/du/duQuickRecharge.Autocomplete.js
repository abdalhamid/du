
define(['jquery', 'jquery.cookie.min', 'prototype/jquery.validator', 'jquery.auto-complete.min'], function ($) {
    var init = function () {

        if ($('.qpay-autocomplete').length > 0) {

            var getType = function (val) {

                var typesList = {
                    "mobile": /^(((9715)|(05))[0-9]{8})$/,
                    "landline": /^(((971[2|3|4|6|7|9]))[0-9]{7})$/,
                    "account": /^(([0-9][.]([0-9]|[0-9][.][0-9])+)|(((971[2|3|4|6|7|9]))[0-9]{7})|((9715)|(05))[0-9]{8})$/
                };

                for (var key in typesList) {
                    var reg = typesList[key];
                    var patt = new RegExp(reg);
                    var res = patt.test(val);
                    if (res) {
                        return key;
                    }
                }
            }

            $('#quickpay .btn-quickpay').closest('form').first().on('submit', function (e) {
                var numbersList = $.cookie('numbersList');
                var value = $('#quickpay .qpay-autocomplete').val();
                if ($('#quickpay .qpay-autocomplete').parent('.error').length) {

                    e.stopPropagation();
                    return false;
                }

                if (numbersList) {
                    var found = false;
                    var list = JSON.parse(numbersList);
                    for (i = 0; i < list.length; i++) {
                        if ((list[i][0]).toLowerCase().indexOf(value) != -1) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        var numberType = getType(value);

                        list.push([value, numberType]);
                        $.cookie('numbersList', JSON.stringify(list));
                    }

                } else if (value) {
                    $.cookie('numbersList', JSON.stringify([[value, getType(value)]]));
                }

            });

            $('.qpay-autocomplete').autoComplete({
                minChars: 0,
                source: function (term, suggest) {
                    term = term.toLowerCase();
                    var numbersList = $.cookie('numbersList');

                    if (numbersList) {
                        var list = JSON.parse(numbersList);
                        var suggestions = [];
                        for (i = 0; i < list.length; i++)
                            if (~(list[i][0] + ' ' + list[i][1]).toLowerCase().indexOf(term)) suggestions.push(list[i]);
                        suggest(suggestions);

                    }
                },
                renderItem: function (item, search) {
                    search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                    var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                    var type = item[1] ? item[1] : '';
                    return '<div class="autocomplete-suggestion" data-number="' + item[0] + '" data-type="' + item[1] + '" data-val="' + search + '"><i class="icon"></i>' + item[1] + '<br>' + item[0].replace(re, "<b>$1</b>") + '</div>';
                },
                onSelect: function (e, term, item) {
                    //console.log('Item "'+item.data('number')+' ('+item.data('type')+')" selected by '+(e.type == 'keydown' ? 'pressing enter or tab' : 'mouse click')+'.');
                    $('.qpay-autocomplete').val(item.data('number'));
                }
            });

        }


        //quick recharge edit action
        $('.quick.recharge').on('click', '.edit-number', function (e) {
            $('#quickPay').show();
            $('#rechage-form').hide()
        })

        //handling confirm window
        $(".confirm-quick-payment .btn-continue-submit").on('click', function () {
            $("a.close-reveal-modal").click();

            var existingcard = 'true';

            // FIX 118 start
            var contactEmail = $("#contactEmail").val();
            var contactNumber = $("#contactMobileNumber").val();
            $("#paymentContactEmail").val(contactEmail);
            $("#paymentContactMobileNumber").val(contactNumber);
            try {
                dataLayer.push({
                    'event': 'eventTracker',
                    'eventCategory': 'payment',
                    'eventAction': 'confirm',
                    'eventLabel': 'transaction_confirmation',
                    'eventValue': $('#payAmount').val()
                });
            } catch (ex) {
                console.log(ex);
            }

            // FIX 118 end
            $("form#rechage-form")[0].submit();
        });


        $(".confirm-quick-payment #chAccept").on("ifChecked", function () {
            $("#continueButton").removeClass('btn-grey');
            $("#continueButton").removeAttr("disabled");
        });

        $(".confirm-quick-payment #chAccept").on("ifUnchecked", function () {
            $("#continueButton").addClass('btn-grey');
            $("#continueButton").attr('disabled', 'disabled');
        });

        $(".confirm-quick-payment #close-reveal-modal").on('click', function () {
            if ($('.unchclass').is(':checked')) {
                $('.unchclass').iCheck('uncheck');
            }
        });
        //show the modal on sibmit
        $("#rechage-form").on('submit', function (e) {
            e.preventDefault();
            setTimeout(function () {
                if ($("#rechage-form").find('.error').length) return;
                $('#confirm-payment').reveal({
                    animation: 'fadeAndPop',
                    animationspeed: 300,
                    closeonbackgroundclick: true,
                    dismissmodalclass: 'close-reveal-modal'
                });

                $('#modalRechargeWallet').html($("#wallet").val())
                $('#modalAmount').html($("#rechargeAmount").val());
                $('#modalAccountNumber').html($("#accountNumber").text());

                 //added by Atos
                    function toTitleCase(str) {
                        return str.replace(/(?:^|\s)\w/g, function (match) {
                            return match.toUpperCase();
                        });
                    }



                    // setting for Easy Social and Extra Social
                    var walletDisplayNoshow = $("#wallet_no_show").attr("data-wallet-type");
                    var walletDisplay = $("#wallet option:selected").text();
                    if (walletDisplayNoshow != undefined && walletDisplayNoshow != "") {
                        $('#modalRechargeWallet').html(toTitleCase(walletDisplayNoshow));
                    }
                    else {
                        $('#modalRechargeWallet').html(toTitleCase(walletDisplay));
                    }

            }, 300);


           



        });

         var action = window.location.href.slice(window.location.href.indexOf('action'));
                if(action==='action=editnumber') {
                    $('.quick.recharge .edit-number').trigger('click');
                }
    };

    $("#quickPay").protoValidator();

    init();


})
