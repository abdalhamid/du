define(function(){
    'use strict';
    if($('.ucTabAccordion').length) {
        $('.ucTabAccordion .accordion').on('click','dt a',function(){
            $(this).closest('.accordion').find('dd').slideToggle();
        });
    }
})