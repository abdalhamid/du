
// =============== <A> ===================
// A bunch of mad layout changes requered by design.
// Should be removed after refactoring.
//


$(function () {
    // drop down menu fix
    $('.mainNavList').on('mouseover', '.mainItem', function () {
        var $item = $(this);
        var offset = $item.offset();
        var bottomOffset = $(window).height() - (offset.top - $(window).scrollTop());
        var $submenu = $item.find('.subNavList').first();
        var submenuHeight = $submenu.height();
        if (bottomOffset < submenuHeight) {
            $submenu.css('top', bottomOffset - submenuHeight - 20 + 'px'); //hardcode ftw
        }
    }).on('mouseout', '.mainItem', function () {
        var $item = $(this);
        var $submenu = $item.find('.subNavList').first();
        $submenu.css('top', 0);
    });

    // menu position fix 
    ApplyWindowSize();
    $(window).resize(function () {
        ApplyWindowSize();
    });

    function ApplyWindowSize() {
        var $window = $(window);

        var $container = $('.mainNav');

        if ($window.width() > 983) {
            var height = $container.find('nav h1').outerHeight()
				+ $container.find('nav h2').outerHeight()
				+ $container.find('nav .mainNavListW').outerHeight() + 50 //hardcode ftw
				+ $container.find('.footer').outerHeight();

            $container.css('min-height', $container.find('nav').outerHeight() + 'px');

            if (height > $window.height()) {
                $container.addClass('squashed');
            } else {
                $container.removeClass('squashed');
            }
        }
    }

    // min-height of the content
    ApplyCanvasMinHeight();
    $(window).resize(function () {
        ApplyCanvasMinHeight();
    });

    function ApplyCanvasMinHeight() {
        var $window = $(window);
        if ($window.width() < 983) {
            var $mainNav = $('.mainNav');

            var navHeight = $mainNav.find('nav').outerHeight() + $mainNav.find('.footer').outerHeight() + $mainNav.find('.chapterNav').outerHeight();

            $('.container-du').css('min-height', navHeight + 'px'); //hardcode ftw
        } else {
            $('.container-du').css('min-height', '100%');
        }
    }

    $("#currentdiv").bind({
        tap: function () {
            var $mainNav = $('.mainNav');

            var navHeight = $mainNav.find('nav').outerHeight() + $mainNav.find('.footer').outerHeight() + $mainNav.find('.chapterNav').outerHeight();

            $('.container-du').css('min-height', navHeight + 'px'); //hardcode ftw
        }
    });

    // =============== </A> ===================
});
