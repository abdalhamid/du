
define(function () {
  'use strict';


  var maxLength = 300;


  $('.ticket textarea').keyup(function () {
    if ($(this).data('maxlength')) {
      maxLength = $(this).data('maxlength');
    }

    var length = $(this).val().length;

    length = maxLength - length;

    if (length < 0) {
      $(this).addClass('error');
    } else {
      $(this).removeClass('error');
    }
    if (length < 0) {
      $(this).siblings('.counter').text(length).show();
      $(this).closest('.ticket').find('.btn.btn-primary.submit-button').attr('disabled', 'disabled');

    } else if (length < maxLength - 10) {
      $(this).siblings('.counter').text('').hide();
      $(this).closest('.ticket').find('.btn.btn-primary.submit-button').attr('disabled', null);

    } else if (length > maxLength - 10) {
      $(this).closest('.ticket').find('.btn.btn-primary.submit-button').attr('disabled', 'disabled');
    }




  });


  $('.ticket .attach-file.icon-container').on('click', function (e) {

    e.preventDefault();
    var $file = $(this).closest('.ticket').find('input.file');
    $file.click();


  });

  $(".ticket input.file").change(function (e) {
    var $container = $(this).closest('.ticket');
    var isValid = ValidateSingleInput(
                              this, 
                              $container.find('.text-error'), 
                              $container.find('.filename'), 
                              $container.find('.file-size'),
                              $container.find('.file-size-error').text(),
                              $container.find('.file-type-error').text()
                          );
    if (isValid) {
      $container.find('.file-attachment-container').css('display', 'inline-block');
      $container.find('.file-upload-container').hide();

      $container.find('.attachment-icon').css('display', 'inline-block');
      $container.find('.icon-container.attach-file').hide();

      $container.find('.text-error').text('');
    }



  });

  $('.ticket .file-attachment-container .icon-container').click(function (e) {
    e.preventDefault();
    var $container = $(this).closest('.ticket');
    $container.find('.file-attachment-container').hide();
    $container.find('.attachment-icon').hide();
    $container.find('.file-upload-container').css('display', 'inline-block');
    $container.find('.icon-container.attach-file').css('display', 'inline-block');
    $container.find('input.file').val(null);

  });

  $('.ticket .reopen-ticket').on('click', function () {

    var $parent = $(this).closest(".ticket");
    $parent.find('.ticket-container').hide();
    $parent.find('.ticket-container-form').show();
    $parent.addClass('open');


  });

  $('.ticket .ticket-container-form .return-to-ticket').on('click', function (e) {
    e.preventDefault();
    var $parent = $(this).closest(".ticket");
    $parent.find('.ticket-container').show();
    $parent.find('.ticket-container-form').hide();
    $parent.removeClass('open');
  });


  var _validFileExtensions = [".jpg", ".png", ".pdf"];
  var _validFileSizeLimit = 4194304;
  function ValidateSingleInput(oInput, validationField, fileNameField, fileSizeField,filesizeError,fileTypeError) {
    if (oInput.type == "file") {
      var sFileName = oInput.value;
      if (sFileName.length > 0) {
        var blnValid = false;
        for (var j = 0; j < _validFileExtensions.length; j++) {
          var sCurExtension = _validFileExtensions[j];
          if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
            blnValid = true;
            break;
          }
        }

        if (!blnValid && validationField) {
          $(validationField).text(fileTypeError);
          oInput.value = "";
          return false;
        }

        if (blnValid && fileNameField) {
          var startIndex = (sFileName.indexOf('\\') >= 0 ? sFileName.lastIndexOf('\\') : sFileName.lastIndexOf('/'));
          var filename = sFileName.substring(startIndex);
          if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            filename = filename.substring(1);
            $(fileNameField).text(filename);
          }
        }

        if (blnValid) {

          var size = oInput.files[0].size;

          if (size <= _validFileSizeLimit) {
            $(fileSizeField).text(formatBytes(size));
          } else {
            $(validationField).text(filesizeError);
            return false;
          }

        }
      }
    }
    return true;
  }


  function formatBytes(bytes, decimals) {
    if (bytes == 0) return '0 Bytes';
    var k = 1000,
      dm = decimals + 1 || 3,
      sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
      i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

})
