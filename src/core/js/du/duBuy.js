
 
    function duBuy(btnPrice, purchaseOptions, btClose) {

        this.content = {
            btnPrice: null,
            purchaseOptions: null,
            btClose: null
        };
        content.btnPrice = btnPrice;
        content.purchaseOptions = purchaseOptions;
        content.btClose = btClose;
        if (content.purchaseOptions) {
            init();
        }

        function init() {
            content.btnPrice.unbind('click').bind(
                'click',
                function (e) {
                    if (!content.purchaseOptions) {
                        return;
                    }
                    e.preventDefault();
                    if (!content.purchaseOptions.hasClass('visible')) {
                        openPurchaseSection();
                    } else {
                        {
                            closePurchaseSection(e);
                        }
                    }
                }
            );
            content.btClose.unbind('click').bind(
                'click',
                function (e) {
                    closePurchaseSection(e);
                }
            );

            if ($(content.purchaseOptions).find('.tabs').length > 0) {
                enableTabs();
                resizeBoxes();
                $(window).resize(function () { resizeBoxes(); });
            }

        };

        function resizeBoxes() {
            var tabs = $(content.purchaseOptions).find('.tabs'),
                cont = $(tabs).siblings('.purchaseOptionsContent'),
                boxes = cont.find('.box'),
                maxheight = 30;

            $(boxes).each(
                function () {
                    if ($(this).height() > maxheight) maxheight = $(this).height();
                }
            )
            $(boxes).css({ 'height': (maxheight) + 'px' });
        }

        function enableTabs() {
            var tabs = $(content.purchaseOptions).find('.tabs'),
                cont = $(tabs).siblings('.purchaseOptionsItems.purchaseOptionsContent'),
                tab = $(cont).children('.purchaseOptionsItem');

            tabs.find('a').unbind('click').bind(
                'click',
                function (e) {
                    e.preventDefault();
                    if (!$(this).parent().hasClass('selected')) {
                        tabs.find('.item.selected').removeClass('selected');
                        $(this).parent().addClass('selected');
                        idx = parseInt($(this).parent('.item').index());
                        $(cont).find('.purchaseOptionsItem.selected').removeClass('selected');
                        $(tab.get(idx)).addClass('selected');
                        resizeBoxes();
                    }
                }
            );
        }

        function openPurchaseSection() {
            content.btnPrice.addClass('pressed');
            content.purchaseOptions.fadeIn('slow', function () {
                content.purchaseOptions.addClass('visible');
            });

            //TODO
            //Remove this distinction between big and small buttons

            var smallButton = $('#buy-now-buttons-holder-small');
            var buyNowButtonsTop = $('#buy-now-buttons-holder-big').offset().top;
            var buyNowButtonsTopAndHeight = buyNowButtonsTop + $('#buy-now-buttons-holder-big').height();
            var scrollOffset = buyNowButtonsTop - $('.btn-price').offset().top - $('.btn-price').height();
            if (smallButton) {
                buyNowButtonsTop = $('#buy-now-buttons-holder-small').offset().top;
                buyNowButtonsTopAndHeight = buyNowButtonsTop + $('#buy-now-buttons-holder-small').height();
                scrollOffset = buyNowButtonsTop - $('.btn-price').height();
            }

            if (buyNowButtonsTopAndHeight > $(window).height()) {
                $('html,body').animate({
                    scrollTop: scrollOffset
                });
            }
        }

        function closePurchaseSection(e) {
            e.preventDefault();
            content.btnPrice.removeClass('pressed');
            content.purchaseOptions.fadeOut('slow', function () {
                content.purchaseOptions.removeClass('visible');
            });
        }
    }


 