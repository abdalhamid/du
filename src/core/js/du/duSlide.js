/*
 *
 * duSlide JS class used to manage horizontal slideshows
 *
 * Requirements: jQuery, Hammer.js, Modernizr.js
 *
 * @param: object { 'target': HTMLObject, 'enableTouch': boolean, 'timer': int, 'customPagination': boolean, 'direction': string }
 * @return: void
 *
 *
 */

function duSlide($options) {
    /* Properties */
    this.options = {
        'target': null,
        'enableTouch': false,
        'timer': 0,
        'customPagination': false,
        'direction': 'ltr'
    };


    /* Methods declarations */
    this.windowResize = windowResize;
    this.useTransforms = useTransforms;
    this.selectPrevious = selectPrevious;
    this.selectNext = selectNext;
    this.manageTimeout = manageTimeout;
    this.scrollToElement = scrollToElement;
    this.changeSlide = changeSlide;
    this.enableLegend = enableLegend;
    this.enableArrows = enableArrows;
    this.addArrows = addArrows;
    this.addLegend = addLegend;
    this.setWidthAndHeight = setWidthAndHeight;
    this.init = init;

    /* Internal variables */
    this.currentWidth = 0; // Used to understand if there is the need to recalculate everything or not on screen resize
    this.currentHeight = 0; // Used to understand if there is the need to recalculate everything or not on screen resize

    this.items = null;
    this.mask = null;
    this.itemCollection = null;
    this.buttonsHolder = null;
    this.buttons = null;
    this.useTransformsCache = ($('html').hasClass('csstransforms3d')) ? true : Modernizr.csstransforms3d;
    this.selectedElement = 1;

    this.isTouch = $('body').hasClass('mobile') ? true : Modernizr.touch;
    this.timeout = null;
    this.autoscroll = false;
    this.pagination = '<div class="pagination"><div class="paginationInt"><ul></ul></div></div>';
    this.pageTemplate = '<li><a href="#[%PAGE%]" title="[%PAGE%]">[%PAGE%]</a></li>';
    this.arrowsTemplate = '<a href="#" title="" class="buttonScroll buttonScrollPrevious moveLeft"></a><a href="#" title="" class="buttonScroll buttonScrollNext moveRight"></a>';
    this.arrows = undefined;
    this.buttonNext = undefined;
    this.buttonPrevious = undefined;


    /* Methods */

    /*
     *
     * windowResize
     * @param: void
     * @return: void
     *
     */
    function windowResize() {
        var du = this,
            // Give the browser some time to apply media-queries and resize elements
            timeout = setTimeout(
                function () {
                    clearTimeout(timeout);
                    du.stepWidth = du.itemCollection.outerWidth(true);
                    du.setWidthAndHeight();
                    du.addLegend();
                    du.scrollToElement();

                    //to fix rebind clicking problem after recreating buttons
                    du.enableLegend();

                },
                250
            );
    }

    /*
     *
     * useTransforms
     * @param: void
     * @return: boolean
     *
     */
    function useTransforms() {
        var du = this;
        return du.useTransformsCache;
    }

    /*
     *
     * selectPrevious
     * @param: void
     * @return: void
     *
     */
    function selectPrevious() {
        var du = this;
        if (du.selectedElement == 1) {
            du.selectedElement = du.buttons.length;
        } else {
            du.selectedElement--;
        }
    }

    /*
     *
     * selectNext
     * @param: void
     * @return: void
     *
     */
    function selectNext() {
        var du = this;
        if (du.selectedElement == du.buttons.length) {
            du.selectedElement = 1;
        } else {
            du.selectedElement++;
        }
    }

    /*
     *
     * manageTimeout
     * @param: void
     * @return: boolean
     *
     */
    function manageTimeout() {
        var du = this;
        du.timeout = setTimeout(
            function () {
                clearTimeout(du.timeout);
                du.buttons.removeClass('sel');
                du.selectNext();
                $(du.buttons.get(du.selectedElement - 1)).addClass('sel');
                du.scrollToElement();
                if (du.autoscroll) du.manageTimeout();
            },
            du.options.timer
        );
    }

    /*
     *
     * scrollToElement
     * @param: void
     * @return: void
     *
     */
    function scrollToElement() {
        var du = this,
            newPosition = -1 * ($(du.itemCollection.get((du.selectedElement - 1))).position().left);







        if (du.arrows) {
            if (du.selectedElement == 1) {
                if (du.options.direction == 'rtl') {
                    du.buttonNext.addClass('inactive');
                } else {
                    du.buttonPrevious.addClass('inactive');
                }
            } else {
                if (du.options.direction == 'rtl') {
                    du.buttonNext.removeClass('inactive');
                } else {
                    du.buttonPrevious.removeClass('inactive');
                }
            }
            if (du.selectedElement == du.itemCollection.length) {
                if (du.options.direction == 'rtl') {
                    du.buttonPrevious.addClass('inactive');
                } else {
                    du.buttonNext.addClass('inactive');
                }
            } else {
                if (du.options.direction == 'rtl') {
                    du.buttonPrevious.removeClass('inactive');
                } else {
                    du.buttonNext.removeClass('inactive');
                }
            }
        }

        if (du.stepWidth * du.itemCollection.length - $(du.itemCollection.get((du.selectedElement - 1))).position().left >= du.options.target.find('.itemsW').width()) {

            if (du.useTransforms()) {
                du.items.css({
                    "-webkit-transform": "translate3d(" + newPosition + "px,0,0)",
                    "-moz-transform": "translate3d(" + newPosition + "px,0,0)",
                    "-o-transform": "translate3d(" + newPosition + "px,0,0)",
                    "-ms-transform": "translate3d(" + newPosition + "px,0,0)",
                    "transform": "translate3d(" + newPosition + "px,0,0)"
                });
            } else {
                //du.items.css(
                //	{
                //		"left": newPosition + "px"
                //	}
                //);
                du.items.animate({
                    'left': newPosition + 'px'
                });
            }




            if ((du.stepWidth * du.itemCollection.length - $(du.itemCollection.get((du.selectedElement - 1))).position().left >= du.options.target.find('.itemsW').width() - 10 && du.stepWidth * du.itemCollection.length - $(du.itemCollection.get((du.selectedElement - 1))).position().left <= du.options.target.find('.itemsW').width() + 10) || du.buttonsHolder.find('li.sel').is(':last-child')) {
                if (du.options.direction == 'rtl') {
                    du.buttonPrevious.addClass('inactive');
                } else {
                    du.buttonNext.addClass('inactive');
                }
            } else {
                if (du.options.direction == 'rtl') {
                    du.buttonPrevious.removeClass('inactive');
                } else {
                    du.buttonNext.removeClass('inactive');
                }
            }








        }






    }


    /*
     *
     * changeSlide
     * @param: void
     * @return: void
     *
     */
    function changeSlide(target, distance, direction) {
        var du = this;
        clearTimeout(du.timeout);
        du.autoscroll = false;
        if (direction == 'right') {
            if (du.selectedElement != 1) {
                du.selectPrevious();
            }
        } else if (direction == 'left') {
            if (du.selectedElement != du.buttons.length) {
                du.selectNext();
            }
        }
        $(du.buttons.get((du.selectedElement - 1))).children('a').trigger('click');
    }

    /*
     *
     * enableLegend
     * @param: void
     * @return: void
     *
     */
    function enableLegend() {
        var du = this;
        du.buttons.each(
            function () {
                $(this).children('a')
                    .unbind('click')
                    .bind(
                        'click',
                        function clickOnLegendElement(e) {
                            e.preventDefault();
                            clearTimeout(du.timeout);
                            du.autoscroll = false;
                            du.selectedElement = parseInt($(this).attr('href').split('#')[1]);
                            du.buttons.removeClass('sel');
                            $(this).parent('li').addClass('sel');
                            du.scrollToElement();
                        }
                    );
            }
        );
    }

    /*
     *
     * enableArrows
     * @param: void
     * @return: void
     *
     */
    function enableArrows() {
        var du = this;
        du.arrows.unbind('click').bind(
            'click',
            function (e) {
                function next() {
                    du.selectNext();
                    $(du.buttons).removeClass('sel');
                    $(du.buttons.get(du.selectedElement - 1)).addClass('sel');
                    du.scrollToElement();
                }

                function previous() {
                    du.selectPrevious();
                    $(du.buttons).removeClass('sel');
                    $(du.buttons.get(du.selectedElement - 1)).addClass('sel');
                    du.scrollToElement();
                }

                /* disable autoscrolling */
                du.autoscroll = false;
                clearTimeout(du.timeout);

                e.preventDefault();
                if ($(this).hasClass('moveLeft')) {
                    if (du.options.direction == 'rtl') {
                        next();
                    } else {
                        previous();
                    }
                } else {
                    if (du.options.direction == 'rtl') {
                        previous();
                    } else {
                        next();
                    }
                }
            }
        );
    }

    /*
     *
     * addArrows
     * @param: void
     * @return: void
     *
     */
    function addArrows() {
        var du = this;
        du.mask.after(du.arrowsTemplate);
        du.arrows = du.options.target.find('.buttonScroll');
        du.buttonNext = du.options.target.find('.buttonScrollNext');
        du.buttonPrevious = du.options.target.find('.buttonScrollPrevious');
        var body = $('body');
        if (body.hasClass('mobile') && !body.hasClass('half-touch')) {
            du.buttonNext.hide();
            du.buttonPrevious.hide();
        };
    }

    /*
     *
     * addLegend
     * @param: void
     * @return: void
     *
     */
    function addLegend() {
        var du = this,
            w = 0;
        if (!du.options.target.find('.pagination').length)
            du.mask.after(du.pagination);


        du.buttonsHolder = du.options.target.find('.pagination ul');

        var index = du.buttonsHolder.find('li.sel').index();
        var selectionIndex = index >= 0 ? index : 0;
        du.buttonsHolder.html('');


        for (var i = 0; i < Math.ceil(du.stepWidth * du.itemCollection.length / du.options.target.width()); i++) {
            du.buttonsHolder.append(du.pageTemplate.replace(/\[\%PAGE\%\]/ig, (i + 1)));
        }
        du.buttons = du.buttonsHolder.find('li');
        w = (du.buttons.length * du.buttons.outerWidth(true));
        du.buttonsHolder.css({
            'width': w + 'px'
        });
        du.buttonsHolder.parent().css({
            'width': w + 'px'
        });


        du.buttonsHolder.find('li').eq(selectionIndex).addClass('sel');


        if (du.buttonsHolder.find('li').length == 1)
            du.buttonsHolder.find('li').hide();
    }


    /*
     *
     * setWidthAndHeight
     * @param: void
     * @return: void
     *
     */
    function setWidthAndHeight() {
        var du = this,
            newWidth = (du.itemCollection.length * du.stepWidth),
            newHeight = du.itemCollection.outerHeight(true),
            maskWidth = du.mask.width();
        // Width of the items

        if (newWidth != du.currentWidth) {
            du.items.css({
                'width': newWidth + 'px'
            });
            du.currentWidth = newWidth;
        }
        // Height of the entire box
        if (newHeight != du.currentHeight) {
            du.mask.css({
                'height': newHeight + 'px'
            });
            du.currentHeight = newHeight;
            // Fix buttons position
            // on screen resize
            if (du.arrows) {
                $(du.arrows).css({
                    'top': ((du.mask.outerHeight(true) / 2) - ($('.buttonScroll').outerHeight(true) / 2)) + 'px'
                });
            }
        }
    }

    /*
     *
     * init
     * @param: void
     * @return: void
     * Called on class initialization
     *
     */
    function init() {

        var du = this,
            tmo = undefined;
        du.items = du.options.target.find('ul.items');
        du.itemCollection = du.options.target.find('li.item');
        du.mask = du.options.target.find('div.itemsW');
        du.stepWidth = du.itemCollection.outerWidth(true);

        if (du.options.customPagination) {
            du.buttonsHolder = du.options.target.find('.paginationThumbnails ul');
            du.buttons = du.buttonsHolder.find('li');
            $(du.buttons.get((du.buttons.length - 1))).addClass('lastElement');
            $(du.buttons.get(0)).addClass('firstElement');
        } else {
            /* Create the legend bullets */
            du.addLegend();
            /* Add arrows */
            du.addArrows();
            du.enableArrows();
        }

        /* Setup width */
        du.setWidthAndHeight();
        /* Enable click on the legend bullets */
        du.enableLegend();

        /* Add listeners on swipe if needed */
        if (du.options.enableTouch && du.isTouch) {
            du.items.hammer({
                "prevent_default": du.options.preventClick,
                "drag": true,
                "drag_vertical": false,
                "drag_horizontal": true,
                "drag_min_distance": 0,
                "transform": false,
                "tap": false,
                "tap_double": false,
                "hold": false
            })
                .unbind("dragstart drag dragend")
                .bind(
                    "dragstart drag dragend",
                    function (e) {
                        switch (e.type) {
                            case 'dragend':
                                du.changeSlide(e.target, e.distance, e.direction);
                                break;
                            case 'drag':
                            case 'dragstart':
                                break;
                        }
                    }
                );
        }

        du.options.target.hover(
            function (e) {
                $(this).addClass('buttonsHover');
            },
            function (e) {
                $(this).removeClass('buttonsHover');
            }
        );

        /* Add listener on screen resize */
        $(window).resize(function () {
            du.windowResize();
        });

        tmo = setTimeout(
            function () {
                clearTimeout(tmo);
                du.windowResize();
                /* Show the first element based on direction */
                $(du.buttons.get(0)).children('a').trigger('click');
                // $(du.options.target).css( { 'opacity' : 1 } );
                /* Add timing if needed */
                if (du.options.timer > 0) {
                    du.autoscroll = true;
                    du.timeout = setTimeout(function () {
                        clearTimeout(du.timeout);
                        du.manageTimeout();
                    }, du.options.timer);
                }
            },
            1000
        );
    }

    this.options = $.extend(this.options, $options);
    // $(this.options.target).css( { 'opacity' : 0 } );
    this.init();
}