$(document).ready(
    function () {



        //this function will initiate the select2 plugin on the given plot number field
        //and will attache the normal events to work with the selectric plugin
        //mainly developed to change the behaviour of plot field on specific forms
        var initSelect2Plugin = function($selector) {

            $selector.each(function(index,item){
                var $item = $(item);

                var placeholder = $item.attr('placeholder');
                var enableSearchBox = $item.attr('enable-search');
    
                var options = {
                    placeholder: placeholder
                }
                if(enableSearchBox) {
                    options.minimumResultsForSearch = Infinity;
                }else {
                    options.language =  {
                            noResults: function(){
                                    return "Plot not found  <a href='#' class='trigger-other'>Type it manually</a>";
                                }
                        };

                        options.escapeMarkup = function (markup) {
                            return markup;
                        }
                }
                var initalizedSelector = $item.select2(options);

                initalizedSelector.on('select2:open',function(){
                    $('#select2-'+$item.attr('id')+'-results').on('click','.trigger-other',function(e){
                        e.preventDefault();
                        initalizedSelector.val("other").trigger("change").select2("close");
                        return  false;
                    });
                });
                
        })


            

        }
        //@ACN: initialize plugin for all select tag of all pages (move on top of the file) or $("#example select").selectric();
        //Run selectric plugin.
        $("select:not(.noselectric)").selectric();
        $("select.select2").each(function(index,item){
            initSelect2Plugin($(item));
        })
        
        // GAQ manual title push
        //$title($('h1').text());

        // Add reverse function to jquery for each elements
        jQuery.fn.reverse = [].reverse;



        // Define variables for LTR or RTL
        var transformsEnabledVar = true;
        var isLTR = true;
        if ($('html').attr("dir") == "rtl") {
            transformsEnabledVar = false;
            isLTR = false;
        }

        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

 /*
         * enableSearchBox
         * @param: event
         * @return: void
         *
         */

        var enableSearchBox = function (e) {
            var hideSearchPanel = function () {

                    $('#searchBoxPanel').removeClass('searchBoxOpen');

                    $('.container-du').css(
                        function setCSS() {
                            var out = {};
                            if (useTransforms) {
                                out = {
                                    'padding-bottom': '0'
                                };
                            }
                            return out;
                        }()
                    ).removeAttr('style');
                    $("#toggleSearchLink").removeClass('open');
                    $('#searchBoxPanel').removeClass(
                        'searchBoxTransform').removeClass(
                        'searchBoxTop').removeAttr('style');
                    $(
                        '#searchBoxPanel form input[type="text"], #searchBoxPanel form input[type="search"]'
                    ).val('');

                },
                showSearchPanel = function () {
                    $('#searchBoxPanel').addClass('searchBoxOpen');
                    $('#searchBoxPanel').addClass(
                        function setCSS() {
                            var out = {};
                            if (useTransforms) {
                                out = 'searchBoxTransform';
                            } else {
                                out = 'searchBoxTop';
                            }
                            return out;
                        }()
                    );
                    $('.container-du').css(
                        function setCSS() {
                            var out = {};
                            if (useTransforms) {
                                out = {
                                    'padding-bottom': '320px'
                                };
                            }
                            return out;
                        }()
                    );

                    $('#searchBoxPanel .close a').off().on('click',function(e){
                                e.preventDefault();
                                hideSearchPanel();
                    });


                    $('.searchField').focus();
                },
            // WARNING!
            // SCREEN SIZES CHECK HARDCODED INTO THE JS FILE!
                windowResize = function () {
                    if ($('body').width() < 1024) hideSearchPanel();
                },
                useTransforms = ($('html').hasClass('csstransforms3d')) ?
                    true : false;

            e.preventDefault();
            $(this).toggleClass('open');
            if ($('#searchBoxPanel').hasClass('searchBoxOpen')) {
                hideSearchPanel();
            } else {
                showSearchPanel();
            }
            $(window).resize(windowResize);
        };


        var autoMatchheight = function autoMatchheight(fixNormalizeErrors) {
            // Apply the a class starting with "matchheight-" to match the height of all the elements with the same class
            var matchheightClasses = [];
            $("[class^='matchheight-'], [class*=' matchheight-']").each(function () {
                var elemClasses = $(this).attr("class").split(/\s+/);
                $(elemClasses).each(function (index, cssClass) {
                    var isCssMatchHeight = false;
                    if (cssClass.startsWith) {
                        isCssMatchHeight = cssClass.startsWith("matchheight-");
                    } else if (cssClass && cssClass.length > "matchheight-".length) {
                        isCssMatchHeight = cssClass.substring(0, "matchheight-".length) === "matchheight-";
                    }
                    if (isCssMatchHeight && matchheightClasses.indexOf(cssClass) < 0) {
                        matchheightClasses.push(cssClass);
                    }
                });
            });
            $(matchheightClasses).each(function (index, cssClass) {
                $("." + cssClass).matchHeight();

                // normalize heights in case of errors
                if (fixNormalizeErrors) {
                    var maxHeightGroup = 0;
                    var elementsWithHeightZero = [];
                    $("." + cssClass).each(function () {
                        if ($(this).height() > maxHeightGroup) {
                            maxHeightGroup = $(this).height();
                        }

                        if ($(this).height() === 0) {
                            var currentHeight = 0;
                            if ($(this).children().length > 0) {
                                currentHeight = $(this).children().first().height() + 5;
                            }
                            if (currentHeight === 0) {
                                elementsWithHeightZero.push(this);
                            } else {
                                $(this).height(currentHeight);
                            }
                        }
                    });
                    $(elementsWithHeightZero).each(function () {
                        $(this).height(maxHeightGroup);
                    });
                }
            });
        };

        autoMatchheight(true);



        // -- Search box init
        $('#search').focus(function () {
            var offsetFromTop = $(window).scrollTop() + 40;
            $('.top-search-btn').addClass('open');
            $('#searchBoxPanel').css('top', offsetFromTop).addClass(
                'searchBoxOpen').addClass('searchBoxTransform');
        }).focusout(function () {
            $('.top-search-btn').removeClass('open');
            $('#searchBoxPanel').removeClass('searchBoxOpen').removeClass(
                'searchBoxTransform');
        });
        // -- Search box init
        // -- change your plan [mobile] init

        //top-search
        //support search
        $('#support-search')
            .focus(function () {
                $('.top-search-btn').addClass('open');
                $('#supportSearchBoxPanel').addClass('searchBoxOpen');
                $('#supportSearchBoxPanel').addClass('searchBoxTransform');
            });
        //.focusout(function () {
        //    $('.top-search-btn').removeClass('open');
        //    $('#supportSearchBoxPanel').removeClass('searchBoxOpen');
        //    $('#supportSearchBoxPanel').removeClass('searchBoxTransform');
        //});




        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * enableAccordion
         * @param: void
         * @return: void
         *
         */
        var enableAccordion = function () {
            /*
             function checkSize() {
             if ($('body').width() > 999 && $('body').width() < 1170) enableScroll();
             else destroyScroll();
             }

             function destroyScroll() {
             if (scroll != undefined) {
             scroll.destroy();
             scroll = undefined;
             }
             $('#accountPane .menuW').removeClass( 'scrollEnabled' );
             $('#menuW').removeAttr('style');
             }

             function enableScroll() {
             if ($('#accountPane .menu .item .open').length > 0) {
             $('#accountPane .menuW').addClass( 'scrollEnabled' );
             if (scroll != undefined) scroll.refresh();
             else scroll = new iScroll('menuW', { 'vScroll': true, 'vScrollbar': true, 'hScroll': false, 'hScrollBar': false, 'hideScrollbar': false, 'fixedScrollbar': true, 'fadeScrollbar': false });
             } else {
             destroyScroll();
             }
             }
             */

            $('#accountPane .menu .item h6 a')
                .hammer({
                    'prevent_default': false
                })
                .on({
                    'tap': function (e) {
                        e.preventDefault();
                        var l = $(this).attr('href'),
                            t = $(this).parent('h6').parent(
                                'li').children(
                                'div[data-name="' + l.replace(
                                    /#/ig, '') + '"]');
                        if ((t).hasClass('open')) {
                            $(this).removeClass('open');
                            t.removeClass('open');
                            // if ($('body').width() > 999 && $('body').width() < 1170) enableScroll();
                        } else {
                            $(this).addClass('open');
                            t.addClass('open');
                            // if ($('body').width() > 999 && $('body').width() < 1170) enableScroll();
                        }

                        // $(window).resize( checkSize );
                    },
                    'click': function (e) {
                        e.preventDefault();
                    }
                });
            $('#accountPane .menu .item h6 a.default').trigger('tap');
        }

        enableAccordion();


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * setplaceholders
         * @param: void
         * @return: void
         * Set placeholder attribute on input elements non supporting it
         */
        var setplaceholders = function () {
            var test = document.createElement('input');
            if (!('placeholder' in test)) {
                $(
                    'input[type="text"], input[type="password"], input[type="email"], input[type="search"]'
                ).each(
                    function () {
                        var plh = $(this).attr('placeholder');
                        $(this).val(plh);
                        $(this).off('focus').on(
                            'focus',
                            function (e) {
                                if ($(this).val() == plh) $(
                                    this).val('');
                            }
                        ).off('blur').on(
                            'blur',
                            function (e) {
                                if ($(this).val() == '') $(this)
                                    .val(plh);
                            }
                        );
                    }
                );
            }
        }
        // Input elements placeholder polyfill
        setplaceholders();


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * Global variables
         */
        var useTransforms = $('html').hasClass('csstransforms3d');

 
        // Check if the user is on a smartphone or on a desktop
        new duDetect();
        // Check if we need to fix columns on ie
        if (!$('html').hasClass('csscolumns') && $('.columnizable').length > 0) {
            $('.columnizable').each(
                function () {
                    if (!$(this).hasClass('du-ready')) {
                     
                        new duColumns({
                            'target': $(this)
                        });
                        $(this).addClass('du-ready');
                    }
                }
            );
        }


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /* CHAPTERS NAVIGATION */
        if ($("#chapterNav").length > 0) {
            $("#chapterNav").clone().attr("id", "chapterNavCloned").prependTo(
                "#topBar");
        }
        //$('.chapterNav a, .footer a:eq(0)').attr('rel', 'external');
        // menu lats item fix
        $('#chapterNavCloned > li:visible:last').addClass('last-child');
        if($('.mainItem.current').hasClass('sub')){
            $('.currentdiv').addClass('sub');
        }
        $('a[rel="external"]').attr('target', '_blank');
        $("#currentdiv").hammer({
            prevent_default: false
        }).on({
            tap: function (e) {
                $(this).parent().toggleClass('open');
                $("#chapterNav").toggleClass('open');
            },
            click: function (e) {
                e.preventDefault();
            }
        });
        $("#dulogomobile").hammer({
            prevent_default: false
        }).on({
            tap: function (e) {
                window.scrollTo(0, 0);
            },
            click: function (e) {
                e.preventDefault();
            }
        });

        /* TOP SEARCH BOX */
        $("#toggleSearchLink").hammer({
            prevent_default: true
        })
            .off('tap')
            .on({
                tap: enableSearchBox,
                click: enableSearchBox
            });


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /* ACCOUNT PANEL BEHAVIOURS */
        /*
         * toggleAccountPane
         * @param: action
         * @return: null
         *
         */
        function toggleAccountPane(action) {
            var isAccountPaneOpen = $("#accountPane").is(":visible");
            if ((isAccountPaneOpen) || (action == 'close')) { // close pane
                $("#accountPane").removeClass('animate');
                setTimeout(function () {
                    $("#accountPane").removeClass('hidden')
                }, 500);
            } else { // open pane
                $("#accountPane").addClass('hidden').delay(100).addClass(
                    'animate');
                // var t = setTimeout( function() { clearTimeout(t); if (scroll != undefined) scroll.refresh(); } , 500);
            }
        }

        $("#toggleAccountPane1024").off("click").on("click", function (e) {
            e.preventDefault();
            toggleAccountPane();
        });
        // Click to slide the account panel in
        $("#toggleAccountPane768").off("click").on(
            "click",
            function (e) {
                e.preventDefault();
                $('#mainNav form input[type="text"]').val('');
                if ($("#content").hasClass(
                        'content-mobile-slide-out-left')) {
                    $("#content, #topBar").removeClass(
                        'content-mobile-slide-out-left');
                    $("#accountPane").removeClass(
                        'account-mobile-slide-in');
                    if (!useTransforms) {
                        $("#content, #topBar").removeClass(
                            'content-mobile-slide-out-left-ie');
                        $("#accountPane").removeClass(
                            'account-mobile-slide-in-ie');
                    }
                } else if ($("#content").hasClass(
                        'content-mobile-slide-out-right')) {
                    $("#content, #topBar").removeClass(
                        'content-mobile-slide-out-right').addClass(
                        'content-mobile-slide-out-left');
                    $("#accountPane").addClass(
                        'account-mobile-slide-in');
                    $("#mainNav").removeClass('menu-mobile-slide-in');
                    if (!useTransforms) {
                        $("#content, #topBar").removeClass(
                            'content-mobile-slide-out-right-ie').addClass(
                            'content-mobile-slide-out-left-ie');
                        $("#accountPane").addClass(
                            'account-mobile-slide-in-ie');
                        $("#mainNav").removeClass(
                            'menu-mobile-slide-in-ie');
                    }
                } else {
                    $("#content, #topBar").addClass(
                        'content-mobile-slide-out-left');
                    $("#accountPane").addClass(
                        'account-mobile-slide-in');
                    if (!useTransforms) {
                        $("#content, #topBar").addClass(
                            'content-mobile-slide-out-left-ie');
                        $("#accountPane").addClass(
                            'account-mobile-slide-in-ie');
                    }
                }
                $('body').scrollTop(0);
            }
        );


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

        // Click to slide the menu in
        /*
         * Left side menu slide in/out
         * @param:
         * @return:
         *
         */
        $("#toggleMenuPane").off("click").on(
            "click",
            function (e) {
                e.preventDefault();
                $('#mainNav form input[type="text"]').val('');
                // Account panel visible, then close it and make main navigation visible
                if ($("#content, #topBar").hasClass(
                        'content-mobile-slide-out-left')) {
                    $("#content").removeClass(
                        'content-mobile-slide-out-left').addClass(
                        'content-mobile-slide-out-right');
                    $("#mainNav").addClass('menu-mobile-slide-in');
                    $("#accountPane").removeClass(
                        'account-mobile-slide-in');
                    if (!useTransforms) {
                        $("#content").removeClass(
                            'content-mobile-slide-out-left-ie').addClass(
                            'content-mobile-slide-out-right-ie');
                        $("#mainNav").addClass(
                            'menu-mobile-slide-in-ie');
                        $("#accountPane").removeClass(
                            'account-mobile-slide-in-ie');
                    }
                    // Main navigation visible, then close it
                } else if ($("#content").hasClass(
                        'content-mobile-slide-out-right')) {
                    $("#content, #topBar").removeClass(
                        'content-mobile-slide-out-right');
                    $("#mainNav").removeClass('menu-mobile-slide-in');
                    if (!useTransforms) {
                        $("#content, #topBar").removeClass(
                            'content-mobile-slide-out-right-ie');
                        $("#mainNav").removeClass(
                            'menu-mobile-slide-in-ie');
                    }
                    // Open main navigation panel
                } else {
                    $("#content, #topBar").addClass(
                        'content-mobile-slide-out-right');
                    $("#mainNav").addClass('menu-mobile-slide-in');
                    if (!useTransforms) {
                        $("#content, #topBar").addClass(
                            'content-mobile-slide-out-right-ie');
                        $("#mainNav").addClass(
                            'menu-mobile-slide-in-ie');
                    }
                }
                $('body').scrollTop(0);
            }
        );

        // close menu and account pane onblur
        $('.content').on('click', function () {
            if ($(this).hasClass('content-mobile-slide-out-right')) {
                $("#toggleMenuPane").trigger('click');
            }
            else if ($(this).hasClass('content-mobile-slide-out-left')) {
                $(".toggleAccountPane").trigger('click');
            }
        })


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        // Buy buttons section
        if ($('.modDetail .btn-price').length > 0 && $('.purchaseOptions').length > 0) {
       
            new duBuy($('.modDetail .btn-price:eq(0)'), $('.purchaseOptions:eq(0)'), $('.bt-close:eq(0)'));
            new duBuy($('.modDetail .btn-price:eq(1)'), $('.purchaseOptions:eq(1)'), $('.bt-close:eq(1)'));
        }
        // Buy buttons section
        if ($('.modDetail .btn-large').length > 0 && $('.purchaseOptions').length > 0) {
 
            new duBuy($('.modDetail .btn-large'), $('.purchaseOptions'), $('.bt-close'));
        }


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

        /* temporary workaround */
        if ($('.modProduct .pic img').length > 0) {
            $('.modProduct .pic img').removeAttr("width").removeAttr(
                "height");
        }

        // in-page navigation
        if ($('.inpagenavigation').length > 0) {
       
            var dustt = new duScrollToTop();

            if ($('.inpagenavigation .items a').length > 0) {
                var n = $('.inpagenavigation .items a').length;
                $('.inpagenavigation .items a').parent('.item').addClass(
                    'b-' + n);
            }
        }


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

        // Accordion function
        var $accor = function () {
            $(".openClose").hammer({
                'prevent_default': true
            }).on({
                'tap': function (e) {
                    e.preventDefault();

                    var $li = $(this).parent();
                    var $ul = $li.parent();

                    if ($li.hasClass('open')) {
                        $('.subNavList', $li).hide();
                        $('.mainItem', $ul).removeClass(
                            'open');
                    } else {
                        $('.subNavList', $ul).hide();
                        $('.mainItem', $ul).removeClass(
                            'open');
                        $li.addClass('open');
                        $('.subNavList', $li).slideDown(400);
                    }

                },
                'click': function (e) {
                    e.preventDefault();
                }
            })
        };

        $accor();


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * Dials plans/usage and plan selector page
         */
        $('.modDials').each(function () {
            if (!$(this).hasClass('du-ready')) {
             
                 
                new protoSlide({
                    'target': $(this),
                    direction: isLTR ? 'ltr' : 'rtl'
                }); 
                $(this).addClass('du-ready');

                $(this).on('refresh',function(){
                    new protoSlide({
                        'target': $(this),
                        direction: isLTR ? 'ltr' : 'rtl'
                    }); 
                });
            }
            //if(!$('li.item',$(this)).length) $(this).css({'min-height':0,'margin-bottom':0});
        });

        //dials
        var plans = [{
            data: 2,
            voice: 600,
            sms: 0,
            minutes: 300,
            price: 300
        }, {
            data: 4,
            voice: 1200,
            sms: 0,
            minutes: 400,
            price: 600
        }, {
            data: 10,
            voice: 2500,
            sms: 500,
            minutes: 600,
            price: 1000
        }];
        $('.static').each(function () {
            if (!$(this).hasClass('du-ready')) {
              
                new protoSlide({
                    'target': $(this),
                    length: plans.length,
                    enableTouch: false
                });
                $(this).addClass('du-ready');
            }
        });
        var pDials = $('.dial').protoDial({});
        var tabs = $("ul.tabs li");

        //attach different events on moveLeft and moveRight
        $(".dial-view .moveLeft").on("moved.prototype", function () {
            console.log("moveLeft");
            var index = $("ul.tabs li.active").index();
            tabs.removeClass("active");
            $(tabs.get(index - 1)).addClass("active");
            updateView(plans[index], plans[index - 1]);
        });
        $(".dial-view .moveRight").on("moved.prototype", function () {
            console.log("moveRight");
            var index = $("ul.tabs li.active").index();
            tabs.removeClass("active");
            $(tabs.get(index + 1)).addClass("active");
            updateView(plans[index], plans[index + 1]);
        });

        $('.modDials .items .item').each(function () {
            $('.plan-grid-view .btn', $(this)).click(function () {
                //alert('');
                $('.modDials .items .item').find(
                    '.plan-grid-view .active').text(
                    'Select').removeClass(
                    'active btn-primary').addClass(
                    'btn-default');
                $(this).text('Selected').removeClass(
                    'btn-default').addClass(
                    'btn-primary active');
                //$($(this).attr('rel')).slideDown('slow');
            });
        });

        var retrieveDataShowSpinner = function (content) {
            content.find(".spinner").removeClass("hide");
            content.find(".due-amount-container-value-text").addClass("hide");
            content.find(".due-date-container-value-text").addClass("hide");

            setTimeout(function () {
                content.find(".spinner").addClass("hide");
                content.find(".due-amount-container-value-text").removeClass("hide");
                content.find(".due-date-container-value-text").removeClass("hide");
            }, 1000);
        };

        function updateView(oldPlan, newPlan) {
            //update dials
            $('#dataplan').data("dial_protoDial").update({
                available: newPlan.data,
                animateFrom: oldPlan.data
            });
            $('#voice').data("dial_protoDial").update({
                available: newPlan.voice,
                animateFrom: oldPlan.voice
            });
            $('#sms').data("dial_protoDial").update({
                available: newPlan.sms,
                animateFrom: oldPlan.sms
            });
            $("#minutes").html(newPlan.minutes);
            $("#price").html(newPlan.price);
            //scroll tabs
            scrollTabs();
        }

        var arrowLocation = $(".arrow-separator").width() / 2;
        var width = tabs.first().width();
        var useTransforms = $("html").hasClass("csstransforms3d");

        function scrollTabs() {
            //awewsome formula;
            var newPosition = arrowLocation //location of arrow
                - ((($("ul.tabs li.active").index()) * width)) //minus the location of the active li
                - (width / 2); //minus half the width to center the arrow
            if (useTransforms) {
                $("ul.tabs").css({
                    "-webkit-transform": "translate3d(" +
                    newPosition + "px,0,0)",
                    "-moz-transform": "translate3d(" +
                    newPosition + "px,0,0)",
                    "-o-transform": "translate3d(" +
                    newPosition + "px,0,0)",
                    "-ms-transform": "translate3d(" +
                    newPosition + "px,0,0)",
                    "transform": "translate3d(" + newPosition +
                    "px,0,0)"
                });
            } else {
                $("ul.tabs").animate({
                    'margin-left': newPosition + 'px'
                });
            }
        }

        $(".planTypeSelector").selectric();
        $(".dial-view ul.tabs li").on("click", function (event) {
            event.preventDefault();
            event.stopPropagation();
            var $this = $(this);
            if (!$this.hasClass("active")) {
                var index = $this.index();
                $($(".dial-view .pagination li").get(index)).find(
                    "a").click();
            }
        });

        $(".change-view").on("click", function (event) {
            event.preventDefault();
            $(".plan-select").show();
            $(this).closest(".plan-select").hide();
            if (!$(".dial-view").is(":hidden")) {
                //re-update arrow width
                arrowLocation = $(".arrow-separator").width() / 2;
                width = tabs.first().width();
                scrollTabs();
            }
            //fix scroll height issue
            $(window).resize();
        });

        // -- change your plan [mobile] init
        // -- upgrade/downgrade manage data init
        $('.knob input').protoDial();
        $('.slideable').each(function () {
            if (!$(this).hasClass('du-ready')) {
 
                new protoSlide({
                    'target': $(this)
                });
                $(this).addClass('du-ready');
            }
        });

        // -- upgrade/downgrade manage data init
        // --- dashboard
        $("#show-details").on("click", function (event) {
            event.preventDefault();
            var $this = $(this);
            var altText = $this.data("alttext");
            var html = $this.html();
            $this.html(altText).data("alttext", html);
            $("#overhead-details").toggleClass("content-active");
        });
        $(".accordion-action").on("click", function (event) {
            event.preventDefault();

            var $this = $(this);
            var content = $($this.attr("href"));
            var altText = $this.data("alttext");
            var html = $this.html();
            var shouldCollapseOthers = $this.hasClass("collapse-others");

            if ($this.hasClass("hide")) {
                content.removeClass("plan-content-active");
                $this.removeClass("hide").addClass("show").html(
                    altText).data("alttext", html);
            } else {
                if (shouldCollapseOthers) {
                    $(".accordion-action.hide").each(function () {
                        var int$this = $(this);
                        var intContent = $(int$this.attr("href"));
                        var intAltText = int$this.data("alttext");
                        var intHtml = int$this.html();

                        intContent.removeClass("plan-content-active");
                        int$this.removeClass("hide").addClass("show").html(
                            intAltText).data("alttext", intHtml);
                    });
                }

                retrieveDataShowSpinner(content);

                content.addClass("plan-content-active");
                $this.removeClass("show").addClass("hide").html(
                    altText).data("alttext", html);
            }
        });


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         *   duSlide for misc plans table
         */


        if ($('.slideable').length > 0 && $('.pricing-filter').length > 0) {

            function initSlider(table) {
           
                new duSlide({
                    'target': table,
                    'enableTouch': true,
                    'timer': 0
                });
            }

            $('.slideable').each(function () {
                var table = $(this);
                var tablehtml = $(this).html();
                var filterSelector = table.data('filterButtons');

                function filterTable() {

                    var selectedSelector = $(filterSelector).find('.is-checked').data('filter');
                    var newTableHtml = $(tablehtml).find('.item:not(' + selectedSelector + ')').remove().end();

                    var previousPos = $(window).scrollTop();
                    table.html(newTableHtml);
                    if (requestAnimationFrame) {
                        requestAnimationFrame(function () {
                            $(window).scrollTop(previousPos);
                        });
                    } else {
                        setTimeout(function () {
                            $(window).scrollTop(previousPos);
                        }, 0);
                    }
                }

                if ($(filterSelector + ' .is-checked').length) {

                    filterTable();
                }
                initSlider(table);
                table.addClass('du-ready');

                $($(filterSelector + ' .btn')).click(function (e) {
                    e.preventDefault();
                    var currentBtn = $(this);

                    //to toggle active on buttons
                    currentBtn.siblings().not(currentBtn).removeClass('is-checked');
                    currentBtn.addClass('is-checked');

                    filterTable();
                    initSlider(table);
                });
            })
        }


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         *   Box sliders
         */


        if ($('[data-bxslider-filterable]').length > 0) {

            $('[data-bxslider-filterable]').each(function () {
                //get Data
                var table = $(this);
                var $originalTableHtml = $(this).html();
                var $scrollableSection = $(this).find('[data-content-scroll]');
                var searchBtnsSelector = table.data('bxsliderFilterBtns');
                var slidesNumber = table.data('slidesNumber');

                //override the selected number of max slides on mobile
                if (($(window).width()) < 768) {
                    slidesNumber = 1
                }
                //END

                var options = {
                    minSlides: slidesNumber,
                    maxSlides: slidesNumber,
                    slideWidth: 360,
                    infiniteLoop: false,
                    hideControlOnEnd: true
                };

                $(searchBtnsSelector).find('a').click(function (e) {
                    e.preventDefault();
                    //get Data
                    var tempTableHtml = $originalTableHtml;
                    var filterVal = $(this).data('filter');

                    //remove other items but the selected and replace table with new html
                    tempTableHtml = $(tempTableHtml).find(table.data('bxsliderFilterSelector') + ':not(' + filterVal + ')').remove().end();
                    var replacedTable = table.html(tempTableHtml);

                    //reinitialize only when items are there to initialize
                    var filteredSlidesNumber = $(tempTableHtml).find(table.data('bxsliderFilterSelector') + filterVal).length;
                    if (filteredSlidesNumber > 0) {
                        var showControlsPager = filteredSlidesNumber / slidesNumber > 1
                        options.controls = showControlsPager;
                        options.pager = showControlsPager;
                        options.touchEnabled = showControlsPager;
                        replacedTable.find('[data-content-scroll]').bxSlider(options);
                    }

                    //to toggle active on buttons
                    $(this).siblings().not(this).removeClass('is-checked');
                    $(this).addClass('is-checked');

                    var slides = table.find(filterVal + ':not(.bx-clone)');
                    if (slides.length > 0) {
                        slides.first().addClass('firsty');
                        slides.last().addClass('lasty');
                    }

                })

                //if one have is-checked class I will filter with it
                $(searchBtnsSelector).find('a.is-checked').click();
                //END

                //first initialize
                $scrollableSection.bxSlider(options);
            });

        }


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         *  Responsive tables
         */


        if ($('.modTextual.modTables').length > 0) {

            var table = $('.modTextual.modTables.buttonsHover');
            var tableHtml = table.html();
            var tableObject = $(tableHtml);

            $(window).on('resize', function () {
                if (($(window).width() + 17) < 768) {
                    tableObject = $(tableHtml);
                    if (!table.hasClass('slideable')) {
                        table.addClass('slideable');
                        table.find('>div').addClass('itemsW').wrapInner('<ul class="items">');
                        var itemTable = table.find('.duTable').clone();
                        var itemTableHtml = itemTable[0].outerHTML;
                        itemTable.find('tbody').html('');
                        table.find('.items').html('');

                        for (var i = 0; i < 3; i++) {
                            var container = '';
                            var title = tableObject.find('tr th:lt(1)');
                            tableObject.find('tr th:lt(1)').remove();
                            tableObject.find('tr:has(td)').each(function () {
                                var row = $(this).find('td:lt(2)');
                                var rowHTML = '';
                                row.each(function () {
                                    rowHTML += $(this)[0].outerHTML
                                })
                                container += '<tr>' + rowHTML + '</tr>';
                                $(this).find('td:lt(2)').remove();
                            });

                            var liItem = $('<li class="item">' + itemTable.html() + '</li>')
                            table.find('.items').append(liItem);

                            liItem.find('tbody').append('<tr>' + title[0].outerHTML + '</tr>' + container)
                        }
                      

                        new duSlide({
                            'target': table,
                            'enableTouch': true,
                            'timer': 0
                        });

                    }
                } else {
                    table.removeClass('slideable');
                    table.html(tableHtml)
                }
            });
            setTimeout(function () {
                $(window).resize();
            }, 1000);
        }


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        //for toggling any class
        $('[data-toggle-class]').on('click', function (e) {
            e.preventDefault();
            var $elem = $(this);
            var $target = $($elem.data('target'));
            $target.toggleClass($elem.data('toggleClass'));
        })
        //End

        //}).click();
        $("#line").on("click", function () {
            $(".graph").html("");
            var charter = $(".graph").protoCharter({
                height: 400,
                type: "line"
            });
            charter.attachData([
                {"label": "Jan", "value": 300},
                {"label": "Feb", "value": 600},
                {"label": "Mar", "value": 490},
                {"label": "Apr", "value": 630},
                {"label": "May", "value": 476},
                {"label": "Jun", "value": 676, unbilled: true}
            ]);
            charter.draw();
        });


        // form validation functions
        $("form[id*='form']").protoValidator();
        $("#login").protoValidator();
        $("#loginCenter").protoValidator();
        $("#quickPay").protoValidator();
        $("div.timer").protoTimer({
            timerSeconds: 20,
            useKnob: true,
            callback: function () {
                //alert("Timer ended");
                $("#save").attr("disabled", "disabled");
            }
        });
        $("#contact-us-form").protoValidator();

        // Popover init
        $('.showpopover').popover({html: true});

        // init ios switch
        $('input:not(.ios-switch)').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });


        /*
         *  Troubleshoot page
         */


        if ($("div[class*='troubleshoot-guide']").length > 0) {

            // change is-checked class on buttons
            $('.btn-group').each(function (i, buttonGroup) {
                var $buttonGroup = $(buttonGroup);
                $buttonGroup.on('click', '> a, > button', function () {
                    $('input', $(this).parent().parent().nextAll()).iCheck('uncheck');
                    if ($buttonGroup.parent().hasClass('troubleshoot-start')) {
                        $('.btn-group').find('.active').removeClass('active btn-primary').addClass('btn-default');
                        $(this).parent().parent().nextAll().slideUp('fast');
                    } else if ($buttonGroup.parent().parent().hasClass('mod-troubleshoot')) {
                        $(this).parent().parent().nextAll().slideUp('fast');
                    } else {
                        $buttonGroup.find('.active').removeClass('active btn-primary').addClass('btn-default');
                    }
                    $(this).removeClass('btn-default').addClass('btn-primary active');
                    $($(this).attr('rel')).slideDown('slow');
                });
            });

            // Troubleshoot page
            $("div[class*='troubleshoot-guide'] label").click(function () {
                $(this).parent().find('label').each(function () {
                    $(this).parent().parent().nextAll().slideUp('fast');
                })
                $('input', $(this).parent().parent().nextAll()).iCheck('uncheck');
                $($(this).find('input').attr('rel')).slideDown('slow', function () {
                });
            })

        }


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        // Fix iRadio label click issue
        $('.iradio_minimal .iCheck-helper').click(function () {
            $(this).parent().parent().trigger('click')
        });

        // Payment history datepicker
        $('#paymentDate.date-time-picker').datetimepicker({
            format: 'd/m/Y',
            timepicker: false,
            closeOnDateSelect: true,
            onChangeDateTime: function (ct, $i) {
                //usage_table.draw();
            }
        });

        // payment history table & filters
        var payment_history = $('#payment-history').DataTable({
            "aoColumns": [{
                "sType": "custom_euro_date"
            }, {}, {}, {}],
            "aaSorting": [
                [0, "desc"]
            ], // Sort by first column descending
            "info": false,
            "paging": false,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Type a keyword..."
            },
            initComplete: function () {
                var api = this.api();
                var i = 1;
                var column = api.column(i);
                var select = $(
                    '<select class="hidden-sm"><option value="">Filter use type</option></select>'
                ).appendTo($('.table-filters')).on('change',
                    function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val());
                        column.search(val ? '^' + val + '$' :
                            '', true, false).draw();
                    });
                column.data().unique().sort().each(function (d,
                                                             j) {
                    select.append('<option value="' + d +
                        '">' + d + '</option>');
                });

                $('.dataTables_filter input[type=search]').addClass('control full-width');
            }
        });

        // recharge history table & filters
        var recharge_history = $('#recharge-history').DataTable({
            "aoColumns": [{
                "sType": "custom_euro_date"
            }, {}, {}, {}],
            "aaSorting": [
                [0, "desc"]
            ], // Sort by first column descending
            "info": false,
            "paging": false,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Type a keyword..."
            },
            initComplete: function () {
                var api = this.api();
                var i = 1;
                var column = api.column(i);
                var select = $(
                    '<select class="visible-xs"><option value="">Filter use type</option></select>'
                ).appendTo($('.table-filters')).on('change',
                    function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val());
                        column.search(val ? '^' + val + '$' :
                            '', true, false).draw();
                    });
                column.data().unique().sort().each(function (d,
                                                             j) {
                    select.append('<option value="' + d +
                        '">' + d + '</option>');
                });

                $('.dataTables_filter input[type=search]').addClass('control full-width');
            }
        });

        // usage history table & filters
        var usage_table = $('#usage').DataTable({
            "aoColumns": [{
                "sType": "custom_euro_date"
            }, {}, {}, {}, {}, {}],
            "aaSorting": [
                [0, "desc"]
            ], // Sort by first column descending
            "info": false,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Type a keyword..."
            },
            initComplete: function () {
                var api = this.api();
                var i = 1;
                var column = api.column(i);
                var select = $(
                    '<select class="visible-xs"><option value="">Filter use type</option></select>'
                ).appendTo($('.table-filters')).on('change',
                    function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val());
                        column.search(val ? '^' + val + '$' :
                            '', true, false).draw();
                    });
                column.data().unique().sort().each(function (d,
                                                             j) {
                    select.append('<option value="' + d +
                        '">' + d + '</option>');
                });
                $('.table-filters .btn-group .btn').on('click',
                    function (e) {
                        e.preventDefault();
                        $('.table-filters .btn-group .btn')
                            .removeClass('is-checked');
                        $(this).addClass('is-checked');
                        var val = $(this).attr('data-use-type');
                        /*if(val.indexOf(',') > -1) {
                         var tempVal = new Array();
                         tempVal = val.split(',');
                         $.each(tempVal,function(i){
                         column.search(val ? '^' + tempVal[i] + '$' : '', true, false);
                         })
                         }
                         else*/
                        column.search(val ? '^' + val + '$' : '', true, false).draw();
                    });

                $('.dataTables_filter input[type=search]').addClass('control full-width');
            }
        });

        // init date time picker
        $('#startdate').datetimepicker({
            format: 'd/m/Y',
            timepicker: false,
            closeOnDateSelect: true,
            onChangeDateTime: function (ct, $i) {
                usage_table.draw();
                payment_history.draw();
                recharge_history.draw();
                ir_anncmnt.draw();
            }
        });

        // init date time picker
        $('#enddate').datetimepicker({
            format: 'd/m/Y',
            timepicker: false,
            closeOnDateSelect: true,
            onChangeDateTime: function (ct, $i) {
                usage_table.draw();
                payment_history.draw();
                recharge_history.draw();
                ir_anncmnt.draw();
            }
        });

        // Payment options
        $("#payexisting").on("ifChecked", function () {
            $(".card-list").show();
        });
        $("#paynew").on("ifChecked", function () {
            $(".card-list").hide();
        });

        //duScrollToTop();

        // Matchheight misc
        $('.login-register .col-md-6').matchHeight();
        $('.login-register2 .col-md-6').matchHeight();
        $('.modProduct .items .item .title').matchHeight();
        $('.modEditorial .items .item .title, .modEditorial .items .item .intro').matchHeight();
        $('.features-container .feature .text').matchHeight();
        $('.modify-plans .plan-content > div > div').matchHeight();
        $(".scrollable .carousel .txt").matchHeight(!1);
        $(".scrollable .detail .dl li").matchHeight(!1);
        $(".scrollable .items .item .txt .title").matchHeight();
        $(".scrollable .items .item .txt .intro").matchHeight();

        ["ucAutoLister", "devices-list"].forEach(function(n) {
            if ($(".modEditorial.divider.productTiles." + n).length > 0) {
                var t = function() {
                    $("." + n + ".modEditorial.divider.productTiles > .itemsW > .items > .item:visible > .txt > .title").matchHeight();
                    $("." + n + ".modEditorial.divider.productTiles > .itemsW > .items > .item:visible > .txt > .intro").matchHeight()
                };
                t();
                window.pubsub.subscribe("isotope/layoutComplete", t)
            }
        });

        // autopayment options
        $('.autopayment-options input[type=radio]').on('ifChanged', function () {
            //alert('hey');
            $('button[type=submit]').removeAttr('disabled');
        });

        // init huzefa tabs
        $(".tabs > li").on("click touchstart", function (event) {
            if ($(this).hasClass('active')) return false;
            if ($('.enterprise-services').length) {
                if ($('.your-cart:visible').length) {
                    if ($('#warning').length) {
                        $('#warning').reveal('show');
                        return;
                    }
                }
                $('.your-cart, .check-out, .upload-documents').slideUp('fast');
            }
            event.preventDefault();
            var $this = $(event.target).closest("li");
            $(".tabs > li").removeClass("active");
            $this.addClass("active");
            $(".tabs-content > div").removeClass("active");
            $($this.find("a").attr("href")).addClass("active");
        });

        //Run icheck plugin
        /*$('input:not(.ios-switch)').iCheck({
         checkboxClass: 'icheckbox_minimal',
         radioClass: 'iradio_minimal'
         });*/


        /*
         * isoTope functions using require
         * @param: void
         * @return: void
         *
         */

        /*require(["jquery", "jquery.isotope.min"], function ($, Isotope) {
            $(function () {
                require(['jquery-bridget.min'],
                    function (jQueryBridget) {

                        var positionFunc = Isotope.prototype._positionItem;
                        Isotope.prototype._positionItem = function (item, x, y, isInstant) {
                            // ignore actual isInstant value, pass in `true` to the original function;
                            positionFunc(item, x, y, true);
                        };
                        Isotope.Item.prototype._create = function () {
                            // assign id, used for original-order sorting
                            this.id = this.layout.itemGUID++;
                            // transition objects
                            this._transn = {
                                ingProperties: {},
                                clean: {},
                                onEnd: {}
                            };
                            this.sortData = {};
                        };
                        Isotope.Item.prototype.layoutPosition = function () {
                            this.emitEvent('layout', [this]);
                        };

                        Isotope.prototype.arrange = function (opts) {
                            // set any options pass
                            this.option(opts);
                            this._getIsInstant();
                            // just filter
                            this.filteredItems = this._filter(this.items);
                            // flag for initalized
                            this._isLayoutInited = true;
                        };

                        // layout mode that does not position items
                        var NoneMode = Isotope.LayoutMode.create('none');
                    })
            });
        });*/


        /*Isotope.Item.prototype._create = function() {
            // assign id, used for original-order sorting
            this.id = this.layout.itemGUID++;
            // transition objects
            this._transn = {
                ingProperties: {},
                clean: {},
                onEnd: {}
            };
            this.sortData = {};
        };

        Isotope.Item.prototype.layoutPosition = function() {
            this.emitEvent( 'layout', [ this ] );
        };

        Isotope.prototype.arrange = function( opts ) {
            // set any options pass
            this.option( opts );
            this._getIsInstant();
            // just filter
            this.filteredItems = this._filter( this.items );
            // flag for initalized
            this._isLayoutInited = true;
        };

        // layout mode that does not position items
        Isotope.LayoutMode.create('none');*/


        

        if ($('.isotope').length > 0) {

            //$.bridget('isotope', Isotope);
            // init Isotope
            var $container = $('.isotope').isotope({
                itemSelector: '.item, .card-item',
                layoutMode: 'fitRows',
                sortAscending: true,
                isOriginLeft: isLTR
            });

        } else {
            //$.bridget('isotope', Isotope);
            // init Isotope
            var $container = $('.isotope-plans').isotope({
                transformsEnabled: transformsEnabledVar,
                itemSelector: '.dItem',
                layoutMode: 'none',
                sortAscending: true,
                transitionDuration: 0,
                isOriginLeft: isLTR
            });
            $container = $('.isotope-plans').isotope({
                filter: '.m24,.minPacks'
            });
        }


        // filter functions
        var filterFns = {
            // show if number is greater than 50
            numberGreaterThan50: function () {
                var number = $(this).find('.number').text();
                return parseInt(number, 10) > 50;
            },
            // show if name ends with -ium
            ium: function () {
                var name = $(this).find('.name').text();
                return name.match(/ium$/);
            }
        };

        // bind filter button click
        $('.brandFilter').on('click', 'a', function () {
            var filterValue = $(this).attr('data-filter');
            // use filterFn if matches value
            filterValue = filterFns[filterValue] || filterValue;
            $container.isotope({filter: filterValue});
            return false;
        });

        // bind filter button click
        $('.lined-buttons, .plan-filters, .plan-sub-filters').each(function () {
            var $this = $(this);
            $(this).on('click', 'a', function () {
                var filterValue = $(this).attr('data-filter');
                //filterValue += $('.lined-buttons').not($this).find('a.is-checked').attr('data-filter');
                console.log(filterValue);
                //alert(filterValue);
                // use filterFn if matches value
                filterValue = filterFns[filterValue] || filterValue;
                $container.isotope({filter: filterValue});
                return false;
            });
        });

        /*if (!dataSlider == null) {
         // bind range filter change
         requirejs(["nouislider.min"], function (noUiSlider) {
         $(function () {
         dataSlider.noUiSlider.on('update', function () {
         var filterValue;
         var $count = parseInt(dataSlider.noUiSlider.get());

         for (i = 1; i <= $count; i++) {
         if (i == $count) filterValue += '.data' + i;
         else filterValue += '.data' + i + ', ';
         }

         // use filterFn if matches value
         filterValue = filterFns[filterValue] || filterValue;
         console.log(filterValue);
         $container.isotope({filter: filterValue});
         });
         })
         })
         }*/

        //if (!contractSlider == null) {
        // bind range filter change
        /*requirejs(["nouislider.min"], function (noUiSlider) {
         $(function () {
         contractSlider.noUiSlider.on('update', function () {
         var filterValue;
         var $count = parseInt(contractSlider.noUiSlider.get());

         //alert($count);

         if($count == 0) filterValue = '.light';
         else if($count == 12) filterValue = '.medium';
         else if ($count == 24) filterValue = '.heavy';

         console.log(filterValue);
         $container.isotope({filter: filterValue});
         });

         })
         })*/
        //}

        // bind sort button click
        $('.sorts').on('click', 'li a', function () {
            var sortByValue = $(this).attr('data-sort-by');
            if (sortByValue == 'price-high' || sortByValue == 'new') {
                $container.isotope({
                    sortBy: sortByValue,
                    sortAscending: true
                });
            } else {
                $container.isotope({
                    sortBy: sortByValue,
                    sortAscending: false
                });
            }
            return false;
        });

        // change is-checked class on buttons
        $('.brandFilter, .btn-group').each(function (i, buttonGroup) {
            var $buttonGroup = $(buttonGroup);
            $buttonGroup.on('click', '> a, > button', function () {
                $('.dropdown-menu', $(this).parent()).hide();
                $buttonGroup.find('.is-checked, .active')
                    .removeClass('is-checked')
                    .removeClass('active')
                    .removeClass('btn-primary')
                    .addClass('btn-default');
                $(this).addClass('is-checked active');
            });
        });

        $('.dropdown-toggle').click(function () {
            if ($(this).hasClass('is-checked')) {
                $('.dropdown-menu', $(this).parent()).hide();
                $(this).removeClass('is-checked');
                $('.brandFilter a:first').trigger('click');
                return false;
            } else {
                $('.dropdown-menu', $(this).parent()).show();
                $('.brandFilter').find('.is-checked').removeClass('is-checked');
                $(this).addClass('is-checked');
                return false;
            }
        });

        // brand selection change for mobile
        $('.brand-filter').change(function () {
            var optionSelected = $(this).find("option:selected");
            var filterValue = optionSelected.attr('data-filter');
            // use filterFn if matches value
            filterValue = filterFns[filterValue] || filterValue;
            $container.isotope({
                filter: filterValue
            });
            return false;
        });


        //Code to make self contained isotope
        $('[data-isotope]').each(function () {

            var $table = $(this);
            var $searchBtnsContainer = $($table.data('filterSelector'));

            //making that table isotope to be filerable
            $table.isotope({
                itemSelector: $table.data('itemSelector'),
                layoutMode: 'none',
                sortAscending: true,
                isOriginLeft: true,
                transitionDuration: 0
            });

            //filter function takes the anchor instance as param
            function filterSelectedVal(that) {
                $(that).siblings().not($(that)).removeClass('is-checked');
                $(that).addClass('is-checked');

                var filterValue = $(that).attr('data-filter');
                // use filterFn if matches value
                filterValue = filterFns[filterValue] || filterValue;
                $table.isotope({
                    filter: filterValue
                });
                return false;
            }

            //filter when click on button
            $searchBtnsContainer.find('a').click(function (e) {
                e.preventDefault();
                filterSelectedVal(this)

            });

            //to filter by deafult with the selected value
            $searchBtnsContainer.find('a.is-checked').length ? filterSelectedVal($searchBtnsContainer.find('a.is-checked').first()) : '';
        })

        /*$('.modTiles').each(function(){

         var $container = $('.modTiles'),
         isTouch = $('body').hasClass('touch');
         if ($container.length > 0) {
         if (($container.find('.tilesFilters').length > 0) && isTouch) {
         // Show/Hide filters on mobile devices
         $container.find('a.filtersTitle').unbind('click').bind(
         'click',
         function (e) {
         e.preventDefault();
         $(this).parent().toggleClass('visible');
         }
         );
         }
         // Enable filtering
         $container.find('a.subA').unbind('click').bind(
         'click',
         function () {
         // Hide dropdown
         if (isTouch) $container.find('.filters').removeClass('visible');
         // Filter list
         $container.find('a.subA.selected').removeClass("selected");
         $(this).addClass("selected");
         var selector = $(this).attr('data-filter');
         require([
         'jquery.isotope.min'
         ], function (Isotope) {
         var iso = new Isotope($container.find('.items'), {filter: selector});
         });
         //$container.find('.items').isotope({ filter: selector });
         return false;
         }
         );
         }

         $('.modTiles .tilesFilters .sorting li a').on('click', function () {
         $('.modTiles .sorting li a.selected').removeClass("selected");
         $(this).addClass("selected");

         var selector = $(this).data('sorting');
         var sortAscending = $(this).data('sortAscending');
         require([
         'jquery.isotope.min'
         ], function (Isotope) {
         var iso = new Isotope('.modTiles .items', {
         sortBy: selector,
         sortAscending: sortAscending
         });
         });
         //$('.modTiles .items').isotope({ sortBy: selector, sortAscending: sortAscending });
         return false;
         });*/


        if ($('.plan-cards').length > 0) {
            var $container = $('.isotope').isotope({
                itemSelector: '.card-item',
                layoutMode: 'fitRows',
                getSortData: {
                    dataGB: '.dataSize parseInt'
                },
                sortBy: 'dataGB',
                sortAscending: true
            });
        }

        // init plans table filter on pageload
        setTimeout(function () {
            $('.plans-master > .brandFilter > a:eq(0)').trigger('click');
            //alert($('.sticky-content .lined-buttons a:eq(0)').text());

            $('.sticky-content .lined-buttons:first a:eq(0)').trigger('click');

            /*$('.sticky-content .lined-buttons').each(function(){
             $('a:eq(0)', $(this)).trigger('click');
             })*/
        }, 50);


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * dropzone using require
         * @param: void
         * @return: void
         *
         */
        requirejs(["prototype/dropzone"], function (Dropzone) {
            $(function () {
                Dropzone.autoDiscover = false;

                $('.dropzone').each(function () {
				var myDropZone = $(this);
				var elements = [myDropZone[0]];
				if (myDropZone.attr('dz-btn'))
					elements.push(myDropZone.parent().parent().find(myDropZone.attr('dz-btn'))[0]);
				myDropZone.dropzone({
					//url: "../fileupload.aspx",
					clickable: elements,
					thumbnail: false,
					maxFiles: ($(myDropZone).hasClass('multiple')) ? 10 : 1,
					init: function () {
					this.on("addedfile", function () {
						if ($(myDropZone).hasClass('multiple')) return;
						if (this.files[1] != null) {
							this.removeFile(this.files[0]);
						}
					})
					this.on('processing', function () {
						if (($('.list-table li:visible').length - 1) == $('.list-table li:visible .dz-processing').length) {
							$('.submit-docs').removeAttr('disabled');
						}
					})
					this.on('success', function () {
						if (($('.list-table li:visible').length - 1) == $('.list-table li:visible .dz-success').length) {
							$('.submit-docs').removeAttr('disabled');
						}
					})
					this.on("removedfile", function () {
						if (($('.list-table li:visible').length - 1) != $('.list-table li:visible .dz-processing').length) {
							$('.details-form, .confirm').slideUp('fast');
							$('.submit-docs').attr('disabled', 'disabled');
						}
					})
				}

				})
			});


                // Fixed service request
                $("[id*='uploader']").dropzone({
                    //url: "../fileupload.aspx",
                    addRemoveLinks: true,
                    maxFiles: 1/*,
                     maxfilesexceeded: function(file) {
                     alert('no more files');
                     this.removeAllFiles();
                     this.addFile(file);
                     }*/,
                    init: function () {
                        this.on("addedfile", function () {
                            if (this.files[1] != null) {
                                this.removeFile(this.files[0]);
                            }
                        });
                        /*this.on("maxfilesexceeded", function (file) {
                         this.removeAllFiles();
                         this.addFile(file);
                         });*/
                    },
                    thumbnail: false,
                    accept: function (file) {
                        //alert('');
                        $('.submit-docs').removeAttr('disabled');
                    },
                    success: function (file, response) {
                        var imgName = response;
                        file.previewElement.classList.add("dz-success");
                        console.log("Successfully uploaded :" + imgName);
                    },
                    error: function (file, response) {
                        file.previewElement.classList.add("dz-error");
                    }
                });

                /*$('.dropzone').each(function () {
                    var myDropZone = $(this);
                    var elements = [myDropZone[0]];
                    if (myDropZone.attr('dz-btn'))
                        elements.push(myDropZone.parent().parent().find(myDropZone.attr('dz-btn'))[0]);
                    myDropZone.dropzone({
                        url: null,
                        //addRemoveLinks: true,
                        clickable: elements,
                        maxFiles: 1,
                        maxFilesize: 3,
                        /!*maxfilesexceeded: function(file) {
                         alert('no more files');
                         this.removeAllFiles();
                         this.addFile(file);
                         },*!/
                        thumbnail: false,
                        accept: function (file) {
                            //alert('');
                            $('.submit-docs').removeAttr('disabled');
                        },
                        init: function () {
                            this.on("addedfile", function () {
                                if (this.files[1] != null) {
                                    this.removeFile(this.files[0]);
                                }
                            });
                            /!*this.on("maxfilesexceeded", function (file) {
                             this.removeAllFiles();
                             this.addFile(file);
                             });*!/
                        },
                        dictDefaultMessage: 'None uploaded',
                        success: function (file, response) {
                            var imgName = response;
                            file.previewElement.classList.add("dz-success");
                            console.log("Successfully uploaded :" + imgName);
                        },
                        error: function (file, response) {
                            file.previewElement.classList.add("dz-error");
                        }
                    })
                });*/
            })
        });


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        // documents rejected page
        $('[data-slide-choosen]').each(function () {
            var $container = $(this);
            var $sectionsContainer = $container.parent().parent().find($container.data('slideChoosen'));
            // debugger;
            var $btns = $container.find('.btn');
            var btnsValues = [];
            var $btnChecked = $btns.closest('.is-checked');
            var selectedValue = $btnChecked.data('filer');
            var $currentBtn;
            $btns.each(function () {
                return btnsValues.push($(this).data('filter'))
            });

            $sectionsContainer.find(btnsValues.toString()).slideUp();
            $sectionsContainer.find(selectedValue).slideDown();

            $btns.click(function (e) {

                if ($(this).data('filter') == selectedValue)
                    return false;

                e.preventDefault();
                $currentBtn = $(this);
                selectedValue = $currentBtn.data('filter');


                $sectionsContainer.find(btnsValues.toString()).slideUp();
                $sectionsContainer.find(selectedValue).slideDown();


                $btns.removeClass('is-checked');
                $currentBtn.addClass('is-checked');

            })


            $($(this).find('.btn.is-checked').data('filter')).slideDown();
        })


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        // init faqs accordion
        var allPanels = $('.faqs > ul div').hide();
        $('.faqs > ul > li h3').hammer({'prevent_default': true}).on({
            'tap': function (e) {
                e.preventDefault();
                if ($(this).parent().find('div:visible').length) return;
                allPanels.slideUp(50);
                $('.faqs > ul > li').removeClass('selected');
                $(this).parent().addClass('selected');
                $(this).parent().find('div').slideDown(100);

                return false;
            }
        });


        /* ************** */
        /* payments_bills */
        /* ************** */

        setTimeout(function () {
            //set Graph
            var paymentBillObj = $(".graph");
            paymentBillObj.html("");
            var charter = paymentBillObj.protoCharter({
                height: 400,
                type: "bar"
            });
            charter.attachData([{
                "label": "Jan",
                "value": 300
            }, {
                "label": "Feb",
                "value": 500
            }, {
                "label": "Mar",
                "value": 490
            }, {
                "label": "Apr",
                "value": 630
            }, {
                "label": "May",
                "value": 476
            }, {
                "label": "Jun",
                "value": 676,
                unbilled: true
            }]);
            charter.draw();

            $('.overhead-details .spinner, .balance .spinner, .price .spinner, .loader-wrapper').remove();
            $('.overhead-details h2, .balance span, #graphLink, .price .value, .modDials .itemsW').removeClass('hidden');
            $('.modDials .alert').addClass('active');
        }, 2000);

        // init graph
        showGraph();


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        // allow only numbers in input box
        $('input.number').on("keypress", function (evt) {

            var keyCode = window.event ? evt.keyCode : evt.which;
            //codes for 0-9
            if (keyCode < 48 || keyCode > 57) {
                //codes for backspace, delete, enter
                if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !evt.ctrlKey) {
                    evt.preventDefault();
                }
            }

            //this.value = this.value.replace(/[^0-9]/g, '');
        });

        // maximum 6 numbers in POBox field
        $('#billingInfo input.number').on("keyup", function (evt) {
            var charLimit = 6;

            if ($(this).val().length >= charLimit) {
                $(this).val($(this).val().substr(0, charLimit));
            }
        });

        /* jQuery.getJSON("../common/content/emirates.json",function(myJson){

         //alert('');

         //populate select with json
         $('#billingInfo select').selectric('destroy');

         console.log(myJson);

         $.each(myJson.country, function (index, value) {
         $("#emirate").append('<option '+value.selected+' value="'+value.id+'">'+value.name+'</option>');
         //console.log(value.name);
         });

         $('#billingInfo select').selectric('init');

         //populate 2nd select with event onchange
         $('#emirate').on('change', function(){
         console.log($(this).val());

         $('#billingInfo select').selectric('destroy');
         $("#city").empty();

         for(var i = 0; i < myJson.country.length; i++)
         {
         if(myJson.country[i].id == $(this).val())
         {
         $.each(myJson.country[i].states, function (index, value) {
         $("#city").append('<option '+ value.selected +' value="'+value.id+'">'+value.name+'</option>');
         });
         }
         }

         $('#billingInfo select').selectric('init');
         });

         //trigger for populating 2nd select at beginning
         $('#emirate').trigger("change");

         }); */

        //delete spaces
        /*$('.cc-payment #friendName').on("keypress", function (evt) {

         var keyCode = window.event ? evt.keyCode : evt.which;

         if (keyCode == 32) {
         evt.preventDefault();
         }

         });*/

        $('.features-container .feature .text').matchHeight();

        // Remove date of birth if enterprise customer and remove 00 from year in consumer
        if ($('input[name="customerType"]').val() != 'Consumer') {
            $('input[name="dateOfBirth"]').parent().remove();
        }
        else if ($('input[name="dateOfBirth"]').val() != undefined) {
            var $date = $('input[name="dateOfBirth"]').val().replace('00', '');
            $('input[name="dateOfBirth"]').val($date);
        }
        ;


        // add class to plans in grid view
        $('.plan-grid-view').each(function (i) {
            $('li', $(this)).each(function (j) {
                $(this).addClass('item' + j);
            })
        });
        setTimeout(function () {
            $('.plan-grid-view li').each(function (i) {
                $('.item' + i).matchHeight({
                    byRow: true
                });
            });
        }, 100);


        /*
         * Coverage map
         */
        if (false && $('#coverageMap').length > 0) {
            //$("select").selectric();

            /*function initializeCoverageMap() {
             // Create the map.
             var mapOptions = {
             zoom: 4,
             center: new google.maps.LatLng(37.09024, -95.712891),
             mapTypeId: google.maps.MapTypeId.TERRAIN
             };
             var map = new google.maps.Map(document.getElementById('mapCoverageMap'),
             mapOptions);

             // Construct the circle for each value in citymap.
             // Note: We scale the area of the circle based on the population.
             for (var city in citymap) {
             var populationOptions = {
             strokeColor: '#FF0000',
             strokeOpacity: 0.75,
             strokeWeight: 0,
             fillColor: citymap[city].background,
             fillOpacity: 0.35,
             map: map,
             center: citymap[city].center,
             radius: Math.sqrt(citymap[city].population) * 100
             };
             // Add the circle for this city to the map.
             cityCircle = new google.maps.Circle(populationOptions);
             }
             }

             var citymap = {};

             citymap['chicago'] = {
             center: new google.maps.LatLng(41.878113, -87.629798),
             population: 2714856,
             background: "#D0A420"
             };
             citymap['newyork'] = {
             center: new google.maps.LatLng(40.714352, -74.005973),
             population: 8405837,
             background: "#5FD0AA"
             };
             citymap['losangeles'] = {
             center: new google.maps.LatLng(34.052234, -118.243684),
             population: 3857799,
             background: "#5DA2FF"
             };
             citymap['vancouver'] = {
             center: new google.maps.LatLng(49.25, -123.1),
             population: 603502,
             background: "#FF7BF0"
             };

             google.maps.event.addDomListener(window, 'load', initializeCoverageMap());

             var cityCircle; */

            var circles = [];

            function initializeCoverageMap() {
                clearMarkers();
                // Create the map.
                var mapOptions = {
                    zoom: 8,
                    center: new google.maps.LatLng(25.065707, 55.310174),
                    mapTypeId: google.maps.MapTypeId.TERRAIN
                };

                var map = new google.maps.Map(document.getElementById('mapCoverageMap'),
                    mapOptions);

                // Construct the circle for each value in citymap.
                // Note: We scale the area of the circle based on the population.
                var types = $(".btn-toggle.active").map(function () {
                    return $(this).data("type")
                }).get().join(",");

                var emirate = $("#emirate").val();
                var city = $("#city").val();
                var area = $("#area").val();

                for (var i = 0, len = citymap.length; i < len; i++) {
                    var city = citymap[i];

                    if (city.key != emirate && emirate != '') {
                        continue;
                    }
                    if (types && types.indexOf(city.type) == -1) {
                        continue;
                    }
                    var populationOptions = {
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.75,
                        strokeWeight: 0,
                        fillColor: city.background,
                        fillOpacity: 0.35,
                        map: map,
                        center: city.center,
                        radius: Math.sqrt(city.population) * 50
                    };
                    // Add the circle for this city to the map.
                    var cityCircle = new google.maps.Circle(populationOptions);
                    circles.push(cityCircle);
                }
            }

            var citymap = [{
                key: 'dubai',
                center: new google.maps.LatLng(25.2048, 55.2708),
                population: 2714856,
                background: "#0099cc",
                type: 'edge'
            }, {
                key: 'dubai',
                center: new google.maps.LatLng(25.2048, 55.2708),
                population: 2714856,
                background: "#66CC33",
                type: 'lte'
            }, {
                key: 'abu-dhabi',
                center: new google.maps.LatLng(24.4667, 54.3667),
                population: 8405837,
                background: "#66CC33",
                type: 'lte'
            }, {
                key: 'abu-dhabi',
                center: new google.maps.LatLng(25.3575, 55.3908),
                population: 3857799,
                background: "#0099cc",
                type: 'edge'
            }];

            $("select").on("change", function () {
                initializeCoverageMap();
            });

            google.maps.event.addDomListener(window, 'load', initializeCoverageMap());

            function clearMarkers() {
                for (var i = 0; i < circles.length; i++) {
                    circles[i].setMap(null);
                }
                circles = [];
            }

            $('.btn-toggle').click(function (e) {
                e.preventDefault();
                $(this).toggleClass("active");
                initializeCoverageMap();
            });
        }


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         *  Device details page
         * */

        $('[toggle-active]').each(function () {
            var $container = $(this);
            var $btns = $($container.attr('toggle-active'));
            $btns.click(function (e) {
                e.preventDefault();
                var $btn = $(this);
                if (!$btn.attr('disabled')) {
                    $btns.removeClass('active');
                    $btn.addClass('active');
                }
            })
        })

        $('.select-alt').each(function () {
            var cont = $(this);
            var btns = cont.find('.btn');
            btns.click(function () {
                btns.parent().find('.active').text('Select').removeClass('active btn-primary').addClass('btn-default');
                $(this).text('Selected').removeClass('btn-default').addClass('btn-primary active');
            })
        });

        $('.color-alt').click(function () {
            $('.color-alt').removeClass('selected');
            $(this).addClass('selected')
        });


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * our stores page
         */

        if ($('#ourStores').length > 0) {

            function bindInfoWindowOurStores(marker, map, headerTitle, topBoxTitle, topBoxSubtitle, bottomBoxTitle, bottomBoxSubtitle) {
                google.maps.event.addListener(marker, 'click', function () {

                    var contentString =
                        '<div id="content">' +
                        '<div class="header">' + headerTitle + '</div>' +
                        '<div class="topBlock">' +
                        '<span class="title">' + topBoxTitle + '</span>' +
                        '<span class="subtitle">' + topBoxSubtitle + '</span>' +
                        '</div>' +
                        '<div class="topBottom">' +
                        '<span class="title">' + bottomBoxTitle + '</span>' +
                        '<span class="subtitle">' + bottomBoxSubtitle + '</span>' +
                        '</div>' +
                        '<a href="#" class="buttonMap"/>Get Directions' +
                        '</div>';

                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });

                    infowindow.open(map, marker);

                    // Event that closes the Info Window with a click on the map
                    google.maps.event.addListener(map, 'click', function () {
                        infowindow.close();
                    });

                    google.maps.event.addListener(infowindow, 'domready', function () {

                        // Reference to the DIV that wraps the bottom of infowindow
                        var iwOuter = $('.gm-style-iw');

                        var iwBackground = iwOuter.prev();

                        //remove background white
                        iwBackground.children(':nth-child(2)').css({
                            'display': 'none'
                        });
                        iwBackground.children(':nth-child(4)').css({
                            'display': 'none'
                        });

                        var innerEl = iwOuter.eq(1);
                        innerEl.children(':nth-child(1)').addClass("innerMap");

                        // Apply the desired effect to the close button
                        var iwCloseBtn = innerEl.next();
                        iwCloseBtn.css({
                            'border-radius': '13px',
                            'top': '25px',
                            'right': '60px'
                        });
                    });

                });
            }

            var markers = [];

            var locations = null;

            function initializeOurStores() {
                $.getJSON("../common/content/newStores.json", function (data) {
                    locations = data;
                    filterStores();
                });
            }

            function filterStores() {

                clearMarkers();
                var mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng(25.065707, 55.310174),
                    mapTypeId: google.maps.MapTypeId.TERRAIN
                };
                var map = new google.maps.Map(document.getElementById('mapOurStores'), mapOptions);

                var data = locations;
                var types = $(".btn-toggle.active").map(function () {
                    return $(this).data("type")
                }).get().join(",");

                var emirate = $("#emirate").val();
                var city = $("#city").val();
                var area = $("#area").val();
                var service = $("#service").val();

                for (var i = 0, len = data.markers.length; i < len; i++) {
                    var mark = data.markers[i];
                    if (mark.emirate != emirate && emirate != '') {
                        continue;
                    }
                    if (mark.city != city && city != '') {
                        continue;
                    }
                    if (mark.area != area && area != '') {
                        continue;
                    }
                    if (mark.service != service && service != '') {
                        continue;
                    }
                    if (types && types.indexOf(mark.type) == -1) {
                        continue;
                    }
                    //retrieve json data
                    var latitude = data.markers[i].latitude;
                    var longitude = data.markers[i].longitude;
                    var headerTitle = data.markers[i].headerTitle;
                    var topBoxTitle = data.markers[i].boxTop[0].title;
                    var topBoxSubtitle = data.markers[i].boxTop[0].subtitle;
                    var bottomBoxTitle = data.markers[i].boxBottom[0].title;
                    var bottomBoxSubtitle = data.markers[i].boxBottom[0].subtitle;

                    // Creating a marker and putting it on the map
                    var marker = new google.maps.Marker({
                        map: map,
                        title: headerTitle,
                        position: new google.maps.LatLng(latitude, longitude)
                    });
                    markers.push(marker);

                    bindInfoWindowOurStores(marker, map, headerTitle, topBoxTitle, topBoxSubtitle, bottomBoxTitle, bottomBoxSubtitle);

                }
            }

            $("#ourStores .areaShop").on("click", ".tabMenu", function () {

                var object = $(this).closest(".areaShop");

                object.find(".tabMenu").removeClass("selected");
                object.find(".switchObj").hide();

                if ($(this).hasClass("map")) {
                    $(this).addClass("selected");
                    object.find(".map").show();
                } else if ($(this).hasClass("list")) {
                    $(this).addClass("selected");
                    object.find(".openList").show();
                }

                return false;
            });

            google.maps.event.addDomListener(window, 'load', initializeOurStores());

            var markers = [];

            $("select").on("change", function () {
                filterStores();
            });

            $('.btn-toggle').click(function (e) {
                e.preventDefault();
                $(this).toggleClass("active")
                filterStores();
            });

            function clearMarkers() {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(null);
                }
                markers = [];
            }

            $('.return-to-map').click(function () {
                $('.tabMenu.map').click();
            });

        }

        /*if (false && $('#ourStores').length > 0) {

         //$("select").selectric();

         function bindInfoWindowOurStores(marker, map, headerTitle, topBoxTitle, topBoxSubtitle, bottomBoxTitle, bottomBoxSubtitle) {
         google.maps.event.addListener(marker, 'click', function () {

         var contentString =
         '<div id="content">' +
         '<div class="header">' + headerTitle + '</div>' +
         '<div class="topBlock">' +
         '<span class="title">' + topBoxTitle + '</span>' +
         '<span class="subtitle">' + topBoxSubtitle + '</span>' +
         '</div>' +
         '<div class="topBottom">' +
         '<span class="title">' + bottomBoxTitle + '</span>' +
         '<span class="subtitle">' + bottomBoxSubtitle + '</span>' +
         '</div>' +
         '<a href="#" class="buttonMap"/>Get Directions' +
         '</div>';

         var infowindow = new google.maps.InfoWindow({
         content: contentString
         });

         infowindow.open(map, marker);

         // Event that closes the Info Window with a click on the map
         google.maps.event.addListener(map, 'click', function () {
         infowindow.close();
         });

         google.maps.event.addListener(infowindow, 'domready', function () {

         // Reference to the DIV that wraps the bottom of infowindow
         var iwOuter = $('.gm-style-iw');

         var iwBackground = iwOuter.prev();

         //remove background white
         iwBackground.children(':nth-child(2)').css({
         'display': 'none'
         });
         iwBackground.children(':nth-child(4)').css({
         'display': 'none'
         });

         var innerEl = iwOuter.eq(1);
         innerEl.children(':nth-child(1)').addClass("innerMap");

         // Apply the desired effect to the close button
         var iwCloseBtn = innerEl.next();
         iwCloseBtn.css({
         'border-radius': '13px',
         'top': '25px',
         'right': '60px'
         });
         });

         });
         }

         function initializeOurStores() {

         $.getJSON("../common/content/ourStores.json", function (data) {
         var latLng = new google.maps.LatLng(data.pos[0].latitude, data.pos[0].longitude);

         var mapOptions = {
         zoom: 4,
         center: latLng
         };

         var map = new google.maps.Map(document.getElementById('mapOurStores'), mapOptions);


         for (var i = 0, len = data.markers.length; i < len; i++) {

         //retrieve json data
         var latitude = data.markers[i].latitude;
         var longitude = data.markers[i].longitude;
         var headerTitle = data.markers[i].headerTitle;
         var topBoxTitle = data.markers[i].boxTop[0].title;
         var topBoxSubtitle = data.markers[i].boxTop[0].subtitle;
         var bottomBoxTitle = data.markers[i].boxBottom[0].title;
         var bottomBoxSubtitle = data.markers[i].boxBottom[0].subtitle;

         // Creating a marker and putting it on the map
         var marker = new google.maps.Marker({
         map: map,
         title: headerTitle,
         position: new google.maps.LatLng(latitude, longitude)
         });

         bindInfoWindowOurStores(marker, map, headerTitle, topBoxTitle, topBoxSubtitle, bottomBoxTitle, bottomBoxSubtitle);

         }
         });
         }

         $("#ourStores .areaShop").on("click", ".tabMenu", function () {

         var object = $(this).closest(".areaShop");

         object.find(".tabMenu").removeClass("selected");
         object.find(".switchObj").hide();

         if ($(this).hasClass("map")) {
         $(this).addClass("selected");
         object.find(".map").show();
         } else if ($(this).hasClass("list")) {
         $(this).addClass("selected");
         object.find(".openList").show();
         }

         return false;
         });

         google.maps.event.addDomListener(window, 'load', initializeOurStores());
         }*/


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * Roaming rates page
         */

        if ($('#roamingRates').length > 0) {
            var buildRatesResultTalble = function(result) {
                //rates result table
                var setCurrencyForDecimal = function (param) {
                    if (typeof(param) == "number") {
                        if (param == 0) {
                            return window.location.href.indexOf("/ar/")<0 ? "Free" : "مجانا";
                        }
                        return param +' ' + countryRoamingRes.Currency;
                    }
                    return param;
                }
                var htmlArray = [];
                htmlArray.push("<table>");
                for (var name in result) {
                    if (name == "CountryCode"
                        || name == "CountryName"
                        || name == "Zone"
                        || name == "UpdatedOn"
                        || name == "Logo"
                        || name == "PlanType"
                        || name == "RoamingOperaters") {
                        continue;
                    }

                    htmlArray.push('<tr><td class="font-bold">');
                    htmlArray.push(countryRoamingRes[name]);
                    htmlArray.push("</td><td>");
                    htmlArray.push(setCurrencyForDecimal(result[name]));
                    htmlArray.push("</td></tr>");
                }
                htmlArray.push("</table>");

                $("#divRatesResult").empty().html(htmlArray.join(""));
            }

            var buildOperatersResultTable = function(result) {
                if (result.RoamingOperaters && result.RoamingOperaters.length) {
                    //Operater Name
                    htmlArray = [];
                    htmlArray.push('<table class="rr-table"><tr class="font-bold"><th class="first-col">');
                    htmlArray.push(operatorRoamingRes.Operaters);
                    htmlArray.push("</th>");
                    $.each(result.RoamingOperaters, function () {
                        htmlArray.push('<th>');
                        htmlArray.push(this.Name);
                        htmlArray.push("</th>");
                        firstCol = false;
                    });
                    htmlArray.push("</tr>");

                    var getBoolTypeImgTag = function (param) {
                        if (typeof(param) == "boolean") {
                            return param ? '<img border="0" src="../common/images/yes.gif">' : '<img border="0" src="../common/images/no.gif">'
                        }
                        return param === "" ? '<img border="0" src="../common/images/no.gif">' : param;
                    }
                    for (var name in result.RoamingOperaters[0]) {
                        if (name == "Name"
                            || name == "RoamingPartnerName"
                            || name == "NetworkCode") {
                            continue;
                        }
                        htmlArray.push('<tr><td class="first-col">');
                        htmlArray.push(operatorRoamingRes[name]);
                        htmlArray.push("</td>");
                        $.each(result.RoamingOperaters, function () {
                            htmlArray.push("<td>")
                            htmlArray.push(getBoolTypeImgTag(this[name]));
                            htmlArray.push("</td>")
                        });
                        htmlArray.push("</tr>");

                        //RateAdditionalIncomingCallCharges
                        //Postpaid
                        if (result.PlanType == 0 && name == "VoiceAndSMSMonthly") {
                            var additionalChargeFieldName = "RateAdditionalIncomingCallChargesMonthly";
                            htmlArray.push('<tr><td class="first-col">');
                            htmlArray.push(countryRoamingRes[additionalChargeFieldName]);
                            htmlArray.push("</td>");
                            for (var i = 0; i < result.RoamingOperaters.length; i++) {
                                htmlArray.push('<td>');
                                htmlArray.push(result[additionalChargeFieldName]);
                                htmlArray.push("</td>");
                            }
                        }
                    }

                    
                    htmlArray.push("</tr></table>");
                    $("#hRoamingOperaters").removeClass("hide");
                    $("#divOperatorsResult").empty().html(htmlArray.join("")).removeClass("hide");
                }
                else {
                    $("#hRoamingOperaters").addClass("hide");
                    $("#divOperatorsResult").addClass("hide");
                }
            }
            var trackingForGTM = function() {
                try {
                    dataLayer.push({
                        'plan-type': $(".plan-type.current").val().toLowerCase(), // 'postpaid' or 'prepaid' (always in english) 
                        'selected-country': $("#cbCountries option:checked").attr("data-ref"),
                        'language': window.location.href.toLowerCase().indexOf("/ar/") != -1 ? "AR" : "EN"
                    });
                }
                catch (err) {
                    // do nothing 
                }
            }
            var refleshRoamingRate = function() {
                var refreshRomaingRateOnSuccess = function (result) {
                    if (result) {
                        $("#divFilterSummary").removeClass("hide");
                        $("#divCountrySummary").removeClass("hide");
                        $("#divFilterSummary span:last").text("'" + $("#cbCountries option:selected").text() + "'");
                        $("#divFilterSummary span:first").text("'" + $(".plan-type.current").text() + countryRoamingRes.MobilePlan + "'");
                        if (result.Logo) {
                            $("#imgLogo").attr("src", result.Logo).closest("div").show();
                        } else {
                            $("#imgLogo").closest("div").hide();
                        }
                        $("#hCountryName").text(result.CountryName);
                        $("#sCountryCode").text("+" + result.CountryCode);
                        $("#divRoamingRateResult .roaming-title span").text(countryRoamingRes.UpdatedOn + " " + result.UpdatedOn)
                        buildRatesResultTalble(result);
                        buildOperatersResultTable(result);
                        $("#divRoamingRateResult").removeClass("hide");
                    } else {
                        $("#divRoamingRateResult").addClass("hide");
                    }
                };

                var refreshRomaingRateOnComplete = function () {
                    $('.fixedColumnWrapper').remove();
                    $("#divLoading").hide();
                    var $table = $('.rr-table');
                    var $fixedColumnWrapper = $('<div class="fixedColumnWrapper"></div>').insertBefore($table.parent());
                    var $fixedColumn = $table.clone().addClass('fixed-column');
                    $fixedColumnWrapper.html($fixedColumn);

                    $fixedColumn.find('th:not(:first-child),td:not(:first-child)').remove();

                    $fixedColumn.find('tr').each(function (i, elem) {
                        $(this).height($table.find('tr:eq(' + i + ')').height());
                        $(this).find('.first-col').css('width', $table.find('.first-col').outerWidth());
                    });

                    $(window).resize(function () {
                        $fixedColumn.find('tr').each(function (i, elem) {
                            $(this).height($table.find('tr:eq(' + i + ')').height());
                            $(this).find('.first-col').css('width', $table.find('.first-col').outerWidth());
                        });
                    });
                };

                var filter = {
                    CountryId: $("#cbCountries").val(),
                    PlanType: $(".plan-type.current").val(),
                    Lang: window.location.href.toLowerCase().indexOf("/ar/") != -1 ? "ar" : "en"
                };

                if (filter.CountryId) {
                    $("#divLoading").show();

                    /*
                    $.ajax({
                        url: '/api/RoamingRates/GetRoamingCountryInfo',
                        dataType: "JSON",
                        data: filter,
                        success: refreshRomaingRateOnSuccess,
                        complete: refreshRomaingRateOnComplete
                    });
                    */

                    // These are mocks that work as examples of the data that should be returned from
                    // the api /api/RoamingRates/GetRoamingCountryInfo
                    var postpaidResultEnMock = {"RateCallingBackToTheUAEMonthly":6,"RateCallingToVisitedCountryMonthly":2,"RateCallingToOutsideVisitedCountryMonthly":9,"RateIncomingCallsMonthly":1.25,"RateAdditionalIncomingCallChargesMonthly":0,"RateTotalIncomingCallChargesMonthly":1.25,"RateDataChargesMonthly":"1.00 AED / 50kb","RateSendingSMSMonthly":2,"RateReceivingSMSMonthly":0,"CountryName":"Algeria","CountryCode":"213","Zone":"Other Arab","Logo":null,"UpdatedOn":"20/11/2014","PlanType":0,"RoamingOperaters":[{"EasyRoamingPostpaid":true,"RoamingDataBundleAvailabilityMonthly":"Easy Roaming Bundles","VoiceAndSMSMonthly":true,"GPRSMonthly":true,"MMSMonthly":true,"Network":null,"RoamingPartnerName":"Djezzy","NetworkCode":"603-02","HandsetType":"GSM 900/1800","Name":"Djezzy"},{"EasyRoamingPostpaid":true,"RoamingDataBundleAvailabilityMonthly":"Easy Roaming Bundles","VoiceAndSMSMonthly":true,"GPRSMonthly":true,"MMSMonthly":true,"Network":null,"RoamingPartnerName":"ATM Mobilis","NetworkCode":"603-01","HandsetType":"GSM 900","Name":"ATM Mobilis"},{"EasyRoamingPostpaid":true,"RoamingDataBundleAvailabilityMonthly":"Easy Roaming Bundles","VoiceAndSMSMonthly":true,"GPRSMonthly":true,"MMSMonthly":true,"Network":null,"RoamingPartnerName":"Ooredoo","NetworkCode":"603-03","HandsetType":"GSM 900/1800","Name":"Ooredoo"}]};

                    var prepaidResultEnMock = {"RateCallingBackToTheUAEPrepaid":6,"RateCallingToTheVisitedCountryPrepaid":2,"RateCallingToOutsideVisitedCountryPrepaid":9,"RateIncomingCallChargesPrepaid":3,"RateDataChargesPrepaid":"1.00 AED / 30kb","RateSendingSMSPrepaid":2,"RateReceivingSMSPrepaid":0,"CountryName":"Algeria","CountryCode":"213","Zone":"Other Arab","Logo":null,"UpdatedOn":"20/11/2014","PlanType":1,"RoamingOperaters":[{"EasyRoamingPrepaid":true,"RoamingDataBundleAvailabilityPrepaid":"Easy Roaming Bundles","OutgoingVoiceCallThruStandardRoamingPrepaid":false,"IncomingCallsPrepaid":true,"SendingSMSPrepaid":true,"ReceivingSMSPrepaid":true,"GPRSPrepaid":true,"MMSPrepaid":true,"RoamingPartnerName":"Djezzy","NetworkCode":"603-02","HandsetType":"GSM 900/1800","Name":"Djezzy"},{"EasyRoamingPrepaid":true,"RoamingDataBundleAvailabilityPrepaid":"Easy Roaming Bundles","OutgoingVoiceCallThruStandardRoamingPrepaid":false,"IncomingCallsPrepaid":true,"SendingSMSPrepaid":true,"ReceivingSMSPrepaid":true,"GPRSPrepaid":true,"MMSPrepaid":true,"RoamingPartnerName":"ATM Mobilis","NetworkCode":"603-01","HandsetType":"GSM 900","Name":"ATM Mobilis"},{"EasyRoamingPrepaid":true,"RoamingDataBundleAvailabilityPrepaid":"Easy Roaming Bundles","OutgoingVoiceCallThruStandardRoamingPrepaid":true,"IncomingCallsPrepaid":true,"SendingSMSPrepaid":true,"ReceivingSMSPrepaid":true,"GPRSPrepaid":true,"MMSPrepaid":true,"RoamingPartnerName":"Ooredoo","NetworkCode":"603-03","HandsetType":"GSM 900/1800","Name":"Ooredoo"}]};

                    var postpaidResultArMock = {"RateCallingBackToTheUAEMonthly":9,"RateCallingToVisitedCountryMonthly":5,"RateCallingToOutsideVisitedCountryMonthly":9,"RateIncomingCallsMonthly":1.25,"RateAdditionalIncomingCallChargesMonthly":2.75,"RateTotalIncomingCallChargesMonthly":4,"RateDataChargesMonthly":"Not available","RateSendingSMSMonthly":2,"RateReceivingSMSMonthly":0,"CountryName":"أثيوبيا","CountryCode":"251","Zone":"ROW (rest of the world)","Logo":null,"UpdatedOn":"20/11/2014","PlanType":0,"RoamingOperaters":[{"EasyRoamingPostpaid":false,"RoamingDataBundleAvailabilityMonthly":"RoamingDataBundleROW","VoiceAndSMSMonthly":true,"GPRSMonthly":true,"MMSMonthly":true,"Network":"3G","RoamingPartnerName":"ETC","NetworkCode":"636-01","HandsetType":"GSM 900","Name":"ETC"}]};

                    var prepaidResultArMock = {"RateCallingBackToTheUAEPrepaid":9,"RateCallingToTheVisitedCountryPrepaid":5,"RateCallingToOutsideVisitedCountryPrepaid":9,"RateIncomingCallChargesPrepaid":4,"RateDataChargesPrepaid":"Not available","RateSendingSMSPrepaid":2,"RateReceivingSMSPrepaid":0,"CountryName":"أثيوبيا","CountryCode":"251","Zone":"ROW (rest of the world)","Logo":null,"UpdatedOn":"20/11/2014","PlanType":1,"RoamingOperaters":[{"EasyRoamingPrepaid":false,"RoamingDataBundleAvailabilityPrepaid":"","OutgoingVoiceCallThruStandardRoamingPrepaid":false,"IncomingCallsPrepaid":false,"SendingSMSPrepaid":false,"ReceivingSMSPrepaid":false,"GPRSPrepaid":false,"MMSPrepaid":false,"RoamingPartnerName":"ETC","NetworkCode":"636-01","HandsetType":"GSM 900","Name":"ETC"}]};

                    var result = postpaidResultEnMock;
                    switch(filter.PlanType) {
                        case "Postpaid":
                            result = isLTR ? postpaidResultEnMock : postpaidResultArMock;
                            break;
                        case "Prepaid":
                            result = isLTR ? prepaidResultEnMock : prepaidResultArMock;
                            break;
                    }

                    refreshRomaingRateOnSuccess(result);
                    refreshRomaingRateOnComplete();
                }
            };
            var setChargingPrinciplesText = function() {
                var plantype = $(".plan-type.current").text();
                if (plantype.toLowerCase() == "postpaid" || plantype.toLowerCase() == "باقة الدفع الآجل") {
                    $('#chargingPrinciplesContentPostpaid').show();
                    $('#chargingPrinciplesContentPrepaid').hide();
                }
                else {
                    $('#chargingPrinciplesContentPostpaid').hide();
                    $('#chargingPrinciplesContentPrepaid').show();
                }
            };

            $(".chosen-select").chosen({}).change(function (s, e) {
                refleshRoamingRate();
                setChargingPrinciplesText()
                trackingForGTM();
            });
            $(".plan-type").click(function () {
                $(".plan-type").removeClass("current");
                $(this).blur().addClass("current");
                refleshRoamingRate();
                setChargingPrinciplesText();
                trackingForGTM();
            });
        }

        /*
         * Home services readiness/availability page
         */

        if ($(".container-du.homeservice-container").length > 0) {
            var fn_GetJSON = function(responseString) {
                var jsonContainer = $(responseString).find('#MainWrapper');

                jsonContainer.find('div').remove();

                var json = jsonContainer.html();
                try {
                    json = $.parseJSON(jsonContainer.html());
                } catch(e) {
                    
                }

                return json;
            };

            var fn_LoadZones = function() {
                var var_Emirate = $('#ddlEmirates').val();
                var urlLink = document.location.pathname + "/GetZonesByEmirate?EmirateTitle=" + var_Emirate;

                var loadZonesCallback = function (data) {
                    if (data)
                    {
                        var json = fn_GetJSON(data);

                        if (json)
                        {
                            var ddlZones = $("#ddlZones");

                            $(ddlZones).html('');

                            $.each(json, function () {
                                ddlZones.append($("<option />").val(this.Id).text(this.Title));
                            });

                            $(ddlZones).removeClass('hide');
                        }
                    }
                };

                // $.get(urlLink, loadZonesCallback);

                // Mock, should be removed and the API should be used instead
                var mock = $("<html>").append("<div id=\"MainWrapper\">[{\"Id\":\"00000000-0000-0000-0000-000000000000\",\"Title\":\"Select Zone\"},{\"Id\":\"42834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"ABU HAIL\"},{\"Id\":\"43834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL AWIR FIRST\"},{\"Id\":\"44834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL AWIR SECOND\"},{\"Id\":\"45834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL BADA AL BADAA\"},{\"Id\":\"46834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL BARAHA\"},{\"Id\":\"47834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL BARSHA FIRST\"},{\"Id\":\"48834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL BARSHA SECOND\"},{\"Id\":\"49834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL BARSHA SOUTH FIFTH JUMEIRAH VILLAGE\"},{\"Id\":\"4a834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL BARSHA SOUTH FIRST\"},{\"Id\":\"4b834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL BARSHA SOUTH FOURTH JUMEIRAH VILLAGE SOUTH\"},{\"Id\":\"4c834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL BARSHA SOUTH SECOND\"},{\"Id\":\"4d834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL BARSHA SOUTH THIRD\"},{\"Id\":\"4e834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL BARSHA THIRD\"},{\"Id\":\"4f834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL BUTEEN\"},{\"Id\":\"50834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL CORNICHE\"},{\"Id\":\"51834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL DAGHAYA\"},{\"Id\":\"52834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL FAGAA DUBAI LAND\"},{\"Id\":\"53834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL GARHOUD\"},{\"Id\":\"54834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL HAMRIYA\"},{\"Id\":\"55834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL HAMRIYA PORT HAMRIYA PORT\"},{\"Id\":\"56834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL HATHMAH DUBAI LAND\"},{\"Id\":\"57834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL HEBIAH FIFTH\"},{\"Id\":\"58834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL HEBIAH FIRST DUBAI AUTODROME\"},{\"Id\":\"59834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL HEBIAH FOURTH DUBAI SPORTS CITY\"},{\"Id\":\"5a834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL HEBIAH SECOND DUBAI LAND\"},{\"Id\":\"5b834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL HEBIAH THIRD DUBAI LAND\"},{\"Id\":\"5c834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL HUDAIBA\"},{\"Id\":\"5d834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL JADAF\"},{\"Id\":\"5e834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL JAFILIYA\"},{\"Id\":\"5f834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL KARAMA\"},{\"Id\":\"60834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL KHABAISI\"},{\"Id\":\"61834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL KHEERAN FIRST RAS AL HAMAR\"},{\"Id\":\"62834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL KHEERAN RAS AL HAMAR\"},{\"Id\":\"63834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL KHEERAN SECOND RAS AL HAMAR\"},{\"Id\":\"64834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL KHWANEEJ FIRST\"},{\"Id\":\"65834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL KHWANEEJ SECOND\"},{\"Id\":\"66834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL KIFAF\"},{\"Id\":\"67834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL LAYAN 2 DUBAI LAND\"},{\"Id\":\"68834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL LESAILY DUBAI LAND\"},{\"Id\":\"69834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL MAHA 800\"},{\"Id\":\"6a834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL MAMZAR\"},{\"Id\":\"6b834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL MANARA\"},{\"Id\":\"6c834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL MARMOOM DUBAI LAND\"},{\"Id\":\"6d834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL MERKADH AL MERKAD\"},{\"Id\":\"6e834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL MERYAL 700\"},{\"Id\":\"6f834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL MIZHAR FIRST\"},{\"Id\":\"70834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL MIZHAR SECOND\"},{\"Id\":\"71834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL MURAQQABAT\"},{\"Id\":\"72834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL MURAR\"},{\"Id\":\"73834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL MUTEENA\"},{\"Id\":\"74834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL NAHDA FIRST\"},{\"Id\":\"75834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL NAHDA SECOND\"},{\"Id\":\"76834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL QOUZ FIRST AL QUOZ FIRST\"},{\"Id\":\"77834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL QOUZ FOURTH AL QUOZ FOURTH\"},{\"Id\":\"78834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL QOUZ SECOND AL QUOZ SECOND\"},{\"Id\":\"79834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL QOUZ THIRD AL QUOZ THIRD\"},{\"Id\":\"7a834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL QOUZE IND.FIRST AL QUOZ IND. FIRST\"},{\"Id\":\"7b834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL QOUZE IND.FOURTH AL QUOZ IND. FOURTH\"},{\"Id\":\"7c834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL QOUZE IND.SECOND AL QUOZ IND. SECOND\"},{\"Id\":\"7d834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL QOUZE IND.THIRD AL QUOZ IND. THIRD\"},{\"Id\":\"7e834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL QUSAIS FIRST\"},{\"Id\":\"7f834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL QUSAIS IND. FIFTH\"},{\"Id\":\"80834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL QUSAIS IND. FIRST\"},{\"Id\":\"81834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL QUSAIS IND. FOURTH\"},{\"Id\":\"82834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL QUSAIS IND. SECOND\"},{\"Id\":\"83834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL QUSAIS IND. THIRD\"},{\"Id\":\"84834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL QUSAIS SECOND\"},{\"Id\":\"85834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL QUSAIS THIRD\"},{\"Id\":\"86834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL RAFFA\"},{\"Id\":\"87834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL RAS\"},{\"Id\":\"88834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL RASHIDIYA\"},{\"Id\":\"89834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL RIGGA\"},{\"Id\":\"8a834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL ROWAIYAH FIRST 800\"},{\"Id\":\"8b834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL ROWAIYAH SECOND 800\"},{\"Id\":\"8c834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL ROWAIYAH THIRD 800\"},{\"Id\":\"8d834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL SABKHA\"},{\"Id\":\"8e834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL SAFA FIRST\"},{\"Id\":\"8f834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL SAFA SECOND\"},{\"Id\":\"90834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL SAFOUH FIRST\"},{\"Id\":\"91834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL SAFOUH SECOND\"},{\"Id\":\"92834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL SATWA\"},{\"Id\":\"93834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL SELAL DUBAI LAND\"},{\"Id\":\"94834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL SHINDAGHA\"},{\"Id\":\"95834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL SOUQ AL KABEER\"},{\"Id\":\"96834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL THANYAH  FOURTH EMIRATES HILL THIRD\"},{\"Id\":\"97834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL THANYAH FIFTH EMIRATES HILL FIRST\"},{\"Id\":\"98834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL THANYAH FIRST AL SAFOUH THIRD\"},{\"Id\":\"99834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL THANYAH SECOND AL THANAYAH SECOND\"},{\"Id\":\"9a834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL THANYAH THIRD EMIRATES HILL SECOND\"},{\"Id\":\"9b834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL TTAY\"},{\"Id\":\"9c834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL TWAR FIRST\"},{\"Id\":\"9d834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL TWAR SECOND\"},{\"Id\":\"9e834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL TWAR THIRD\"},{\"Id\":\"9f834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL WARQAA FIFTH AL WARQA FIFTH\"},{\"Id\":\"a0834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL WARQAA FIRST AL WARQA FIRST\"},{\"Id\":\"a1834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL WARQAA FOURTH AL WARQA FOURTH\"},{\"Id\":\"a2834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL WARQAA SECOND AL WARQA SECOND\"},{\"Id\":\"a3834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL WARQAA THIRD AL WARQA THIRD\"},{\"Id\":\"a4834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL WASL\"},{\"Id\":\"a5834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL WOHOOSH 700\"},{\"Id\":\"a6834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL WUHEIDA\"},{\"Id\":\"a7834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL YALAYIS 1 DUBAI LAND\"},{\"Id\":\"a8834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL YALAYIS 2 DUBAI LAND\"},{\"Id\":\"a9834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL YALAYIS 4 DUBAI LAND\"},{\"Id\":\"aa834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL YALAYIS 5 DUBAI LAND\"},{\"Id\":\"ab834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL YUFRAH 1 DUBAI LAND\"},{\"Id\":\"ac834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL YUFRAH 2 DUBAI LAND\"},{\"Id\":\"ad834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"ALEYAS\"},{\"Id\":\"ae834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AYAL NASIR\"},{\"Id\":\"af834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"BU KADRA\"},{\"Id\":\"b0834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"BURJ KHALIFA BUSINESS BAY\"},{\"Id\":\"b1834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"CORNICHE DEIRA\"},{\"Id\":\"b2834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"DUBAI INTL AIRPORT DUBAI INTERNATIONAL AIRPORT\"},{\"Id\":\"b3834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"DUBAI INVESTMENT PARK FIRST\"},{\"Id\":\"b4834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"DUBAI INVESTMENT PARK SECOND\"},{\"Id\":\"b5834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"ENKHALI 700\"},{\"Id\":\"b6834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"GHADEER BARASHY DUBAI LAND\"},{\"Id\":\"b7834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"GRAYTEESAH DUBAI LAND\"},{\"Id\":\"b8834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"HADAEQ SHEIKH MOHAMMED BIN RASHID RANCHES\"},{\"Id\":\"b9834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"HATTA\"},{\"Id\":\"ba834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"HEFAIR DUBAI LAND\"},{\"Id\":\"bb834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"HESSYAN FIRST\"},{\"Id\":\"bc834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"HESSYAN SECOND\"},{\"Id\":\"bd834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"HOR AL ANZ\"},{\"Id\":\"be834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"HOR AL ANZ EAST\"},{\"Id\":\"bf834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"INDUSTRIAL AREA 3\"},{\"Id\":\"c0834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"JABAL ALI FIRST\"},{\"Id\":\"c1834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"JABAL ALI INDUSTRIAL FIRST JEBEL ALI INDUSTRIAL AREA\"},{\"Id\":\"c2834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"JABAL ALI INDUSTRIAL SECOND JABAL ALI INDUSTRIAL 2\"},{\"Id\":\"c3834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"JABAL ALI INDUSTRIAL THIRD JABAL ALI INDUSTRIAL 3\"},{\"Id\":\"c4834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"JABAL ALI SECOND\"},{\"Id\":\"c5834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"JABAL ALI THIRD\"},{\"Id\":\"c6834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"JUMEIRA FIRST\"},{\"Id\":\"c7834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"JUMEIRA SECOND\"},{\"Id\":\"c8834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"JUMEIRA THIRD\"},{\"Id\":\"c9834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"LE HEMAIRA 800\"},{\"Id\":\"ca834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"LEHBAB FIRST 700\"},{\"Id\":\"cb834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"LEHBAB SECOND 800\"},{\"Id\":\"cc834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MADINAT AL MATAAR\"},{\"Id\":\"cd834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MADINAT DUBAI AL MELAHEYAH AL MINA\"},{\"Id\":\"ce834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MANKHOOL\"},{\"Id\":\"cf834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MARGAB 800\"},{\"Id\":\"d0834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MARGHAM 800\"},{\"Id\":\"d1834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MARSA DUBAI\"},{\"Id\":\"d2834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MEAISEM FIRST JUMEIRAH GOLF ESTATE\"},{\"Id\":\"d3834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MEAISEM SECOND DUBAI LAND\"},{\"Id\":\"d4834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MENA JABAL ALI\"},{\"Id\":\"d5834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MEREIYEEL 800\"},{\"Id\":\"d6834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MIRDIF\"},{\"Id\":\"d7834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MUGATRAH DUBAI LAND\"},{\"Id\":\"d8834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MUHAISANAH FIFTH\"},{\"Id\":\"d9834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MUHAISANAH FOURTH\"},{\"Id\":\"da834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MUHAISANAH SECOND\"},{\"Id\":\"db834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MUHAISANAH THIRD\"},{\"Id\":\"dc834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MUHAISNAH FIRST\"},{\"Id\":\"dd834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"MUSHRAIF\"},{\"Id\":\"de834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"NADD AL HAMAR\"},{\"Id\":\"df834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"NADD AL SHIBA FIRST RANCHES\"},{\"Id\":\"e0834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"NADD AL SHIBA FOURTH\"},{\"Id\":\"e1834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"NADD AL SHIBA SECOND\"},{\"Id\":\"e2834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"NADD AL SHIBA THIRD\"},{\"Id\":\"e3834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"NADD HESSA DUBAI SILICON OASIS\"},{\"Id\":\"e4834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"NADD SHAMMA\"},{\"Id\":\"e5834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"NAIF\"},{\"Id\":\"e6834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"NAKHLAT DEIRA PALM DEIRA\"},{\"Id\":\"e7834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"NAKHLAT JABAL ALI NAKHLAT JABAL ALI - THE PALM JEBEL ALI\"},{\"Id\":\"e8834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"NAKHLAT JUMEIRA THE PALM JUMEIRAH\"},{\"Id\":\"e9834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"NAZWAH 700\"},{\"Id\":\"ea834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"OUD AL MUTEENA FIRST\"},{\"Id\":\"eb834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"OUD AL MUTEENA SECOND\"},{\"Id\":\"ec834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"OUD AL MUTEENA THIRD\"},{\"Id\":\"ed834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"OUD METHA\"},{\"Id\":\"ee834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"PORT SAEED\"},{\"Id\":\"ef834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"RAS AL KHOR AL JADAF\"},{\"Id\":\"f0834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"RAS AL KHOR IND. FIRST\"},{\"Id\":\"f1834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"RAS AL KHOR IND. SECOND\"},{\"Id\":\"f2834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"RAS AL KHOR IND. THIRD\"},{\"Id\":\"f3834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"REMAH 800\"},{\"Id\":\"f4834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"RIGGAT AL BUTEEN\"},{\"Id\":\"f5834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"SAIH AL DAHAL DUBAI LAND\"},{\"Id\":\"f6834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"SAIH SHUAALAH DUBAI LAND\"},{\"Id\":\"f7834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"SAIH SHUAIB 1\"},{\"Id\":\"f8834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"SAIH SHUAIB 2\"},{\"Id\":\"f9834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"SAIH SHUAIB 3\"},{\"Id\":\"fa834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"SAIH SHUAIB 4\"},{\"Id\":\"fb834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"THE WORLD ISLAND\"},{\"Id\":\"fc834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"TRADE CENTER FIRST\"},{\"Id\":\"fd834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"TRADE CENTER SECOND\"},{\"Id\":\"fe834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"UMM AL DAMAN 800\"},{\"Id\":\"ff834e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"UMM AL MO\u0027MENEEN 800\"},{\"Id\":\"00844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"UMM AL SHEIF\"},{\"Id\":\"01844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"UMM ESELAY 800\"},{\"Id\":\"02844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"UMM HURAIR FIRST\"},{\"Id\":\"03844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"UMM HURAIR SECOND\"},{\"Id\":\"04844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"UMM NAHAD FIRST DUBAI LAND\"},{\"Id\":\"05844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"UMM NAHAD FOURTH DUBAI LAND\"},{\"Id\":\"06844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"UMM NAHAD SECOND DUBAI LAND\"},{\"Id\":\"07844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"UMM NAHAD THIRD DUBAI LAND\"},{\"Id\":\"08844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"UMM RAMOOL\"},{\"Id\":\"09844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"UMM SUQEIM FIRST\"},{\"Id\":\"0a844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"UMM SUQEIM SECOND\"},{\"Id\":\"0b844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"UMM SUQEIM THIRD\"},{\"Id\":\"0c844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"WADI AL SAFA 2 RANCHES\"},{\"Id\":\"0d844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"WADI AL SAFA 3 RANCHES\"},{\"Id\":\"0e844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"WADI AL SAFA 4 RANCHES\"},{\"Id\":\"0f844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"WADI AL SAFA 5 DUBAI LAND\"},{\"Id\":\"10844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"WADI AL SAFA 6 ARABIAN RANCHES\"},{\"Id\":\"11844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"WADI AL SAFA 7 DUBAI LAND\"},{\"Id\":\"12844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"WADI ALAMARDI\"},{\"Id\":\"13844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"WADI ALSHABAK\"},{\"Id\":\"14844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"WARSAN FIRST\"},{\"Id\":\"15844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"WARSAN FOURTH DUBAI SILICON OASIS\"},{\"Id\":\"16844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"WARSAN SECOND\"},{\"Id\":\"17844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"WARSAN THIRD 800\"},{\"Id\":\"18844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"YARAAH 800\"},{\"Id\":\"19844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"ZAABEEL FIRST ZAABEEL FIRST\"},{\"Id\":\"1a844e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"ZAABEEL SECOND\"}]</div>");
                
                loadZonesCallback(mock);
            };

            var fn_ClearSubzones = function() {
                $("#ddlSubZones option:not(:first-child)").each(function () {
                    $(this).remove();
                });
            };

            var fn_LoadSubZones = function() {
                var var_ZoneID = $('#ddlZones').val();
                var urlLink = document.location.pathname + "/GetSubZones?ZoneID=" + var_ZoneID;

                var loadSubZonesCallback = function (data) {
                    if (data) {

                        var json = fn_GetJSON(data);

                        if (json) {

                            var ddlSubZones = $("#ddlSubZones");

                            $(ddlSubZones).html('');

                            $.each(json, function () {
                                ddlSubZones.append($("<option />").val(this.Id).text(this.Title));
                            });

                            $(ddlSubZones).removeClass('hide');
                        }
                    }
                };

                // $.get(urlLink, loadSubZonesCallback);

                // Mock, should be removed and the API should be used instead
                var mock = $("<html>").append("<div id=\"MainWrapper\">[{\"Id\":\"00000000-0000-0000-0000-000000000000\",\"Title\":\"Select sub-zone\"},{\"Id\":\"02a84e28-3c49-6e48-96d2-ff5a00702077\",\"Title\":\"AL BARAHA\"}]</div>");
                
                loadSubZonesCallback(mock);
            };

            var fn_LoadServices = function() {
                var var_SubZoneID = $('#ddlSubZones').val();
                var urlLink = document.location.pathname + "/GetServicesByArea?AreaID=" + var_SubZoneID;

                var loadServicesCallback = function (data) {
                    if (data) {            

                        var json = fn_GetJSON(data);

                        if (json) {

                            var divServices = $("#divServices");

                            //divServices.append("<ul class='blue-list mt30' id='listSerivces' />")

                            //$.each(json, function () {

                            //    $('#listSerivces').append($("<li style='pointer-events: none;' />").html(this.Name));
                            //});

                            divServices.append("<div class='mt30' />");
                            divServices.append(json);

                        }
                    }
                };

                // $.get(urlLink, loadServicesCallback);

                // Mock, should be removed and the API should be used instead
                var mock = $("<html>").append("<div id=\"MainWrapper\">You can get <span class='home-service-plan'>Talk & Surf</span> in your area. Please see <a href=\"http://www.du.ae/personal/at-home/packages\">our plans and order online.</a></div>");
                
                loadServicesCallback(mock);
            };

            var fn_ClearServices = function () {
                var divServices = $("#divServices");

                $(divServices).html('');
            };

            var fn_UploadImportFile = function()
            {
                $('form').attr("enctype", "multipart/form-data").attr("encoding", "multipart/form-data");
                $('form').attr("action", "/test/UploadFile");
                $('form').submit();
            };

            // Bootstrapping
            $("#ddlEmirates").on("change", function() {
                fn_ClearServices();
                fn_LoadZones();
                fn_ClearSubzones();
            });
            $("#ddlZones").on("change", function() {
                fn_ClearServices();
                fn_LoadSubZones();
            });
            $("#ddlSubZones").on("change", function() {
                fn_ClearServices();
                fn_LoadServices();
            });
        }

        /*
         * Media Centre
         */

        if ($(".aboutus-mediacentre").length > 0) {
            var getPressReleaseNews = function() {
                var getPressReleaseNewsCallback = function(data) {
                    var newsTemplate = $("#pressReleaseNews").html();
                    var newsObj;

                    $(".ucPressRelease").html("");

                    data.news.forEach(function(news) {
                        newsObj = $(newsTemplate).clone();
                        newsObj.find(".date").text(news.date);
                        newsObj.find(".press-release-title").text(news.title);
                        newsObj.find(".press-release-title").attr("href", news.url);
                        newsObj.find(".more").attr("href", news.url);
                        newsObj.appendTo($(".ucPressRelease"));
                    });
                };

                /*
                $.get("/api/News", {
                        "limit": 2,
                        "lang": isLTR ? "en" : "ar"
                    }, getPressReleaseNewsCallback);
                */
                var apiNewsMockEn = {
                    news: [
                        {
                            date: "Tuesday, 14 February 2017",
                            title: "du makes free time more entertaining for customers",
                            url: "newsdetail/2017/02/14/entertainer-offer"
                        }, {
                            date: "Thursday, 09 February 2017",
                            title: "du and Etihad Guest announce major partnership and introduce all new Smart Plan Platinum offering unprecedented benefits",
                            url: "newsdetail/2017/02/09/smart-plan-platinum"
                        }
                    ]
                };
                var apiNewsMockAr = {
                    news: [
                        {
                            date: "Tuesday, 14 February 2017",
                            title: "دو تتعاون مع شركة «ذا إنترتينر» لتطوير تطبيق ترفيهي خاص بالشركة",
                            url: "newsdetail/2017/02/14/entertainer-offer"
                        }, {
                            date: "Thursday, 09 February 2017",
                            title: 'دو تعلن عن شراكة استراتيجية مع "برنامج ضيف الاتحاد" لاطلاق الباقة البلاتينية الذكية بمزايا استثنائية',
                            url: "newsdetail/2017/02/09/smart-plan-platinum"
                        }
                    ]
                };
                getPressReleaseNewsCallback(isLTR ? apiNewsMockEn : apiNewsMockAr);
            };
            var getDropdownsFilterValues = function() {
                var getCategoriesCallback = function(data) {
                    $("#ddlCategory").find("option").not( "[value='0']" ).remove();
                
                    data.categories.forEach(function(category) {
                        var ctrl = $("<option>");
                        ctrl.text(category.title);
                        ctrl.val(category.id);
                        ctrl.appendTo($("#ddlCategory"));
                    });
                };
                var getYearsCallback = function(data) {
                    $("#ddlYear").find("option").not( "[value='0']" ).remove();
                
                    data.years.forEach(function(year) {
                        var ctrl = $("<option>");
                        ctrl.text(year);
                        ctrl.val(year);
                        ctrl.appendTo($("#ddlYear"));
                    });
                };

                /*
                $.get("/api/Categories", getCategoriesCallback);
                */
                var apiCategoriesMock = {
                    categories: [
                        {
                            id: "cf3a1128-3c49-6e48-96d2-ff5a00702077",
                            title: "100%"
                        }, {
                            id: "48123c28-3c49-6e48-96d2-ff5a00702077",
                            title: "100gb"
                        }, {
                            id: "3f6a3228-3c49-6e48-96d2-ff5a00702077",
                            title: "135#"
                        }, {
                            id: "f7d11128-3c49-6e48-96d2-ff5a00702077",
                            title: "1 fils per second"
                        }, {
                            id: "c3651528-3c49-6e48-96d2-ff5a00702077",
                            title: "1 Gbps"
                        }, {
                            id: "5f573d28-3c49-6e48-96d2-ff5a00702077",
                            title: "1 million promo"
                        }, {
                            id: "a01b1d28-3c49-6e48-96d2-ff5a00702077",
                            title: "400G"
                        }, {
                            id: "36171128-3c49-6e48-96d2-ff5a00702077",
                            title: "4G+"
                        }, {
                            id: "4d22fa27-3c49-6e48-96d2-ff5a00702077",
                            title: "4G LTE"
                        }, {
                            id: "3c171128-3c49-6e48-96d2-ff5a00702077",
                            title: "5G"
                        }, {
                            id: "5323fa27-3c49-6e48-96d2-ff5a00702077",
                            title: "abu dhabi"
                        }
                    ]
                };
                getCategoriesCallback(apiCategoriesMock);

                /*
                $.get("/api/PressRelease/Years", getYearsCallback);
                */
                var apiYearsMock = {
                    years: [2017, 2016, 2015, 2014, 2013, 2012, 2011, 2010, 2009, 2008, 2007, 2006]
                };
                getYearsCallback(apiYearsMock);
            };

            var createPagination = function(data, page) {
                var numVisiblePageLinks = 5;

                var paginationTemplate = $("#paginationTemplate").html();
                var paginationLinkTemplate = $("#paginationLinkTemplate").html();

                $(".pagination .RadDataPager").html("");

                var paginationObj = $(paginationTemplate).clone();

                var prevPage = page > 1 ? page - 1 : 1;
                var nextPage = page + 1 <= data.pagination.numPages ? page + 1 : page;

                paginationObj.find("#PrevButton").click(function() {
                    filterMediaCentre(prevPage);
                });
                paginationObj.find("#NextButton").click(function() {
                    filterMediaCentre(nextPage);
                });

                paginationObj.appendTo($(".pagination .RadDataPager"));

                var pageLinksContainer = $(".rdpNumPart");
                pageLinksContainer.html("");
                
                if (data.pagination.firstPage > 1) {
                    var pageLinkObj = $(paginationLinkTemplate).clone();
                    pageLinkObj.find("span").text("...");
                    pageLinkObj.click(function() {
                        var targetPage = page - numVisiblePageLinks;
                        if (targetPage < 1) {
                            targetPage = 1;
                        }

                        filterMediaCentre(targetPage);
                    });
                    pageLinkObj.appendTo(pageLinksContainer);
                }

                var maxLimitPage = data.pagination.firstPage + numVisiblePageLinks - 1;
                if (maxLimitPage > data.pagination.numPages) {
                    maxLimitPage = data.pagination.numPages;
                }
                for (var idxPage = data.pagination.firstPage; idxPage <= maxLimitPage; idxPage++) {
                    var pageLinkObj = $(paginationLinkTemplate).clone();
                    pageLinkObj.find("span").text(idxPage);

                    (function(indexPage){
                        pageLinkObj.click(function() {
                            filterMediaCentre(indexPage);
                        });
                    })(idxPage);
                    
                    if (idxPage === page) {
                        pageLinkObj.addClass("rdpCurrentPage");
                    }
                    pageLinkObj.appendTo(pageLinksContainer);
                }

                if (data.pagination.firstPage + numVisiblePageLinks < data.pagination.numPages) {
                    var pageLinkObj = $(paginationLinkTemplate).clone();
                    pageLinkObj.find("span").text("...");
                    pageLinkObj.click(function() {
                        var targetPage = page + numVisiblePageLinks;
                        if (targetPage > data.pagination.numPages) {
                            targetPage = data.pagination.numPages;
                        }

                        filterMediaCentre(targetPage);
                    });
                    pageLinkObj.appendTo(pageLinksContainer);
                }
            };

            var getMediaCentreCallback = function(data, page) {
                if (data.news && data.news.length > 0) {
                    $("#pMessage").hide();
                } else {
                    $("#pMessage").show();
                }

                var newsTemplate = $("#mediaCentreNews").html();
                var newsObj;

                $("#pMediaCentreNews").html("");

                data.news.forEach(function(news) {
                    newsObj = $(newsTemplate).clone();
                    newsObj.find(".date").text(news.date);
                    newsObj.find(".headlink").text(news.title);
                    newsObj.find(".headlink").attr("href", news.url);
                    newsObj.find(".more").attr("href", news.url);
                    newsObj.appendTo($("#pMediaCentreNews"));
                });

                createPagination(data, page);
            };

            var filterMediaCentre = function(page) {
                page = isNaN(page) ? 1 : page;

                /*
                $.get("/api/MediaCentre", {
                        "page": page,
                        "lang": isLTR ? "en" : "ar",
                        "category": $("#ddlCategory").val(),
                        "month": $("#ddlMonth").val(),
                        "year": $("#ddlYear").val()
                    }, getMediaCentreCallback);
                */
                var mediaCentreMockEn = {
                    news: [
                        {
                            date: "Tuesday, 18 October 2016",
                            title: "du and Qualcomm Introduce Next Generation Wi-Fi Technology to the Middle East Region",
                            url: "/about/media-centre/newsdetail/2016/10/18/du-and-qualcomm-introduce-next-generation-wi-fi-technology-to-the-middle-east-region"
                        }, {
                            date: "Tuesday, 18 October 2016",
                            title: "Hewlett Packard Enterprise and du announce collaboration in support of Dubai Smart City Initiative",
                            url: "/about/media-centre/newsdetail/2016/10/18/hewlett-packard-enterprise-and-du-announce-collaboration-in-support-of-dubai-smart-city-initiative"
                        }, {
                            date: "Monday, 17 October 2016",
                            title: "du Drives Digital Transformation in the Media Industry with Media Cloud Services",
                            url: "/about/media-centre/newsdetail/2016/10/17/du-drives-digital-transformation-in-the-media-industry-with-media-cloud-services"
                        }, {
                            date: "Sunday, 16 October 2016",
                            title: "His Highness Sheikh Maktoum Bin Mohammed Bin Rashid Al Maktoum and His Highness Shaikh Mansour Bin Mohammad Bin Rashid Al Maktoum visit du on Day 1 of GITEX 2016",
                            url: "/about/media-centre/newsdetail/2016/10/16/his-highness-sheikh-maktoum-bin-mohammed-bin-rashid-al-maktoum-and-his-highness-shaikh-mansour-bin-mohammad-bin-rashid-al-maktoum-visit-du-on-day-1-of-gitex-2016"
                        }, {
                            date: "Sunday, 16 October 2016",
                            title: "Nokia and du demonstrate Middle East's first 4K 3D 360 virtual reality video streaming using 5G-ready network at GITEX 2016",
                            url: "/about/media-centre/newsdetail/2016/10/16/nokia-and-du-demonstrate-middle-east's-first-4k-3d-360-virtual-reality-video-streaming-using-5g-ready-network-at-gitex-2016"
                        }, {
                            date: "Sunday, 16 October 2016",
                            title: "ADIB enters into a multi-year data centre co-location services agreement with du Another step forward in digital transformation of ADIB",
                            url: "/about/media-centre/newsdetail/2016/10/16/adib-enters-into-a-multi-year-data-centre-co-location-services-agreement-with-du-another-step-forward-in-digital-transformation-of-adib"
                        }, {
                            date: "Saturday, 15 October 2016",
                            title: "“Cyber security should not be an afterthought for an organisation, it should be at the heart of the thought itself” – Osman Sultan",
                            url: "/about/media-centre/newsdetail/2016/10/15/cybersecurity-postevent"
                        }, {
                            date: "Friday, 14 October 2016",
                            title: "WiFi UAE expands its footprint in Abu Dhabi with its latest roll out at Marina Mall",
                            url: "/about/media-centre/newsdetail/2016/10/14/wifi-uae-expands-its-footprint-in-abu-dhabi-with-its-latest-roll-out-at-marina-mall"
                        }, {
                            date: "Thursday, 13 October 2016",
                            title: "Launch of the du Ladies Premium Number fair provides a premium experience for Emirates women",
                            url: "/about/media-centre/newsdetail/2016/10/13/ladies-fair"
                        }, {
                            date: "Wednesday, 12 October 2016",
                            title: "du strengthens its commitment to youth empowerment with participation in the inaugural Kafa’at Internship Fair",
                            url: "/about/media-centre/newsdetail/2016/10/12/du-strengthens-its-commitment-to-youth-empowerment-with-participation-in-the-inaugural-kafa-at-internship-fair"
                        }
                    ],
                    pagination: {
                        firstPage: 1,
                        numPages: 10
                    }
                };
                var mediaCentreMockAr = {
                    news: [
                        {
                            date: "Tuesday, 14 February 2017",
                            title: "دو تتعاون مع شركة «ذا إنترتينر» لتطوير تطبيق ترفيهي خاص بالشركة",
                            url: "/ar/about/media-centre/newsdetail/2017/02/14/entertainer-offer"
                        }, {
                            date: "Thursday, 09 February 2017",
                            title: 'دو تعلن عن شراكة استراتيجية مع "برنامج ضيف الاتحاد" لاطلاق الباقة البلاتينية الذكية بمزايا استثنائية',
                            url: "/ar/about/media-centre/newsdetail/2017/02/09/smart-plan-platinum"
                        }, {
                            date: "Tuesday, 07 February 2017",
                            title: "تماشياً مع أهداف بوابة الإمارات لابتكارات الجيل الخامس",
                            url: "/ar/about/media-centre/newsdetail/2017/02/07/du-sharjahuniversity-u5gig-mou"
                        }, {
                            date: "Wednesday, 04 January 2017",
                            title: 'دو تحصد جائزتي "أفضل مبادرة لخدمات المدينة الذكية" و"أفضل مزود للخدمات السحابية" للعام 2016 من "تيليكوم ريفيو"',
                            url: "/ar/about/media-centre/newsdetail/2017/01/04/telecom-review-award"
                        }, {
                            date: "Tuesday, 03 January 2017",
                            title: 'دو تتعاون مع مجموعة "إن إم سي" للرعاية الصحية لتطبيق تقنية التعاملات الرقمية "بلوك تشين" على السجلات الطبية الإلكترونية الخاصة بالمرضى',
                            url: "/ar/about/media-centre/newsdetail/2017/01/03/du-nmc-blockchain"
                        }, {
                            date: "Wednesday, 28 December 2016",
                            title: "دو تتعاون مع (تاكسي دبي) لتزويد مركبات المؤسسة بـشاشات ذكية وخدمة واي فاي",
                            url: "/ar/about/media-centre/newsdetail/2016/12/28/du-empowers-dubai-taxi-with-wifi-uae-and-smart-screens-in-a-first-of-its-kind-infotainment-collaboration"
                        }, {
                            date: "Tuesday, 27 December 2016",
                            title: "دو تقدم اسهامات فاعلة في مشروع تطوير نظام الكابل البحري الخامس لدول جنوب شرق آسيا والشرق الأوسط وغرب أوروبا",
                            url: "/ar/about/media-centre/newsdetail/2016/12/27/du-instrumental-in-award-winning-collaboration-to-construct-sea-me-we-5---a-subsea-fiber-system-connecting-continents"
                        }, {
                            date: "Thursday, 22 December 2016",
                            title: "دو تطلق حلول مبتكرة لاستضافة خدمات المواقع الإلكترونية للمؤسسات لحمايتها من الهجمات والتهديدات الإلكترونية",
                            url: "/ar/about/media-centre/newsdetail/2016/12/22/secure-webhosting"
                        }, {
                            date: "Monday, 19 December 2016",
                            title: 'دو تتيح للعملاء إجراء عمليات شراء الألعاب بشكل سلس عبر هواتفهم المتحركة بتعاونها الجديد مع "كرتي ستور"',
                            url: "/ar/about/media-centre/newsdetail/2016/12/19/karti-store"
                        }, {
                            date: "Sunday, 18 December 2016",
                            title: '"مستلزمات العمل الرئيسية" من دو توفر للشركات الصغيرة والمتوسطة حلولاً مكتبية مميزة بأقساط شهرية ميسرة بدون أي دفعات مسبقة',
                            url: "/ar/about/media-centre/newsdetail/2016/12/18/business-devices"
                        }
                    ],
                    pagination: {
                        firstPage: 1,
                        numPages: 10
                    }
                };
                getMediaCentreCallback(isLTR ? mediaCentreMockEn : mediaCentreMockAr, page);
            };

            getPressReleaseNews();
            getDropdownsFilterValues();
            filterMediaCentre();
            $("#btnFilter").click(filterMediaCentre);
        }

        /*
         * TV Package Selector
         */

        if ($(".tvPackageSelector-container").length > 0) {
            /* MOCKS */
            var getLanguagesMock = [{"Language":"Arabic"},{"Language":"Arabic, English"},{"Language":"Arabic,English"},{"Language":"Azerbaijani"},{"Language":"Bengali"},{"Language":"Dari"},{"Language":"Dutch"},{"Language":"English"},{"Language":"French"},{"Language":"German"},{"Language":"Hindi"},{"Language":"Italian"},{"Language":"Japanese"},{"Language":"Korean"},{"Language":"Malayalam"},{"Language":"Mandarin"},{"Language":"Marathi"},{"Language":"Pashtu"},{"Language":"Persian"},{"Language":"Polish"},{"Language":"Punjabi"},{"Language":"Romanian"},{"Language":"Russian"},{"Language":"Sawhili"},{"Language":"Tagalog"},{"Language":"Tamil"},{"Language":"Turkish"},{"Language":"Urdu"}];

            var getGenreByLangMock = [{"Genre":"Documentaries","Language":["Arabic"]},{"Genre":"Entertainment","Language":["Arabic"]},{"Genre":"General Entertainment","Language":["Arabic"]},{"Genre":"Islamic","Language":["Arabic"]},{"Genre":"Kids","Language":["Arabic"]},{"Genre":"Lifestyle","Language":["Arabic"]},{"Genre":"Local & Regional News & Info","Language":["Arabic"]},{"Genre":"Movies","Language":["Arabic"]},{"Genre":"Music","Language":["Arabic"]},{"Genre":"News","Language":["Arabic"]},{"Genre":"Series","Language":["Arabic"]},{"Genre":"Sports","Language":["Arabic"]}];

            var getAllChannelsMock = [{"ChannelName":"ART Aflam 1","Language":"Arabic","Genre":"Movies"},{"ChannelName":"ART Aflam 2","Language":"Arabic","Genre":"Movies"},{"ChannelName":"ART Cinema","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Cinema 1","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Cinema 2","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Nile Cinema","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Panorama Film","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Rotana","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Rotana Aflam","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Rotana Cinema","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Rotana Classic","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Zee Aflam","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Zee Alwan","Language":"Arabic","Genre":"Movies"}];

            var getPackagesMock = [{"Id":"3384","TvPackage":"Channel Name","PackagePrice":"","PackageAdditionalSubscription":"","PackageDescription":"","ChannelLists":[{"ChannelName":"MTV Lebanon","Language":"Arabic","Genre":""}]},{"Id":"3399","TvPackage":"Basic Package","PackagePrice":"","PackageAdditionalSubscription":"","PackageDescription":"","ChannelLists":[{"ChannelName":"MTV Lebanon","Language":"Arabic","Genre":""}]}];

            var getPackageDetailsMock = [{"Genre":"Documentaries","Channels":["JSC Documentary","National Geographic Abu Dhabi HD"]},{"Genre":"General Entertainment","Channels":["Canal Algerie (Algeria TV)","Bahrain TV","Channel i","TVB 8","CCTV 4","Al Hayat Mosalsalaat","Al Hayat 2 ","Al Nahar","Al Nahar Drama","AlHayat","Dream","Dream 2","Blue Nile","Moga Comedy","Al Kahera W’al Nas","CBC Drama","Nile Drama","Nile Family & Children","Nile Life","Panorama Drama","Rotana Masriya","CBC","Al Nahar Drama 2","ARTE","TV5 Orient","ARD*","ZDF*","*Channels available only upon request","BVN TV","Amrita TV","JaiHind TV","B4U Plus","Jeevan TV","Nex 1 TV","Iran TV","IRIB3","Gem TV","TV1 Iran","TV2 Iran","TV3 Iran","TV5 Iran","Farsi 1","Al Iraqia","RAI1","RAI2","RAI3","Jordan TV","Arirang TV","KBS World","Mehwar ","Saudi 1","Saudi 2","Al Rai TV","Kuwait TV","Funoon TV","OTV Lebanon","Future TV","LDC","Ro’ya TV","LBC International","MTV Lebanon","NBN","New TV","Tele Liban","Al Maghribya","2M Maroc","Al Aoula Tv ","Oman TV","Urdu 1","Palestine TV","TV Polonia","Qatar TV","TV Romania","CCTV R","TV3 Russia*","*Channels available only upon request","Syria Drama","Hannibal TV","Nessma TV","Tunis 7","TRT 1","Fox","MBC Action","MBC 4","Abu Dhabi Drama","Al Aan","Al Dafrah TV","Al Manar","Al Masriya","Al Sharqiya","Jawna","MBC","Dubai One TV","AZTV","Lider TV","Servus TV *","*Channels available only upon request","Face 1","MBC Drama","MBC Masr","MBC Bollywood","CBC Sofra","MBC Masr +2","Mediaone","Baynounah TV","CBC Extra","Kappa TV","Yes IndiaVision","Qahera Wa Al Nas 2","CBC TV","ARY Zauq","GEM LIFE HD","Al Faraeen","WE TV","Hawas TV","Tolo HD","Lemar"]},{"Genre":"Islamic","Channels":["Al Nas TV","Majd","Huda TV","Peace TV","Saudi Quran","Saudi Sunnah","Al Resalah TV","ARY Qtv","Iqraa","Noor Dubai"]},{"Genre":"Kids","Channels":["Toyour Al Jannah","Karameesh TV","Jeem TV","Baraem","Space Toon","TRT Çocuk","Cartoon Network Arabia","MBC3","Majid Kids TV"]},{"Genre":"Lifestyle","Channels":["Ikono HD","Fatafeat","Physique TV HD","Citruss TV","Zee Living","Fashion One HD","Life TV"]},{"Genre":"Local & Regional News & Info","Channels":["Abu Dhabi TV HD","Ajman TV","Al Emarat HD","City 7","CNBC Arabia","Dubai TV HD","Sama Dubai","Sharjah TV"]},{"Genre":"Movies","Channels":["Panorama Film","Nile Cinema","Imagine Movies","B4U Aflam","Rotana","Rotana Aflam","Rotana Cinema","Rotana Classic","MBC Variety HD","MBC2","Fox Movies","Zee Alwan","Zee Aflam","MBC Max","ONYX HD"]},{"Genre":"Music","Channels":["Mazzika","AAA Music","PMC Music","Rotana Clip","Rotana Khalijiah","ARY Musik","TRT Müzik ","I-Concerts HD","Wanasah","Arabica Music","TV Persia","E 24","Channel 4","Music Plus","Hawacom","mifa HD","Lana TV","Clouds TV"]},{"Genre":"News","Channels":["People TV","CCTV News","Nile TV","On TV","Orascom TV","Al Tahrir","EuroNews","France 24","DW TV*","Manorama News","BBC Persia","NHK HD","Al Ekhbariya","Al Mayadeen","ARY News","Al Aqsa Tv","Al Jazeera","Al Jazeera Live","Al Jazeera International","Russia Today","TRT Arabia","Al Hurra","Al Arabiya","Al Arabiya Al Haddath","Sky News Arabia","BBC Arabic","BBC World","CNN International","E 24 News","Al Jazeera America","Mathrubhumi News","IndiaVision","Samaa TV","Rusiya Al-Yaum","Al Arab News","Al Baghdadia","Reporter TV","Tolo News"]},{"Genre":"Series","Channels":["Sada El Balad","RUBIX HD","RIVER HD","Dubai Drama","Dubai Zaman"]},{"Genre":"Sports ","Channels":["Bahrain Sports","Al Ahly Club","Nile Sports","Al Nahar Sports","Saudi Sports","Kuwait Sports","Al Jazeera Sports News","beIN SPORTS","Al Kass wa Al Dawri","Abu Dhabi Sports HD","Abu Dhabi Sports2 HD","YAS Sports HD","Dubai Sports","Dubai Racing HD","Dubai Sports HD","Eurosport News","Dubai Sports 3","Dubai Sports 4","MBC Pro Sports 1 HD","MBC Pro Sports 2 HD","MBC Pro Sports 3 HD","MBC Pro Sports 4","Gear One","Oman Sports","OSN WWE Network"]}];

            var getPrintItemsMock = [{"Id":"3383","TvPackage":"AD Sports Extra","PackagePrice":"AED 69 per month","PackageAdditionalSubscription":"AED 69 per month","PackageDescription":"AED 69 per month","ChannelLists":[{"ChannelName":"AD + Drama HD","Language":"Arabic","Genre":"Series"},{"ChannelName":"AD Sports 3 HD","Language":"Arabic, English","Genre":"Sports"},{"ChannelName":"AD Sports 4 HD","Language":"Arabic, English","Genre":"Sports"},{"ChannelName":"AD Sports 5 HD","Language":"Arabic, English","Genre":"Sports"},{"ChannelName":"AD Sports 6 HD","Language":"Arabic, English","Genre":"Sports"},{"ChannelName":"Dubai Sports 2","Language":"Arabic, English","Genre":"Sports"},{"ChannelName":"JimJam","Language":"English","Genre":"Kids "},{"ChannelName":"Nat Geo People HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"Nat Geo Wild HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"OSN Sports 4","Language":"English","Genre":"Sports"},{"ChannelName":"Sky News HD","Language":"English","Genre":"News "},{"ChannelName":"Star Movies HD","Language":"English","Genre":"Movies"},{"ChannelName":"Star World HD","Language":"English","Genre":"General Entertainment "}]},{"Id":"3382","TvPackage":"New AD Sports","PackagePrice":"AED 39 per month","PackageAdditionalSubscription":"AED 39 per month","PackageDescription":"AED 39 per month","ChannelLists":[{"ChannelName":"AD + Drama HD","Language":"Arabic","Genre":"Series"},{"ChannelName":"AD Sports 3 HD","Language":"Arabic, English","Genre":"Sports"},{"ChannelName":"AD Sports 4 HD","Language":"Arabic, English","Genre":"Sports"},{"ChannelName":"AD Sports 5 HD","Language":"Arabic, English","Genre":"Sports"},{"ChannelName":"AD Sports 6 HD","Language":"Arabic, English","Genre":"Sports"},{"ChannelName":"Dubai Sports 2","Language":"Arabic, English","Genre":"Sports"}]},{"Id":"3397","TvPackage":"Alfa Arabic","PackagePrice":"AED 60 per month","PackageAdditionalSubscription":"AED 60 per month","PackageDescription":"AED 60 per month","ChannelLists":[{"ChannelName":"Al Safwa","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"Al Yawm","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"Arabic Series Channel","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"Arabic Series Channel +4","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"Cinema 1","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Cinema 2","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Fann","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"Music Now","Language":"English","Genre":"Music"}]},{"Id":"3413","TvPackage":"Arabic Package","PackagePrice":"AED 39 per month","PackageAdditionalSubscription":"AED 39 per month","PackageDescription":"AED 39 per month","ChannelLists":[]},{"Id":"3398","TvPackage":"ART Family","PackagePrice":"AED 25 per month","PackageAdditionalSubscription":"AED 25 per month","PackageDescription":"AED 25 per month","ChannelLists":[{"ChannelName":"ART Aflam 1","Language":"Arabic","Genre":"Movies"},{"ChannelName":"ART Aflam 2","Language":"Arabic","Genre":"Movies"},{"ChannelName":"ART Cinema","Language":"Arabic","Genre":"Movies"},{"ChannelName":"ART Hekayat ","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"ART Hekayat 2","Language":"Arabic","Genre":"General Entertainment"}]},{"Id":"3399","TvPackage":"Basic Package","PackagePrice":null,"PackageAdditionalSubscription":null,"PackageDescription":null,"ChannelLists":[{"ChannelName":"2M Maroc","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"AAA Music","Language":"Persian","Genre":"Music"},{"ChannelName":"Abu Dhabi Drama","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Abu Dhabi Sports HD","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Abu Dhabi Sports2 HD","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Abu Dhabi TV HD","Language":"Arabic","Genre":"Local & Regional News & Info"},{"ChannelName":"Ajman TV","Language":"Arabic","Genre":"Local & Regional News & Info"},{"ChannelName":"Al Aan","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Ahly Club","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Al Aoula Tv ","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Aqsa Tv","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Arab News","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Arabiya","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Arabiya Al Haddath","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Baghdadia","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Dafrah TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Ekhbariya","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Emarat HD","Language":"Arabic","Genre":"Local & Regional News & Info"},{"ChannelName":"Al Faraeen","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Hayat 2 ","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Hayat Mosalsalaat","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Hurra","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Iraqia","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Jazeera","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Jazeera America","Language":"English","Genre":"News"},{"ChannelName":"Al Jazeera International","Language":"English","Genre":"News"},{"ChannelName":"Al Jazeera Live","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Jazeera Sports News","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Al Kahera W’al Nas","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Kass wa Al Dawri","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Al Maghribya","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Manar","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Masriya","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Mayadeen","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Nahar","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Nahar Drama","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Nahar Drama 2","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Nahar Sports","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Al Nas TV","Language":"Arabic","Genre":"Islamic"},{"ChannelName":"Al Rai TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Resalah TV","Language":"Arabic","Genre":"Islamic"},{"ChannelName":"Al Sharqiya","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Tahrir","Language":"Arabic","Genre":"News"},{"ChannelName":"AlHayat","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Amrita TV","Language":"Malayalam","Genre":"General Entertainment"},{"ChannelName":"Arabica Music","Language":"Arabic","Genre":"Music"},{"ChannelName":"ARD","Language":"German","Genre":"General Entertainment"},{"ChannelName":"Arirang TV","Language":"Korean","Genre":"General Entertainment"},{"ChannelName":"ARTE","Language":"French","Genre":"General Entertainment"},{"ChannelName":"ARY Musik","Language":"Urdu","Genre":"Music"},{"ChannelName":"ARY News","Language":"Urdu","Genre":"News"},{"ChannelName":"ARY Qtv","Language":"Urdu","Genre":"Islamic"},{"ChannelName":"ARY Zauq","Language":"Urdu","Genre":"General Entertainment"},{"ChannelName":"AZTV","Language":"Azerbaijani","Genre":"General Entertainment"},{"ChannelName":"B4U Aflam","Language":"Hindi","Genre":"Movies"},{"ChannelName":"B4U Plus","Language":"Hindi","Genre":"General Entertainment"},{"ChannelName":"Bahrain Sports","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Bahrain TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Baraem","Language":"Arabic","Genre":"Kids"},{"ChannelName":"Baynounah TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"BBC Arabic","Language":"Arabic","Genre":"News"},{"ChannelName":"BBC Persia","Language":"Persian","Genre":"News"},{"ChannelName":"BBC World","Language":"English","Genre":"News"},{"ChannelName":"beIN SPORTS","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Blue Nile","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"BVN TV","Language":"Dutch","Genre":"General Entertainment"},{"ChannelName":"Canal Algerie (Algeria TV)","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Cartoon Network Arabia","Language":"Arabic","Genre":"Kids"},{"ChannelName":"CBC","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"CBC Drama","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"CBC Extra","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"CBC Sofra","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"CBC TV","Language":"Azerbaijani","Genre":"General Entertainment"},{"ChannelName":"CCTV 4","Language":"Mandarin","Genre":"General Entertainment"},{"ChannelName":"CCTV News","Language":"English","Genre":"News"},{"ChannelName":"CCTV R","Language":"Russian","Genre":"General Entertainment"},{"ChannelName":"Channel 4","Language":"English","Genre":"Music"},{"ChannelName":"Channel i","Language":"Bengali","Genre":"General Entertainment"},{"ChannelName":"Channels available only upon request","Language":"German","Genre":"General Entertainment"},{"ChannelName":"Channels available only upon request","Language":"German","Genre":"General Entertainment"},{"ChannelName":"Channels available only upon request","Language":"Russian","Genre":"General Entertainment"},{"ChannelName":"Citruss TV","Language":"English","Genre":"Lifestyle"},{"ChannelName":"City 7","Language":"English","Genre":"Local & Regional News & Info"},{"ChannelName":"Clouds TV","Language":"Sawhili","Genre":"Music"},{"ChannelName":"CNBC Arabia","Language":"Arabic","Genre":"Local & Regional News & Info"},{"ChannelName":"CNN International","Language":"English","Genre":"News"},{"ChannelName":"Dream","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Dream 2","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Dubai Drama","Language":"Arabic","Genre":"Series"},{"ChannelName":"Dubai One TV","Language":"English","Genre":"General Entertainment"},{"ChannelName":"Dubai Racing HD","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Dubai Sports","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Dubai Sports 3","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Dubai Sports 4","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Dubai Sports HD","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Dubai TV HD","Language":"Arabic","Genre":"Local & Regional News & Info"},{"ChannelName":"Dubai Zaman","Language":"Arabic","Genre":"Series"},{"ChannelName":"DW TV","Language":"German","Genre":"News"},{"ChannelName":"E 24","Language":"Hindi","Genre":"Music"},{"ChannelName":"E 24 News","Language":"Hindi","Genre":"News"},{"ChannelName":"EuroNews","Language":"English","Genre":"News"},{"ChannelName":"Eurosport News","Language":"English","Genre":"Sports"},{"ChannelName":"Face 1","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"Farsi 1","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"Fashion One HD","Language":"English","Genre":"Lifestyle"},{"ChannelName":"Fatafeat","Language":"Arabic","Genre":"Lifestyle"},{"ChannelName":"Fox","Language":"English","Genre":"General Entertainment"},{"ChannelName":"Fox Movies","Language":"English","Genre":"Movies"},{"ChannelName":"France 24","Language":"English","Genre":"News"},{"ChannelName":"Funoon TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Future TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Gear One","Language":"Arabic","Genre":"Sports"},{"ChannelName":"GEM LIFE HD","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"Gem TV","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"Hannibal TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Hawacom","Language":"Arabic","Genre":"Music"},{"ChannelName":"Hawas TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Huda TV","Language":"English","Genre":"Islamic"},{"ChannelName":"I-Concerts HD","Language":"English","Genre":"Music"},{"ChannelName":"Ikono HD","Language":"English","Genre":"Lifestyle"},{"ChannelName":"Imagine Movies","Language":"Hindi","Genre":"Movies"},{"ChannelName":"IndiaVision","Language":"Hindi","Genre":"News"},{"ChannelName":"Iqraa","Language":"Arabic","Genre":"Islamic"},{"ChannelName":"Iran TV","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"IRIB3","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"JaiHind TV","Language":"Malayalam","Genre":"General Entertainment"},{"ChannelName":"Jawna","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Jeem TV","Language":"Arabic","Genre":"Kids"},{"ChannelName":"Jeevan TV","Language":"Malayalam","Genre":"General Entertainment"},{"ChannelName":"Jordan TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"JSC Documentary","Language":"Arabic","Genre":"Documentaries"},{"ChannelName":"Kappa TV","Language":"Bengali","Genre":"General Entertainment"},{"ChannelName":"Karameesh TV","Language":"Arabic","Genre":"Kids"},{"ChannelName":"KBS World","Language":"Korean","Genre":"General Entertainment"},{"ChannelName":"Kuwait Sports","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Kuwait TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Lana TV","Language":"Arabic","Genre":"Music"},{"ChannelName":"LBC International","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"LDC","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Lemar","Language":"Pashtu","Genre":"General Entertainment"},{"ChannelName":"Lider TV","Language":"Azerbaijani","Genre":"General Entertainment"},{"ChannelName":"Life TV","Language":"English","Genre":"Lifestyle"},{"ChannelName":"Majd","Language":"Arabic","Genre":"Islamic"},{"ChannelName":"Majid Kids TV","Language":"Arabic","Genre":"Kids"},{"ChannelName":"Manorama News","Language":"Malayalam","Genre":"News"},{"ChannelName":"Mathrubhumi News","Language":"Malayalam","Genre":"News"},{"ChannelName":"Mazzika","Language":"Arabic","Genre":"Music"},{"ChannelName":"MBC","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"MBC 4","Language":"English","Genre":"General Entertainment"},{"ChannelName":"MBC Action","Language":"English","Genre":"General Entertainment"},{"ChannelName":"MBC Bollywood","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"MBC Drama","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"MBC Masr","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"MBC Masr +2","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"MBC Max","Language":"English","Genre":"Movies"},{"ChannelName":"MBC Pro Sports 1 HD","Language":"Arabic","Genre":"Sports"},{"ChannelName":"MBC Pro Sports 2 HD","Language":"Arabic","Genre":"Sports"},{"ChannelName":"MBC Pro Sports 3 HD","Language":"Arabic","Genre":"Sports"},{"ChannelName":"MBC Pro Sports 4","Language":"Arabic","Genre":"Sports"},{"ChannelName":"MBC Variety HD","Language":"English","Genre":"Movies"},{"ChannelName":"MBC2","Language":"English","Genre":"Movies"},{"ChannelName":"MBC3","Language":"Arabic","Genre":"Kids"},{"ChannelName":"Mediaone","Language":"Malayalam","Genre":"General Entertainment"},{"ChannelName":"Mehwar ","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"mifa HD","Language":"Persian","Genre":"Music"},{"ChannelName":"Moga Comedy","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"MTV Lebanon","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Music Plus","Language":"Arabic","Genre":"Music"},{"ChannelName":"National Geographic Abu Dhabi HD","Language":"Arabic","Genre":"Documentaries"},{"ChannelName":"NBN","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Nessma TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"New TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Nex 1 TV","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"NHK HD","Language":"English","Genre":"News"},{"ChannelName":"Nile Cinema","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Nile Drama","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Nile Family & Children","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Nile Life","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Nile Sports","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Nile TV","Language":"Arabic","Genre":"News"},{"ChannelName":"Noor Dubai","Language":"Arabic","Genre":"Islamic"},{"ChannelName":"Oman Sports","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Oman TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"On TV","Language":"Arabic","Genre":"News"},{"ChannelName":"ONYX HD","Language":"Persian","Genre":"Movies"},{"ChannelName":"Orascom TV","Language":"Arabic","Genre":"News"},{"ChannelName":"OSN WWE Network","Language":"English","Genre":"Sports "},{"ChannelName":"OTV Lebanon","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Palestine TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Panorama Drama","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Panorama Film","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Peace TV","Language":"Arabic","Genre":"Islamic"},{"ChannelName":"People TV","Language":"Malayalam","Genre":"News"},{"ChannelName":"Physique TV HD","Language":"English","Genre":"Lifestyle"},{"ChannelName":"PMC Music","Language":"Persian","Genre":"Music"},{"ChannelName":"Qahera Wa Al Nas 2","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Qatar TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"RAI1","Language":"Italian","Genre":"General Entertainment"},{"ChannelName":"RAI2","Language":"Italian","Genre":"General Entertainment"},{"ChannelName":"RAI3","Language":"Italian","Genre":"General Entertainment"},{"ChannelName":"Reporter TV","Language":"Malayalam","Genre":"News"},{"ChannelName":"RIVER HD","Language":"Persian","Genre":"Series"},{"ChannelName":"Rotana","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Rotana Aflam","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Rotana Cinema","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Rotana Classic","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Rotana Clip","Language":"Arabic","Genre":"Music"},{"ChannelName":"Rotana Khalijiah","Language":"Arabic","Genre":"Music"},{"ChannelName":"Rotana Masriya","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Roya TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"RUBIX HD","Language":"Persian","Genre":"Series"},{"ChannelName":"Rusiya Al-Yaum","Language":"Arabic","Genre":"News"},{"ChannelName":"Russia Today","Language":"English","Genre":"News"},{"ChannelName":"Sada El Balad","Language":"Arabic","Genre":"Series"},{"ChannelName":"Sama Dubai","Language":"Arabic","Genre":"Local & Regional News & Info"},{"ChannelName":"Samaa TV","Language":"Urdu","Genre":"News"},{"ChannelName":"Saudi 1","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Saudi 2","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Saudi Quran","Language":"Arabic","Genre":"Islamic"},{"ChannelName":"Saudi Sports","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Saudi Sunnah","Language":"Arabic","Genre":"Islamic"},{"ChannelName":"Servus TV ","Language":"German","Genre":"General Entertainment"},{"ChannelName":"Sharjah TV","Language":"Arabic","Genre":"Local & Regional News & Info"},{"ChannelName":"Sky News Arabia","Language":"Arabic","Genre":"News"},{"ChannelName":"Space Toon","Language":"Arabic","Genre":"Kids"},{"ChannelName":"Syria Drama","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Tele Liban","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Tolo HD","Language":"Dari","Genre":"General Entertainment"},{"ChannelName":"Tolo News","Language":"Dari","Genre":"News"},{"ChannelName":"Toyour Al Jannah","Language":"Arabic","Genre":"Kids"},{"ChannelName":"TRT 1","Language":"Turkish","Genre":"General Entertainment"},{"ChannelName":"TRT Arabia","Language":"Arabic","Genre":"News"},{"ChannelName":"TRT Çocuk","Language":"Turkish","Genre":"Kids"},{"ChannelName":"TRT Müzik ","Language":"Turkish","Genre":"Music"},{"ChannelName":"Tunis 7","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"TV Persia","Language":"Persian","Genre":"Music"},{"ChannelName":"TV Polonia","Language":"Polish","Genre":"General Entertainment"},{"ChannelName":"TV Romania","Language":"Romanian","Genre":"General Entertainment"},{"ChannelName":"TV1 Iran","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"TV2 Iran","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"TV3 Iran","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"TV3 Russia","Language":"Russian","Genre":"General Entertainment"},{"ChannelName":"TV5 Iran","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"TV5 Orient","Language":"French","Genre":"General Entertainment"},{"ChannelName":"TVB 8","Language":"Mandarin","Genre":"General Entertainment"},{"ChannelName":"Urdu 1","Language":"Urdu","Genre":"General Entertainment"},{"ChannelName":"Wanasah","Language":"Arabic","Genre":"Music"},{"ChannelName":"WE TV","Language":"Malayalam","Genre":"General Entertainment"},{"ChannelName":"YAS Sports HD","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Yes IndiaVision","Language":"Hindi","Genre":"General Entertainment"},{"ChannelName":"ZDF","Language":"German","Genre":"General Entertainment"},{"ChannelName":"Zee Aflam","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Zee Alwan","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Zee Living","Language":"English","Genre":"Lifestyle"}]},{"Id":"3384","TvPackage":"Channel Name","PackagePrice":null,"PackageAdditionalSubscription":null,"PackageDescription":null,"ChannelLists":[{"ChannelName":"*Channels available only upon request","Language":"German","Genre":"General Entertainment"},{"ChannelName":"*Channels available only upon request","Language":"Russian","Genre":"General Entertainment"},{"ChannelName":"*Channels available only upon request","Language":"German","Genre":"General Entertainment"},{"ChannelName":"2M Maroc","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"AAA Music","Language":"Persian","Genre":"Music"},{"ChannelName":"Abu Dhabi Drama","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Abu Dhabi Sports HD","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Abu Dhabi Sports2 HD","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Abu Dhabi TV HD","Language":"Arabic","Genre":"Local & Regional News & Info"},{"ChannelName":"Ajman TV","Language":"Arabic","Genre":"Local & Regional News & Info"},{"ChannelName":"Al Aan","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Ahly Club","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Al Aoula Tv ","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Aqsa Tv","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Arab News","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Arabiya","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Arabiya Al Haddath","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Baghdadia","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Dafrah TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Ekhbariya","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Emarat HD","Language":"Arabic","Genre":"Local & Regional News & Info"},{"ChannelName":"Al Faraeen","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Hayat 2 ","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Hayat Mosalsalaat","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Hurra","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Iraqia","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Jazeera","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Jazeera America","Language":"English","Genre":"News"},{"ChannelName":"Al Jazeera International","Language":"English","Genre":"News"},{"ChannelName":"Al Jazeera Live","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Jazeera Sports News","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Al Kahera W’al Nas","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Kass wa Al Dawri","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Al Maghribya","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Manar","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Masriya","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Mayadeen","Language":"Arabic","Genre":"News"},{"ChannelName":"Al Nahar","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Nahar Drama","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Nahar Drama 2","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Nahar Sports","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Al Nas TV","Language":"Arabic","Genre":"Islamic"},{"ChannelName":"Al Rai TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Resalah TV","Language":"Arabic","Genre":"Islamic"},{"ChannelName":"Al Sharqiya","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Al Tahrir","Language":"Arabic","Genre":"News"},{"ChannelName":"AlHayat","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Amrita TV","Language":"Malayalam","Genre":"General Entertainment"},{"ChannelName":"Arabica Music","Language":"Arabic","Genre":"Music"},{"ChannelName":"ARD*","Language":"German","Genre":"General Entertainment"},{"ChannelName":"Arirang TV","Language":"Korean","Genre":"General Entertainment"},{"ChannelName":"ARTE","Language":"French","Genre":"General Entertainment"},{"ChannelName":"ARY Musik","Language":"Urdu","Genre":"Music"},{"ChannelName":"ARY News","Language":"Urdu","Genre":"News"},{"ChannelName":"ARY Qtv","Language":"Urdu","Genre":"Islamic"},{"ChannelName":"ARY Zauq","Language":"Urdu","Genre":"General Entertainment"},{"ChannelName":"AZTV","Language":"Azerbaijani","Genre":"General Entertainment"},{"ChannelName":"B4U Aflam","Language":"Hindi","Genre":"Movies"},{"ChannelName":"B4U Plus","Language":"Hindi","Genre":"General Entertainment"},{"ChannelName":"Bahrain Sports","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Bahrain TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Baraem","Language":"Arabic","Genre":"Kids"},{"ChannelName":"Baynounah TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"BBC Arabic","Language":"Arabic","Genre":"News"},{"ChannelName":"BBC Persia","Language":"Persian","Genre":"News"},{"ChannelName":"BBC World","Language":"English","Genre":"News"},{"ChannelName":"beIN SPORTS","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Blue Nile","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"BVN TV","Language":"Dutch","Genre":"General Entertainment"},{"ChannelName":"Canal Algerie (Algeria TV)","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Cartoon Network Arabia","Language":"Arabic","Genre":"Kids"},{"ChannelName":"CBC","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"CBC Drama","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"CBC Extra","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"CBC Sofra","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"CBC TV","Language":"Azerbaijani","Genre":"General Entertainment"},{"ChannelName":"CCTV 4","Language":"Mandarin","Genre":"General Entertainment"},{"ChannelName":"CCTV News","Language":"English","Genre":"News"},{"ChannelName":"CCTV R","Language":"Russian","Genre":"General Entertainment"},{"ChannelName":"Channel 4","Language":"English","Genre":"Music"},{"ChannelName":"Channel i","Language":"Bengali","Genre":"General Entertainment"},{"ChannelName":"Citruss TV","Language":"English","Genre":"Lifestyle"},{"ChannelName":"City 7","Language":"English","Genre":"Local & Regional News & Info"},{"ChannelName":"Clouds TV","Language":"Sawhili","Genre":"Music"},{"ChannelName":"CNBC Arabia","Language":"Arabic","Genre":"Local & Regional News & Info"},{"ChannelName":"CNN International","Language":"English","Genre":"News"},{"ChannelName":"Dream","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Dream 2","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Dubai Drama","Language":"Arabic","Genre":"Series"},{"ChannelName":"Dubai One TV","Language":"English","Genre":"General Entertainment"},{"ChannelName":"Dubai Racing HD","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Dubai Sports","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Dubai Sports 3","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Dubai Sports 4","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Dubai Sports HD","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Dubai TV HD","Language":"Arabic","Genre":"Local & Regional News & Info"},{"ChannelName":"Dubai Zaman","Language":"Arabic","Genre":"Series"},{"ChannelName":"DW TV*","Language":"German","Genre":"News"},{"ChannelName":"E 24","Language":"Hindi","Genre":"Music"},{"ChannelName":"E 24 News","Language":"Hindi","Genre":"News"},{"ChannelName":"EuroNews","Language":"English","Genre":"News"},{"ChannelName":"Eurosport News","Language":"English","Genre":"Sports"},{"ChannelName":"Face 1","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"Farsi 1","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"Fashion One HD","Language":"English","Genre":"Lifestyle"},{"ChannelName":"Fatafeat","Language":"Arabic","Genre":"Lifestyle"},{"ChannelName":"Fox","Language":"English","Genre":"General Entertainment"},{"ChannelName":"Fox Movies","Language":"English","Genre":"Movies"},{"ChannelName":"France 24","Language":"English","Genre":"News"},{"ChannelName":"Funoon TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Future TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Gear One","Language":"Arabic","Genre":"Sports"},{"ChannelName":"GEM LIFE HD","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"Gem TV","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"Hannibal TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Hawacom","Language":"Arabic","Genre":"Music"},{"ChannelName":"Hawas TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Huda TV","Language":"English","Genre":"Islamic"},{"ChannelName":"I-Concerts HD","Language":"English","Genre":"Music"},{"ChannelName":"Ikono HD","Language":"English","Genre":"Lifestyle"},{"ChannelName":"Imagine Movies","Language":"Hindi","Genre":"Movies"},{"ChannelName":"IndiaVision","Language":"Hindi","Genre":"News"},{"ChannelName":"Iqraa","Language":"Arabic","Genre":"Islamic"},{"ChannelName":"Iran TV","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"IRIB3","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"JaiHind TV","Language":"Malayalam","Genre":"General Entertainment"},{"ChannelName":"Jawna","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Jeem TV","Language":"Arabic","Genre":"Kids"},{"ChannelName":"Jeevan TV","Language":"Malayalam","Genre":"General Entertainment"},{"ChannelName":"Jordan TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"JSC Documentary","Language":"Arabic","Genre":"Documentaries"},{"ChannelName":"Kappa TV","Language":"Bengali","Genre":"General Entertainment"},{"ChannelName":"Karameesh TV","Language":"Arabic","Genre":"Kids"},{"ChannelName":"KBS World","Language":"Korean","Genre":"General Entertainment"},{"ChannelName":"Kuwait Sports","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Kuwait TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Lana TV","Language":"Arabic","Genre":"Music"},{"ChannelName":"LBC International","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"LDC","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Lemar","Language":"Pashtu","Genre":"General Entertainment"},{"ChannelName":"Lider TV","Language":"Azerbaijani","Genre":"General Entertainment"},{"ChannelName":"Life TV","Language":"English","Genre":"Lifestyle"},{"ChannelName":"Majd","Language":"Arabic","Genre":"Islamic"},{"ChannelName":"Majid Kids TV","Language":"Arabic","Genre":"Kids"},{"ChannelName":"Manorama News","Language":"Malayalam","Genre":"News"},{"ChannelName":"Mathrubhumi News","Language":"Malayalam","Genre":"News"},{"ChannelName":"Mazzika","Language":"Arabic","Genre":"Music"},{"ChannelName":"MBC","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"MBC 4","Language":"English","Genre":"General Entertainment"},{"ChannelName":"MBC Action","Language":"English","Genre":"General Entertainment"},{"ChannelName":"MBC Bollywood","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"MBC Drama","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"MBC Masr","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"MBC Masr +2","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"MBC Max","Language":"English","Genre":"Movies"},{"ChannelName":"MBC Pro Sports 1 HD","Language":"Arabic","Genre":"Sports"},{"ChannelName":"MBC Pro Sports 2 HD","Language":"Arabic","Genre":"Sports"},{"ChannelName":"MBC Pro Sports 3 HD","Language":"Arabic","Genre":"Sports"},{"ChannelName":"MBC Pro Sports 4","Language":"Arabic","Genre":"Sports"},{"ChannelName":"MBC Variety HD","Language":"English","Genre":"Movies"},{"ChannelName":"MBC2","Language":"English","Genre":"Movies"},{"ChannelName":"MBC3","Language":"Arabic","Genre":"Kids"},{"ChannelName":"Mediaone","Language":"Malayalam","Genre":"General Entertainment"},{"ChannelName":"Mehwar ","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"mifa HD","Language":"Persian","Genre":"Music"},{"ChannelName":"Moga Comedy","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"MTV Lebanon","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Music Plus","Language":"Arabic","Genre":"Music"},{"ChannelName":"National Geographic Abu Dhabi HD","Language":"Arabic","Genre":"Documentaries"},{"ChannelName":"NBN","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Nessma TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"New TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Nex 1 TV","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"NHK HD","Language":"English","Genre":"News"},{"ChannelName":"Nile Cinema","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Nile Drama","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Nile Family & Children","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Nile Life","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Nile Sports","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Nile TV","Language":"Arabic","Genre":"News"},{"ChannelName":"Noor Dubai","Language":"Arabic","Genre":"Islamic"},{"ChannelName":"Oman Sports","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Oman TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"On TV","Language":"Arabic","Genre":"News"},{"ChannelName":"ONYX HD","Language":"Persian","Genre":"Movies"},{"ChannelName":"Orascom TV","Language":"Arabic","Genre":"News"},{"ChannelName":"OSN WWE Network","Language":"English","Genre":"Sports "},{"ChannelName":"OTV Lebanon","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Palestine TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Panorama Drama","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Panorama Film","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Peace TV","Language":"Arabic","Genre":"Islamic"},{"ChannelName":"People TV","Language":"Malayalam","Genre":"News"},{"ChannelName":"Physique TV HD","Language":"English","Genre":"Lifestyle"},{"ChannelName":"PMC Music","Language":"Persian","Genre":"Music"},{"ChannelName":"Qahera Wa Al Nas 2","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Qatar TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"RAI1","Language":"Italian","Genre":"General Entertainment"},{"ChannelName":"RAI2","Language":"Italian","Genre":"General Entertainment"},{"ChannelName":"RAI3","Language":"Italian","Genre":"General Entertainment"},{"ChannelName":"Reporter TV","Language":"Malayalam","Genre":"News"},{"ChannelName":"RIVER HD","Language":"Persian","Genre":"Series"},{"ChannelName":"Ro’ya TV","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Rotana","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Rotana Aflam","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Rotana Cinema","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Rotana Classic","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Rotana Clip","Language":"Arabic","Genre":"Music"},{"ChannelName":"Rotana Khalijiah","Language":"Arabic","Genre":"Music"},{"ChannelName":"Rotana Masriya","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"RUBIX HD","Language":"Persian","Genre":"Series"},{"ChannelName":"Rusiya Al-Yaum","Language":"Arabic","Genre":"News"},{"ChannelName":"Russia Today","Language":"English","Genre":"News"},{"ChannelName":"Sada El Balad","Language":"Arabic","Genre":"Series"},{"ChannelName":"Sama Dubai","Language":"Arabic","Genre":"Local & Regional News & Info"},{"ChannelName":"Samaa TV","Language":"Urdu","Genre":"News"},{"ChannelName":"Saudi 1","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Saudi 2","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Saudi Quran","Language":"Arabic","Genre":"Islamic"},{"ChannelName":"Saudi Sports","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Saudi Sunnah","Language":"Arabic","Genre":"Islamic"},{"ChannelName":"Servus TV *","Language":"German","Genre":"General Entertainment"},{"ChannelName":"Sharjah TV","Language":"Arabic","Genre":"Local & Regional News & Info"},{"ChannelName":"Sky News Arabia","Language":"Arabic","Genre":"News"},{"ChannelName":"Space Toon","Language":"Arabic","Genre":"Kids"},{"ChannelName":"Syria Drama","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Tele Liban","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"Tolo HD","Language":"Dari","Genre":"General Entertainment"},{"ChannelName":"Tolo News","Language":"Dari","Genre":"News"},{"ChannelName":"Toyour Al Jannah","Language":"Arabic","Genre":"Kids"},{"ChannelName":"TRT 1","Language":"Turkish","Genre":"General Entertainment"},{"ChannelName":"TRT Arabia","Language":"Arabic","Genre":"News"},{"ChannelName":"TRT Çocuk","Language":"Turkish","Genre":"Kids"},{"ChannelName":"TRT Müzik ","Language":"Turkish","Genre":"Music"},{"ChannelName":"Tunis 7","Language":"Arabic","Genre":"General Entertainment"},{"ChannelName":"TV Persia","Language":"Persian","Genre":"Music"},{"ChannelName":"TV Polonia","Language":"Polish","Genre":"General Entertainment"},{"ChannelName":"TV Romania","Language":"Romanian","Genre":"General Entertainment"},{"ChannelName":"TV1 Iran","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"TV2 Iran","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"TV3 Iran","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"TV3 Russia*","Language":"Russian","Genre":"General Entertainment"},{"ChannelName":"TV5 Iran","Language":"Persian","Genre":"General Entertainment"},{"ChannelName":"TV5 Orient","Language":"French","Genre":"General Entertainment"},{"ChannelName":"TVB 8","Language":"Mandarin","Genre":"General Entertainment"},{"ChannelName":"Urdu 1","Language":"Urdu","Genre":"General Entertainment"},{"ChannelName":"Wanasah","Language":"Arabic","Genre":"Music"},{"ChannelName":"WE TV","Language":"Malayalam","Genre":"General Entertainment"},{"ChannelName":"YAS Sports HD","Language":"Arabic","Genre":"Sports"},{"ChannelName":"Yes IndiaVision","Language":"Hindi","Genre":"General Entertainment"},{"ChannelName":"ZDF*","Language":"German","Genre":"General Entertainment"},{"ChannelName":"Zee Aflam","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Zee Alwan","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Zee Living","Language":"English","Genre":"Lifestyle"}]},{"Id":"3415","TvPackage":"Euro 2016","PackagePrice":"AED 255 (one time charge)","PackageAdditionalSubscription":"AED 255 (one time charge)","PackageDescription":"AED 255 (one time charge)","ChannelLists":[]},{"Id":"3400","TvPackage":"English Package","PackagePrice":"AED 39 per month","PackageAdditionalSubscription":"AED 39 per month","PackageDescription":"AED 39 per month","ChannelLists":[{"ChannelName":"Animaux","Language":"English","Genre":"Documentary"},{"ChannelName":"AWE TV HD","Language":"English","Genre":"Lifestyle"},{"ChannelName":"Baby TV HD","Language":"English","Genre":"Kids"},{"ChannelName":"CBS Reality","Language":"English","Genre":"Lifestyle"},{"ChannelName":"Channel News Asia","Language":"English","Genre":"News"},{"ChannelName":"Channel V International","Language":"English","Genre":"Music"},{"ChannelName":"Clouds TV","Language":"English","Genre":"Music"},{"ChannelName":"C-Music TV","Language":"English","Genre":"Music"},{"ChannelName":"DocuBOX HD","Language":"English","Genre":"Documentary"},{"ChannelName":"Duck TV HD","Language":"English","Genre":"Kids"},{"ChannelName":"ENGLISH Club","Language":"English","Genre":"Lifestyle"},{"ChannelName":"Extreme Sports","Language":"English","Genre":"Sports"},{"ChannelName":"Fast&FunBOX HD","Language":"English","Genre":"Sports"},{"ChannelName":"FightBOX HD","Language":"English","Genre":"Sports"},{"ChannelName":"Fix & Foxi","Language":"English","Genre":"Kids"},{"ChannelName":"Ginx TV","Language":"English","Genre":"Kids"},{"ChannelName":"H2","Language":"English","Genre":"Documentary"},{"ChannelName":"My Music Always HD","Language":"English","Genre":"Music"},{"ChannelName":"My Music Tropical HD","Language":"English","Genre":"Music"},{"ChannelName":"Nat Geo Music HD","Language":"English","Genre":"Music"},{"ChannelName":"Nat Geo People HD","Language":"English","Genre":"Documentary"},{"ChannelName":"Nat Geo Wild HD","Language":"English","Genre":"Documentary"},{"ChannelName":"National Geographic HD","Language":"English","Genre":"Documentary"},{"ChannelName":"Nautical Channel","Language":"English","Genre":"Sports"},{"ChannelName":"OANN HD","Language":"English","Genre":"News"},{"ChannelName":"Physique TV HD","Language":"English","Genre":"Lifestyle"},{"ChannelName":"Pursuit Channel HD","Language":"English","Genre":"Lifestyle"},{"ChannelName":"RT Documentary HD","Language":"English","Genre":"Documentary"},{"ChannelName":"Star Movies HD","Language":"English","Genre":"Movies"},{"ChannelName":"Star World HD","Language":"English","Genre":"Series"},{"ChannelName":"Ultra Film","Language":"English","Genre":"Movies"},{"ChannelName":"Untamed Sports TV","Language":"English","Genre":"Sports"}]},{"Id":"3411","TvPackage":"English Package","PackagePrice":"AED 39 per month","PackageAdditionalSubscription":"AED 39 per month","PackageDescription":"AED 39 per month","ChannelLists":[]},{"Id":"3414","TvPackage":"French Package","PackagePrice":"AED 39 per month","PackageAdditionalSubscription":"AED 39 per month","PackageDescription":"AED 39 per month","ChannelLists":[]},{"Id":"3401","TvPackage":"Japanese Satellite TV","PackagePrice":"AED 185 per month","PackageAdditionalSubscription":"AED 185 per month","PackageDescription":"AED 185 per month","ChannelLists":[{"ChannelName":"JSTV","Language":"Japanese","Genre":"Entertainment"},{"ChannelName":"JSTV 2","Language":"Japanese","Genre":"Entertainment"},{"ChannelName":"JSTV Radio","Language":"Japanese","Genre":"Radio"}]},{"Id":"3402","TvPackage":"MYGMA","PackagePrice":"AED 99 per month","PackageAdditionalSubscription":"AED 99 per month","PackageDescription":"AED 99 per month","ChannelLists":[{"ChannelName":"Animaux Eng","Language":"English","Genre":"Documentary"},{"ChannelName":"AWE TV HD","Language":"English","Genre":"Lifestyle"},{"ChannelName":"CBS Reality","Language":"English","Genre":"Lifestyle"},{"ChannelName":"Channel News Asia","Language":"English","Genre":"News"},{"ChannelName":"C-Music TV","Language":"English","Genre":"Music"},{"ChannelName":"DocuBOX HD","Language":"English","Genre":"Documentary"},{"ChannelName":"Duck TV HD","Language":"English","Genre":"Kids"},{"ChannelName":"Extreme Sports","Language":"English","Genre":"Sports"},{"ChannelName":"Fast&FunBOX HD","Language":"English","Genre":"Sports"},{"ChannelName":"FightBOX HD","Language":"English","Genre":"Sports"},{"ChannelName":"Fix & Foxi","Language":"English","Genre":"Kids"},{"ChannelName":"Ginx TV","Language":"English","Genre":"Kids"},{"ChannelName":"GMA DWLS (RADIO)","Language":"Tagalog","Genre":"Radio"},{"ChannelName":"GMA DZBB (RADIO)","Language":"Tagalog","Genre":"Radio"},{"ChannelName":"GMA Life","Language":"Tagalog","Genre":"Entertainment"},{"ChannelName":"GMA News TV","Language":"Tagalog","Genre":"News"},{"ChannelName":"GMA Pinoy TV","Language":"Tagalog","Genre":"Entertainment"},{"ChannelName":"My Music Always HD","Language":"English","Genre":"Music"},{"ChannelName":"My Music Tropical HD","Language":"English","Genre":"Music"},{"ChannelName":"OANN HD","Language":"English","Genre":"News"},{"ChannelName":"Physique TV HD","Language":"English","Genre":"Lifestyle"},{"ChannelName":"Pursuit Channel HD","Language":"English","Genre":"Lifestyle"},{"ChannelName":"Ultra Film HD","Language":"English","Genre":"Movies"}]},{"Id":"3407","TvPackage":"OSN Sports Extra","PackagePrice":"AED 109 per month","PackageAdditionalSubscription":"AED 109 per month","PackageDescription":"AED 109 per month","ChannelLists":[{"ChannelName":"AD + Drama HD","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"AD Sports 3 HD","Language":"Arabic,English","Genre":"Sports"},{"ChannelName":"AD Sports 4 HD","Language":"Arabic,English","Genre":"Sports"},{"ChannelName":"AD Sports 5 HD","Language":"Arabic,English","Genre":"Sports"},{"ChannelName":"AD Sports 6 HD","Language":"Arabic,English","Genre":"Sports"},{"ChannelName":"Dubai Sports 2","Language":"Arabic,English","Genre":"Sports"},{"ChannelName":"Fuel TV","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Fight Network HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Sports 2HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Sports 3 HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Sports 4 HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Sports Cricket HD","Language":"Hindi","Genre":"Sports"},{"ChannelName":"OSN Sports Rugby World Cup HD","Language":"English","Genre":"Sports"},{"ChannelName":"Ten Cricket","Language":"English","Genre":"Sports"}]},{"Id":"3405","TvPackage":"Pinoy Plus Extra","PackagePrice":"AED 152 per month","PackageAdditionalSubscription":"AED 152 per month","PackageDescription":"AED 152 per month","ChannelLists":[{"ChannelName":"ABD-CBN News","Language":"Tagalog","Genre":"News"},{"ChannelName":"ABS-CBN TFC","Language":"Tagalog","Genre":"Entertainment"},{"ChannelName":"Aksyon TV","Language":"Tagalog","Genre":"Entertainment"},{"ChannelName":"AMC","Language":"English","Genre":"Movies"},{"ChannelName":"Animal Planet HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"BBC Entertainment","Language":"English","Genre":"Entertainment"},{"ChannelName":"Bloomberg TV","Language":"English","Genre":"News"},{"ChannelName":"Boomerang","Language":"English","Genre":"Kids"},{"ChannelName":"Bro","Language":"Tagalog","Genre":"Entertainment"},{"ChannelName":"Cartoon Network","Language":"English","Genre":"Kids"},{"ChannelName":"Cinema One","Language":"Tagalog","Genre":"Movies"},{"ChannelName":"Discovery HD ","Language":"English","Genre":"Documentaries"},{"ChannelName":"Discovery Science HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"Disney Channel","Language":"English","Genre":"Kids"},{"ChannelName":"Disney Junior","Language":"English","Genre":"Kids"},{"ChannelName":"Disney XD","Language":"English","Genre":"Kids"},{"ChannelName":"E! HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"Fashion TV HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"Food Network HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"Fuel TV","Language":"English","Genre":"Sports"},{"ChannelName":"History Channel HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"JimJam","Language":"English","Genre":"Kids"},{"ChannelName":"MBC+ Drama HD","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"MTV Live","Language":"English","Genre":"Music"},{"ChannelName":"National Geographic HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"OSN First Comedy +2","Language":"English","Genre":"Comedy"},{"ChannelName":"OSN First Comedy HD","Language":"English","Genre":"Comedy"},{"ChannelName":"OSN Sports 2HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Sports 3 HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Sports 4 HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Sports Rugby World Cup HD","Language":"English","Genre":"Sports"},{"ChannelName":"Star Movies HD","Language":"English","Genre":"Movies"},{"ChannelName":"Star World HD","Language":"English","Genre":"Movies"},{"ChannelName":"Trace TV","Language":"English","Genre":"Music"},{"ChannelName":"Turner Classic Movies","Language":"English","Genre":"Movies"},{"ChannelName":"TV5 Kapatip","Language":"Tagalog","Genre":"Entertainment"},{"ChannelName":"VH-1 ","Language":"English","Genre":"Music"},{"ChannelName":"Viva TV","Language":"Tagalog","Genre":"Entertainment"}]},{"Id":"3406","TvPackage":"Pinoy Prime","PackagePrice":"AED 118 per month","PackageAdditionalSubscription":"AED 118 per month","PackageDescription":"AED 118 per month","ChannelLists":[{"ChannelName":"ABD-CBN News","Language":"Tagalog","Genre":"News"},{"ChannelName":"ABS-CBN TFC","Language":"Tagalog","Genre":"Entertainment"},{"ChannelName":"Aksyon TV","Language":"Tagalog","Genre":"Entertainment"},{"ChannelName":"AMC","Language":"English","Genre":"Movies"},{"ChannelName":"Animal Planet HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"Bloomberg TV","Language":"English","Genre":"News"},{"ChannelName":"Bro","Language":"Tagalog","Genre":"Entertainment"},{"ChannelName":"Cartoon Network","Language":"English","Genre":"Kids"},{"ChannelName":"Cinema One","Language":"Tagalog","Genre":"Movies"},{"ChannelName":"Discovery Science HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"Disney Channel","Language":"English","Genre":"Kids"},{"ChannelName":"Disney Junior","Language":"English","Genre":"Kids"},{"ChannelName":"Disney XD","Language":"English","Genre":"Kids"},{"ChannelName":"E! HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"Food Network HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"Fuel TV","Language":"English","Genre":"Sports"},{"ChannelName":"MBC+ Drama HD","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"MTV Live","Language":"English","Genre":"Music"},{"ChannelName":"National Geographic HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"OSN Sports 2HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Sports 3 HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Sports 4 HD","Language":"English","Genre":"Sports"},{"ChannelName":"Star Movies HD","Language":"English","Genre":"Movies"},{"ChannelName":"Star World HD","Language":"English","Genre":"Movies"},{"ChannelName":"Trace TV","Language":"English","Genre":"Music"},{"ChannelName":"Turner Classic Movies","Language":"English","Genre":"Movies"},{"ChannelName":"TV5 Kapatip","Language":"Tagalog","Genre":"Entertainment"},{"ChannelName":"VH-1","Language":"English","Genre":"Music"},{"ChannelName":"Viva TV","Language":"Tagalog","Genre":"Entertainment"}]},{"Id":"3404","TvPackage":"Platinum Extra HD","PackagePrice":"AED 397 per month","PackageAdditionalSubscription":"AED 397 per month","PackageDescription":"AED 397 per month","ChannelLists":[{"ChannelName":"Aaj Tak","Language":"Hindi","Genre":"News"},{"ChannelName":"AMC","Language":"English","Genre":"Movies"},{"ChannelName":"Animal Planet HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"Baby TV","Language":"English","Genre":"Kids"},{"ChannelName":"BBC Entertainment","Language":"English","Genre":"Entertainment"},{"ChannelName":"BBC Lifestyle ","Language":"English","Genre":"Entertainment"},{"ChannelName":"Bloomberg TV","Language":"English","Genre":"News"},{"ChannelName":"Boomerang","Language":"English","Genre":"Kids"},{"ChannelName":"Cartoon Network","Language":"English","Genre":"Kids"},{"ChannelName":"CNBC","Language":"English","Genre":"News"},{"ChannelName":"Crime & Investigation","Language":"English","Genre":"Documentaries"},{"ChannelName":"Discovery HD ","Language":"English","Genre":"Documentaries"},{"ChannelName":"Discovery ID","Language":"English","Genre":"Documentaries"},{"ChannelName":"Discovery Science HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"Discovery World","Language":"English","Genre":"Documentaries"},{"ChannelName":"Disney Channel","Language":"English","Genre":"Kids"},{"ChannelName":"Disney Junior","Language":"English","Genre":"Kids"},{"ChannelName":"Disney XD","Language":"English","Genre":"Kids"},{"ChannelName":"E! HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"Fashion TV HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"Fine Living","Language":"English","Genre":"Entertainment"},{"ChannelName":"Food Network HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"Fox News","Language":"English","Genre":"News"},{"ChannelName":"Fuel TV","Language":"English","Genre":"Sports"},{"ChannelName":"History Channel HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"ITV Choice HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"JimJam","Language":"English","Genre":"Kids"},{"ChannelName":"Life OK","Language":"Hindi","Genre":"Entertainment"},{"ChannelName":"MBC+ Drama HD","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"MTV India","Language":"Hindi","Genre":"Music"},{"ChannelName":"MTV Live","Language":"English","Genre":"Music"},{"ChannelName":"Nat Geo People HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"Nat Geo Wild HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"National Geographic HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"Nick Junior","Language":"English","Genre":"Kids"},{"ChannelName":"Nickelodeon HD","Language":"English","Genre":"Kids"},{"ChannelName":"OSN Fight Network HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN First +2","Language":"English","Genre":"Entertainment"},{"ChannelName":"OSN First Comedy +2","Language":"English","Genre":"Comedy"},{"ChannelName":"OSN First Comedy HD","Language":"English","Genre":"Comedy"},{"ChannelName":"OSN First HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"OSN First More","Language":"English","Genre":"Entertainment"},{"ChannelName":"OSN Movies Action +2","Language":"English","Genre":"Movies"},{"ChannelName":"OSN Movies Action HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"OSN Movies Comedy HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"OSN Movies Drama +2","Language":"English","Genre":"Movies"},{"ChannelName":"OSN Movies Drama HD","Language":"English","Genre":"Movies"},{"ChannelName":"OSN Movies Festival HD","Language":"English","Genre":"Movies"},{"ChannelName":"OSN Movies HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"OSN Movies HD+2","Language":"English","Genre":"Entertainment"},{"ChannelName":"OSN Movies Kids HD","Language":"English","Genre":"Movies"},{"ChannelName":"OSN Movies Premiere +2","Language":"English","Genre":"Movies"},{"ChannelName":"OSN Movies Premiere HD","Language":"English","Genre":"Movies"},{"ChannelName":"OSN News","Language":"English","Genre":"News"},{"ChannelName":"OSN Sports 2HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Sports 3 HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Sports 4 HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Sports Cricket HD","Language":"Hindi","Genre":"Sports"},{"ChannelName":"OSN Sports Rugby World Cup HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN WWE Network","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Ya Hala! HD","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"OSN Yahala Drama HD","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"OSN Yahala HD +2","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"OSN YaHala Shabab","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"Outdoor HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"Sky News Arabia HD","Language":"Arabic","Genre":"News"},{"ChannelName":"Sky News HD","Language":"English","Genre":"News"},{"ChannelName":"Star Gold","Language":"Hindi","Genre":"Movies"},{"ChannelName":"Star Movies HD","Language":"English","Genre":"Movies"},{"ChannelName":"Star Plus","Language":"Hindi","Genre":"Entertainment"},{"ChannelName":"Star World HD","Language":"English","Genre":"Movies"},{"ChannelName":"Sundance HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"TLC HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"Trace TV","Language":"English","Genre":"Music"},{"ChannelName":"Travel Channel","Language":"English","Genre":"Documentaries"},{"ChannelName":"Turner Classic Movies","Language":"English","Genre":"Movies"},{"ChannelName":"VH-1 ","Language":"English","Genre":"Music"},{"ChannelName":"Zee Cinema HD","Language":"Hindi","Genre":"Movies"},{"ChannelName":"Zee TV","Language":"Hindi","Genre":"Entertainment"}]},{"Id":"3403","TvPackage":"Premier Plus HD","PackagePrice":"AED 312 per month","PackageAdditionalSubscription":"AED 312 per month","PackageDescription":"AED 312 per month","ChannelLists":[{"ChannelName":"AMC","Language":"English","Genre":"Movies"},{"ChannelName":"Animal Planet HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"BBC Entertainment","Language":"English","Genre":"Entertainment"},{"ChannelName":"BBC Lifestyle ","Language":"English","Genre":"Entertainment"},{"ChannelName":"Bloomberg TV","Language":"English","Genre":"News"},{"ChannelName":"Boomerang","Language":"English","Genre":"Kids"},{"ChannelName":"Cartoon Network","Language":"English","Genre":"Kids"},{"ChannelName":"CNBC","Language":"English","Genre":"News"},{"ChannelName":"Crime & Investigation","Language":"English","Genre":"Documentaries"},{"ChannelName":"Discovery HD ","Language":"English","Genre":"Documentaries"},{"ChannelName":"Discovery ID","Language":"English","Genre":"Documentaries"},{"ChannelName":"Discovery Science HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"Discovery World","Language":"English","Genre":"Documentaries"},{"ChannelName":"Disney Channel","Language":"English","Genre":"Kids"},{"ChannelName":"Disney Junior","Language":"English","Genre":"Kids"},{"ChannelName":"Disney XD","Language":"English","Genre":"Kids"},{"ChannelName":"E! HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"Fashion TV HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"Fine Living","Language":"English","Genre":"Entertainment"},{"ChannelName":"Food Network HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"Fox News","Language":"English","Genre":"News"},{"ChannelName":"Fuel TV","Language":"English","Genre":"Sports"},{"ChannelName":"History Channel HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"ITV Choice HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"JimJam","Language":"English","Genre":"Kids"},{"ChannelName":"MBC+ Drama HD","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"MTV Live","Language":"English","Genre":"Music"},{"ChannelName":"Nat Geo People HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"Nat Geo Wild HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"National Geographic HD","Language":"English","Genre":"Documentaries"},{"ChannelName":"Nick Junior","Language":"English","Genre":"Kids"},{"ChannelName":"Nickelodeon HD","Language":"English","Genre":"Kids"},{"ChannelName":"OSN Fight Network HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN First +2","Language":"English","Genre":"Entertainment"},{"ChannelName":"OSN First Comedy +2","Language":"English","Genre":"Comedy"},{"ChannelName":"OSN First Comedy HD","Language":"English","Genre":"Comedy"},{"ChannelName":"OSN First HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"OSN First More","Language":"English","Genre":"Entertainment"},{"ChannelName":"OSN Movies Action +2","Language":"English","Genre":"Movies"},{"ChannelName":"OSN Movies Action HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"OSN Movies Comedy HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"OSN Movies Drama +2","Language":"English","Genre":"Movies"},{"ChannelName":"OSN Movies Drama HD","Language":"English","Genre":"Movies"},{"ChannelName":"OSN Movies Festival HD","Language":"English","Genre":"Movies"},{"ChannelName":"OSN Movies HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"OSN Movies HD+2","Language":"English","Genre":"Entertainment"},{"ChannelName":"OSN Movies Kids HD","Language":"English","Genre":"Movies"},{"ChannelName":"OSN Movies Premiere +2","Language":"English","Genre":"Movies"},{"ChannelName":"OSN Movies Premiere HD","Language":"English","Genre":"Movies"},{"ChannelName":"OSN News","Language":"English","Genre":"News"},{"ChannelName":"OSN Sports 2HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Sports 3 HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Sports 4 HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Sports Rugby World Cup HD","Language":"English","Genre":"Sports"},{"ChannelName":"OSN Ya Hala! HD","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"OSN Yahala Drama HD","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"OSN Yahala HD +2","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"OSN YaHala Shabab","Language":"Arabic","Genre":"Entertainment"},{"ChannelName":"Outdoor HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"Sky News Arabia HD","Language":"Arabic","Genre":"News"},{"ChannelName":"Sky News HD","Language":"English","Genre":"News"},{"ChannelName":"Star Movies HD","Language":"English","Genre":"Movies"},{"ChannelName":"Star World HD","Language":"English","Genre":"Movies"},{"ChannelName":"Sundance HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"TLC HD","Language":"English","Genre":"Entertainment"},{"ChannelName":"Trace TV","Language":"English","Genre":"Music"},{"ChannelName":"Travel Channel","Language":"English","Genre":"Documentaries"},{"ChannelName":"Turner Classic Movies","Language":"English","Genre":"Movies"},{"ChannelName":"VH-1 ","Language":"English","Genre":"Music"}]},{"Id":"3408","TvPackage":"Russian Prime","PackagePrice":"AED 99  per month","PackageAdditionalSubscription":"AED 99  per month","PackageDescription":"AED 99  per month","ChannelLists":[{"ChannelName":"CCTV Russian","Language":"Russian","Genre":"General Entertainment"},{"ChannelName":"CTC","Language":"Russian","Genre":"General Entertainment"},{"ChannelName":"Destki Mir TV","Language":"Russian","Genre":"Kids"},{"ChannelName":"Karusel TV","Language":"Russian","Genre":"Kids"},{"ChannelName":"Nashe Kino","Language":"Russian","Genre":"Movies"},{"ChannelName":"NTV Mir","Language":"Russian","Genre":"General Entertainment"},{"ChannelName":"RBK","Language":"Russian","Genre":"News"},{"ChannelName":"Rossiya 24","Language":"Russian","Genre":"News"},{"ChannelName":"RTG","Language":"Russian","Genre":"Documentary"},{"ChannelName":"RTR Planeta","Language":"Russian","Genre":"General Entertainment"},{"ChannelName":"RTVI","Language":"Russian","Genre":"General Entertainment"},{"ChannelName":"RU TV","Language":"Russian","Genre":"General Entertainment"},{"ChannelName":"Rus ORTi","Language":"Russian","Genre":"General Entertainment"},{"ChannelName":"Teleclub","Language":"Russian","Genre":"Shopping"}]},{"Id":"3412","TvPackage":"South Asian Package","PackagePrice":"AED 39 per month","PackageAdditionalSubscription":"AED 39 per month","PackageDescription":"AED 39 per month","ChannelLists":[]},{"Id":"3410","TvPackage":"Zee Family + HD","PackagePrice":"AED 34 per month","PackageAdditionalSubscription":"AED 34 per month","PackageDescription":"AED 34 per month","ChannelLists":[{"ChannelName":"Zee Action","Language":"Hindi","Genre":"Movies"},{"ChannelName":"Zee Aflam","Language":"Arabic","Genre":"Movies"},{"ChannelName":"Zee Bangla","Language":"Bengali","Genre":"Entertainment"},{"ChannelName":"Zee Business","Language":"Hindi","Genre":"News"},{"ChannelName":"Zee Cinema HD","Language":"Hindi","Genre":"Movies"},{"ChannelName":"Zee Classic","Language":"Hindi","Genre":"Movies"},{"ChannelName":"Zee ETC","Language":"Hindi","Genre":"Entertainment"},{"ChannelName":"Zee Marathi","Language":"Marathi","Genre":"Entertainment"},{"ChannelName":"Zee Punjabi","Language":"Punjabi","Genre":"News"},{"ChannelName":"Zee Salaam","Language":"Urdu","Genre":"Islamic"},{"ChannelName":"Zee Smile","Language":"Hindi","Genre":"Entertainment"},{"ChannelName":"Zee Tamil","Language":"Tamil","Genre":"Entertainment"},{"ChannelName":"Zee TV HD","Language":"Hindi","Genre":"Entertainment"},{"ChannelName":"Zing","Language":"Hindi","Genre":"Music"}]}];

            // Google Analytics mock
            var _gaq = [];
            /* /END MOCKS */

            $.fn.onUpdating = function () {
                $.msg({
                    clickUnblock : false,
                    bgPath: '/common/images/tv-package-selector/',
                    content: '<img src="/common/images/tv-package-selector/loader.gif" border="0"	/>'
                });
            // $.extend($.blockUI.defaults.overlayCSS, { backgroundColor: '#ffffff' });
            // $(this).block('<img	src="/legacy/Files/tv-packages/Images/loader.gif"	border="0"	/>', { background: 'transparent', border: '0' });
            };

            $.fn.onUpdated = function () {
            // $(this).unblock();
                //$(this).css({position:"static"});
            };

            $.fn.customSelect = function (_options) {
                var _options = $.extend({
                    selectStructure: '<div class="selectArea"><div class="left"></div><div class="center"></div><a href="#" class="selectButton">&nbsp;</a><div class="disabled"></div></div>',
                    selectText: '.center',
                    selectBtn: '.selectButton',
                    selectDisabled: '.disabled',
                    optStructure: '<div class="selectOptions"><div class="select-top"><div class="select-bottom-left"></div><div class="select-bottom-right"></div></div><ul></ul><div class="select-bottom"><div class="select-bottom-left"></div><div class="select-bottom-right"></div></div></div>',
                    optList: 'ul'
                }, _options);
                return this.each(function () {
                    var select = $(this);
                    select.removeClass("outtaHere");
                    $(".selectArea,.selectOptions").remove();
                    if (!select.hasClass('outtaHere')) {
                        if (select.is(':visible')) {
                            var replaced = $(_options.selectStructure);
                            var selectText = replaced.find(_options.selectText);
                            var selectBtn = replaced.find(_options.selectBtn);
                            var selectDisabled = replaced.find(_options.selectDisabled).hide();
                            var optHolder = $(_options.optStructure);
                            var optList = optHolder.find(_options.optList);
                            if (select.attr('disabled')) selectDisabled.show();
                            select.find('option').each(function (index) {
                                var selOpt = $(this);
                                var _opt = $('<li><a href="#">' + selOpt.html() + '</a></li>');
                                if (selOpt.attr('selected')) {
                                    selectText.html(selOpt.html());
                                    _opt.addClass('selected');
                                }
                                _opt.children('a').click(function (e) {
                                    e.preventDefault();
                                    e.stopPropagation();
                                    optList.find('li').removeClass('selected');
                                    select.find('option').removeAttr('selected');
                                    $(this).parent().addClass('selected');
                                    selOpt.attr('selected', 'selected');
                                    selectText.html(selOpt.html());
                                    select.change();
                                    optHolder.hide();
                                    return false;
                                });
                                optList.append(_opt);
                            });
                            replaced.width(select.outerWidth()).addClass(select.attr('class'));
                            replaced.insertBefore(select);
                            optHolder.css({
                                width: select.outerWidth(),
                                display: 'none',
                                position: 'absolute'
                            });
                            if (select.attr('class') && select.attr('class').length) optHolder.addClass('drop-' + select.attr('class'));
                            $(document.body).append(optHolder);

                            var optTimer;
                            replaced.hover(function () {
                                if (optTimer) clearTimeout(optTimer);
                            }, function () {
                                optTimer = setTimeout(function () {
                                    optHolder.hide();
                                }, 200);
                            });
                            optHolder.hover(function () {
                                if (optTimer) clearTimeout(optTimer);
                            }, function () {
                                optTimer = setTimeout(function () {
                                    optHolder.hide();
                                }, 200);
                            });
                            selectBtn.click(function (e) {
                                e.preventDefault();
                                e.stopPropagation();
                                if ($(this).hasClass("disabled")) return false;
                                if (optHolder.is(':visible')) {
                                    optHolder.hide();
                                }
                                else {
                                    optHolder.children('ul').css({ height: 'auto', overflow: 'hidden' });
                                    optHolder.css({
                                        top: replaced.offset().top + replaced.outerHeight(),
                                        left: replaced.offset().left,
                                        display: 'block'
                                    });
                                    var _dynamicHeight = optHolder.children('ul').height();
                                    optHolder.children('ul').css({ height: _dynamicHeight, overflow: 'visible' });
                                }
                                return false;
                            });
                            select.addClass('outtaHere');
                            $(".selectOptions ul").each(function () {
                                var list = $(this);
                                var size = 15;
                                var current_size = 0;
                                list.children().each(function () {
                                    if (++current_size > size) {
                                        var new_list = $("<ul></ul>").insertAfter(list);
                                        list = new_list;
                                        current_size = 1;
                                    }
                                    list.append(this);
                                });
                            });

                        }
                    }
                });
            };

            var animatePane = function(el, h, op) {
                if (op == 1) {
                    $(el).css({ display: "block" });
                }

                $(el).animate({
                    height: h,
                    opacity: op
                }, {
                    duration: 500,
                    queue: false,
                    complete: function () {
                        if (op == 1) {
                            $(el).css({ marginBottom: "0px" });

                            if (el == ".complete-packages") {
                                $(".complete-packages").addClass("auto-height");
                            }
                            if (el == ".genre-selector-pane") {
                                $(".genre-selector-pane").addClass("auto-height");
                                $(el).css({ marginBottom: "0px" });
                            }
                            if (el == ".package-selector") {
                                $(".complete-packages").addClass("auto-height");
                                $(".package-selector").css({ "height": "auto" });
                            }
                            if (el == ".genre-selector-pane") {
                                $(".genre-selector-pane").addClass("auto-height");
                                $(el).css({ marginBottom: "0px" });
                            }
                        }
                        else {
                            $(el).css({ marginBottom: "0px" });
                            if (el == ".complete-packages") {
                                $(".complete-packages").removeClass("auto-height");
                            }
                            if (el == ".genre-selector-pane") {
                                $(".genre-selector-pane").removeClass("auto-height");
                            }
                        }
                    }
                })
                if (op == 1) {
                    //$("body").stop().scrollTo( el, 800 );
                }
            };

            var createUniqueList = function(filterVal) {
                var notInList = false;
                if (lans.length <= 0) {
                    lans.push({
                        value: filterVal
                    });
                    return false;
                }
                for (i = 0; i < lans.length; i++) {
                    if (lans[i].value == filterVal)
                        notInList = true;
                };
                if (!notInList) {
                    lans.push({
                        value: filterVal
                    });
                }
            };

            var createUniqueChnlList = function(filterVal) {
                var notInList = false;
                if (chanl.length <= 0) {
                    chanl.push({
                        value: filterVal
                    });
                    return false;
                }
                for (i = 0; i < chanl.length; i++) {
                    if (chanl[i].value == filterVal)
                        notInList = true;
                };
                if (!notInList) {
                    chanl.push({
                        value: filterVal
                    });
                }
            };

            var splitUI = function(count, parent, cls) {
                $("." + parent + " ul").each(function () {
                    var list = $(this);
                    var size = count;
                    var current_size = 0;
                    list.children().each(function () {
                        //console.log(current_size + ": " + $(this).text());
                        if (++current_size > size) {
                            var new_list = $("<ul class='" + cls + "'></ul>").insertAfter(list);
                            list = new_list;
                            current_size = 1;
                        }
                        list.append(this);
                    });
                });
            };

            var tvChannels = {
                service: '/legacy/UserControls/TVPackages/Services/SvcTVChannelService.svc/',
                dds: "genre-comedy,genre-documentaries,genre-entertainment,genre-movies",
                controlUI: function () {
                    var thisObj = this;
                    $(".close-btn").click(function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        $('.full-channel-list').hide();
                        return false;
                    });
                    $(".fav-genre li").click(function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        $(".genre-content").onUpdating();
                        if ($(this).hasClass("active")) {
                            $(this).removeClass("active");
                            $(".genre-content").onUpdated();
                        }
                        else {
                            $(this).addClass("active")
                        }
                        if ($(".fav-genre li.active").length > 0) {
                            $(".selectArea .center").html("Select Language");
                        }
                        else {
                            $(".tv-channels .tv-channel-content").html("");
                            animatePane(".tv-channels", 0, 0);
                        }
                        selectedChnl = [];
                        $(".tv-channels .chnl-list .active").each(function () {
                            selectedChnl.push($(this).find("p").html());
                        })
                        _gaq.push(['_trackEvent', 'TV package selector', 'Genre', selectedChnl.join()]);
                        animatePane(".package-selector", 0, 0);
                        animatePane(".complete-packages", 0, 0);
                        animatePane(".print-panel", 0, 0);
                        var selectedGenre = thisObj.getSelectedGenre();
                        tvChannels.getAllChannels(selectedGenre, "", function () {
                            $(".genre-content").onUpdated();
                            animatePane(".tv-channels", 440, 1);
                            $(".chnl-list li").unbind().bind("click", function (e) {
                                e.preventDefault();
                                e.stopPropagation();
                                $(".tv-channels").onUpdating();
                                if ($(this).hasClass("active")) {
                                    $(this).removeClass("active")
                                    $(".tv-channels").onUpdated();
                                }
                                else {
                                    $(this).addClass("active")
                                }
                                $(".full-channel-list").hide();
                                if ($(".chnl-list li.active").length > 0) {
                                    thisObj.getPackages();
                                    animatePane(".package-selector", 400, 1);
                                    animatePane(".complete-packages", 0, 0);
                                    animatePane(".print-panel", 0, 0);
                                }
                                else {
                                    animatePane(".package-selector", 0, 0);
                                }
                            })

                        });
                        return false;
                    })

                    $(".nav-next.step-1").click(function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        if ($(".lang-selector li.active").length <= 0) return false;
                        $("body").stop().scrollTo(".genre-selector-pane", 800);
                    })
                    $(".nav-next.step-2").click(function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        if ($(".fav-genre li.active").length <= 0) return false;
                        $("body").stop().scrollTo(".tv-channels", 800);
                    })
                    $(".nav-next.step-3").click(function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        if ($(".tv-channels .chnl-list li.active").length <= 0) return false;

                        $("body").stop().scrollTo(".package-selector", 800);
                    })
                    $(".nav-next.step-4").click(function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        if ($(".packages li.active").length <= 0) return false;
                        $("body").stop().scrollTo(".complete-packages", 800);
                    })
                    $(".nav-next.step-5").click(function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        $("body").stop().scrollTo(".print-panel", 800);
                    })
                    $(".print-btn").click(function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        $(".complete-packages").printElement(
                        {
                            overrideElementCSS: [
                        '/tv-packages/css/package-print.css',
                        { href: '/tv-packages/css/package-print.css?decache=true', media: 'print' }]
                        });
                    });

                },
                getLanguages: function () {
                    var selectedLan = "";
                    var lnM = "";
                    var thisObj = this;

                    var getLanguagesCallback = function (data) {
                        for (i = 0; i < data.length; i++) {
                            lnM = lnM + "<li><span>" + data[i].Language + "</span></li>";
                        }
                        $(".lang-selector").html(lnM);

                        $(".lang-selector li").unbind().bind("click", function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            if ($(this).hasClass("active")) {
                                $(this).removeClass("active")
                            }
                            else {
                                $(this).addClass("active")
                            }
                            selectedLan = "";
                            $(".lang-selector li.active").each(function () {
                                if (selectedLan != "") selectedLan = selectedLan + ",";
                                selectedLan = selectedLan + $(this).find("span").html();
                            })
                            if (selectedLan != "") {
                                thisObj.getGenreByLang(selectedLan);
                            }
                            else {
                                animatePane(".genre-selector-pane", 0, 0);
                            }
                            _gaq.push(['_trackEvent', 'TV package selector', 'language', selectedLan]);
                        })
                        $(".lang-selector li").eq(0).trigger("click");
                    };

                    /*
                    $.ajax({
                        type: "GET",
                        url: this.service + "getalldistincttvlanguages",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: getLanguagesCallback
                    });
                    */
                    getLanguagesCallback(getLanguagesMock);
                },
                getGenreByLang: function (langs) {
                    var chnls = "";

                    var getGenreByLangCallback = function (data) {
                        for (i = 0; i < data.length; i++) {
                            var genreLangs = "";
                            if (data[i].Language.length > 1) {
                                for (var j = 0; j < data[i].Language.length; j++) {
                                    genreLangs += data[i].Language[j] + ";"
                                }
                                genreLangs = genreLangs.substring(0, genreLangs.length - 1);
                            }
                            else {
                                genreLangs = data[i].Language;
                            }
                            chnls = chnls + '<li class="g-' + data[i].Genre.toLowerCase().replace(/\&/g, '').replace(/\ /g, '-').replace(/\--/g, '-') + '"><span>' + data[i].Genre + '</span> <span class="gnrelang">' + data[i].Genre + ":" + genreLangs + '</span></li>';
                        }
                        $(".fav-genre").html(chnls);
                        tvChannels.controlUI();
                        animatePane(".genre-selector-pane", 420, 1);
                    };

                    /*
                    $.ajax({
                        type: "GET",
                        url: this.service + "gettvchannelsbylanguage/?language=" + langs,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: getGenreByLangCallback
                    });
                    */
                    getGenreByLangCallback(getGenreByLangMock);
                },
                getSelectedGenre: function () {
                    var gnres = "";
                    $(".fav-genre li.active span.gnrelang").each(function (index) {
                        if (gnres != "") {
                            gnres = gnres + "," + $(this).html();
                        }
                        else {
                            gnres = gnres + $(this).html();
                        }
                    })
                    return gnres;
                },
                getAllChannels: function (channel, lang, callback) {
                    if (channel == "") return false;

                    var getAllChannelsCallback = function (data) {
                        var options = "";
                        for (i = 0; i < data.length; i++) {
                            if ($.inArray(data[i].ChannelName, selectedChnl) >= 0) {
                                selectedChnl.push(data[i].ChannelName);
                                options = options + '<li class="active ' + data[i].Genre.toLowerCase() + ' cls-' + data[i].Language.toLowerCase() + '"><div class="box"><p>' + data[i].ChannelName + '</p></div><span>' + data[i].Genre + '</span>' + '<label title="' + data[i].Language + '">[' + data[i].Language.toUpperCase().substring(0, 2) + ']</label></li>';
                            }
                            else {
                                options = options + '<li class="' + data[i].Genre.toLowerCase() + ' cls-' + data[i].Language.toLowerCase() + '"><div class="box"><p>' + data[i].ChannelName + '</p></div><span>' + data[i].Genre + '</span>' + '<label title="' + data[i].Language + '">[' + data[i].Language.toUpperCase().substring(0, 2) + ']</label></li>';
                            }
                        }
                        options = "<div class='tv-channel-content'><div class='chnl-mask'><ul class='chnl-list'>" + options + "</ul></div></div>";
                        $(".tv-channels .tv-channel-content").remove();
                        $(".tv-channels").append(options);
                        if (selectedChnl.length > 0) {
                            $this.getPackages();
                            animatePane(".package-selector", 480, 1);
                            animatePane(".complete-packages", 0, 0);
                            animatePane(".print-panel", 0, 0);
                        }
                        callback();
                        if (lang == "")
                            $this.getUniqueLanguages();

                        splitUI(3, "tv-channels .tv-channel-content", "chnl-list");
                        var cW = ($(".tv-channels .tv-channel-content .chnl-list").length) * ($(".tv-channels .tv-channel-content .chnl-list").width());
                        $(".tv-channels .tv-channel-content .chnl-mask").width(cW);
                        $('.tv-channels .tv-channel-content').jScrollPane({
                            horizontalDragMinWidth: 209,
                            horizontalDragMaxWidth: 209
                        });
                        if (data.length <= 0) {
                            $(".chnl-mask").width(600).html("<p>There are currently no channels in the selected genre. Please try other genres.</p>")
                        }
                    };

                    var $this = this;
                    var serviceURL = this.service + "gettvchannelsinselectedgenre/?genre=" + channel + "&language=";
                    $(".tv-channels .tv-channel-content").html("");
                    if (lang != "Select Language") {
                        serviceURL = this.service + "gettvchannelsinselectedgenre/?genre=" + channel + "&language=" + lang;
                    }

                    /*
                    $.ajax({
                        type: "GET",
                        url: encodeURI(serviceURL).replace(/\&amp;/g, '%26'),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: getAllChannelsCallback,
                        error: function () {

                            //$(".verificationMsg .info").append("<p class='error'>Sorry, due to an unidentified error, your request could not be processed at this time. Please check back later!</p>");
                            //$(".verificationMsg").show();
                        }
                    });
                    */
                    getAllChannelsCallback(getAllChannelsMock);
                },
                getPackages: function () {
                    stringCh = "";
                    var $this = this;
                    $(".tv-channels .chnl-list .active").each(function () {
                        selectedChnl.push($(this).find("p").html());
                        if (stringCh != "") stringCh = stringCh + ",";
                        stringCh = stringCh + $(this).find("p").html() + ":" + $(this).find("label").attr("title");
                    })
                    var postValue = {};
                    postValue.selectedTVChannels = stringCh;
                    _gaq.push(['_trackEvent', 'TV package selector', 'Channel', stringCh]);

                    var getPackagesCallback = function (data) {
                        var mu = "";
                        $(".h-scroll").remove();
                        var ch = "";
                        for (i = 0; i < data.length; i++) {
                            ch = "";
                            for (j = 0; j < data[i].ChannelLists.length; j++) {
                                ch = ch + "<li>" + data[i].ChannelLists[j].ChannelName + " <label style='font-size:8px' title='" + data[i].ChannelLists[j].Language + "'>[" + data[i].ChannelLists[j].Language.toUpperCase().substring(0, 2) + "]</label></li>";
                            }
                            ch = '<p>The following selected channels are included in this package:</p><ul>' + ch + '</ul>';
                            mu = mu + '<li data="' + data[i].Id + '"><label class="selector-btn"></label><div class="short-summary"><div class="package-summary"><h3>' + data[i].TvPackage + '</h3><p class="pkg-price"><strong>' + data[i].PackagePrice + '</strong></p></div></div><div class="short-list">' + ch + '<a href="#" onclick="tvChannels.getPackageDetails(' + data[i].Id + ');return false" rel="' + data[i].Id + '" class="pack-read-more">View all channels included in this package <span>&gt;</span></a></div></li>';
                        }

                        mu = '<div class="h-scroll"><a class="prev browse left"></a><div class="scrollable" id="packageScroll"><div class="items"><ul class="packages">' + mu + '</ul></div></div><a class="next browse right"></a></div>';
                        $(".h-scroll").remove();
                        $(".package-selector-content").prepend(mu);
                        splitUI(6, "package-selector-content .scrollable", "packages");
                        /* removing the un necessary class*/
                        $("#packageScroll .short-list ul").each(function () {
                            if ($(this).hasClass("packages")) $(this).removeClass("packages");
                        });

                        $("#packageScroll>.items>ul.packages>li").each(function () {
                            if ($(this).find(".short-list ul").length > 3) {
                                var totalL = $(this).find(".short-list li").length;
                                totalL = totalL - 15;

                                $(this).find(".short-list ul:gt(2)").hide();
                                $(this).find(".short-list ul:eq(2)").after("<a href='#' class='showMore'>Show " + totalL + " more</a>");
                                $(this).find(".short-list a.showMore").click(function () {
                                    $(this).parent().find("ul:gt(2)").show();
                                    $(this).parent().find("a.showMore").hide();
                                    return false;
                                });
                            }
                            else {
                                $(this).find("short-list a.showMore").hide();
                            }
                        });

                        $("#packageScroll").scrollable();

                        $(".packages li .selector-btn").click(function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            $(".package-selector").onUpdating();
                            if ($(this).parent().hasClass("active")) {
                                $(this).parent().removeClass("active");
                                $(".package-selector").onUpdated();
                            }
                            else {
                                $(this).parent().addClass("active");
                                _gaq.push(['_trackEvent', 'TV package selector', 'Package', $(this).parent().find("h3").html()]);
                            }
                            setTimeout(function () {
                                $this.getPrintItems();
                                if ($(".packages li.active").length > 0) {
                                    $this.getPackageTotal();
                                    $this.getUniqueSelectedChannels();

                                    animatePane(".print-panel", 200, 1);
                                    if ($(".complete-package-header").height() + $(".complete-package-content").height() > 200) {
                                        animatePane(".complete-packages", 400, 1);
                                        $(".complete-packages").css({ height: "auto!important" });
                                    }
                                    else {
                                        animatePane(".complete-packages", 400, 1); //open package pane
                                        animatePane(".print-panel", 200, 1);
                                    }
                                }
                                else {
                                    animatePane(".complete-packages", 0, 0); //open package pane
                                }
                                $(".package-selector").onUpdated();
                            }, 2000);
                            return false;
                        })
                        //show the tv-channel pane
                        $(".tv-channels").onUpdated();
                    };

                    /*
                    $.ajax({
                        type: "POST",
                        url: this.service + "getrecommendedtvpackages",
                        data: JSON.stringify(postValue),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: getPackagesCallback
                    });
                    */
                    getPackagesCallback(getPackagesMock);
                },
                getPrintItems: function () {
                    _gaq.push(['_trackEvent', 'TV package selector', 'Print Package', "Print Package list"]);

                    var getPrintItemsCallback = function (data) {
                        var fullpkgList = "";
                        for (var i = 0; i < data.length; i++) {
                            var fullchlList = "";

                            for (var j = 0; j < data[i].ChannelLists.length; j++) {
                                if (j < data[i].ChannelLists.length - 1) {
                                    fullchlList += "<li>" + data[i].ChannelLists[j].ChannelName + ", </li>";
                                }
                                else {
                                    fullchlList += "<li>" + data[i].ChannelLists[j].ChannelName + "</li>";
                                }
                            }
                            fullpkgList += "<li><span>" + data[i].TvPackage + " [" + data[i].PackagePrice + "]</span><ul>" + fullchlList + "</ul></li>";
                        }
                        $(".fullchlList,.all-avail-package").remove();
                        $(".complete-packages").append("<a href='#' class='all-avail-package'>Click here to view all available packages</a><div class='fullchlList'><h3>All available packages</h3><ul>" + fullpkgList + "</ul></div>");
                        $(".complete-packages .fullchlList ul li ul li:last-child").addClass("last");
                        $(".all-avail-package").click(function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            $(this).hide();
                            $(".fullchlList").show();
                            return false;
                        })
                    };

                    /*
                    $.ajax({
                        type: "GET",
                        url: "/legacy/UserControls/TVPackages/Services/SvcTVChannelService.svc/getallchannelsintvpackages",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: getPrintItemsCallback,
                        error: function () {
                        }
                    });
                    */
                    getPrintItemsCallback(getPrintItemsMock);
                },
                getPackageTotal: function () {
                    var markup = "";
                    var pattern = /[0-9]+/g;
                    var total = 0;
                    $(".packages li.active").each(function (index) {

                        var matches = $(this).find(".pkg-price strong").html().match(pattern);
                        if (matches != null) {
                            total = total + parseFloat(matches);
                        }

                        if (index == 0) {
                            markup = markup + '<div data=' + matches + ' class="package first">' +
                                    '<h3>' + $(this).find("h3").html() + '</h3>' +
                                    '<span>' + $(this).find(".pkg-price").html() + '</span>' +
                                '</div>';
                        }
                        else {
                            var matches = $(this).find(".pkg-price strong").html().match(pattern);
                            markup = markup + '<div data=' + matches + ' class="package">' +
                                    '<h3>' + $(this).find("h3").html() + '</h3>' +
                                    '<span>' + $(this).find(".pkg-price").html() + '</span>' +
                                '</div>';
                        }
                    })
                    markup = markup + '<div class="package-total">' +
                            '<h3>Total Price</h3>' +
                            '<span>' + total + ' AED</span>' +
                        '</div>';
                    $(".pack-selection").html(markup);
                },
                getPackageDetails: function (id) {
                    var $this = this;

                    var getPackageDetailsCallback = function (data) {
                        var mu = '';
                        $(".full-channel-list .tv-channel-content .h-scroll").remove();
                        for (i = 0; i < data.length; i++) {
                            for (j = 0; j < data[i].Channels.length; j++) {
                                var chnl = data[i].Channels[j];
                                if (stringCh.toLowerCase().indexOf(chnl.toLowerCase()) >= 0) {
                                    mu = mu + '<li class="active ' + data[i].Genre.toLowerCase().replace(/\ /g, '-') + '">' + data[i].Channels[j] + '</li>';
                                }
                                else {
                                    mu = mu + '<li class="' + data[i].Genre.toLowerCase().replace(/\ /g, '-') + '">' + data[i].Channels[j] + '</li>';
                                }
                            }
                        }
                        $(".full-channel-list .tv-channel-content").append('<div class="h-scroll"><a class="prev browse left"></a><div class="scrollable" id="chanelScroll"><div class="items"><ul class="chnl-list">' + mu + '</ul></div></div><a class="next browse right"></a></div>');
                        splitUI(18, "full-channel-list .items", "chnl-list");
                        $("#chanelScroll").scrollable();
                        $(".full-channel-list").show();
                        var newH = $(".full-channel-list").height() + 200;
                    };

                    $.msg({
                        clickUnblock: false,
                        bgPath: '/common/images/tv-package-selector/',
                        content: '<img src="/common/images/tv-package-selector/loader.gif" border="0"	/>'
                    });

                    /*
                    $.ajax({
                        type: "GET",
                        url: $this.service + "gettvchannelsinselectedtvcategory/?tvpackageid=" + id,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: getPackageDetailsCallback,
                        error: function () {
                            //$(".verificationMsg .info").append("<p class='error'>Sorry, due to an unidentified error, your request could not be processed at this time. Please check back later!</p>");
                            //$(".verificationMsg").show();
                        }
                    });
                    */
                    getPackageDetailsCallback(getPackageDetailsMock);
                },
                getUniqueSelectedChannels: function () {
                    chanl = [];
                    var $this = this;

                    var getUniqueSelectedChannelsCallback = function (data) {
                        if (data) {
                            for (i = 0; i < data.length; i++) {
                                var item = data[i];
                                if (typeof (item.Channels) != "undefined") {
                                    for (j = 0; j < item.Channels.length; j++) {
                                        createUniqueChnlList(item.Channels[j]);
                                    }
                                }
                            }
                        }
                    };

                    $(".packages li.active").each(function () {
                        packageid = $(this).attr("data");

                        /*
                        $.ajax({
                            type: "GET",
                            async: false,
                            url: $this.service + "gettvchannelsinselectedtvcategory/?tvpackageid=" + packageid,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            cache: false,
                            success: getUniqueSelectedChannelsCallback
                        });
                        */
                        getUniqueSelectedChannelsCallback(getPackageDetailsMock);
                    });
                    var chnlList = "";
                    for (i = 0; i < chanl.length; i++) {
                        chnlList = chnlList + '<li>' + chanl[i].value + '</li>';
                    }
                    $(".complete-packages .short-list ul").html(chnlList);
                },
                getUniqueLanguages: function () {
                    lans = [];

                    var $this = this;
                    var thisObj = this;
                    if ($(".tv-channels  .chnl-list label").length <= 0) {
                        $(".panel-nav").hide(); return false
                    }
                    else {
                        $(".panel-nav").show()
                    }
                    $(".tv-channels  .chnl-list label").each(function () {
                        createUniqueList($(this).html());
                    })
                    var lanlist = "<option value='0'>Select Language</option>";
                    for (i = 0; i < lans.length; i++) {
                        lanlist = lanlist + '<option>' + lans[i].value + '</option>';
                    }
                    $("#language").html(lanlist);
                    $(".tv-channels #language").customSelect();
                    $(".tv-channels #language").unbind().bind("change", function () {
                        var selectedGenre = thisObj.getSelectedGenre();
                        tvChannels.getAllChannels(selectedGenre, $(".selectArea .center").html(), function () {
                            $(".genre-content").onUpdated();
                            animatePane(".tv-channels", 440, 1); //open channel pane
                            $(".chnl-list li").unbind().bind("click", function (e) {
                                e.preventDefault();
                                e.stopPropagation();
                                $(".tv-channels").onUpdating();
                                if ($(this).hasClass("active")) {
                                    $(this).removeClass("active")
                                    $(".tv-channels").onUpdated();
                                }
                                else {
                                    $(this).addClass("active")
                                }
                                $(".full-channel-list").hide();
                                if ($(".chnl-list li.active").length > 0) {
                                    thisObj.getPackages();
                                    animatePane(".package-selector", 480, 1); //open package pane
                                    animatePane(".complete-packages", 0, 0);
                                    animatePane(".print-panel", 0, 0);

                                }
                                else {

                                    animatePane(".package-selector", 0, 0); //open package pane

                                }
                            })

                        });


                        animatePane(".package-selector", 0, 0);
                        animatePane(".complete-packages", 0, 0);
                        animatePane(".print-panel", 0, 0);
                        return false;
                    })
                },
                setLanguageFilter: function () {
                    var selLang = $(".lang-filter li.active");
                    if (selLang.length <= 0) { $(".channels li").show(); return false }
                    $(".channels li").hide();
                    $(".channels li.channel-header").show();
                    selLang.each(function () {
                        var clsN = ".cls-" + $(this).find("span").html().toLowerCase();
                        $(".channels li" + clsN).show();
                    })
                    $(".channels li .button-2").removeClass("active");
                    $(".channels-right-pane").hide();
                }
            };

            tvChannels.getLanguages();
            window.tvChannels = tvChannels;
        }

        //create generic BXSlider component
        $('[data-bxslider]').bxSlider({
            minSlides: 1,
            maxSlides: 1
        });
        //END


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * TV Channels page
         */
        if ($('#modTVChannels').length > 0) {

            var tableCh = $('#tbl_TVChannel').dataTable({
                "paging": false
            });

            var tablePkg = $('#tbl_TVPackage').dataTable({
                "paging": false
            });

            $('#modTVChannels').on("keyup", ".input-search", function () {
                tableCh.fnFilter($(this).val());
                tablePkg.fnFilter($(this).val());
            });

            //open advanced filter
            $("#modTVChannels .openFilter").on("click", function (e) {
                e.preventDefault();

                if ($(this).closest(".activityCat").find(".collapseLang").is(":visible")) {
                    $(this).closest(".activityCat").find(".collapseLang").slideUp(300);
                } else {
                    $(this).closest(".activityCat").find(".collapseLang").slideDown(500);
                }
            });

            //select button category (channel - package)
            $("#modTVChannels .activityCat #filter1 a").on("click", function (e) {
                e.preventDefault();

                if ($(this).hasClass("selected")) {
                    $(this).removeClass("selected");
                    $(this).closest(".activityCat").closest(".modProduct").find(".boxContainer").find(".modTiles").show();

                } else {
                    $(this).closest("#filter1").find(".selected").removeClass("selected");
                    $(this).addClass("selected");

                    if ($(this).hasClass("channel")) {
                        $(this).closest(".activityCat").closest(".modProduct").find(".boxContainer").find(".modTiles.package").hide();
                        $(this).closest(".activityCat").closest(".modProduct").find(".boxContainer").find(".modTiles.channel").show();
                    } else if ($(this).hasClass("package")) {
                        $(this).closest(".activityCat").closest(".modProduct").find(".boxContainer").find(".modTiles.channel").hide();
                        $(this).closest(".activityCat").closest(".modProduct").find(".boxContainer").find(".modTiles.package").show();
                    }
                }
            });

            //filter search
            $('#modTVChannels .input-search').on('keyup', function () {
                var rex = new RegExp($(this).val(), 'i');
                $('.boxContainer .item').hide();
                $('.boxContainer .item').filter(function () {
                    return rex.test($(this).text());
                }).show();
            });

            //selected items channel
            $('#modTVChannels .boxContainer .itemsW.channels').on('click', '.item', function () {
                var boxSelected = 0;

                if ($(this).find(".mainPic .icon").is(":visible")) {
                    $(this).removeClass("selected");
                    $(this).find(".mainPic .icon").hide();
                } else {
                    $(this).addClass("selected");
                    $(this).find(".mainPic .icon").fadeIn();
                }

                //if you have choose at least a block
                boxSelected = $(this).closest(".boxContainer").find(".mainPic .icon:visible").length;
                if (boxSelected > 0) {
                    $(this).closest(".boxContainer").closest(".modProduct").find(".chartObject").fadeIn();
                } else {
                    $(this).closest(".boxContainer").closest(".modProduct").find(".chartObject").fadeOut();
                }

                return false;
            });

            $('#modTVChannels .chartObject .chartHeader').on('click', '.collapseIcon', function () {

                var divCollapse = $(this).closest(".chartHeader").closest(".chartObject").find(".chartBoxCollapsed");

                if (divCollapse.is(":visible")) {
                    divCollapse.slideUp();
                } else {
                    divCollapse.slideDown();
                }

            });

            //on close delete box and insert a gray placeholder
            $('#modTVChannels .detailBox .chosenBox').on('click', '.closeBox', function () {
                $(this).closest(".chosenBox").empty().addClass("chosenBoxEmpty").removeClass("chosenBox");
                return false;
            });

            //when click pack open full width pack detail
            $('#modTVChannels .detailBox').on('click', '.chosenBox', function () {
                $(this).closest(".detailBox").hide();
                $(this).closest(".detailBox").siblings(".detailPack").show();
                return false;
            });

            //close full width pack detail
            $('#modTVChannels .detailPack .titleChannels .title').on('click', '.closeSingleChannel', function () {
                $(this).closest(".detailPack").hide();
                $(this).closest(".detailPack").siblings(".detailBox").show();
                return false;
            });

            //selected items channel and show full widrh detail
            $('#modTVChannels .boxContainer .itemsW.packages').on('click', '.item', function () {
                $(this).closest(".items").find(".item").show();
                $(this).hide();
                $(this).closest(".itemsW.packages").find(".selectedItems").show();
                return false;
            });

            //add items channel
            $('#modTVChannels .boxContainer .itemsW.packages .costPack').on('click', '.buttonPack', function () {
                var boxSelected = 0;

                var node = $(this).closest(".itemsW.packages").find(".items");

                if (node.find(".mainPic .icon").is(":visible")) {
                    node.removeClass("selected");
                    node.find(".mainPic .icon").hide();
                } else {
                    node.addClass("selected");
                    node.find(".mainPic .icon").fadeIn();
                }

                //if you have choose at least a block
                boxSelected = node.closest(".boxContainer").find(".mainPic .icon:visible").length;
                if (boxSelected > 0) {
                    node.closest(".boxContainer").closest(".modProduct").find(".chartObjectPackage").fadeIn();
                } else {
                    node.closest(".boxContainer").closest(".modProduct").find(".chartObjectPackage").fadeOut();
                }

                return false;
            });
            $('#modTVChannels .chartObjectPackage .chartHeader').on('click', '.collapseIcon', function () {

                var divCollapse = $(this).closest("[class^='chartObject']").find(".chartBoxCollapsed");

                if (divCollapse.is(":visible")) {
                    divCollapse.slideUp();
                } else {
                    divCollapse.slideDown();
                }

            });

            //close full width pack detail
            $('#modTVChannels .detailPackages .titleChannels .title').on('click', '.closeSingleChannel', function () {
                $(this).closest(".selectedItems").hide();
                $(this).closest(".selectedItems").siblings(".items").find(".item").show();
                return false;
            });

            //change grid to list
            $('#modTVChannels .activityCat .groupButtons').on('click', '.btn', function () {
                $(this).closest(".groupButtons").find(".btn").removeClass("selected");
                $(this).addClass("selected");

                var boxContent = $(this).closest(".activityCat").closest(".modProduct").find(".boxContainer");

                if ($(this).hasClass("grid")) {
                    boxContent.find(".dataTables_wrapper").hide();
                    boxContent.find(".itemsW").show();
                } else if ($(this).hasClass("list")) {
                    boxContent.find(".itemsW").hide();
                    boxContent.find(".dataTables_wrapper").show();
                }
                return false;
            });

            $("#modTVChannels .modTiles.package .tbl_TVDashboard tbody").on("click", "tr:not(.collapsedRow)", function () {
                var el = $(this);

                if (el.next().hasClass("collapsedRow")) {

                    if (el.next().is(":visible")) {
                        el.next().slideUp();
                    } else {
                        el.next().slideDown();
                    }

                } else {
                    createNode(el);
                }

                return false;
            });

            $("#modTVChannels .modTiles.package .tbl_TVDashboard tbody").on("click", "tr.collapsedRow .costPack", function () {
                var boxSelected = 0;


                $(this).closest("tr.collapsedRow").prev("tr").addClass("selectedRow");

                $(this).closest("tr.collapsedRow").prev("tr").find(".imageObj i.icon").show();

                boxSelected = $(this).closest(".boxContainer").find(".modTiles.package .selectedRow").length;

                if (boxSelected > 0) {
                    $(this).closest(".boxContainer").closest(".modProduct").find(".chartObjectPackage").fadeIn();
                } else {
                    $(this).closest(".boxContainer").closest(".modProduct").find(".chartObjectPackage").fadeOut();
                }

                return false;
            });

            $("#modTVChannels .modTiles.channel .tbl_TVDashboard tbody tr").on("click", ".icon", function () {

                var count = 0;

                if ($(this).closest("tr").hasClass("selected")) {
                    $(this).closest("tr").removeClass("selected");
                } else {
                    $(this).closest("tr").addClass("selected");
                }

                count = $(this).closest(".tbl_TVDashboard").find("tr.selected").length;

                if (count > 0) {
                    $(this).closest(".boxContainer").closest(".modProduct").find(".chartObject").fadeIn();
                } else {
                    $(this).closest(".boxContainer").closest(".modProduct").find(".chartObject").fadeOut();
                }

                return false;
            });


            $(".chosenPack").on("click", ".badge", function (ev) {

                $(this).remove();
                ev.preventDefault();
            });
        }


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * Enterprise Data pool page
         */
        var data_pool = $('#data-pool').DataTable({
            "bFilter": false,
            "info": false
        });
        data_pool.draw();

        $('#data-pool td a.view-usage').click(function () {
            var $t = $(this);
            $t.parent().append("<span class='spinner'><span class='bounce1'></span><span class='bounce2'></span><span class='bounce3'></span></span>");
            $t.addClass('hidden');
            setTimeout(function () {
                $t.parent().find('.spinner').remove();
                $t.parent().find('.pool-usage').removeClass('hidden');
            }, 700);
            return false;
        })

        $('#data-pool select').selectric().on('change', function () {
            var $t = $(this);
            $('input[type=checkbox]', $t.closest('tr')).iCheck('check');
            $('input[type=radio]', $t.closest('tr')).iCheck('check');
        });


        $('#data-pool input').on('ifChecked', function (event) {
            var $t = $(this);
            $t.closest('tr').addClass('selected');
            $('.btn-large').removeAttr('disabled');
        });


        $('#data-pool input').on('ifUnchecked', function (event) {
            var $t = $(this);
            $t.closest('tr').removeClass('selected');
            if (!$('#data-pool input:checked').length) {
                $('.btn-large').attr('disabled', 'disabled');
            }
        });


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * Write a cookie for du.ae for looged in users
         */
        var __myacdu = function () {

            var SUFN, BTNTYPE, DISPLNG;

            BTNTYPE = ";BTNTYPE=0";

            SUFN = "SUFN=" + $.trim($('#topUsername').text());
            if ($('input[name="paymentMethod"]').val() == 'Postpaid') {
                BTNTYPE = ";BTNTYPE=1";
            }
            else {
                if ($('input[name="contractAge"]').val() == 'New') BTNTYPE = ";BTNTYPE=2";
                else if ($('input[name="contractAge"]').val() == 'Old') BTNTYPE = ";BTNTYPE=3";
            }
            DISPLNG = $('html').attr('dir') == 'rtl' ? ';DISPLNG=ar' : ';DISPLNG=en';

            $.removeCookie('__myacdu', {path: '/'});
            $.cookie('__myacdu', SUFN + BTNTYPE + DISPLNG, {expires: $.now() + 1200, path: '/'});

        }
        __myacdu();


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        // Investor relations table & filters
        var ir_anncmnt = $('#ir-anncmnt').DataTable({
            "aoColumns": [{
                "sType": "custom_euro_date"
            }, {}, {}, {}],
            "aaSorting": [
                [0, "desc"]
            ], // Sort by first column descending
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Type a keyword..."
            }
        });


        /*
         * Disable copy/paste on registration step 1
         */
        $('#regFormStep1 #number').on('copy paste', function (e) {
            e.preventDefault();
        });

        //if (!$('input[name="market"]').val() || $('input[name="market"]').val() != 'GSM' && $('input[name="contractType"]').val() != "Mobile") {
        //    $('.icon-id').parent().remove();
        //    $('.profile > div:last').remove();
        //}


        /*
         * replace button on transaction page for TNPS
         */
        $('.details .transaction button.btn').replaceWith('<a class="btn btn-info-strong btn-large btn-right btn-full" href="https://du.typeform.com/to/RfvL0C?source=myaccount-postpaid" target="_blank">Share feedback</a>');


        /*
         * Workaround for billing page
         */
        if ($('#onlyEmail').is(':checked')) {
            if ($('#poBoxId').val() == "") $('#poBoxId').val('0');
            $('#emiratesId, #cityId')
                .removeAttr('data-required')
                .attr('disabled', 'disabled');
            $('#addressId, #poBoxId')
                .removeAttr('data-required')
                .attr('readonly', 'readonly');
            $("select").selectric('refresh');
        }
        ;

        $('#onlyEmail').on('ifChecked', function (event) {
            if ($('#poBoxId').val() == "") $('#poBoxId').val('0');
            $('#emiratesId, #cityId')
                .removeAttr('data-required')
                .attr('disabled', 'disabled');
            $('#addressId, #poBoxId')
                .removeAttr('data-required')
                .attr('readonly', 'readonly');
            $("select").selectric('refresh');
        });

        $('#emailAndPaper').on('ifChecked', function (event) {
            $('#emiratesId, #cityId')
                .removeAttr('disabled')
                .attr('data-required', 'true');
            $('#addressId, #poBoxId')
                .removeAttr('readonly')
                .attr('data-required', 'true');
            $("select").selectric('refresh');
        });


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * Survey page
         */


        if ($('.popin').length > 0) {

            $('.popin #rating-stars').barrating({
                theme: 'stars',
                showSelectedRating: true
            });

            $('.popin #rating-hearts').barrating({
                theme: 'hearts',
                showSelectedRating: true
            });

            $('.popin #rating-square').barrating({
                theme: 'bars-square',
                showValues: true,
                allowEmpty:null,
                showSelectedRating: false
            });

            $('.popin #rating-square_1').barrating({
                theme: 'bars-square',
                showValues: true,
                allowEmpty:null,
                showSelectedRating: false
            });

            if ($('#deactivate').length <= 0) {
                $(".popin").slideDown();
            }

            $("#deactivate").on("click", function (event) {
                event.preventDefault();
                var ths = $(this);
                ths.attr("disabled", "disbaled");
                var popin = $(".popin");
                popin.slideDown();
                popin.find(".close").one("click", function (event) {
                    event.preventDefault();
                    ths.removeAttr("disabled");
                    popin.slideUp();
                });

                popin.find("#submit").one("click", function (event) {
                    event.preventDefault();
                    var btn = $(this);
                    btn.removeClass("btn-primary").addClass("btn-default").html("Close").one("click", function (event) {
                        event.preventDefault();
                        ths.removeAttr("disabled");
                        popin.slideUp();
                    });
                    ths.removeAttr("disabled").off("click");
                    //attach second event
                });
            });
        }
        $("#final_submit").hide();
        $(".chk_bar").on("click", function (event) {
            $j=validate_bar("#rating-square");
            $k=validate_bar("#rating-stars");
            $l=validate_bar("#rating-hearts");
            $m=validate_check(".rating-ques");
            $n=validate_field("#list");
            $0=validate_field("#txt");
            //alert($j);
            //alert($k);
            //alert($l);

            if($j == true && $k == true && $l== true && $m== true && $n== true  && $0== true  ) {
                $("#final_submit").click();
            }

        });


        $(".chk_bar_unsub").on("click", function (event) {
            $j=validate_bar("#rating-square");
            $k=validate_bar("#rating-stars");
            $l=validate_bar("#rating-hearts");
            $m=validate_check(".rating-ques");
            $n=validate_field("#list");

            //alert($j);
            //alert($k);
            //alert($l);

            if($j == true && $k == true && $l== true && $m== true  && $n== true ) {
                $(".chk_bar_unsub").hide();
                $(".popin").slideUp();
                $("#deactivate").removeAttr("disabled");
                $("#deactivate").hide();
                $("#final_submit").show();

            }

        });

        function validate_check(a){
            $chk = $(a). prop("checked") == true;
            if ($chk == false) {
                $errorid = a + "-error";
                $($errorid).addClass("barerror");
                $(a).focus();
                return false;
            } else {
                $errorid = a + "-error";
                $($errorid).removeClass("barerror");
                return true;
            }
        }

        function validate_bar(i) {
            $bval = $(i).val();
            if ($bval == '') {
                $errorid = i + "-error";
                $($errorid).addClass("barerror");
                $(i).focus();
                return false;
            } else {
                $errorid = i + "-error";
                $($errorid).removeClass("barerror");
                return true;
            }
        }
        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         *   Poll block
         */

            if ($('.poll').length > 0) {
            var $poll = $('.poll');
            $poll.find('.bars').hide();
            $poll.find('.answers').show();
            var $btnSubmit = $poll.find('#submit');
            $btnSubmit.attr('disabled','disabled');

            $poll.on('ifClicked', function(){
                $btnSubmit.attr('disabled',null);
            });


            $btnSubmit.on('click', function (event) {
                event.preventDefault();
                var btn = $(this);
                btn.removeClass('btn-primary').addClass('hide').html('Close').one('click', function (event) {
                    event.preventDefault();
                    popin.slideUp();
                });

                $poll.find('.bars').show();

                $poll.find('.bars').addClass('active');
                $poll.find('.bars .bar').each(function (index) {
                    var bar = $(this);
                    var span = bar.append('<span></span>').find('span');
                    var value = bar.data('width') / 100;
                    requestAnimationFrame(function () {
                        span.css({
                            '-webkit-transform': 'scaleX(' + value + ')',
                            '-moz-transform': 'scaleX(' + value + ')',
                            '-ms-transform': 'scaleX(' + value + ')',
                            'transform': 'scaleX(' + value + ')'
                        });
                    });
                });
                $poll.find('.answers').hide();
            })
            var popin = $('.popin');
            popin.slideDown();
            popin.find('.close').one('click', function (event) {
                event.preventDefault();
                popin.slideUp();
            });

        }


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        // Fix 'Show More' button issue on Single Old/New Prepaid Dashboard
        if ($('input[name="paymentMethod"]').val() == 'Prepaid' && !$('#overhead-details-resp0').length) $('#overhead-details-resp').attr('id', 'overhead-details-resp0');

        // Fix <input contenteditable='true' /> issue on IE
        $('input[type=text], input[type=password]', '.login-register').removeAttr('contenteditable');

        // Fix <input contenteditable='true' /> issue on IE for L3 IM3004196
        $('input[type=text], input[type=password]', '.confirm').removeAttr('contenteditable');

        // Print button or link script
        $('.cta-print').click(function () {
            window.print();
        })

        // VAS services table & filters
        var VAS_services = $('#VAS').DataTable({
            "aoColumns": [
                {},
                {},
                {},
                {},
                {"sType": "custom_euro_date"},
                {"sType": "custom_euro_date"},
                {},
                {},
                {"sType": "string"}
            ],
            "aaSorting": [
                [8, "asc"]
            ], // Sort by status column descending
            "info": false,
            "bAutoWidth": true,
            "paging": false,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Type a keyword..."
            },
            initComplete: function () {
                var api = this.api();
                var i = 1;
                var column = api.column(i);
                var select = $(
                    '<select class="visible-xs"><option value="">Filter use type</option></select>'
                ).appendTo($('.table-filters')).on('change',
                    function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val());
                        column.search(val ? '^' + val + '$' :
                            '', true, false).draw();
                    });
                column.data().unique().sort().each(function (d,
                                                             j) {
                    select.append('<option value="' + d +
                        '">' + d + '</option>');
                });

                $('.dataTables_filter input[type=search]').addClass('control full-width');

            }
        });


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * Sticky bottom padding fix and show/hide
         */

        //alert($('#topBar').width());

        $('.headerWrapper, .innerContainer').width($('.topBar').width());
        $(window).resize(function () {
            //$('.headerWrapper').width($('.topBar').width());
            $('.headerWrapper, .innerContainer').width($('.topBar').width());
        });

        if ($('.sticky-bottom')) $('.contentIn').css({'padding-bottom': $('.sticky-bottom').height() + 20 + 'px'});

        $('.breakdown').click(function () {
            if ($(this).hasClass('show')) {
                //$('.closed').switchClass( "closed", "open", 1000, "easeInOutQuad" );
                $('.closed').addClass('open').removeClass('closed');
                $(this).addClass('hide').removeClass('show');
            }
            else {
                //$('.open').switchClass( "open", "closed", 1000, "easeInOutQuad" );
                $('.open').addClass('closed').removeClass('open');
                $(this).addClass('show').removeClass('hide');
            }
            return false;
        });


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * New plans pages
         */

        if($('.choose-package-title').length > 0) var $pageTop = $('.choose-package-title').offset().top;
        var scrollUp = function () {
            $('html, body').stop(true).animate({
                scrollTop: $pageTop
        }, 300);
        }
        //scrollUp();

        if ($('.you-get').length > 0) {

            var $showhidesticky;

            //if ($('body.mobile').length > 0) {
            $(".sticky-top").stick_in_parent({
                sticky_class: 'sticky',
                offset_top: -($('.sticky-top').height() - 30)//-380//(($('body.mobile').length > 0) ? -405 : -140)
            }).on("sticky_kit:stick", function (e) {
                $('.sticky-top .arrow').fadeOut(100);
                $showhidesticky = setTimeout(function () {
                    $('.selection').slideDown(100);
                    $('.selection').click(function () {
                        $('.selection').hide();
                        scrollUp();
                    })
                },1000);
                //console.log(($('.selection').height()));
                /*if ($('body.mobile').length > 0) $('.show-hide-filters').fadeIn(500, function () {
                 $('.show-hide-filters a').click(function () {
                 $(".sticky-top").stick_in_parent({
                 offset_top: -155
                 })
                 return false;
                 })
                 });*/
                //$('.sticky-top h1').fadeOut(500);
                //$('.sticky-top').removeClass('floating');
                console.log("has stuck!", e.target);
            }).on("sticky_kit:unstick", function (e) {
                clearTimeout($showhidesticky);
                $('.selection').hide();
                $('.sticky-top .arrow').fadeIn(300);
                //$('.show-hide-filters').fadeOut(300);
                //$('.sticky-top h1').fadeIn(500);
                //$('.sticky-top').addClass('floating');
                console.log("has unstuck!", e.target);
            });
            //}

            $('.you-get').each(function () {
                $('> div:not(".benefits")', $(this)).each(function (i) {
                    $(this).addClass('item' + i);
                })
                $('> div > div', $(this)).each(function (i) {
                    $(this).addClass('benefit' + i);
                })
            })

            $('.you-get > div:not(".benefits"), .you-get > div > div').hover(function () {
                    $('.' + $(this).attr('class')).addClass('highlight');
                },
                function () {
                    $('.you-get > div:not(".benefits"), .you-get > div > div').removeClass('highlight');
                })

            /*requirejs(["jquery.stickymenu.min"], function (stickymenu) {
             $(function () {
             $(".sticky-nav").stickymenu({
             "stickyBarSelector": ".sticky-nav",
             "menuItemSelector": "li",
             });
             })
             });*/

        }

        // PROJECT BEATS - POSTPAID
        if ($(".mobile-plan-postpaid").length > 0) {
            //Initialize matchHeight
            var matchRowHeights = function matchRowHeights(visibleOnly) {

                // pure JS simple implementation matchHeight
                var matchHeight = function matchHeight(sel) {
                    if (sel.length == 0) return;
                    // the elements within each container
                    var elements = sel;

                    var largest = 0;

                    // loop over all the elements within each container
                    var i = elements.length;
                    while (i--) {

                        elements[i].style.height = "auto";

                        var height = elements[i].offsetHeight;
                        if (height > largest) {

                            largest = height;
                        }
                    }
                    var j = elements.length;
                    while (j--) {
                        elements[j].style.height = largest + "px";
                    }
                };

                var visible = '';
                if (visibleOnly) {
                    visible = ':visible';
                }
                var cards = $('.card-item:visible');
                var cardContent = cards.find(' > .card-content');
                var cardContentYouPay = cardContent.find(' > .you-pay');
                var cardContentYouGet = cardContent.find(' > .you-get');

                matchHeight(cardContentYouPay.find(' > h4' + visible), {});
                matchHeight(cardContentYouPay.find(' > h5' + visible), {});
                matchHeight(cardContentYouGet.find(' > h4' + visible), {});

                matchHeight($(".sticky-top .plan-filters .filter-catg h4"), {});

                var firstCard = cards.eq(0);

                firstCard.find('.you-get > div:not(".benefits")').each(function (index, element) {
                    var elementIndex = $(element).index() + 1;
                    matchHeight(cardContentYouGet.find(' > div:not(".benefits"):nth-child(' + elementIndex + ')' + visible), {});
                });

                firstCard.find('.you-get > div.benefits > div').each(function (index, element) {
                    var elementIndex = $(element).index() + 1;
                    var concatenatedSelector = cardContentYouGet.find('> div.benefits > div:not(.extra):nth-child(' + elementIndex + ')');
                    var concatenatedText = concatenatedSelector.text().replace(/-/g, '');
                    if ($.trim(concatenatedText).length == 0) {
                        concatenatedSelector.attr('style', 'display: none !important');
                    } else {
                        concatenatedSelector.removeAttr('style');
                    }

                    matchHeight(cardContentYouGet.find('> div.benefits > div:nth-child(' + elementIndex + ') > h5' + visible), {});
                    matchHeight(cardContentYouGet.find('> div.benefits > div:nth-child(' + elementIndex + ') > h6' + visible), {});
                });
            };

            $("[data-toggle=popover]").popover({
                trigger: 'focus',
                container: 'body',
                placement: 'auto top'
            });

            // Fixes to iPhone and iPad
            if (navigator.userAgent.indexOf('iPad') >= 0 || navigator.userAgent.indexOf('iPhone') >= 0) {
                // isolate the fix to apply only when a tooltip is shown
                $("[data-toggle=popover]").on("shown.bs.popover", function () {
                    $('body').css('cursor', 'pointer');
                });

                // Return cursor to default value when tooltip hidden.
                // Use 'hide' event (as opposed to "hidden") to ensure .css() fires before
                // 'shown' event for another tooltip.
                $("[data-toggle=popover]").on("hide.bs.popover", function () {
                    $('body').css('cursor', 'auto');
                });

                //Fixes to iPad only
                if (navigator.userAgent.indexOf('iPad') >= 0) {
                    //Layout change doesn't work on iPad, so initiate matchHeight here.
                    setTimeout(function () {
                        matchRowHeights(true);
                    }, 500);
                }
            }

            window.debounceMatchHeight = false;
            $container.isotope('on', 'layoutComplete', function () {

                if (window.debounceMatchHeight == false) {
                    window.debounceMatchHeight = true;

                    matchRowHeights(true);

                    setTimeout(function () {
                        $('.isotope').isotope('layout');
                    }, 100);
                } else {
                    window.debounceMatchHeight = false;
                }
            });

            $(window).on("orientationchange", function () {
                setTimeout(function() {
                    $(window).resize();
                    $('.isotope').isotope('layout');
                }, 500);

                matchRowHeights();

                $('.planDetailsPopup:visible').each(function () {

                    var popupWidth = $(this).width();
                    $(this).css('margin-left', -(popupWidth / 2) + 'px');

                    this.style.removeProperty('width');
                });
            });

            var addGtmDataLayer = function addGtmDataLayer(eventAction, eventLayer) {
                var action = eventAction;
                var actionMatch = action.match(/\.?(.*)/);
                if (actionMatch != null && actionMatch.length > 1) {
                    action = actionMatch[1];
                }

                var label = eventLayer;
                var labelMatch = label.match(/\.?(.*)/);
                if (labelMatch != null && labelMatch.length > 1) {
                    label = labelMatch[1];
                }

                label = $.trim(label);

                if (label.length > 1 && label.startsWith('aed') == false) {
                    label = label.charAt(0).toUpperCase() + label.substr(1);
                }

                dataLayer.push({ event: "click", category: "postpaid_plans", action: action, label: label });
            };
            var addGtmDataLayerActionButton = function addGtmDataLayerActionButton(cardItem, buttonType) {
                var $minutes = $('.plan-sub-filters a.selected').attr('data-filter');
                var $planFee = cardItem.attr("data-monthlyFee").toLowerCase().replace("aed", "");
                var $planType = $.trim($minutes).indexOf('national') >= 0 ? "Emirati" : "Smart";
                addGtmDataLayer($minutes, buttonType + "_" + $planType + "_" + $planFee);
            };

            var infoPopupCardItem;

            //Plan information popup
            $('a[data-reveal-id="info"]').on('click', function (e) {
                e.preventDefault();

                infoPopupCardItem = $(this).closest('.card-item');
                var planInformationUrl = $(this).attr('href');
                var planInformationPrintUrl = planInformationUrl;
                var planInformationDocumentUrl = infoPopupCardItem.attr('data-documentUrl');
                var purchasePlanUrl = infoPopupCardItem.find('.buyPlanButton').attr('href');

                if (planInformationPrintUrl.indexOf('?') == -1) {
                    planInformationPrintUrl += '?print=true';
                } else {
                    planInformationPrintUrl += '&print=true';
                }

                var planDetailsPopup = $('#info');

                planDetailsPopup.find('.loadingOverlay').show();

                planDetailsPopup.find('.planDetailsFrame').attr('src', planInformationUrl);
                planDetailsPopup.find('.printButton').attr('href', planInformationPrintUrl).attr('target', '_blank');
                planDetailsPopup.find('.downloadButton').attr('href', planInformationDocumentUrl).attr('target', '_blank');
                planDetailsPopup.find('.buyButton').attr('href', purchasePlanUrl);

                planDetailsPopup.css('margin-left', -planDetailsPopup.outerWidth() / 2);
                planDetailsPopup.css('display', 'block');

                var width = Math.round(planDetailsPopup.width());
                planDetailsPopup[0].style.setProperty('width', width + 'px', 'important');

                var height = Math.round(planDetailsPopup.height());
                var topPosition = planDetailsPopup.offset().top;
                var bottomOffset = 70;
                var windowHeight = $(window).height();
                if (height + topPosition + bottomOffset > windowHeight) {
                    planDetailsPopup.height(windowHeight - topPosition - bottomOffset);
                }

                addGtmDataLayerActionButton(infoPopupCardItem, "info");
            });

            $('#info .close-reveal-modal').on('click', function () {
                infoPopupCardItem = null;

                var planDetailsPopup = $('#info');

                planDetailsPopup.find('.planDetailsFrame').attr('src', 'about:blank');
                planDetailsPopup.find('.printButton').attr('href', 'javascript:void(0);').attr('target', null);
                planDetailsPopup.find('.downloadButton').attr('href', 'javascript:void(0);').attr('target', null);
                planDetailsPopup.find('.buyButton').attr('href', 'javascript:void(0);');
                planDetailsPopup[0].style.removeProperty('width');
                planDetailsPopup[0].style.removeProperty('display');
            });

            $('#info .printButton').on('click', function (e) {
                e.preventDefault();

                if ($('body.android').length > 0 || $('.firefox .desktop').length > 0) {
                    var printUrl = $(this).attr('href');
                    window.open(printUrl);
                } else {
                    document.PlanDetailsFrame.printDocument();
                }

                addGtmDataLayerActionButton(infoPopupCardItem, "print");
            });

            $('#info .buyButton').on("click", function (e) {
                addGtmDataLayerActionButton(infoPopupCardItem, "buy");
            });

            window.frameReadyCallback = function (window) {
                $('#info .loadingOverlay').hide();
            };
        }

        // PROJECT BEATS - PREPAID
        var dataSliderMonthlyBundle, dataSliderDailyBundle, cachedFilterValueMonthly, cachedFilterValueDaily;

        var prepaidAddGtmDataLayer = function (eventAction, eventLayer) {
            dataLayer.push({ event: "click", category: "prepaid_plans", action: eventAction, label: eventLayer });
        };
        var prepaidAddGtmDataLayerActionButton = function (cardItem, buttonType) {
            var $planFee = cardItem.attr("data-price");
            var $planData = cardItem.attr('data-monthlyData');
            var dataSuffix = '';

            if ($.trim($planData).length > 0) {
                dataSuffix = '_' + $planData;
            }

            prepaidAddGtmDataLayer("recharge_amount", buttonType + "_aed" + $planFee + dataSuffix);
        };

        var prepaidDisplayMessageIfNoCards = function () {
            var isotopeHasCards = $container.data("isotope").filteredItems.length;

            if (isotopeHasCards) {
                $(".no-cards-message").removeClass("show-message");
            } else {
                $(".no-cards-message").addClass("show-message");
            }
        };

        var prepaidIsotopeFilterWithForcedAnimation = function (filterValue) {
            var currentContainerHeight = $(".plan-cards-container.fullWidthContainer").height();

            $(".plan-cards-container.fullWidthContainer").css("min-height", currentContainerHeight + "px");

            $container.isotope({ filter: ".no-cards" });

            setTimeout(function () {
                $container.isotope({ filter: filterValue });

                $(".plan-cards-container.fullWidthContainer").css("min-height", "");

                prepaidDisplayMessageIfNoCards();
            }, 300);
        };

        //lock filters changing for 300ms
        window.prepaidRunFilterId = null;
        var prepaidRunFilters = function (filter) {
            clearTimeout(window.prepaidRunFilterId);

            window.prepaidRunFilterId = setTimeout(function () {
                prepaidIsotopeFilterWithForcedAnimation(filter);
            }, 300);
        };

        if ($(".mobile-plan-prepaid").length > 0) {
            //Initialize matchHeight
            var matchRowHeights = function (visibleOnly) {

                // pure JS simple implementation matchHeight
                var matchHeight = function (sel) {
                    if (sel.length == 0) return;
                    // the elements within each container
                    var elements = sel;

                    var largest = 0;

                    // loop over all the elements within each container
                    var i = elements.length;
                    while (i--) {

                        elements[i].style.height = "auto";

                        var height = elements[i].offsetHeight;
                        if (height > largest) {

                            largest = height;
                        }
                    }
                    var j = elements.length;
                    while (j--) {
                        elements[j].style.height = largest + "px";
                    }
                };

                var visible = '';
                if (visibleOnly) {
                    visible = ':visible';
                }
                var cards = $('.card-item:visible');
                var cardContent = cards.find(' > .card-content');
                var cardContentYouPay = cardContent.find(' > .you-pay');
                var cardContentYouGet = cardContent.find(' > .you-get');

                matchHeight(cardContentYouPay.find(' > h4' + visible), {});
                matchHeight(cardContentYouPay.find(' > h5' + visible), {});
                matchHeight(cardContentYouGet.find(' > h4' + visible), {});

                matchHeight($(".sticky-top .plan-filters .filter-catg h4"), {});

                var firstCard = cards.eq(0);

                firstCard.find('.you-get > div:not(".benefits")').each(function (index, element) {
                    var elementIndex = $(element).index() + 1;
                    matchHeight(cardContentYouGet.find(' > div:not(".benefits, .social-icons"):nth-child(' + elementIndex + ')' + visible), {});
                });

                firstCard.find('.you-get > div.benefits > div').each(function (index, element) {
                    var elementIndex = $(element).index() + 1;
                    var concatenatedSelector = cardContentYouGet.find('> div.benefits > div:not(.extra):nth-child(' + elementIndex + ')');
                    var concatenatedText = concatenatedSelector.text();
                    if ($.trim(concatenatedText).length == 0) {
                        concatenatedSelector.attr('style', 'display: none !important');
                    } else {
                        concatenatedSelector.removeAttr('style');
                    }

                    matchHeight(cardContentYouGet.find('> div.benefits > div:nth-child(' + elementIndex + ') > h5' + visible), {});
                    matchHeight(cardContentYouGet.find('> div.benefits > div:nth-child(' + elementIndex + ') > h6' + visible), {});
                });

                matchHeight($('.card-item:visible > .card-content > .you-pay > h4' + visible), {});
                //matchHeight($('.card-item:visible .card-content .you-get' + visible), {});
            };

            // Fixes to iPhone and iPad
            if (navigator.userAgent.indexOf('iPad') >= 0 || navigator.userAgent.indexOf('iPhone') >= 0) {
                // isolate the fix to apply only when a tooltip is shown
                $("[data-toggle=popover]").on("shown.bs.popover", function () {
                    $('body').css('cursor', 'pointer');
                });

                // Return cursor to default value when tooltip hidden.
                // Use 'hide' event (as opposed to "hidden") to ensure .css() fires before
                // 'shown' event for another tooltip.
                $("[data-toggle=popover]").on("hide.bs.popover", function () {
                    $('body').css('cursor', 'auto');
                });

                //Fixes to iPad only
                if (navigator.userAgent.indexOf('iPad') >= 0) {
                    //Layout change doesn't work on iPad, so initiate matchHeight here.
                    setTimeout(function () {
                        matchRowHeights(true);
                    }, 500);
                }
            }

            var recenterPopup = function recenterPopup(popupId) {
                var popup = $("#" + popupId);
                popup.css("margin-left", -popup.outerWidth() / 2);
            };

            var fixPopup = function fixPopup() {
                if ($("#info").css("display") !== "none") {
                    recenterPopup("info");
                }
                if ($("#activate").css("display") !== "none") {
                    recenterPopup("activate");
                }
            };

            var forceCardsRepositioning = function forceCardsRepositioning() {
                $container.isotope('layout');
            };

            var firstTimeLayoutComplete = true;
            $container.isotope("on", "layoutComplete", function () {
                fixPopup();
                setTimeout(function () {
                    matchRowHeights(true);
                    if (firstTimeLayoutComplete) {
                        // It shouldn't run every time because it slows down the site
                        setTimeout(forceCardsRepositioning, 200);
                        firstTimeLayoutComplete = false;
                    }
                }, 500);

                autoMatchheight(true);

                $('.plan-cards').addClass('loaded');
            });

            $('.you-get').each(function () {
                $('> div:not(".benefits, .social-icons, .special-feature-wrapper"), > .special-feature-wrapper > div', $(this)).each(function (i) {
                    $(this).addClass('prepaidItem' + i);
                });
                $('> div > div', $(this)).each(function (i) {
                    $(this).addClass('prepaidBenefit' + i);
                });
            });

            $('.you-get > div:not(".benefits"), .you-get > div > div').hover(function () {
                $('.' + $(this).attr('class')).addClass('highlight');
            }, function () {
                $('.you-get > div:not(".benefits"), .you-get > div > div').removeClass('highlight');
            });

            //Initialize filters
            $('.prepaid-categories').on('click', 'a', function (e, args) {

                var button = $(e.currentTarget);

                //Apply filter
                var filter = $.trim(button.attr('data-filter')).toLowerCase();
                var filterToApply = "." + filter;
                var selectedSpend = 200;

                if (filter == 'monthlybundle') {
                    selectedSpend = 200;
                    if (dataSliderMonthlyBundle) {
                        selectedSpend = parseInt(dataSliderMonthlyBundle.noUiSlider.get());
                    }

                    if (typeof args == 'undefined' || args == null || !args.preventTagFromFiring) {
                        prepaidAddGtmDataLayer("recharge_type", 'monthly');
                    }
                    $(".dataContract.monthlyBundle").removeClass("hide");
                    $(".dataContract.dailyBundle").addClass("hide");

                    if (!!cachedFilterValueMonthly) {
                        filterToApply = cachedFilterValueMonthly;
                    }
                } else if (filter == 'dailybundle') {
                    selectedSpend = 5;
                    if (dataSliderDailyBundle) {
                        selectedSpend = parseInt(dataSliderDailyBundle.noUiSlider.get());
                    }

                    if (typeof args == 'undefined' || args == null || !args.preventTagFromFiring) {
                        prepaidAddGtmDataLayer("recharge_type", 'daily');
                    }
                    $(".dataContract.monthlyBundle").addClass("hide");
                    $(".dataContract.dailyBundle").removeClass("hide");

                    if (!!cachedFilterValueDaily) {
                        filterToApply = cachedFilterValueDaily;
                    }
                }

                prepaidRunFilters(filterToApply);

                //Apply button states
                button.parent().find('a').removeClass('selected');
                button.addClass('selected');

                //Update sticky filter
                var text = button.text();

                $('.sticky-top .selection .bundle h5').text(text);

                text = !isLTR ? selectedSpend + ' درهم' : 'AED ' + selectedSpend;
                $('.sticky-top .selection .spend h5').text(text);
            });

            $('.prepaid-categories a.selected').eq(0).trigger('click', { preventTagFromFiring: true });

            $('.du-accordion .block-header').on('click', function () {

                var block = $(this).closest('.block');

                if (!block.hasClass('open')) {
                    return;
                }

                if (block.hasClass('how-to-get-a-sim-block')) {
                    prepaidAddGtmDataLayer("details", 'howtogetasim');
                } else if (block.hasClass('how-to-recharge-block')) {
                    prepaidAddGtmDataLayer("details", 'howtorecharge');
                } else if (block.hasClass('faqs-block')) {
                    prepaidAddGtmDataLayer("details", 'faqs');
                } else if (block.hasClass('terms-and-conditions-block')) {
                    prepaidAddGtmDataLayer("details", 'termsandconditions');
                }
            });

            var updateHeightAndPosition = function updateHeightAndPosition() {
                setTimeout(function () {
                    matchRowHeights(true);
                    setTimeout(forceCardsRepositioning, 200);
                }, 500);
            };

            $(window).on("orientationchange", function () {
                setTimeout(function() {
                    $(window).resize();
                    $('.isotope').isotope('layout');
                }, 500);

                matchRowHeights();

                setTimeout(function () {
                    fixPopup();
                    autoMatchheight();
                }, 500);

                updateHeightAndPosition();

                $('.planDetailsPopup:visible').each(function () {

                    var popupWidth = $(this).width();
                    $(this).css('margin-left', -(popupWidth / 2) + 'px');

                    this.style.removeProperty('width');
                });
            });

            // Activate popup
            $('a[data-reveal-id="activate"]').on('click', function () {
                var planDetailsPopup = $('#activate');

                planDetailsPopup.css('display', 'block');

                recenterPopup("activate");

                var cardItem = $(this).closest('.card-item');
                var planTitle = cardItem.attr('data-planTitle');

                $('#activate .header .plan-title').text(planTitle);

                prepaidAddGtmDataLayerActionButton(cardItem, "activate");
            });

            //Plan information popup
            var infoPopupCardItem;

            $('a[data-reveal-id="info"]').on('click', function (e) {
                e.preventDefault();

                infoPopupCardItem = $(this).closest('.card-item');
                var planInformationUrl = $(this).attr('href');
                var planInformationPrintUrl = planInformationUrl;
                var planInformationDocumentUrl = infoPopupCardItem.attr('data-documentUrl');
                var purchasePlanUrl = infoPopupCardItem.find('.buyPlanButton').attr('href');

                if (planInformationPrintUrl.indexOf('?') == -1) {
                    planInformationPrintUrl += '?print=true';
                } else {
                    planInformationPrintUrl += '&print=true';
                }

                var planDetailsPopup = $('#info');

                planDetailsPopup.find('.loadingOverlay').show();

                planDetailsPopup.find('.planDetailsFrame').attr('src', planInformationUrl);
                planDetailsPopup.find('.printButton').attr('href', planInformationPrintUrl).attr('target', '_blank');
                planDetailsPopup.find('.downloadButton').attr('href', planInformationDocumentUrl).attr('target', '_blank');
                planDetailsPopup.find('.activateButton-info').show();
                planDetailsPopup.find('.activateButton-info-text').remove();

                planDetailsPopup.css('display', 'block');

                recenterPopup("info");

                var width = Math.round(planDetailsPopup.width());
                planDetailsPopup[0].style.setProperty('width', width + 'px', 'important');

                var height = Math.round(planDetailsPopup.height());
                var topPosition = planDetailsPopup.offset().top;
                var bottomOffset = 70;
                var windowHeight = $(window).height();
                if (height + topPosition + bottomOffset > windowHeight) {
                    planDetailsPopup.height(windowHeight - topPosition - bottomOffset);
                }

                prepaidAddGtmDataLayerActionButton(infoPopupCardItem, "info");
            });

            $('#info .close-reveal-modal').on('click', function () {

                var planDetailsPopup = $('#info');

                planDetailsPopup.find('.planDetailsFrame').attr('src', 'about:blank');
                planDetailsPopup.find('.printButton').attr('href', 'javascript:void(0);').attr('target', null);
                planDetailsPopup.find('.downloadButton').attr('href', 'javascript:void(0);').attr('target', null);
                planDetailsPopup[0].style.removeProperty('width');
            });

            $('#info .printButton').on('click', function (e) {

                e.preventDefault();

                if ($('body.android').length > 0 || $('.firefox .desktop').length > 0) {
                    var printUrl = $(this).attr('href');
                    window.open(printUrl);
                } else {
                    document.PlanDetailsFrame.printDocument();
                }

                prepaidAddGtmDataLayerActionButton(infoPopupCardItem, "print");
            });

            $("#info .activateButton-info").on("click", function (e) {
                e.preventDefault();

                var dialText = !isLTR ? "للتفعيل، اضغط<span dir=\"ltr\">*135*4#&nbsp;</span>" : "To activate, dial *135*4#";

                $(this).hide();
                $(this).after("<span class='activateButton-info-text'>" + dialText + "</span>");

                prepaidAddGtmDataLayerActionButton(infoPopupCardItem, "activate");
            });

            window.frameReadyCallback = function (window) {
                $('#info .loadingOverlay').hide();
            };
        }

        // PROJECT BEATS - TOURIST PLAN
        if ($(".mobile-plan-tourist").length > 0) {
            var recenterPopup = function recenterPopup(popupId) {
                var popup = $("#" + popupId);
                popup.css("margin-left", -popup.outerWidth() / 2);
            };

            var fixPopup = function fixPopup() {
                if ($("#info").css("display") !== "none") {
                    recenterPopup("info");
                }
            };

            $(window).on("orientationchange", function () {
                setTimeout(function () {
                    fixPopup();
                }, 500);

                $('.planDetailsPopup:visible').each(function () {

                    var popupWidth = $(this).width();
                    $(this).css('margin-left', -(popupWidth / 2) + 'px');

                    this.style.removeProperty('width');
                });
            });

            //Plan information popup
            var infoPopupCardItem;

            $('a[data-reveal-id="info"]').on('click', function (e) {
                e.preventDefault();

                infoPopupCardItem = $(this).closest('.card-item');
                var planInformationUrl = $(this).attr('href');
                var planInformationPrintUrl = planInformationUrl;
                var planInformationDocumentUrl = infoPopupCardItem.attr('data-documentUrl');
                var purchasePlanUrl = infoPopupCardItem.find('.buyPlanButton').attr('href');

                if (planInformationPrintUrl.indexOf('?') == -1) {
                    planInformationPrintUrl += '?print=true';
                } else {
                    planInformationPrintUrl += '&print=true';
                }

                var planDetailsPopup = $('#info');

                planDetailsPopup.find('.loadingOverlay').show();

                planDetailsPopup.find('.planDetailsFrame').attr('src', planInformationUrl);
                planDetailsPopup.find('.printButton').attr('href', planInformationPrintUrl).attr('target', '_blank');
                planDetailsPopup.find('.downloadButton').attr('href', planInformationDocumentUrl).attr('target', '_blank');
                planDetailsPopup.find('.activateButton-info').show();
                planDetailsPopup.find('.activateButton-info-text').remove();

                planDetailsPopup.css('display', 'block');

                recenterPopup("info");

                var width = Math.round(planDetailsPopup.width());
                planDetailsPopup[0].style.setProperty('width', width + 'px', 'important');

                var height = Math.round(planDetailsPopup.height());
                var topPosition = planDetailsPopup.offset().top;
                var bottomOffset = 70;
                var windowHeight = $(window).height();
                if (height + topPosition + bottomOffset > windowHeight) {
                    planDetailsPopup.height(windowHeight - topPosition - bottomOffset);
                }

                // prepaidAddGtmDataLayerActionButton(infoPopupCardItem, "info");
            });

            $('#info .close-reveal-modal').on('click', function () {

                var planDetailsPopup = $('#info');

                planDetailsPopup.find('.planDetailsFrame').attr('src', 'about:blank');
                planDetailsPopup.find('.printButton').attr('href', 'javascript:void(0);').attr('target', null);
                planDetailsPopup.find('.downloadButton').attr('href', 'javascript:void(0);').attr('target', null);
                planDetailsPopup[0].style.removeProperty('width');
            });

            $('#info .printButton').on('click', function (e) {

                e.preventDefault();

                if ($('body.android').length > 0 || $('.firefox .desktop').length > 0) {
                    var printUrl = $(this).attr('href');
                    window.open(printUrl);
                } else {
                    document.PlanDetailsFrame.printDocument();
                }

                // prepaidAddGtmDataLayerActionButton(infoPopupCardItem, "print");
            });

            $("#info .activateButton-info").on("click", function (e) {
                e.preventDefault();

                var dialText = !isLTR ? "للتفعيل، اضغط<span dir=\"ltr\">*135*4#&nbsp;</span>" : "To activate, dial *135*4#";

                $(this).hide();
                $(this).after("<span class='activateButton-info-text'>" + dialText + "</span>");

                // prepaidAddGtmDataLayerActionButton(infoPopupCardItem, "activate");
            });

            window.frameReadyCallback = function (window) {
                $('#info .loadingOverlay').hide();
            };

            $("[data-toggle=popover]").popover({
                trigger: 'focus',
                container: 'body',
                placement: 'auto top'
            });

            // Fixes to iPhone and iPad
            if (navigator.userAgent.indexOf('iPad') >= 0 || navigator.userAgent.indexOf('iPhone') >= 0) {
                // isolate the fix to apply only when a tooltip is shown
                $("[data-toggle=popover]").on("shown.bs.popover", function () {
                    $('body').css('cursor', 'pointer');
                });

                // Return cursor to default value when tooltip hidden.
                // Use 'hide' event (as opposed to "hidden") to ensure .css() fires before
                // 'shown' event for another tooltip.
                $("[data-toggle=popover]").on("hide.bs.popover", function () {
                    $('body').css('cursor', 'auto');
                });
            }

            // Fix to close Popover on iPad
            if ($("html").hasClass("ipad")) {
                $(document).on("click touchstart", function (e) {
                    if ($(e.target).data("toggle") !== "popover" && $(e.target).parents("[data-toggle='popover']").length === 0 && $(e.target).parents(".popover.in").length === 0) {
                        $("[data-toggle='popover']").popover("hide");
                    }
                });
            }

            var touristAddGtmDataLayer = function addGtmDataLayer(eventAction, eventLayer) {
                dataLayer.push({ EventCategory: "tourist_plans", EventAction: eventAction, EventLabel: eventLayer });
            };

            $('.du-accordion .block-header').on('click', function () {
                var block = $(this).closest('.block');

                if (!block.hasClass('open')) {
                    return;
                }

                if (block.hasClass('how-to-get-a-sim-block')) {
                    touristAddGtmDataLayer("details", 'howtogetasim');
                } else if (block.hasClass('how-to-recharge-block')) {
                    touristAddGtmDataLayer("details", 'howtorecharge');
                } else if (block.hasClass('faqs-block')) {
                    touristAddGtmDataLayer("details", 'faqs');
                } else if (block.hasClass('terms-and-conditions-block')) {
                    touristAddGtmDataLayer("details", 'termsandconditions');
                }
            });
        }


        // DU ACCORDIONS - FAQ, T&C
        if ($(".du-accordion").length > 0) {
            var closeAllAccordions = function closeAllAccordions(accordionWidget) {
                accordionWidget.find(".block.open").removeClass("open");
            };

            $('.du-accordion .block-header').click(function () {
                var accordionWidget = $(this).closest(".du-accordion");
                var accordionBlock = $(this).closest(".block");
                var accordionBlockWasOpen = accordionBlock.hasClass("open");

                closeAllAccordions(accordionWidget);

                if (!accordionBlockWasOpen) {
                    accordionBlock.addClass("open");

                    /*
                    var blockVerticalPosition = accordionBlock.position().top;
                    var menuHeight = $('#topBar').outerHeight();
                    var scrollTarget = blockVerticalPosition - menuHeight;
                    var currentScroll = $('body').scrollTop();

                    if (currentScroll > scrollTarget) {
                        $('html, body').scrollTop(scrollTarget);
                    }
                    */
                }
            });

            if ($(".filterBtns").length > 0) {
                $(".filterBtns").on("click", "a", function () {
                    var buttonGroup = $(this).closest(".btn-group");
                    var panelToShowFilter = $(this).attr("rel");

                    buttonGroup.find("a").removeClass("is-checked");
                    $(this).addClass("is-checked");

                    $(".packs-list").hide();
                    $(".packs-list" + panelToShowFilter).show();

                    return false;
                });
            }

            /*
            $('.filterBtns').on('click', 'a', function () {
                var filterValue = $(this).attr('rel');

                $('.packs-list').hide();
                $(filterValue).show();

                return false;
            });

            $('.filterBtns').each(function (i, buttonGroup) {
                var $buttonGroup = $(buttonGroup);
                $buttonGroup.on('click', '> a', function () {
                    $('.dropdown-menu', $(this).parent()).hide();
                    $buttonGroup.find('.is-checked').removeClass('is-checked');
                    $(this).addClass('is-checked');
                });
            });
            */
        }

        // PROJECT BEATS - PAY AS YOU GO / HOW TO RECHARGE
        if ($(".plans.pay-as-you-go").length > 0) {
            var handleStickySectionEnum = function handleStickySectionEnum() {
                var sections = [];
                var sectionsTopPosition = [];
                var firstSectionHeight;

                var handleStickySectionEnumOnScroll = function handleStickySectionEnumOnScroll() {
                    var isTouch = $("html").hasClass("touch");
                    var windowTopPosition = window.scrollY || window.pageYOffset;
                    var sectionTopPosition = 0;
                    var stuckSectionId = -1;

                    for (var idxSection = 0; idxSection < sectionsTopPosition.length; idxSection++) {
                        if (windowTopPosition > sectionsTopPosition[idxSection] && sectionsTopPosition[idxSection] > sectionTopPosition) {
                            sectionTopPosition = sectionsTopPosition[idxSection];
                            stuckSectionId = idxSection;
                        }
                    }

                    if (isTouch && $(window).scrollTop() + $(window).height() >= $(document).height() - 80) {
                        stuckSectionId = -1;
                    }

                    if (stuckSectionId < 0) {
                        $(".sticky-section-enum").removeClass("is-stuck");
                        $(".plans.pay-as-you-go").css("margin-top", "");
                        return;
                    }
                    if (!sections[stuckSectionId].hasClass("is-stuck")) {
                        $(".sticky-section-enum").removeClass("is-stuck");
                        sections[stuckSectionId].addClass("is-stuck");
                        if (!isTouch && stuckSectionId === 0) {
                            $(".plans.pay-as-you-go").css("margin-top", firstSectionHeight + "px");
                        }
                    }
                };

                $(".sticky-section-enum").each(function (index) {
                    sections.push($(this));

                    var sectionTopPosition = $(this).offset().top;
                    if (index > 0) {
                        sectionTopPosition -= $(this).height();
                    } else {
                        firstSectionHeight = $(this).height();
                    }

                    sectionsTopPosition.push(sectionTopPosition);
                });

                handleStickySectionEnumOnScroll();
                $(window).on("scroll", handleStickySectionEnumOnScroll);
            };

            handleStickySectionEnum();
        }


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * Generate bullets on modify rate plan page and init sticky
         */
        if ($('.modify-plans').length || $('.payment-plans').length) {
            var offset_top = -20;
            // offset_top = $('.payment-plans').length ? 50 : offset_top;

            $generateBullets($('.plans-accordion > ul > li').reverse());
            selfDistruct('.selfdistruct', 2000);

            $(".sticky-top").stick_in_parent({
                sticky_class: 'sticky',
                offset_top: offset_top
            }).on("sticky_kit:stick", function (e) {
                console.log("has stuck!", e.target);
                //$('.sticky-top h1').fadeOut(500);
                //$('.sticky-top').removeClass('floating');
            }).on("sticky_kit:unstick", function (e) {
                console.log("has unstuck!", e.target);
                //$('.sticky-top h1').fadeIn(500);
                //$('.sticky-top').addClass('floating');
            });
        }


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * Fix service request page
         */
        if ($('.request-fixed').length) {

                // enable next select box
            var locationSelector = function(){

                var showNext = function(element) {
                    if(element.length){
                       
                                if( element.hasClass('select2')){
                                        element.closest('.group').find('.select2').hide();
                                        
                                    }else if(element.attr('type')!='text'){
                                        element.closest('.group').find('.selectric').hide();
                                    }

                                    element.closest('.group').find('input.control').show();
                                    

                                    var nextElement = $('#' + element.attr('rel'));
                                    if(nextElement.length) {
                                            showNext(nextElement);
                                    }
                            }
                }
                var hideNext = function(element) {
                        if(element.length){
                            
                                if( element.hasClass('select2')){
                                        element.closest('.group').find('.select2').show();
                                    
                                        initSelect2Plugin(element);
                                        
                                    }else if(!element.hasClass('noselectric') && element.attr('type')!='text'){
                                        element.closest('.group').find('.selectric').show();
                                        element.selectric('refresh');
                                    } 
                                    
                                    if(element.attr('type')!='text'){
                                        element.closest('.group').find('input.control').hide();
                                    }

                                    var nextElement = $('#' + element.attr('rel'));
                                    if(nextElement.length) {
                                            hideNext(nextElement);
                                    }
                            }
                }
                $('#address-options select, #address-options input').each(function () {
                    $(this).on('change', function(){
                       
                        var $this = $(this);
                  
                        if($('option:selected', $this).attr('value') == 'other') {


                           if($this.closest('.group').find('.select2').length){
                               $this.closest('.group').find('.select2').hide();
                               $this.closest('.group').find('input.control').show();
                           }else {
                               $this.closest('.group').find('.selectric').hide();
                           } 

                            var nextElement = $('#' + $this.attr('rel'));

                            showNext(nextElement);

                            if($this.closest('.group').find('.select2').length){

                                $this.closest('.group').find('input.control').show();
                                $this.closest('.group').find('input.control').focus();

                            }else {
                                $this.closest('.group').find('input.control').show().focus();

                            }

                            $this.closest('.group').find('.input-icon').show();
                            $this.closest('.group').find('.dd-popover').hide();

                     
                           $('.input-icon').on('click', function(){
                                $this.closest('.group').find('input.control').hide();
                                $('option:first',$(this).closest('.group').find('select')).attr('selected','selected');

                                var selectInput = $(this).closest('.group').find('select');

                                if(selectInput.hasClass("select2")) {

                                    $this.closest('.group').find('.select2').show();
                                    $this.closest('.group').find('select.select2').val(null).trigger("change"); 

                                }else {

                                    selectInput.selectric('refresh');
                                    $(this).closest('.group').find('.selectric-control').show()
                                    $this.closest('.group').find('.selectric').show();
                                }


                                $this.closest('.group').find('.input-icon').hide();
                                $this.closest('.group').find('.dd-popover').show();

                                $(this).hide();
 
                                 var nextElement = $('#' + $this.attr('rel'));
                                 hideNext(nextElement);

                                return false;
                            });

                            return;

                        }
                        else {
                            var nextElement = $('#' + $this.attr('rel'));
                            hideNext(nextElement);
                        }
 
                       if ($('#' + $(this).attr('rel')).attr('type') == 'text') {
                            $('#' + $(this).attr('rel')).removeAttr('disabled');
                        }
                        else {
                            var nextElement = $('#' + $(this).attr('rel'));
                            if(nextElement.length && !nextElement.hasClass('noselectric')){
                                nextElement.removeAttr('disabled').selectric('refresh');
                            }else {
                                nextElement.removeAttr('disabled')
                                initSelect2Plugin(nextElement);
                            }

                        }

                    })
                })

                $('input.last-field, textarea.last-field').on('keydown', function() {
                    $('#address-options .continue').removeAttr('disabled');
                })

                $('select.last-field').on('change', function(){
                    $('#address-options .continue').removeAttr('disabled');
                })
            }

            if($('.fixed-relocation').length > 0) {
                locationSelector();
            }

            $('.show-map').click(function(){
                if($(this).hasClass('open')) {
                    $(this).text($(this).attr('data-show-text')).removeClass('open');
                    $('.map').slideUp('fast');
                    $(document).trigger('map-opened');
                }
                else {
                    $(this).text($(this).attr('data-hide-text')).addClass('open');
                    $('.map').slideDown('fast');
                    $(document).trigger('map-closed');
                }
                return false;
            })

          

            // documents section if/else logic
            $('[name=nationality]').on('ifChecked', function (e) {
                //alert('hi');
                $('[name=yourid],[name=yourid1],[name=tenancy]').iCheck('uncheck');
                var uaenational = $('.uae-national');
                var nonuaenational = $('.non-uae-national');
                var visa = $('.visa');
                $('.tenancy-noc, .tenancy-noc-yes-no').slideUp();
                if (e.target.id == "uae") {
                    $('.list-table').slideUp();
                    nonuaenational.slideUp();
                    visa.slideUp();
                    uaenational.slideDown();
                }
                else if (e.target.id == "gcc" || e.target.id == "uaeex") {
                    $('.list-table').slideUp();
                    uaenational.slideUp();
                    nonuaenational.slideDown();
                    visa.slideDown();
                }
            });

            // Passport/EID if/else
            $('[name=yourid],[name=yourid1]').on('ifChecked', function (e) {
                //alert('hi');
                $('.tenancy-noc-yes-no').slideDown();
                var passport = $('.passport');
                var visa = $('.visa');
                var emiratesid = $('.emirates-id');
                if (e.target.id == "eid" || e.target.id == "eid1") {
                    passport.slideUp();
                    visa.slideUp();
                    emiratesid.slideDown();
                }
                else if (e.target.id == "passport" || e.target.id == "passport1") {
                    emiratesid.slideUp();
                    passport.slideDown();
                    if (!$('#uae:checked').length) visa.slideDown();
                }
            });

            // Tenancy NOC if/else logic
            $('[name=tenancy]').on('ifChecked', function (e) {
                //alert('hi');
                var tenancy = $('.tenancy-noc');
                if (e.target.id == "tenancy-no") {
                    tenancy.slideDown();
                }
                else {
                    tenancy.slideUp();
                }
                $('.list-table').slideDown();
            });
        }


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * Request fixed services for Enterprise/Consumer
         */
        if ($('.request-fixed, .enterprise-services').length) {

            $('.rounded-options .item').click(function () {
                $(this).addClass('active').siblings().removeClass('active')
            });

            // Div show/hide function
            $('[data-slide-down]').click(function () {
                var selector = $(this).data('slideDown');
                $(selector).slideDown(function(){
                    //alert($(selector).offset().top);
                    $('html, body').animate({
                        scrollTop: $(selector).offset().top
                    }, 1000);
                });
            });

            // Etisalat or du?
            $('[name=preference]').on('ifChecked', function (e) {
                $('[name=preference1]').iCheck('uncheck');
                var noSection = $('.no-section');
                var yesSection = $('.yes-section');
                if (e.target.value == "yes") {
                    yesSection.slideDown();
                    noSection.slideUp();
                    /*$('#address-options select')
                        .find('option:first')
                        .attr('selected','selected');
                    $('#address-options select').selectric('refresh');*/
                    initSelect2Plugin($("select.select2"));

                } else {
                    noSection.slideDown(function () {
                        $('#add-appt-number').parent().find('select, input').addClass('last-field');
                        $('#neghNumber')
                            .attr('data-required','false')
                            .attr('data-valid','landline')
                            //.attr('disabled','disabled')
                            .attr('placeholder','Neighbour\'s landline num. (optional)')
                            .removeClass('last-field');
                    });
                    yesSection.slideUp();
                    locationSelector();
                    initSelect2Plugin($("select.select2"));

                }
            });

            // Order additional du line or change to du
            $('[name=preference1]').on('ifChecked', function (e) {
               
                var adddtionaldu = $('.additional-du');
                var changetodu = $('.change-to-du');
                if (e.target.value == "yes") {
                    changetodu.slideDown();
                    adddtionaldu.slideUp();
                    initSelect2Plugin($("select.select2"));
                } else {
                    adddtionaldu.slideDown(function(){
                        $('.last-field, .selectric-last-field').each(function(){
                            $(this)
                                .removeClass('last-field')
                                .removeClass('selectric-last-field');
                        })
                        $('#neghNumber')
                            .attr('data-required','true')
                            .attr('data-valid','landline')
                            //.attr('disabled','disabled')
                            .attr('placeholder','Neighbour\'s landline number')
                            .addClass('last-field');
                        locationSelector();
                    });
                    changetodu.slideUp();
                    initSelect2Plugin($("select.select2"));
                }
            });

            // Form validator
            $('#address-options, #contact-billing-details').protoValidator();

            // show package section if form validates
            $("#address-options").on('submit', function (e) {
                if ($(this).children().find('.group.error').length == 0) {
                    if ($('.choose-package').length) {
                        //alert('nay');
                        $('.choose-package').slideDown(function(){
                            $('html, body').animate({
                                scrollTop: $('.choose-package').offset().top
                            }, 1000);
                        });
                    }
                    else {
                        //alert('hey');
                        $('.upload-documents').slideDown(function(){
                            $('html, body').animate({
                                scrollTop: $('.upload-documents').offset().top
                            }, 1000);
                        });
                    }
                    $(window).resize();
                    return false;
                }
            })

            // show confirmation section if contact/billing form validates
            $("#contact-billing-details").on('submit', function(e) {
                if ($(this).children().find('.group.error').length == 0) {

                        $('.confirm').slideDown(function(){
                            $('html, body').animate({
                                scrollTop: $('.confirm').offset().top
                            }, 1000);
                        });

                    $(window).resize();
                    return false;
                }
            })

            // Credit Card page

            /*$('body').on('click', '[data-reveal-id]', function (e) {
             e.preventDefault();
             var modalLocation = $(this).attr('data-reveal-id');
             $('#' + modalLocation).reveal($(this).data());
             });*/
        }


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * Creadit card page show/hide
         */
        if ($('.cc-form').length) {
            $("#rechargevoucher").on("ifChecked", function () {
                $(".cc-payment").hide();
                $(".voucher-payment").show();
            });

            $("#rechargecc").on("ifChecked", function () {
                $(".cc-payment").show();
                $(".voucher-payment").hide();
            });

            $(".voucher-payment").hide();

            $(".new-card").hide();

            $(".use-saved").on("click", function (event) {
                event.preventDefault();
                $(".new-card").hide();
                $(".saved-card").show();
            });

            $(".use-new").on("click", function (event) {
                event.preventDefault();
                $(".new-card").show();
                $(".saved-card").hide();
            });
        }


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        // Generic Screens page
        $(".showalert").on("click", function () {
            alertView.alert({
                message: $(this).data("class") + " - Autopayment is active. Your bill will automatically be paid in full each month.",
                action: "Edit",
                link: "#",
                _class: $(this).data("class")
            });
        });
        $('.showpopover').popover({
            html: true,
            title: "Alert",
            content: "Your bill is overdue, <a href='#'>click here</a> to payit now and avoid possible service interruption."
        });


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * TNPS survey in a lightbox
         */
        $('#tnps1').reveal({
            animation: 'fadeAndPop',                   //fade, fadeAndPop, none
            animationspeed: 300,                       //how fast animtions are
            closeonbackgroundclick: false,              //if you click background will modal close?
            dismissmodalclass: 'close-reveal-modal'    //the class of a button or element that will close an open modal
        });
        $('#tnps2 #rating-squares').barrating({
            theme: 'bars-square',
            showValues: true,
            showSelectedRating: false
        });
        $('#tnps2 #example-1to10').barrating({
            theme: 'bars-1to10',
            showValues: false,
            showSelectedRating: true
        });


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        /*
         * Sliders for plans pages
         */
        if ($("[id*='slider']").length) {
            requirejs(["nouislider.min"], function (noUiSlider) {
                $(function () {

                    var priceSlider = document.getElementById('price-slider');
                    if (priceSlider != undefined) {
                        noUiSlider.create(priceSlider, {
                            start: [1000], // Handle start position
                            step: 150, // Slider moves in increments of '10'
                            snap: true,
                            //margin: 2, // Handles must be more than '20' apart
                            connect: 'lower', // Display a colored bar between the handles
                            direction: isLTR ? 'ltr' : 'rtl', // Put '0' at the bottom of the slider
                            //orientation: 'vertical', // Orient the slider vertically
                            //behaviour: 'tap-drag', // Move handle on tap, bar is draggable
                            range: { // Slider can select '0' to '100'
                                'min': [150],
                                '33.33%': [300],
                                '66.66%': [450],
                                'max': [1000]
                            },
                            pips: { // Show a scale with the slider
                                mode: 'steps',
                                density: 150,
                                format: wNumb({
                                    prefix: isLTR ? 'AED ' : 'درهم '
                                })
                            }
                        });

                        priceSlider.noUiSlider.on('change', function(){
                            scrollUp();
                        });
                    }

                    var priceUpdate = function () {
                        if (!priceSlider) {
                            return;
                        }

                        priceSlider.noUiSlider.on('update', function () {
                            var filterValue;
                            var $count = parseInt(priceSlider.noUiSlider.get());
                            var $value = $('.price .value');
                            var $minutes = $('.plan-sub-filters a.selected').attr('data-filter');
                            var $dataContract = $('.dataContract .catg-content').attr('rel');

                            if ($count == 150) {
                                filterValue = '.price150' + $minutes + $dataContract;
                                $value.text('150');
                                $('#price-slider .noUi-value').removeClass('active');
                                $('#price-slider .noUi-value:eq(0)').addClass('active');
                            }
                            else if ($count == 300) {
                                filterValue = '.price150' + $minutes + $dataContract;
                                filterValue += ', .price300' + $minutes + $dataContract;
                                $value.text('300');
                                $('#price-slider .noUi-value').removeClass('active');
                                $('#price-slider .noUi-value:eq(1)').addClass('active');
                            }
                            else if ($count == 450) {
                                filterValue = '.price150' + $minutes + $dataContract;
                                filterValue += ', .price300' + $minutes + $dataContract;
                                filterValue += ', .price450' + $minutes + $dataContract;
                                $value.text('450');
                                $('#price-slider .noUi-value').removeClass('active');
                                $('#price-slider .noUi-value:eq(2)').addClass('active');
                            }
                            else if ($count == 1000) {
                                filterValue = '.price150' + $minutes + $dataContract;
                                filterValue += ', .price300' + $minutes + $dataContract;
                                filterValue += ', .price450' + $minutes + $dataContract;
                                filterValue += ', .price1000' + $minutes + $dataContract;
                                $value.text('1000');
                                $('#price-slider .noUi-value').removeClass('active');
                                $('#price-slider .noUi-value:eq(3)').addClass('active');
                            }

                            $('.selection .spend h5 span').text($count);
                            $('.price .catg-content').attr('rel', filterValue);

                            console.log(filterValue);
                            $container.isotope({filter: filterValue});
                        });
                    }

                    $('#price-slider .noUi-value').on('click', function () {
                        //alert($(this).text());
                        $('#price-slider .noUi-value').removeClass('active');
                        $(this).addClass('active');
                        priceSlider.noUiSlider.set(($(this).text()).replace('AED ', '').replace('درهم ', ''));
                        scrollUp();
                    })


                    var dataSlider = document.getElementById('data-slider');
                    if (dataSlider != undefined) {
                        noUiSlider.create(dataSlider, {
                            start: [100], // Handle start position
                            step: 1, // Slider moves in increments of '10'
                            snap: true,
                            //margin: 2, // Handles must be more than '20' apart
                            connect: 'lower', // Display a colored bar between the handles
                            direction: isLTR ? 'ltr' : 'rtl', // Put '0' at the bottom of the slider
                            //orientation: 'vertical', // Orient the slider vertically
                            //behaviour: 'tap-drag', // Move handle on tap, bar is draggable
                            range: { // Slider can select '0' to '100'
                                'min': [1],
                                '14.28%': [2, 6],
                                '28.57%': [6, 10],
                                '42.85%': [10, 18],
                                '57.13%': [18, 25],
                                '71.41%': [25, 50],
                                '85.69%': [50],
                                'max': [100]
                            },
                            pips: { // Show a scale with the slider
                                mode: 'steps',
                                density: 2
                            }
                        });
                    }

                    var dataUpdate = function () {
                        dataSlider.noUiSlider.on('update', function () {
                            var filterValue = "";
                            var $count = parseInt(dataSlider.noUiSlider.get());
                            var $value = $('.data .value');
                            var $minutes = $('.plan-sub-filters a.selected').attr('data-filter');

                            $value.text($count);

                            for (i = 1; i <= $count; i++) {
                                if (i == $count) filterValue += '.data' + i + $minutes;
                                else filterValue += '.data' + i + $minutes + ', ';
                            }

                            $('.data .catg-content').attr('rel', filterValue);

                            //alert($('[data-gb]'));

                            // use filterFn if matches value
                            //filterValue = filterFns[filterValue] || filterValue;
                            console.log(filterValue);
                            $container.isotope({
                                filter: filterValue
                            });
                        });

                        //alert(parseInt($('.you-get h5:eq(0) span').text()));
                    }



                    dataSliderMonthlyBundle = document.getElementById('data-slider-monthlyBundle');
                    if (dataSliderMonthlyBundle != undefined) {
                        noUiSlider.create(dataSliderMonthlyBundle, {
                            start: [200], // Handle start position
                            step: 250, // Slider moves in increments of '10'
                            snap: true,
                            //margin: 2, // Handles must be more than '20' apart
                            connect: 'lower', // Display a colored bar between the handles
                            direction: isLTR ? 'ltr' : 'rtl', // Put '0' at the bottom of the slider
                            //orientation: 'vertical', // Orient the slider vertically
                            //behaviour: 'tap-drag', // Move handle on tap, bar is draggable
                            range: { // Slider can select '0' to '100'
                                'min': [25],
                                '33.33%': [50],
                                '66.66%': [100],
                                'max': [200]
                            },
                            pips: { // Show a scale with the slider
                                mode: 'steps',
                                density: 150,
                                format: wNumb({
                                    prefix: !isLTR ? 'درهم ' : 'AED '
                                })
                            }
                        });

                        var dataSliderChangeValueMonthly = function dataSliderChangeValueMonthly() {
                            scrollUp();

                            var selectedValue = dataSliderMonthlyBundle.noUiSlider.get();
                            prepaidAddGtmDataLayer("expected_spend", selectedValue);
                        };

                        if (dataSliderMonthlyBundle != undefined) {
                            dataSliderMonthlyBundle.noUiSlider.on('change', dataSliderChangeValueMonthly);
                        }

                        $('#data-slider-monthlyBundle .noUi-value').on('click', function () {
                            $('#data-slider-monthlyBundle .noUi-value').removeClass('active');
                            $(this).addClass('active');

                            var value = $(this).text().replace('درهم ', '').replace('AED ', '');

                            dataSliderMonthlyBundle.noUiSlider.set(value);

                            dataSliderChangeValueMonthly();
                        });
                    };

                    var dataUpdateMonthlyBundle = function () {
                        if (!dataSliderMonthlyBundle) {
                            return;
                        }

                        dataSliderMonthlyBundle.noUiSlider.on('update', function () {
                            if ($(".dataContract.monthlyBundle").hasClass("hide")) {
                                return;
                            }

                            var filterValue;
                            var $count = parseInt(dataSliderMonthlyBundle.noUiSlider.get());
                            var $value = $('.dataContract.monthlyBundle .value');
                            var $bundle = "." + $('.plan-sub-filters a.selected').attr('data-filter');

                            if ($count == 25) {
                                filterValue = '.data25' + $bundle;
                                $value.text('25');
                                $('#data-slider-monthlyBundle .noUi-value').removeClass('active');
                                $('#data-slider-monthlyBundle .noUi-value:eq(0)').addClass('active');
                            } else if ($count == 50) {
                                filterValue = '.data25' + $bundle;
                                filterValue += ', .data50' + $bundle;
                                $value.text('50');
                                $('#data-slider-monthlyBundle .noUi-value').removeClass('active');
                                $('#data-slider-monthlyBundle .noUi-value:eq(1)').addClass('active');
                            } else if ($count == 100) {
                                filterValue = '.data25' + $bundle;
                                filterValue += ', .data50' + $bundle;
                                filterValue += ', .data100' + $bundle;
                                $value.text('100');
                                $('#data-slider-monthlyBundle .noUi-value').removeClass('active');
                                $('#data-slider-monthlyBundle .noUi-value:eq(2)').addClass('active');
                            } else if ($count == 200) {
                                filterValue = '.data25' + $bundle;
                                filterValue += ', .data50' + $bundle;
                                filterValue += ', .data100' + $bundle;
                                filterValue += ', .data200' + $bundle;
                                $value.text('200');
                                $('#data-slider-monthlyBundle .noUi-value').removeClass('active');
                                $('#data-slider-monthlyBundle .noUi-value:eq(3)').addClass('active');
                            }
                            $('.selection .spend h5 span').text($count);
                            $('.price .catg-content').attr('rel', filterValue);

                            prepaidRunFilters(filterValue);

                            cachedFilterValueMonthly = filterValue;

                            //Update sticky filter
                            var text = !isLTR ? $count + ' درهم' : 'AED ' + $count;
                            $('.sticky-top .selection .spend h5').text(text);
                        });
                    };

                    dataUpdateMonthlyBundle();


                    // Data Slider DailyBundle
                    dataSliderDailyBundle = document.getElementById('data-slider-dailyBundle');
                    if (dataSliderDailyBundle != undefined) {
                        noUiSlider.create(dataSliderDailyBundle, {
                            start: [5], // Handle start position
                            step: 150, // Slider moves in increments of '10'
                            snap: true,
                            //margin: 2, // Handles must be more than '20' apart
                            connect: 'lower', // Display a colored bar between the handles
                            direction: !isLTR ? 'rtl' : 'ltr', // Put '0' at the bottom of the slider
                            //orientation: 'vertical', // Orient the slider vertically
                            //behaviour: 'tap-drag', // Move handle on tap, bar is draggable
                            range: { // Slider can select '0' to '100'
                                'min': [2],
                                '50%': [3],
                                'max': [5]
                            },
                            pips: { // Show a scale with the slider
                                mode: 'steps',
                                density: 150,
                                format: wNumb({
                                    prefix: !isLTR ? 'درهم ' : 'AED '
                                })
                            }
                        });
                    }

                    var dataUpdateDailyBundle = function () {
                        if (!dataSliderDailyBundle) {
                            return;
                        }

                        dataSliderDailyBundle.noUiSlider.on('update', function () {
                            if ($(".dataContract.dailyBundle").hasClass("hide")) {
                                return;
                            }

                            var filterValue;
                            var $count = parseInt(dataSliderDailyBundle.noUiSlider.get());
                            var $value = $('.dataContract.dailyBundle .value');
                            var $bundle = "." + $('.plan-sub-filters a.selected').attr('data-filter');

                            if ($count == 2) {
                                filterValue = '.data2' + $bundle;
                                $value.text('2');
                                $('#data-slider-dailyBundle .noUi-value').removeClass('active');
                                $('#data-slider-dailyBundle .noUi-value:eq(0)').addClass('active');
                            } else if ($count == 3) {
                                filterValue = '.data2' + $bundle;
                                filterValue += ', .data3' + $bundle;
                                $value.text('3');
                                $('#data-slider-dailyBundle .noUi-value').removeClass('active');
                                $('#data-slider-dailyBundle .noUi-value:eq(1)').addClass('active');
                            } else if ($count == 5) {
                                filterValue = '.data2' + $bundle;
                                filterValue += ', .data3' + $bundle;
                                filterValue += ', .data5' + $bundle;
                                $value.text('5');
                                $('#data-slider-dailyBundle .noUi-value').removeClass('active');
                                $('#data-slider-dailyBundle .noUi-value:eq(2)').addClass('active');
                            }
                            $('.selection .spend h5 span').text($count);
                            $('.price .catg-content').attr('rel', filterValue);

                            prepaidRunFilters(filterValue);

                            cachedFilterValueDaily = filterValue;

                            //Update sticky filter
                            var text = !isLTR ? $count + ' درهم' : 'AED ' + $count;
                            $('.sticky-top .selection .spend h5').text(text);
                        });
                    };

                    var dataSliderChangeValueDaily = function dataSliderChangeValueDaily() {
                        scrollUp();

                        var selectedValue = dataSliderDailyBundle.noUiSlider.get();
                        prepaidAddGtmDataLayer("expected_spend", selectedValue);
                    };

                    if (dataSliderDailyBundle != undefined) {
                        dataSliderDailyBundle.noUiSlider.on('change', dataSliderChangeValueDaily);
                    }

                    $('#data-slider-dailyBundle .noUi-value').on('click', function () {
                        $('#data-slider-dailyBundle .noUi-value').removeClass('active');
                        $(this).addClass('active');

                        var value = $(this).text().replace('درهم ', '').replace('AED ', '');

                        dataSliderDailyBundle.noUiSlider.set(value);

                        dataSliderChangeValueDaily();
                    });

                    dataUpdateDailyBundle();

                    setTimeout(function() {
                        var noUiBase = $("#data-slider-monthlyBundle > .noUi-base");
                        if (noUiBase.length > 1) {
                            noUiBase[0].remove();
                        }
                        var noUiPips = $("#data-slider-monthlyBundle > .noUi-pips");
                        if (noUiPips.length > 1) {
                            noUiPips[0].remove();
                        }

                        var noUiBaseDaily = $("#data-slider-dailyBundle > .noUi-base");
                        if (noUiBaseDaily.length > 1) {
                            noUiBaseDaily[0].remove();
                        }
                        var noUiPipsDaily = $("#data-slider-dailyBundle > .noUi-pips");
                        if (noUiPipsDaily.length > 1) {
                            noUiPipsDaily[0].remove();
                        }
                    }, 1000);



                    var dataContractSlider = document.getElementById('data-contract-slider');
                    if (dataContractSlider != undefined) {
                        noUiSlider.create(dataContractSlider, {
                            start: [24], // Handle start position
                            //step: 1, // Slider moves in increments of '10'
                            snap: true,
                            //margin: 2, // Handles must be more than '20' apart
                            //connect: 'lower', // Display a colored bar between the handles
                            direction: isLTR ? 'ltr' : 'rtl', // Put '0' at the bottom of the slider
                            //orientation: 'vertical', // Orient the slider vertically
                            //behaviour: 'drag', // Move handle on tap, bar is draggable
                            range: { // Slider can select '0' to '100'
                                'min': 0,
                                '50%': 12,
                                'max': 24
                            }/*,
                             pips: { // Show a scale with the slider
                             mode: 'values',
                             values: ['Small', 'Medium', 'Large'],
                             density: 3
                             }*/
                        });

                        dataContractSlider.noUiSlider.on('change', function(){
                            scrollUp();
                        });
                    }

                    var dataContractUpdate = function () {
                        if (!dataContractSlider) {
                            return;
                        }

                        dataContractSlider.noUiSlider.on('update', function () {
                            var filterValue;
                            var $count = parseInt(dataContractSlider.noUiSlider.get());
                            var $value = $('.dataContract .value');
                            //var $minutes = $('.plan-sub-filters a.selected').attr('data-filter');

                            if ($count == 0) {
                                filterValue = '.light';// + $minutes;
                                $value.text(isLTR ? 'Light' : 'بسيط');
                                $('.dataContract .labels li').removeClass('active');
                                $('.dataContract .labels li:eq(0)').addClass('active');
                                $('.selection .data h5').text(isLTR ? 'Light' : 'بسيط');
                            }
                            else if ($count == 12) {
                                filterValue = '.medium';// + $minutes;
                                $value.text(isLTR ? 'Medium' : 'متوسط');
                                $('.dataContract .labels li').removeClass('active');
                                $('.dataContract .labels li:eq(1)').addClass('active');
                                $('.selection .data h5').text(isLTR ? 'Medium' : 'متوسط');
                            }
                            else if ($count == 24) {
                                filterValue = '.heavy';// + $minutes;
                                $value.text(isLTR ? 'Heavy' : 'ثقيل');
                                $('.dataContract .labels li').removeClass('active');
                                $('.dataContract .labels li:eq(2)').addClass('active');
                                $('.selection .data h5').text(isLTR ? 'Heavy' : 'ثقيل');
                            }

                            $('.dataContract .catg-content').attr('rel', filterValue);

                            console.log(filterValue);
                            //$container.isotope({filter: filterValue});
                            priceUpdate();
                        });
                    }

                    dataContractUpdate();
                    priceUpdate();

                    $('.dataContract .labels li').on('click', function () {
                        //alert($(this).text());
                        $('.dataContract .labels li').removeClass('active');
                        $(this).addClass('active');
                        dataContractSlider.noUiSlider.set($(this).attr('rel'));
                        scrollUp();
                    })

                    var contractSlider = document.getElementById('contract-slider');
                    if (contractSlider != undefined) {
                        noUiSlider.create(contractSlider, {
                            start: [24], // Handle start position
                            //step: 1, // Slider moves in increments of '10'
                            snap: true,
                            //margin: 2, // Handles must be more than '20' apart
                            //connect: 'lower', // Display a colored bar between the handles
                            direction: isLTR ? 'ltr' : 'rtl', // Put '0' at the bottom of the slider
                            //orientation: 'vertical', // Orient the slider vertically
                            //behaviour: 'drag', // Move handle on tap, bar is draggable
                            range: { // Slider can select '0' to '100'
                                'min': 0,
                                '50%': 12,
                                'max': 24
                            },
                            pips: { // Show a scale with the slider
                                mode: 'steps',
                                density: 150,
                                format: wNumb({
                                    postfix: ' mnth'
                                })
                            }
                        });
                    }

                    var contractUpdate = function () {
                        contractSlider.noUiSlider.on('update', function () {
                            var filterValue;
                            var $count = parseInt(contractSlider.noUiSlider.get());
                            var $value = $('.contract .value');
                            var $measure = $('.contract .measure');
                            var $minutes = $('.plan-sub-filters a.selected').attr('data-filter');


                            if ($count == 0) {
                                filterValue = '.light' + $minutes;
                                $value.text('No');
                                $measure.text('Contract');
                            }
                            else if ($count == 12) {
                                filterValue = '.medium' + $minutes;
                                $value.text('12');
                                $measure.text('Months');
                            }
                            else if ($count == 24) {
                                filterValue = '.heavy' + $minutes;
                                $value.text('24');
                                $measure.text('Months');
                            }

                            $('.contract .catg-content').attr('rel', filterValue);

                            console.log(filterValue);
                            $container.isotope({filter: filterValue});
                        });
                    }

                    $('.catg-content .round-box a').click(function () {
                        var elem = $(this).parent().parent();
                        $('.catg-content').removeClass('selected')

                        setTimeout(function () {
                            elem.addClass('selected');
                        }, 400);

                        if (elem.parent().hasClass('data')) dataUpdate();
                        else if (elem.parent().hasClass('price')) priceUpdate();
                        else if (elem.parent().hasClass('contract')) contractUpdate();

                        return false;

                    })

                    $('.plan-sub-filters').each(function () {
                        var $this = $(this);
                        $(this).on('click', 'a', function () {
                            $('a', $this).removeClass('selected');
                            $('.selection .mins h5').text($(this).attr('rel'));
                            $('a .icon', $this)
                                .removeClass('icon-ok-circled')
                                .addClass('icon-ok-circled2');
                            $(this).addClass('selected');
                            $('.icon', $(this))
                                .removeClass('icon-ok-circled2')
                                .addClass('icon-ok-circled');

                            //contractUpdate();
                            //dataUpdate();
                            priceUpdate();
                            scrollUp();

                            //var filterValue = $('.catg-content.selected').attr('rel');//$(this).attr('data-filter');
                            //filterValue += $('.lined-buttons').not($this).find('a.is-checked').attr('data-filter');
                            //console.log(filterValue);
                            //alert(filterValue);
                            // use filterFn if matches value
                            //filterValue = filterFns[filterValue] || filterValue;
                            //$container.isotope({filter: filterValue});
                            return false;
                        });
                    });

                })
            })
        }


        /* - - - - - - - - - - - - - /Section closed - - - - - - - - - - - - */
        /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

        /* Payment Plans */
        var fillSelectedPaidAmount = function (value) {
            var regexOnlyNumbers = new RegExp("^[0-9]*$");
            if (regexOnlyNumbers.test(value)) {
                $("#totalbill-amount").text("AED " + value + ".00");
                $("#totalbill-amount-mobile").text("AED " + value + ".00");

                return true;
            }

            return false;
        }

        var bindMobileNumbersToModal = function () {
            $(".modal-mobilenumbers").on("click", function () {
                var id = $(this).attr("id");
                var accountId = id.substring(0, id.indexOf("-"));

                $("#modal-mobilenumbers-title").text($("#" + accountId + "-title").text());
                $("#modal-mobilenumbers-account").text($("#" + accountId + "-num").text());

                $("#modal-mobilenumbers").reveal();
            });
        };

        if ($("#payment-plans-list-online").length) {
            var bindSummaryToModal = function () {
                $(".modal-summary").on("click", function () {
                    $("#modal-summary-accounts tr").remove();

                    $("#modal-summary-accounts").append(
                        "<tr><th>" + $("#totalbill-account").text() + "</th><td>" +
                        $("#totalbill-amount").text() + "</td></tr>");

                    $("#modal-summary").reveal();
                });
            };

            bindMobileNumbersToModal();
            bindSummaryToModal();

            $(".list-filter-paginate").each(function () {
                var id = $(this).attr("id");
                var options = JSON.parse($(this).attr("data-list-options"));

                options.plugins = [ListPagination({})];

                var listObj = new List(id, options);

                // When changing page
                listObj.on("updated", function () {
                    $("input[type=radio][name=paymentplans-account]").each(function () {
                        var checked = $(this).iCheck("update")[0].checked;
                        var radioId = $(this).attr("id");
                        var accountId = radioId.substring(0, radioId.indexOf("-"));
                        if (checked) {
                            if (accountId !== $("#paymentplans-accountid").val()) {
                                $(this).iCheck("uncheck");
                            } else {
                                $(this).iCheck("check");
                            }
                        }
                    });

                    bindMobileNumbersToModal();
                });

                // When searching: if no results I show the alert
                var delayOnShowingAlert;

                listObj.on("searchComplete", function () {
                    var numAccounts = $("ul.list li").length;
                    var filterText = $("#input-search").val();

                    clearTimeout(delayOnShowingAlert);

                    if (numAccounts === 0) {
                        delayOnShowingAlert = setTimeout(function () {
                            $("#no-results-error-searchtext").text(filterText);

                            if ($("#no-results-error").css("display") === "none") {
                                $('#no-results-error').removeClass('hinge-out-from-top mui-leave mui-leave-active');
                                $("#no-results-error").show();
                                selfDistruct('.selfdistruct', 2000);
                            }
                        }, 700);
                    }
                });
            });

            $("body").on("ifChecked", "input[type=radio][name=paymentplans-account]", function () {
                if ($("#paymentplans-paynow").attr("disabled")) {
                    $("#paymentplans-paynow").removeAttr("disabled");
                    $("#paymentplans-paynow-mobile").removeAttr("disabled");
                }

                var accountId = $(this).val();
                var paidAmount = $("#" + accountId + "-payamount").val();
                var success = fillSelectedPaidAmount(paidAmount);

                $("#paymentplans-accountid").val(accountId);
                $("#totalbill-account").text($("#" + accountId + "-num").text());
                $("#totalbill-account-mobile").text($("#" + accountId + "-num").text());
            });

            $("body").on("input", "input.payamount", function () {
                var selfInput = this;

                var accountId = $(selfInput).attr("id");
                accountId = accountId.substring(0, accountId.indexOf("-"));
                $("input[type=radio][name=paymentplans-account][value=" + accountId + "]").each(function () {
                    var checked = $(this).iCheck("update")[0].checked;
                    if (checked) {
                        fillSelectedPaidAmount($(selfInput).val());
                    }
                });
            });
        }

        if ($("#payment-plans-list-store").length) {
            var allAccounts = [];
            $("input[type=checkbox][name=paymentplans-account]").each(function () {
                var checkId = $(this).attr("id");
                var accountId = checkId.substring(0, checkId.indexOf("-"));
                var value = $("#" + accountId + "-payamount").val();
                var num = $("#" + accountId + "-num").text();
                var valueNum = 0;
                var regexOnlyNumbers = new RegExp("^[0-9]*$");
                if (regexOnlyNumbers.test(value)) {
                    valueNum = parseInt(value);
                }

                allAccounts.push({
                    id: accountId,
                    value: valueNum,
                    num: num
                });
            });

            var SelectedAccounts = {
                value: [],

                add: function (accountId) {
                    this.remove(accountId);

                    var value = $("#" + accountId + "-payamount").val();
                    var num = $("#" + accountId + "-num").text();
                    var valueNum = 0;
                    var regexOnlyNumbers = new RegExp("^[0-9]*$");
                    if (!!value && regexOnlyNumbers.test(value)) {
                        valueNum = parseInt(value);
                    }

                    this.value.push({
                        id: accountId,
                        value: valueNum,
                        num: num
                    });
                },

                remove: function (accountId) {
                    this.value = this.value.filter(function (elem) {
                        return elem.id !== accountId;
                    });
                },

                exists: function (accountId) {
                    return this.value.some(function (elem) {
                        return elem.id === accountId;
                    });
                },

                getSelectedAccountText: function () {
                    if (this.value.length === 1) {
                        return "1 Account Selected";
                    }

                    return this.value.length + " Accounts Selected";
                },

                getTotalAmountText: function () {
                    var total = this.value.map(function (elem) {
                        return elem.value;
                    }).reduce(function (val1, val2) {
                        return val1 + val2;
                    }, 0);

                    return "AED " + total + ".00";
                }
            };

            var bindSummaryToModal = function () {
                $(".modal-summary").on("click", function () {
                    $("#modal-summary-accounts tr").remove();

                    $(SelectedAccounts.value).each(function () {
                        $("#modal-summary-accounts").append(
                            "<tr><th>" + this.num + "</th><td>AED " + this.value + ".00</td></tr>");
                    });
                    $("#modal-summary-accounts").append(
                        "<tr class=\"modal-summary-accounts-total\"><th>Total</th><td>"
                        + SelectedAccounts.getTotalAmountText() + "</td></tr>");

                    $("#modal-summary").reveal();
                });
            };

            var updateCheckboxesState = function () {
                $("input[type=checkbox][name=paymentplans-account]").each(function () {
                    var radioId = $(this).attr("id");
                    var accountId = radioId.substring(0, radioId.indexOf("-"));

                    if (SelectedAccounts.exists(accountId)) {
                        $(this).iCheck("check");
                    } else {
                        $(this).iCheck("uncheck");
                    }
                });
            };

            var listObj;

            var clearListfilters = function () {
                listObj.filter();
                while (listObj.filtered) {
                    listObj.filter();
                }
            };

            bindMobileNumbersToModal();
            bindSummaryToModal();

            $(".list-filter-paginate").each(function () {
                var id = $(this).attr("id");
                var options = JSON.parse($(this).attr("data-list-options"));

                options.plugins = [ListPagination({})];

                listObj = new List(id, options);

                // When changing page
                listObj.on("updated", function () {
                    bindMobileNumbersToModal();
                    updateCheckboxesState();
                });

                // When searching: if no results I show the alert
                var delayOnShowingAlert;

                listObj.on("searchComplete", function () {
                    var numAccounts = $("ul.list li").length;
                    var filterText = $("#input-search").val();

                    clearTimeout(delayOnShowingAlert);

                    if (numAccounts === 0) {
                        delayOnShowingAlert = setTimeout(function () {
                            $("#no-results-error-searchtext").text(filterText);

                            if ($("#no-results-error").css("display") === "none") {
                                $('#no-results-error').removeClass('hinge-out-from-top mui-leave mui-leave-active');
                                $("#no-results-error").show();
                                selfDistruct('.selfdistruct', 2000);
                            }
                        }, 700);
                    }
                });
            });

            var applyShowOnlySelectedFilter = function (checked) {
                if (checked) {
                    listObj.filter(function (item) {
                        var checkId = $($(item.elm).find("input[type=checkbox]")[0]).attr("id");
                        var accountId = checkId.substring(0, checkId.indexOf("-"));

                        return SelectedAccounts.exists(accountId);
                    });
                } else {
                    clearListfilters();
                }
            };

            $("body").on("ifToggled", "input[type=checkbox][name=paymentplans-account]", function () {
                var checked = $(this).iCheck("update")[0].checked;
                var checkId = $(this).attr("id");
                var accountId = checkId.substring(0, checkId.indexOf("-"));

                if (checked) {
                    SelectedAccounts.add(accountId);
                } else {
                    SelectedAccounts.remove(accountId);

                    $("#selectAll").iCheck("uncheck");
                }
                $("#totalbill-account").text(SelectedAccounts.getSelectedAccountText());
                $("#totalbill-account-mobile").text(SelectedAccounts.value.length);
                $("#totalbill-amount").text(SelectedAccounts.getTotalAmountText());
                $("#totalbill-amount-mobile").text(SelectedAccounts.getTotalAmountText());

                var showOnlySelectedChecked = $("#showOnlySelected").iCheck("update")[0].checked;
                if (showOnlySelectedChecked) {
                    applyShowOnlySelectedFilter(showOnlySelectedChecked);
                }

                if (SelectedAccounts.value.length > 0) {
                    if ($("#paymentplans-generaterefnum").attr("disabled")) {
                        $("#paymentplans-generaterefnum").removeAttr("disabled");
                        $("#paymentplans-generaterefnum-mobile").removeAttr("disabled");
                    }
                } else {
                    $("#paymentplans-generaterefnum").attr("disabled", "disabled");
                    $("#paymentplans-generaterefnum-mobile").attr("disabled", "disabled");
                }
            });

            $("body").on("ifClicked", "#selectAll", function () {
                // Inverted because it fires before the checkbox get the new value
                var checked = !$(this).iCheck("update")[0].checked;

                if (checked) {
                    SelectedAccounts.value = allAccounts;
                } else {
                    SelectedAccounts.value = [];
                }

                $("#totalbill-account").text(SelectedAccounts.getSelectedAccountText());
                $("#totalbill-account-mobile").text(SelectedAccounts.value.length);
                $("#totalbill-amount").text(SelectedAccounts.getTotalAmountText());
                $("#totalbill-amount-mobile").text(SelectedAccounts.getTotalAmountText());

                var showOnlySelectedChecked = $("#showOnlySelected").iCheck("update")[0].checked;
                if (showOnlySelectedChecked) {
                    applyShowOnlySelectedFilter(showOnlySelectedChecked);
                }

                updateCheckboxesState();

                if (SelectedAccounts.value.length > 0) {
                    if ($("#paymentplans-generaterefnum").attr("disabled")) {
                        $("#paymentplans-generaterefnum").removeAttr("disabled");
                        $("#paymentplans-generaterefnum-mobile").removeAttr("disabled");
                    }
                } else {
                    $("#paymentplans-generaterefnum").attr("disabled", "disabled");
                    $("#paymentplans-generaterefnum-mobile").attr("disabled", "disabled");
                }
            });

            $("body").on("ifClicked", "#showOnlySelected", function () {
                // Inverted because it fires before the checkbox get the new value
                var checked = !$("#showOnlySelected").iCheck("update")[0].checked;

                applyShowOnlySelectedFilter(checked);
            });

            $("body").on("input", "input.payamount", function () {
                var selfInput = this;

                var accountId = $(selfInput).attr("id");
                accountId = accountId.substring(0, accountId.indexOf("-"));

                if (SelectedAccounts.exists(accountId)) {
                    SelectedAccounts.add(accountId);

                    $("#totalbill-amount").text(SelectedAccounts.getTotalAmountText());
                    $("#totalbill-amount-mobile").text(SelectedAccounts.getTotalAmountText());
                }
            });
        }

        // open/close accordion when selected/unselected
        $("body").on("ifToggled", "input[type=checkbox][name=paymentplans-account], input[type=radio][name=paymentplans-account]", function () {
            var checked = $(this).iCheck("update")[0].checked;
            var checkId = $(this).attr("id");
            var accountId = checkId.substring(0, checkId.indexOf("-"));
            var arrowObj = $("#" + accountId + "-arrow");
            var content = $(arrowObj.attr("href"));
            var altText = arrowObj.data("alttext");
            var html = arrowObj.html();

            if (checked) {
                if (arrowObj.hasClass("show")) {
                    retrieveDataShowSpinner(content);

                    content.addClass("plan-content-active");
                    arrowObj.removeClass("show").addClass("hide").html(
                        altText).data("alttext", html);
                }
            } else {
                if (arrowObj.hasClass("hide")) {
                    content.removeClass("plan-content-active");
                    arrowObj.removeClass("hide").addClass("show").html(
                        altText).data("alttext", html);
                }
            }
        });

        /*$('a:disabled').click(function(){
            event.preventDefault;
        })*/
        
    }



);

// function for lightgox terms & conditions checkbox
function $acceptTnC() {
    $('[id^="chAccept"],[class^="acceptTnC"]').each(function () {
        $(this).on("ifChecked", function () {
            //alert($(this).closest('.footer').find('[disabled]').html());
            $(this).closest('.footer').find('[disabled]').removeAttr("disabled");
            //$("#btnContinue", $(this).closest('.footer')).removeAttr("disabled");
        });
        $(this).on("ifUnchecked", function () {
            $(this).closest('.footer').find('[id^="btnContinue"], [id^="btnSubmit"]').attr('disabled', 'disabled');
            //$("#btnContinue", $(this).closest('.footer')).attr('disabled', 'disabled');
        });
    })
}
$acceptTnC();


//function for generating bullet numbers on enterprise modify rate plan page
function $generateBullets(elem) {
    elem.each(function (i) {
        //alert(i);
        $('.bullets', $(this)).html(i + 1);
    });
    $('.bullets', $('.sticky-content')).html(elem.length + 1);
}
//$generateBullets($('.plans-accordion > ul > li').reverse());


// call this function to remove an object from dom after specified miliseconds
function selfDistruct(elem, ms) {
    setTimeout(function () {
        //$(elem).hide(500);
        requirejs(["motion-ui.min"], function (MotionUI) {
            $(function () {
                MotionUI.animateOut(elem, 'hinge-out-from-top');
            })
        })
    }, ms);
}


// function to show/hide graph
function showGraph() {
    $("#graphLink a").text("Hide graph");
    $(".graph").show();
}
function graphF() {
    var display = $(".graph").css('display');
    if (display == "block") {
        $(".graph").hide();
        $("#graphLink a").text("Show graph");
        $(".graphButton").hide();
    } else {
        $(".graph").show();
        $("#graphLink a").text("Hide graph");
        $(".graphButton").show();
    }
}

// TV package dynamic element creation
function createNode(el) {
    el.after(
        '<tr class="collapsedRow"><td colspan="6">' +
        '<div class="titleCollapsed">' +
        '<div class="row">' +
        '<div class="col-xs-12 col-sm-12 col-md-12">' +
        '<div class="title">Included channels</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="contentBox">' +
        '<div class="row">' +
        '<div class="col-xs-12 col-sm-4 col-md-4">' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '</div>' +
        '<div class="col-xs-12 col-sm-4 col-md-4">' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '</div>' +
        '<div class="col-xs-12 col-sm-4 col-md-4">' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '<a href="#">Lorem Ipsum</a>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="costPack">' +
        '<div class="row">' +
        '<div class="col-xs-12 col-sm-12 col-md-12">' +
        '<div class="buttonPack">' +
        '<span class="leftLabel"><span>AED 125</span> Monthly</span>' +
        '<a href="#" class="rightLabel">Add</a>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</td></tr>');
}

// GA manual page title push
function $title(elem) {
    var oldtitle = document.title;
    document.title = elem;
    _gaq.push(["_trackPageview"]);
    document.title = oldtitle;
}

