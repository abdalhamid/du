$(document).ready(function () {

    /* ENTERTAINMENT TILES isotope

     var $container = $('.modTiles .items');
     if ($container.length > 0) {
     var tmo = setTimeout(function () {
     clearTimeout(tmo);
     $container.imagesLoaded(function () {
     //$container.isotope(
     //{
     // itemSelector: '.item',
     // layoutMode: 'masonry',
     // resizable: true
     //}

     //Comment this script because it's require RequireJS

     require(['isotope.pkgd.min'],
     function (Isotope) {
     var iso = new Isotope(
     $container[0], {
     itemSelector: '.item',
     layoutMode: 'masonry',
     resizable: true
     });
     });
     });
     }, 250);
     }

     */

    /* Enable scrollable contents * */
    $('.scrollable').each(function () {
        if (!$(this).hasClass('du-ready')) {
            
            new duScroll({
                'target': $(this),
                'enableTouch': true,
                'direction': 'ltr'
            });
            $(this).addClass('du-ready');

            $(this).on('refresh',function(){
                new duScroll({
                    'target': $(this),
                    'enableTouch': true,
                    'direction': 'ltr'
                });

                 $(this).find('.select-alt').each(function () {
                        var cont = $(this);
                        var btns = cont.find('.btn');
                        btns.click(function () {
                            btns.parent().find('.active').text('Select').removeClass('active btn-primary').addClass('btn-default');
                            $(this).text('Selected').removeClass('btn-default').addClass('btn-primary active');
                        });
                    });

            });
        }
    });

    /* Enable product scrolls */
    $('.productScrollable').each(function () {
        if (!$(this).hasClass('du-ready')) {
        
            new duProductScroll({
                'target': $(this),
                'enableTouch': true,
                'direction': 'ltr'
            });
            $(this).addClass('du-ready');
        }
    });

    /* Enable simple sliders */
    /*$('.slideable').each(function () {
        if ($(this).hasClass('modDials') || $(this).hasClass("nodu")) return;
        if (!$(this).hasClass('du-ready')) {
        
            new duSlide({
                'target': $(this),
                'enableTouch': true,
                'timer': 0
            });
            $(this).addClass('du-ready');
        }
    });*/

    /* Enable advanced sliders */
    $('.slideableAdvanced').each(function () {
        if (!$(this).hasClass('du-ready')) {
        
             new duSlide({
                'target': $(this),
                'enableTouch': true,
                'timer': 0,
                'customPagination': true
            });
            $(this).addClass('du-ready');
        }
    });

    /* Enable Coverflow */
    $('.coverflowable').each(function () {
        if (!$(this).hasClass('du-ready')) {
 
            new duCoverflow({
                'target': $(this).attr('id')
            });
            $(this).addClass('du-ready');
        }
    });


	// payment history table & filters
        var payment_history = $('#payment-history').DataTable({
            "aoColumns": [{
                "sType": "custom_euro_date"
            }, {}, {}],
            "aaSorting": [
                [0, "desc"]
            ], // Sort by first column descending
            "info": false,
            "paging": false,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Type a keyword..."
            },
            initComplete: function () {
                var api = this.api();
                var i = 1;
                var column = api.column(i);
                var select = $(
                    '<select class="hidden-sm"><option value="">Filter use type</option></select>'
                ).appendTo($('.table-filters')).on('change',
                    function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val());
                        column.search(val ? '^' + val + '$' :
                            '', true, false).draw();
                    });
                column.data().unique().sort().each(function (d,
                                                             j) {
                    select.append('<option value="' + d +
                        '">' + d + '</option>');
                });
            }
        });

        // recharge history table & filters
        var recharge_history = $('#recharge-history').DataTable({
            "aoColumns": [{
                "sType": "custom_euro_date"
            }, {}, {}, {}],
            "aaSorting": [
                [0, "desc"]
            ], // Sort by first column descending
            "info": false,
            "paging": false,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Type a keyword..."
            },
            initComplete: function () {
                var api = this.api();
                var i = 1;
                var column = api.column(i);
                var select = $(
                    '<select class="visible-xs"><option value="">Filter use type</option></select>'
                ).appendTo($('.table-filters')).on('change',
                    function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val());
                        column.search(val ? '^' + val + '$' :
                            '', true, false).draw();
                    });
                column.data().unique().sort().each(function (d,
                                                             j) {
                    select.append('<option value="' + d +
                        '">' + d + '</option>');
                });
            }
        });

        // usage history table & filters
        var usage_table = $('#usage').DataTable({
            "aoColumns": [{
                "sType": "custom_euro_date"
            }, {}, {}, {}, {}, {}],
            "aaSorting": [
                [0, "desc"]
            ], // Sort by first column descending
            "info": false,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Type a keyword..."
            },
            initComplete: function () {
                var api = this.api();
                var i = 1;
                var column = api.column(i);
                var select = $(
                    '<select class="visible-xs"><option value="">Filter use type</option></select>'
                ).appendTo($('.table-filters')).on('change',
                    function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val());
                        column.search(val ? '^' + val + '$' :
                            '', true, false).draw();
                    });
                column.data().unique().sort().each(function (d,
                                                             j) {
                    select.append('<option value="' + d +
                        '">' + d + '</option>');
                });
                $('.table-filters .btn-group .btn').on('click',
                    function (e) {
                        e.preventDefault();
                        $('.table-filters .btn-group .btn')
                            .removeClass('is-checked');
                        $(this).addClass('is-checked');
                        var val = $(this).attr(
                            'data-use-type');
                        column.search(val ? '^' + val + '$' :
                            '', true, false).draw();
                    });
            }
        });

        // init date time picker
        $('#startdate').datetimepicker({
            format: 'd/m/Y',
            timepicker: false,
            closeOnDateSelect: true,
            onChangeDateTime: function (ct, $i) {
                usage_table.draw();
                payment_history.draw();
                recharge_history.draw();
            }
        });

        // init date time picker
        $('#enddate').datetimepicker({
            format: 'd/m/Y',
            timepicker: false,
            closeOnDateSelect: true,
            onChangeDateTime: function (ct, $i) {
                usage_table.draw();
                payment_history.draw();
                recharge_history.draw();
            }
        });

    var intro_hi = "<h1>Hi!</h1> We have recently updated our website with exciting new features. Take a tour to learn more.",
        intro_graph = "Get a quick look at your usage trend for the past 6 months including current month charges.",
        intro_minPay = "Now know how much minimum amount you need to pay by when to avoid service suspension.",
        intro_wallet = "Know your current prepaid balance and do a quick recharge as needed. ",
        intro_billInfo = "View your bills, credit limit, usage & payment history here. Click on view for more details. ",
        intro_billSettings = "View and manage your credit card, bill setting and automatic payments for self and friends.",
        intro_prepaid_history = "View last recharge and Usage history. Manage your credit card and automatic payments for self and friends.",
        intro_freeUnits = "View your available free units to keep a tab on your usage. ",
        intro_acctServices = "All the available services like rate plan for each contract can be modified here. Also Quickly manage the activation & deactivation of your mobile settings.",
        intro_accountBar = "Update your profile and access quick support links from the right menu.",
        intro_entManage = "Search for your employee details and manage their account.";


    require(
        ["intro.min"],
        function (duIntro) {
        var gtmImplementation = {
                nextTrigger : function(introElement){

                    if(typeof introElement._prevStep == 'undefined'){
                            introElement._prevStep = 0;
                        } 
                        
                            if(introElement._prevStep < introElement._currentStep) {
                              
                                introElement._prevStep++;
                                if(typeof dataLayer != 'undefined'){
                                    dataLayer.push({
                                        'event':  'eventTracker',
                                        'eventCategory': 'tutorial',
                                        'eventAction': 'click',
                                        'eventLabel': 'next'
                                        });
                                }
                                

       
                            }else {
                                 introElement._prevStep--;
                            }

                },
                skipTrigger: function() {
                    if(typeof dataLayer != 'undefined') {
                             dataLayer.push({
                                    'event':  'eventTracker',
                                    'eventCategory': 'tutorial',
                                    'eventAction': 'click',
                                    'eventLabel': 'skip'
                                });

                         }
                }
            }
                        // Postpaid single account --
            function intro_postpaid_single_desktop() {
                var intro = duIntro();
                intro.setOptions({
                    //showBullets: false,
                    //showProgress: true,
                    showStepNumbers: false,
                    exitOnOverlayClick: false,
                    steps: [
                        {
                            intro: intro_hi
                        },
                        {
                            element: '.accountPane .accountPaneIn',
                            intro: intro_accountBar,
                            position: 'left'
                        },
                        {
                            element: '.graph',
                            intro: intro_graph,
                            position: 'bottom'
                        },
                        {
                            element: '.min-due-info',
                            intro: intro_minPay,
                            position: 'right'
                        },
                        {
                            element: '.details-container',
                            intro: intro_billInfo,
                            position: 'top'
                        },
                        {
                            element: '.features-container',
                            intro: intro_billSettings,
                            position: 'top'
                        },
                        {
                            element: '.modDials',
                            intro: intro_freeUnits,
                            position: 'top'
                        },
                        {
                            element: '.plan-features-container',
                            intro: intro_acctServices,
                            position: 'top'
                        }
                    ]/*.filter(function (obj) {
                     return $(obj.element).is(':visible');
                     })*/
                });

                intro
                    .start()
                    .oncomplete(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })})
                    .onexit(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })});
            }

            function intro_postpaid_single_tablet() {
                var intro = duIntro();
                intro.setOptions({
                    showStepNumbers: false,
                    exitOnOverlayClick: false,
                    steps: [
                        {
                            intro: intro_hi
                        },
                        {
                            element: '#topUsername',
                            intro: intro_accountBar,
                            position: 'bottom-right-aligned'
                        },
                        {
                            element: '.graph',
                            intro: intro_graph,
                            position: 'bottom'
                        },
                        {
                            element: '.min-due-info',
                            intro: intro_minPay,
                            position: 'right'
                        },
                        {
                            element: '.details-container',
                            intro: intro_billInfo,
                            position: 'top'
                        },
                        {
                            element: '.features-container',
                            intro: intro_billSettings,
                            position: 'top'
                        },
                        {
                            element: '.modDials',
                            intro: intro_freeUnits,
                            position: 'top'
                        },
                        {
                            element: '.plan-features-container',
                            intro: intro_acctServices,
                            position: 'top'
                        }
                    ]
                });

                intro
                    .start()
                    .oncomplete(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })})
                    .onexit(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })});
            }

            function intro_postpaid_single_mobile() {
                var intro = duIntro();
                intro.setOptions({
                    showStepNumbers: false,
                    exitOnOverlayClick: false,
                    steps: [
                        {
                            intro: intro_hi
                        },
                        {
                            element: '#toggleAccountPane768',
                            intro: intro_accountBar,
                            position: 'bottom-right-aligned'
                        },
                        {
                            element: '.min-due-info',
                            intro: intro_minPay,
                            position: 'bottom'
                        },
                        {
                            element: '#show-details',
                            intro: intro_billInfo + intro_billSettings,
                            position: 'bottom'
                        },
                        {
                            element: '.modDials',
                            intro: intro_freeUnits,
                            position: 'top'
                        },
                        {
                            element: '.plan-features-container',
                            intro: intro_acctServices,
                            position: 'top'
                        }
                    ]
                });

                intro
                    .start()
                    .oncomplete(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })})
                    .onexit(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })});
            }

            // Postpaid mixed account --

            function intro_postpaid_mixed_desktop() {
                var intro = duIntro();
                intro.setOptions({
                    //showBullets: false,
                    //showProgress: true,
                    showStepNumbers: false,
                    exitOnOverlayClick: false,
                    steps: [
                        {
                            intro: intro_hi
                        },
                        {
                            element: '.accountPane .accountPaneIn',
                            intro: intro_accountBar,
                            position: 'left'
                        },
                        {
                            element: '.graph',
                            intro: intro_graph,
                            position: 'bottom'
                        },
                        {
                            element: '.min-due-info',
                            intro: intro_minPay,
                            position: 'right'
                        },
                        {
                            element: '.details-container',
                            intro: intro_billInfo,
                            position: 'top'
                        },
                        {
                            element: '.features-container',
                            intro: intro_billSettings,
                            position: 'top'
                        },
                        {
                            element: '.plans-accordion',
                            intro: intro_freeUnits + intro_acctServices,
                            position: 'top'
                        }
                    ]
                });

                intro
                    .start()
                    .oncomplete(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })})
                    .onexit(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })});
            }

            function intro_postpaid_mixed_tablet() {
                var intro = duIntro();
                intro.setOptions({
                    showStepNumbers: false,
                    exitOnOverlayClick: false,
                    steps: [
                        {
                            intro: intro_hi
                        },
                        {
                            element: '#topUsername',
                            intro: intro_accountBar,
                            position: 'bottom-right-aligned'
                        },
                        {
                            element: '.graph',
                            intro: intro_graph,
                            position: 'bottom'
                        },
                        {
                            element: '.min-due-info',
                            intro: intro_minPay,
                            position: 'right'
                        },
                        {
                            element: '.details-container',
                            intro: intro_billInfo,
                            position: 'top'
                        },
                        {
                            element: '.features-container',
                            intro: intro_billSettings,
                            position: 'top'
                        },
                        {
                            element: '.plans-accordion',
                            intro: intro_freeUnits + intro_acctServices,
                            position: 'top'
                        }
                    ]
                });

                intro
                    .start()
                    .oncomplete(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })})
                    .onexit(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })});
            }

            function intro_postpaid_mixed_mobile() {
                var intro = duIntro();
                intro.setOptions({
                    showStepNumbers: false,
                    exitOnOverlayClick: false,
                    steps: [
                        {
                            intro: intro_hi
                        },
                        {
                            element: '#toggleAccountPane768',
                            intro: intro_accountBar,
                            position: 'bottom-right-aligned'
                        },
                        {
                            element: '.min-due-info',
                            intro: intro_minPay,
                            position: 'bottom'
                        },
                        {
                            element: '#show-details',
                            intro: intro_billInfo + intro_billSettings,
                            position: 'bottom'
                        },
                        {
                            element: '.plans-accordion',
                            intro: intro_freeUnits + intro_acctServices,
                            position: 'top'
                        }
                    ]
                });

                intro
                    .start()
                    .oncomplete(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })})
                    .onexit(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })});
            }

            // Prepaid single account --

            var $overhead = $('.payment-overhead').length ? '.payment-overhead' : '.overhead';

            function intro_prepaid_single_desktop() {
                var intro = duIntro();
                intro.setOptions({
                    //showBullets: false,
                    //showProgress: true,
                    showStepNumbers: false,
                    exitOnOverlayClick: false,
                    steps: [
                        {
                            intro: intro_hi
                        },
                        {
                            element: '.accountPane .accountPaneIn',
                            intro: intro_accountBar,
                            position: 'left'
                        },
                        {
                            element: $overhead,
                            intro: intro_wallet,
                            position: 'bottom'
                        },
                        {
                            element: '.features-container',
                            intro: intro_prepaid_history,
                            position: 'top'
                        },
                        {
                            element: '.plans-accordion',
                            intro: intro_freeUnits + intro_acctServices,
                            position: 'top'
                        }
                    ]
                });

                intro
                    .start()
                    .oncomplete(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })})
                    .onexit(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })});
            }

            function intro_prepaid_single_tablet() {
                var intro = duIntro();
                intro.setOptions({
                    showStepNumbers: false,
                    exitOnOverlayClick: false,
                    steps: [
                        {
                            intro: intro_hi
                        },
                        {
                            element: '#topUsername',
                            intro: intro_accountBar,
                            position: 'bottom-right-aligned'
                        },
                        {
                            element: $overhead,
                            intro: intro_wallet,
                            position: 'bottom'
                        },
                        {
                            element: '.features-container',
                            intro: intro_prepaid_history,
                            position: 'top'
                        },
                        {
                            element: '.plans-accordion',
                            intro: intro_freeUnits + intro_acctServices,
                            position: 'top'
                        }
                    ]
                });

                intro
                    .start()
                    .oncomplete(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })})
                    .onexit(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })});
            }

            function intro_prepaid_single_mobile() {
                var intro = duIntro();
                intro.setOptions({
                    showStepNumbers: false,
                    exitOnOverlayClick: false,
                    steps: [
                        {
                            intro: intro_hi
                        },
                        {
                            element: '#toggleAccountPane768',
                            intro: intro_accountBar,
                            position: 'bottom-right-aligned'
                        },
                        {
                            element: $overhead,
                            intro: intro_wallet,
                            position: 'bottom'
                        },
                        {
                            element: '#show-details',
                            intro: intro_billInfo + intro_billSettings,
                            position: 'bottom'
                        },
                        {
                            element: '.plans-accordion',
                            intro: intro_freeUnits + intro_acctServices,
                            position: 'top'
                        }
                    ]
                });

                intro
                    .start()
                    .oncomplete(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })})
                    .onexit(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })});
            }

            // Prepaid mixed account --

            function intro_prepaid_mixed_desktop() {
                var intro = duIntro();
                intro.setOptions({
                    //showBullets: false,
                    //showProgress: true,
                    showStepNumbers: false,
                    exitOnOverlayClick: false,
                    steps: [
                        {
                            intro: intro_hi
                        },
                        {
                            element: '.accountPane .accountPaneIn',
                            intro: intro_accountBar,
                            position: 'left'
                        },
                        {
                            element: '.plans-accordion',
                            intro: intro_wallet + intro_freeUnits + ' ' + intro_acctServices,
                            position: 'bottom'
                        }
                    ]
                });

                intro
                    .start()
                    .oncomplete(function() {
                        $.cookie('firstvisit', 'firstvisit', {
                            expires: 365
                        })
                    })
                    .onexit(function() {
                        $.cookie('firstvisit', 'firstvisit', {
                            expires: 365
                        })
                         gtmImplementation.skipTrigger();

                    }).onchange(function(){
                 
                        gtmImplementation.nextTrigger(this);
                 
                    });
            }            

            function intro_prepaid_mixed_tablet() {
                var intro = duIntro();
                intro.setOptions({
                    showStepNumbers: false,
                    exitOnOverlayClick: false,
                    steps: [
                        {
                            intro: intro_hi
                        },
                        {
                            element: '#topUsername',
                            intro: intro_accountBar,
                            position: 'bottom-right-aligned'
                        },
                        {
                            element: '.plans-accordion',
                            intro: intro_wallet + intro_freeUnits + ' ' + intro_acctServices,
                            position: 'bottom'
                        }
                    ]
                });

                intro
                    .start()
                    .oncomplete(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })})
                    .onexit(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })});
            }

            function intro_prepaid_mixed_mobile() {
                var intro = duIntro();
                intro.setOptions({
                    showStepNumbers: false,
                    exitOnOverlayClick: false,
                    steps: [
                        {
                            intro: intro_hi
                        },
                        {
                            element: '#toggleAccountPane768',
                            intro: intro_accountBar,
                            position: 'bottom-right-aligned'
                        },
                        {
                            element: '.plans-accordion',
                            intro: intro_wallet + intro_freeUnits + ' ' + intro_acctServices,
                            position: 'bottom'
                        }
                    ]
                });

                intro
                    .start()
                    .oncomplete(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })})
                    .onexit(function(){$.cookie('firstvisit', 'firstvisit', { expires: 365 })});
            }

            if (!$.cookie('firstvisit') && $('div.dashboard').length){
                if ($('input[name="paymentMethod"]').val() == 'Postpaid' && $('input[name="number"]').val() == 'Single') {
                    if ($('input[name="userType"]').val() == 'Consumer' || $('input[name="userType"]').val() == 'EnterprisePR') {
                    if ($('.desktop').length) intro_postpaid_single_desktop();
                    if ($('.tablet').length) intro_postpaid_single_tablet();
                    if ($('.mobile').not('.tablet').length) intro_postpaid_single_mobile();
                }
            }

                if ($('input[name="paymentMethod"]').val() == 'Postpaid' && $('input[name="number"]').val() == 'Mixed') {
                    if ($('input[name="userType"]').val() == 'Consumer' || $('input[name="userType"]').val() == 'EnterprisePR') {
                        if ($('.desktop').length) intro_postpaid_mixed_desktop();
                        if ($('.tablet').length) intro_postpaid_mixed_tablet();
                        if ($('.mobile').not('.tablet').length) intro_postpaid_mixed_mobile();
                    }
                }

                if ($('input[name="paymentMethod"]').val() == 'Prepaid' && $('input[name="contractAge"]').val() == 'Old' && $('input[name="number"]').val() == 'Single') {
                        if ($('.desktop').length) intro_prepaid_single_desktop();
                        if ($('.tablet').length) intro_prepaid_single_tablet();
                        if ($('.mobile').not('.tablet').length) intro_prepaid_single_mobile();
                }

                if ($('input[name="paymentMethod"]').val() == 'Prepaid' && $('input[name="number"]').val() == 'Mixed') {
                    if ($('.desktop').length) intro_prepaid_mixed_desktop();
                    if ($('.tablet').length) intro_prepaid_mixed_tablet();
                    if ($('.mobile').not('.tablet').length) intro_prepaid_mixed_mobile();
                }
            }
        }
    )

    $('.contentIn > .hero').remove();


    if ($('#feature1').length > 0) {
        if ($('#feature1 .itemsW .items li').length <= 0) {
            $('#feature1').remove();
            $('.contentIn').prepend('<div class="row20"></div>');
        }
    }

});