/*
 *
 * duColumns JS class used to manage horizontal slideshows
 *
 * Requirements: jQuery, Hammer.js, Modernizr.js
 *
 * @param: object { 'target': HTMLObject, 'breakpoints': Array }
 * @return: void
 *
 * This class is executed on Microsoft's IE only
 * 'breakpoints' option represents the number of columns to draw for the following breakpoints:
 * - 320px : iphone portrait : 2
 * - 480px : iphone landscape : 3
 * - 768px : ipad portrait : 3
 * - 1024 : ipad landscape : 3
 * - larger screens : 3
 *
 */
 

	function duColumns($options) {
		/* Properties */
		this.options = { 'target': null, 'breakpoints': [2, 3, 3, 3, 3] };

		/* Methods declarations */
		this.drawColumns = drawColumns;
		this.createColumns = createColumns;
		this.setWidthAndHeight = setWidthAndHeight;
		this.windowResize = windowResize;
		this.init = init;

		/* Internal variables */
		this.elements = new Array();
		this.screenWidth = -1;
		this.columnTemplate = '<div class="column column[%INDEX%]"></div>';

		/* Methods */

		/*
		 * drawColumns
		 * @param: int
		 * @return: void
		 *
		 * Based on the size of the screen, writes the content on 2 or 3 columns
		 */
		function drawColumns(number) {
			var du = this,
				elementsTotal = du.elements.length,
				elementsPerColumn = Math.round(elementsTotal / number),
				remainingElements = ((elementsTotal % number) * number),
				column;
			du.options.target.find('.column').remove();
			du.options.target.find('.columns').html('');

			for (var i = 0; i < number; i++) {
				du.options.target.find('.columns').append(du.columnTemplate.replace(/\[\%INDEX\%\]/, i));

				for (var x = (i * elementsPerColumn); x < ((i * elementsPerColumn) + elementsPerColumn); x++) {
					du.options.target.find('.column' + i).append('<div class="item">' + du.elements[x] + '</div>');
				}
			}

			if (remainingElements > 0) {
				for (var x = (elementsTotal - remainingElements); x < elementsTotal; x++) {
					du.options.target.find('.column' + (number - 1)).append(du.elements[x]);
				}
			}
		}

		/*
		 * createColumns
		 * @param: void
		 * @return: void
		 *
		 * Based on the size of the screen, writes the content on 2 or 3 columns
		 */
		function createColumns() {
			var du = this;
			if (du.screenWidth <= 321) {
				du.drawColumns(du.options.breakpoints[0]);
			} else if (du.screenWidth <= 481) {
				du.drawColumns(du.options.breakpoints[1]);
			} else if (du.screenWidth <= 769) {
				du.drawColumns(du.options.breakpoints[2]);
			} else if (du.screenWidth <= 1025) {
				du.drawColumns(du.options.breakpoints[3]);
			} else {
				du.drawColumns(du.options.breakpoints[4]);
			}
		}

		/*
		 * setWidthAndHeight
		 * @param: void
		 * @return: void
		 * Called on class initialization and or resize, sets the width of the items' container
		 *
		 */
		function setWidthAndHeight() {
			var du = this;
			du.screenWidth = parseInt($('body').outerWidth(true));
		}

		/*
		 * windowResize
		 * @param: void
		 * @return: void
		 * Called on window resize
		 *
		 */
		function windowResize() {
			var du = this,
				// Give the browser some time to apply media-queries and resize elements			
				timeout = setTimeout(
					function () {
						clearTimeout(timeout);
						du.setWidthAndHeight();
						du.createColumns();
					},
					25
				);
		}

		/*
		 *
		 * init
		 * @param: void
		 * @return: void
		 * Called on class initialization
		 *
		 */
		function init() {
			var du = this;
			if (du.options.target != null) {

				du.options.target.find('.item').each(
					function () {
						du.elements.push($(this).html());
					}
				);

				du.setWidthAndHeight();
				du.createColumns();
				/* Add listener on screen resize */
				$(window).resize(function () { du.windowResize(); });
			}
		}

		this.options = $.extend(this.options, $options);
		this.init();
	}

 