$(function () {

    var $container = $('.modTiles'),
        isTouch = $('body').hasClass('touch');
    if ($container.length > 0) {
        if (($container.find('.tilesFilters').length > 0) && isTouch) {
            // Show/Hide filters on mobile devices
            $container.find('a.filtersTitle').unbind('click').bind(
                'click',
                function (e) {
                    e.preventDefault();
                    $(this).parent().toggleClass('visible');
                }
            );
        }
        // Enable filtering
        $container.find('a.subA').unbind('click').bind(
            'click',
            function () {
                // Hide dropdown
                if (isTouch) $container.find('.filters').removeClass('visible');
                // Filter list
                $container.find('a.subA.selected').removeClass("selected");
                $(this).addClass("selected");
                var selector = $(this).attr('data-filter');
                require([
                    'jquery.isotope.min'
                ], function (Isotope) {
                    var iso = new Isotope($container.find('.items'), {filter: selector});
                });
                //$container.find('.items').isotope({ filter: selector });
                return false;
            }
        );
    }

    $('.modTiles .tilesFilters .sorting li a').on('click', function () {
        $('.modTiles .sorting li a.selected').removeClass("selected");
        $(this).addClass("selected");

        var selector = $(this).data('sorting');
        var sortAscending = $(this).data('sortAscending');
        require([
            'jquery.isotope.min'
        ], function (Isotope) {
            var iso = new Isotope('.modTiles .items', {sortBy: selector, sortAscending: sortAscending});
        });
        //$('.modTiles .items').isotope({ sortBy: selector, sortAscending: sortAscending });
        return false;
    });

    //require( [
    //'jquery.isotope.min'
    //], function( Isotope ) {
    // var iso = new Isotope( '.modTiles .items', {
    //  getSortData: {
    //   order: function ($elem) {
    //    return parseInt($elem.data('order'), 10);
    //   },
    //   price: function ($elem) {
    //    return parseInt($elem.data('price'), 10);
    //   },
    //   brand: function ($elem) {
    //    console.log($elem.data('brand'));
    //    return $elem.data('brand');
    //   },
    //   newest: function ($elem) {
    //    return Date.parse($elem.data('created'));
    //   }
    //  }
    // });
    //});


    //$('.modTiles .items').isotope({
    //	getSortData: {
    //		order: function ($elem) {
    //			return parseInt($elem.data('order'), 10);
    //		},
    //		price: function ($elem) {
    //			return parseInt($elem.data('price'), 10);
    //		},
    //		brand: function ($elem) {
    //			console.log($elem.data('brand'));
    //			return $elem.data('brand');
    //		},
    //		newest: function ($elem) {
    //			return Date.parse($elem.data('created'));
    //		}
    //	}
    //});

});