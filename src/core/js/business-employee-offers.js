var BusinessOffers = (function() {

    var toggleAccordion = function () {
        $('.ucTabAccordian .accordion').on('click','dt a',function(){
            $(this).closest('.accordion').find('dd').slideToggle();
        });

    }

    var init = function() {
        toggleAccordion();
    };

    return {
        init: init,
    }
})();
$(function() {
    BusinessOffers.init();
});
