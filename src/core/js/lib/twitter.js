/*********************************************************************
  #### Twitter Post Fetcher! ####
  Coded by Jason Mayes 2013.
  www.jasonmayes.com
  Please keep this disclaimer with my code if you use it. Thanks. :-)
  Got feedback or questions, ask here: http://goo.gl/JinwJ
  
  Ammended by Ben Lumley and djb31st 2013
  www.dijitul.com
  Ammended to display latest tweet only with links
********************************************************************/
var twitterFetcher = function () { var d = null; return { fetch: function (a, b) { d = b; var c = document.createElement("script"); c.type = "text/javascript"; c.src = "http://cdn.syndication.twimg.com/widgets/timelines/" + a + "?&lang=en&callback=twitterFetcher.callback&suppress_response_codes=true&rnd=" + Math.random(); document.getElementsByTagName("head")[0].appendChild(c) }, callback: function (a) { var b = document.createElement("div"); b.innerHTML = a.body; a = b.getElementsByClassName("timeline-Tweet-text"); d(a) } } }();

twitterFetcher.fetch('441631760221106176', function(tweets){
  // Do what you want with your tweets here! For example:
  var x = tweets.length;
  var n = 0;
  var element = document.getElementById('tweets');
  if (!element) {
      return;
  }
  
  var html = '<ul>';
    if (x > 0) {
        if (x > 5) {
            x = 5;
        }
        for (i = 0; i < x; i++) {
            html += '<li>' + tweets[i].innerHTML + '</li>';
        }
    } /*
    if (tweets[n].innerHTML) {
        html += '<li>' + tweets[n].innerHTML + '</li>';
        html += '<li>' + tweets[n+1].innerHTML + '</li>';
    } else {
        html += '<li>' + tweets[n].textContent + '</li>';
        html += '<li>' + tweets[n+1].innerHTML + '</li>';
    }*/
    n++;
  html += '</ul>';
  element.innerHTML = html;
});
