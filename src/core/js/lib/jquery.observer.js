﻿/*
* Observer Pattern with jQuery + Mutation Observer for DOMs
*/

"use strict";

(function ($) {

    jQuery.observable = function (initValue) {
        var _value = initValue;
        var _subscriptions = [];

        var fun = function fun(value) {
            if (value !== undefined && value !== _value) {
                var oldValue = _value;
                _value = value;

                _subscriptions.forEach(function (cb) {
                    cb(value, oldValue);
                });
            }

            return _value;
        };
        fun.subscribe = function (cb) {
            _subscriptions.push(cb);
        };

        return fun;
    };

    jQuery.fn.extend({
        observe: function observe(callback, config) {
            var self = this;
            var observer = new MutationObserver(callback);
            if (!config) {
                config = {
                    attributes: true,
                    childList: true,
                    characterData: true
                };
            }

            self.each(function () {
                observer.observe(this, config);
            });

            // observer.disconnect();
        }
    });
})(jQuery);

