
var listDiv;
var opts;
var plyrHight = 427;
var plyrwidth = 710;

(function ($) {
    $.fn.youTubeChannel = function (options) {
        listDiv = $(this);
        $.fn.youTubeChannel.defaults = {
            loadingText: "Loading...",
            videoBox: null,
            playerHeight: plyrHight,
            playerWidth: plyrwidth,
            autoPlay: true,
            showRelated: false,
            allowFullScreen: true
        }

        opts = $.extend({}, $.fn.youTubeChannel.defaults, options);

        return this.each(function () {
            preLoaderHTML = $("<p class=\"loader\">" + opts.loadingText + "</p>");
            listDiv.html(preLoaderHTML);
            // TODO: Error handling!
            //$.getScript("https://gdata.youtube.com/feeds/base/users/" + opts.userName + "/uploads?orderby=viewCount&alt=json-in-script&callback=jQueryYouTubeChannelReceiveData");
            $.get("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=UUy6Pvlythh0mHb1RmSCMdCw&key=AIzaSyBSBZDDPOKAcP2g4q4BAb8xJHxsF9CA23U",
                function (data) {
                    jQueryYouTubeChannelReceiveData(data);
                });
           // $.getScript("https://gdata.youtube.com/feeds/base/users/" + opts.userName + "/uploads?orderby=updated&alt=json-in-script&callback=jQueryYouTubeChannelReceiveData");


        });
    };
})(jQuery);

function jQueryYouTubeChannelReceiveData(data) {
    var videos = [];
    $.each(data.items, function (i, e) {
        var video = e;

        videos.push({
            title: video.snippet.title,
            image: video.snippet.thumbnails.high.url,
            url: 'https://www.youtube.com/watch?v=' + video.snippet.resourceId.videoId
        });
    });
    renderPlayList(videos);
    $(".ucscrollbar").ClassyScroll();
}

function renderPlayList(data) {
    var playList = [];
    playList.push('<div class="ucscrollbar"><ul>');
    for (var i = 0; i < data.length; i++) {
        playList.push('<li ' + (i == 0 ? ' class="active" ' : '') + '><a onclick="showVideo(this);return false;" href="' + data[i].url + '"><img height="69px" class="pull-left" src="' + data[i].image + '" alt="' + data[i].title + '"/>' + data[i].title + '</a></li>');
    }
    playList.push("</ul></div>");

    listDiv.html(playList.join(''));

    //var itemHolder = $("#p1 li:first-child").html();
    //$("#p1 li:first-child").html($("#p2 li:first-child").html());
    //$("#p2 li:first-child").html(itemHolder);

    $(".ucPopularVideos .content a[href]:first").click();

    // $('.fluid-video').fitVids();

}

function showVideo(item) {
    var id = youtubeid($(item).attr("href"));
    //$(".videoTitle p").text($("p", item).text());
    $(".ucPopularVideos .content li").removeClass("active");
    $(item).parent().addClass("active");
    $("#" + opts.videoBox).html(getPlayer(id));
    $('.fluid-video').fitVids();
    return false;
}

function getPlayer(id) {
    var html = '';
    var autoPlay = "";
    var showRelated = "&rel=0";
    var fullScreen = "";
    if (opts.autoPlay) autoPlay = "&autoplay=1";
    if (opts.showRelated) showRelated = "&rel=1";
    if (opts.allowFullScreen) fullScreen = "&fs=1";
    html += '<object height="' + opts.playerHeight + '" width="' + opts.playerWidth + '">';
    html += '<param name="movie" value="https://www.youtube.com/v/' + id + autoPlay + showRelated + fullScreen + '"> </param>';
    html += '<param name="wmode" value="transparent"> </param>';
    if (opts.allowFullScreen) {
        html += '<param name="allowfullscreen" value="true"> </param>';
    }
    html += '<embed src="https://www.youtube.com/v/' + id + autoPlay + showRelated + fullScreen + '"';
    if (opts.allowFullScreen) {
        html += ' allowfullscreen="true" ';
    }
    html += 'type="application/x-shockwave-flash" wmode="transparent"  height="' + opts.playerHeight + '" width="' + opts.playerWidth + '"></embed>';
    html += '</object>';

    return html;

}

function youtubeid(url) {
    var ytid = url.match("[\\?&]v=([^&#]*)");
    ytid = ytid[1];
    return ytid;
}
