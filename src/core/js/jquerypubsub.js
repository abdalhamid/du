"use strict";
(function(n) {
    var t = n({});
    window.pubsub = {};
    window.pubsub.subscribe = function() { t.on.apply(t, arguments) };
    window.pubsub.unsubscribe = function() { t.off.apply(t, arguments) };
    window.pubsub.publish = function() { t.trigger.apply(t, arguments) } })(jQuery);
