'use strict';

require(['bootstrap'], function () {

    var recenterPopup = function recenterPopup() {
        var popup = $("#PlanDetailsPopup");
        popup.css("margin-left", -popup.outerWidth() / 2);
    };

    $('.plans .hidden').removeClass('hidden').addClass('softHidden');

    var addGtmDataLayer = function addGtmDataLayer(eventAction, eventLayer) {
        dataLayer.push({ EventCategory: "postpaid_plans", EventAction: eventAction, EventLabel: eventLayer });
    };

    $('.filterBtns').on('click', 'a', function () {
        var filterValue = $(this).attr('rel');

        $('.packs-list').hide();
        $(filterValue).show();

        return false;
    });

    $('.filterBtns').each(function (i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on('click', '> a', function () {
            $('.dropdown-menu', $(this).parent()).hide();
            $buttonGroup.find('.is-checked').removeClass('is-checked');
            $(this).addClass('is-checked');
        });
    });

    /*
        * New plans pages
        */

    if ($('.you-get').length > 0) {

        $('.you-get').each(function () {
            $('> div:not(".benefits")', $(this)).each(function (i) {
                $(this).addClass('item' + i);
            });
            $('> div > div', $(this)).each(function (i) {
                $(this).addClass('benefit' + i);
            });
        });

        $('.you-get > div:not(".benefits"), .you-get > div > div').hover(function () {
            $('.' + $(this).attr('class')).addClass('highlight');
        }, function () {
            $('.you-get > div:not(".benefits"), .you-get > div > div').removeClass('highlight');
        });
    }

    //Plan information popup
    $('a[data-reveal-id="PlanDetailsPopup"]').on('click', function () {

        var cardItem = $(this).closest('.card-item');
        var planInformationUrl = $(this).attr('href');
        var planInformationPrintUrl = planInformationUrl;
        var planInformationDocumentUrl = cardItem.attr('data-documentUrl');
        var purchasePlanUrl = cardItem.find('.buyPlanButton').attr('href');

        if (planInformationPrintUrl.indexOf('?') == -1) {
            planInformationPrintUrl += '?print=true';
        } else {
            planInformationPrintUrl += '&print=true';
        }

        var planDetailsPopup = $('#PlanDetailsPopup');
        planDetailsPopup.find('.loadingOverlay').show();

        planDetailsPopup.find('.planDetailsFrame').attr('src', planInformationUrl);
        planDetailsPopup.find('.printButton').attr('href', planInformationPrintUrl).attr('target', '_blank');
        planDetailsPopup.find('.downloadButton').attr('href', planInformationDocumentUrl).attr('target', '_blank');
        planDetailsPopup.find('.buyButton').attr('href', purchasePlanUrl);
        planDetailsPopup.find('.loadingOverlay').hide();

        planDetailsPopup.show();

        recenterPopup();

        var width = Math.round(planDetailsPopup.width());
        planDetailsPopup[0].style.setProperty('width', width + 'px', 'important');

        if ($('.ipad').length > 0) {
            setTimeout(function () {
                var top = parseInt(planDetailsPopup.css('top')) - 35;
                planDetailsPopup.css('top', top + 'px');
            }, 1000);
        }
    });

    $('#PlanDetailsPopup .close-reveal-modal').on('click', function () {

        var planDetailsPopup = $('#PlanDetailsPopup');

        planDetailsPopup.hide();
        planDetailsPopup.find('.planDetailsFrame').attr('src', 'about:blank');
        planDetailsPopup.find('.printButton').attr('href', 'javascript:void(0);').attr('target', null);
        planDetailsPopup.find('.downloadButton').attr('href', 'javascript:void(0);').attr('target', null);
        planDetailsPopup.find('.buyButton').attr('href', 'javascript:void(0);');
        planDetailsPopup[0].style.removeProperty('width');
    });

    $('#PlanDetailsPopup .printButton').on('click', function (e) {

        e.preventDefault();

        if ($('body.android').length > 0 || $('.firefox .desktop').length > 0) {
            var printUrl = $(this).attr('href');
            window.open(printUrl);
        } else {
            document.PlanDetailsFrame.printDocument();
        }
    });

    $("[data-toggle=popover]").popover({
        trigger: 'focus',
        container: 'body',
        placement: 'auto top'
    });

    // Fixes to iPhone and iPad
    if (navigator.userAgent.indexOf('iPad') >= 0 || navigator.userAgent.indexOf('iPhone') >= 0) {
        // isolate the fix to apply only when a tooltip is shown
        $("[data-toggle=popover]").on("shown.bs.popover", function () {
            $('body').css('cursor', 'pointer');
        });

        // Return cursor to default value when tooltip hidden.
        // Use 'hide' event (as opposed to "hidden") to ensure .css() fires before
        // 'shown' event for another tooltip.
        $("[data-toggle=popover]").on("hide.bs.popover", function () {
            $('body').css('cursor', 'auto');
        });

        //Fixes to iPad only
        //if (navigator.userAgent.indexOf('iPad') >= 0) {
        //    //Layout change doesn't work on iPad, so initiate matchHeight here.
        //    setTimeout(function () {
        //        matchRowHeights(true);
        //    }, 500);
        //}
    }

    $(window).on("orientationchange", function () {
        //setTimeout(function () {
        //    recenterPopup();
        //    //initializePopover();
        //}, 500);

        $('.planDetailsPopup:visible').each(function () {

            var _this = this;

            setTimeout(function () {
                var windowWidth = $(window).width();
                var popupWidth = windowWidth * 0.9;
                var leftMargin = popupWidth / 2;

                $(_this).css('margin-left', -leftMargin + 'px');
                _this.style.setProperty('width', popupWidth + 'px', 'important');
            }, 500);
        });
    });

    // Fix to close Popover on iPad
    if ($("html").hasClass("ipad")) {
        $(document).on("click touchstart", function (e) {
            if ($(e.target).data("toggle") !== "popover" && $(e.target).parents("[data-toggle='popover']").length === 0 && $(e.target).parents(".popover.in").length === 0) {
                $("[data-toggle='popover']").popover("hide");
            }
        });
    }

    $('.du-accordion .block-header').on('click', function () {

        var block = $(this).closest('.block');

        if (!block.hasClass('open')) {
            return;
        }

        if (block.hasClass('faqs-block')) {
            addGtmDataLayer("National", 'info_faqs');
        } else if (block.hasClass('terms-and-conditions-block')) {
            addGtmDataLayer("National", 'info_termsandconditions');
        }
    });

    $('.plan-cards').addClass('loaded');
});

window.printPopupCallback = function (window) {

    setTimeout(function () {
        window.close();
    }, 250);
};

window.frameReadyCallback = function (window) {

    $('#PlanDetailsPopup .loadingOverlay').hide();
};

//Multilingual patch for tourist plans
$(document).ready(function () {

    var defaultLanguages = [{ url: '/personal/mobile/prepaid-plans/tourist-sim', code: 'en', label: 'English' }, { url: '/ar/personal/mobile/prepaid-plans/tourist-sim', code: 'ar', label: 'العربية' }];

    var additionalLanguages = [{ url: '/personal/mobile/prepaid-plans/tourist-plan-russian', code: 'ru', label: 'русский' }, /* { url: '/personal/mobile/prepaid-plans/tourist-plan-hindi', code: 'hi', label: 'हिंदी' },*/{ url: '/personal/mobile/prepaid-plans/tourist-plan-chinese', code: 'zh-CN', label: '中文' }];

    var languagesList = $('#langsWrapper');
    var mobileLanguagesList = $('#langsWrapperMobile');

    var anyDefaultLanguagesShown = false;
    for (var i = 0; i < defaultLanguages.length; i++) {
        var defaultLanguage = defaultLanguages[i];
        if ($('#langsWrapper li a[lang="' + defaultLanguage.code + '"]').length > 0) {
            anyDefaultLanguagesShown = true;
            break;
        }
    }

    if (!anyDefaultLanguagesShown) {
        for (var i = 0; i < defaultLanguages.length; i++) {
            var defaultLanguage = defaultLanguages[i];
            languagesList.append('<li class="sflanguageItem sflang_ar"><a href="' + defaultLanguage.url + '" class="sflanguageLnk" lang="' + defaultLanguage.code + '"><span>' + defaultLanguage.label + '</span></a></li>');
            mobileLanguagesList.append('<li class="sflanguageItem sflang_ar"><a href="' + defaultLanguage.url + '" class="sflanguageLnk" lang="' + defaultLanguage.code + '"><span>' + defaultLanguage.label + '</span></a></li>');
        }
    }

    for (var i = 0; i < additionalLanguages.length; i++) {

        var additionalLanguage = additionalLanguages[i];
        if (location.pathname.toLowerCase() !== additionalLanguage.url.toLowerCase()) {
            languagesList.append('<li class="sflanguageItem sflang_ar"><a href="' + additionalLanguage.url + '" class="sflanguageLnk" lang="' + additionalLanguage.code + '"><span>' + additionalLanguage.label + '</span></a></li>');
            mobileLanguagesList.append('<li class="sflanguageItem sflang_ar"><a href="' + additionalLanguage.url + '" class="sflanguageLnk" lang="' + additionalLanguage.code + '"><span>' + additionalLanguage.label + '</span></a></li>');
        }
    }
});

