var OurNetworkController = (function() {


    var initSlide = function() {
        var swiper = new Swiper('.swiper-container', {
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            pagination: '.swiper-pagination',
            paginationClickable: true,
            spaceBetween: 30,
            autoplay: 3000
        });
    }

    var validateForm = function() {
        $('.sfFormSubmit input[type=submit]').click(function () {
            dataLayer.push([{}]);
        });
        $('.sfFormsEditor .successDataLayer input').val("");
    }

    var duScrollToTop = function() {
        function t() {
            $(".inpagenavigation").offset().top < $(window).scrollTop() - 100 ? $(".go-to-top").show() : $(".go-to-top").hide()
        }
        this.windowScroll = t;
        $(".go-to-top a").click(function(n) {
            n.preventDefault();
            navigator.userAgent.match(/(iPod|iPhone|iPad)/) ? $(".inpagenavigation")[0].scrollIntoView(!1) : $("html, body").animate({
                scrollTop: "0px"
            }, 800, function() {
                $("html, body").clearQueue()
            })
        });
        var n = this;
        $(window).on("scroll touchmove", function() {
            n.windowScroll()
        });
        setInterval(function() {
            n.windowScroll()
        }, 1e3)
    }

    var configEmirates = {};

    if (window.location.href.toLowerCase().indexOf("/ar") > 0) {
        $("body").addClass("arabic");
        $("#lblselectWrapEmirate").text("اختيار إمارة");
        $("#emirates option[value=AUH]").html("أبو ظبي");
        $("#emirates option[value=SHJ]").html("الشارقة");
        $("#emirates option[value=AJM]").html("عجمان");
        $("#emirates option[value=ALA]").html("‫العين‬");
        $("#emirates option[value=DXB]").html("دبي");
        $("#emirates option[value=FUJ]").html("الفجيرة");
        $("#emirates option[value=RAK]").html("رأس الخيمة");
        $("#emirates option[value=UAQ]").html("أمّ القيوين");
        $("#emirates option[value=AAN]").html("العين");
        $("#emirates option[value=DXB_HATTA]").html("حتا");
        $("#emirates option[value=WR]").html("المنطقة الغربية");

        $("#lblselectWrapNetworkType").text("اختيار شبكة");
    }

    function initialize() {
        //Get JSON for necessary map data 
        //center: [Lat, Lng]
        $.getJSON("../common/scripts/prototype/coverage-map-data.json", function(data) {
            configEmirates = data;

            loadMap("UAE", "");

            $(selectionInputs).change();
        });

        $("#emirates").change(function() {
            var emirate = $("select#emirates").val();
            if (emirate != "") {
                if (emirate != "DXB_HATTA" && emirate != "WR") {
                    $("select#networkTypes").val('4G');
                } else
                    $("select#networkTypes").val('3G');
            } else {
                $('select#networkTypes')[0].selectedIndex = 0;
            }
        });

        var selectionInputs = '#networkTypes,#emirates';

        //On changing the dropdown, getting the values from the corresponding items and resetting map
        $(selectionInputs).change(function() {

            var selectedEmirate = $('#emirates').val().trim().toUpperCase();
            var selectedNetwork = $('#networkTypes').val().toUpperCase();

            if (selectedEmirate == "DXB_HATTA" || selectedEmirate == "WR") {
                $("#networkTypes option[value=4G]").css('display', 'none');
            } else {
                $("#networkTypes option[value=4G]").css('display', 'block');
            }

            if (selectedEmirate != "" && selectedNetwork != "") {
                loadMap(selectedEmirate, selectedNetwork);
            } else if (selectedEmirate != "") {
                loadMap(selectedEmirate, "");
            } else {
                loadMap("UAE", "");
            }

        });
    }

    function loadMap(emirate, network) {
        if (typeof google === 'object' && typeof google.maps === 'object') {
            var coordinates = (emirate != "" && network != "") ? configEmirates[emirate].mapFiles[network].center : configEmirates[emirate].center;
            var zoomLevel = network != "" ? configEmirates[emirate].mapFiles[network].zoom : configEmirates[emirate].zoom;

            var options = {
                zoom: zoomLevel,
                streetViewControl: false,
                center: new google.maps.LatLng(coordinates[0], coordinates[1]),
                mapTypeId: google.maps.MapTypeId.TERRAIN
            }
            var map = new google.maps.Map(document.getElementById('map-canvas'), options);
            if (emirate != "" && network != "") {
                var src = "http://www.du.ae/UserControls/CoverageMaps/" + configEmirates[emirate].mapFiles[network].file;

                loadKmlLayer(src, map);
            }
        }
    }

    function loadKmlLayer(src, map) {
        var kmlLayer = new google.maps.KmlLayer(src, {
            suppressInfoWindows: true,
            preserveViewport: true,
            map: map
        });
    }


    var init = function() {
        initSlide();
        initialize();
        validateForm();
        duScrollToTop();

    };

    return {
        init: init,
    }
})();
$(function() {
    if ($('#ourNetwork').length > 0) {
        OurNetworkController.init();
    }
});
