"use strict";
window.isDebugMode && console.log("formWizard.js: Loaded");
require(["jquerypubsub"], function() {
    var t, i, n, r;
    return window.isDebugMode && console.log("formWizard.js: Initializing"),
    t = function(n, t) {
        function ft(n) {
            $(n.currentTarget).closest(".optionalFormWizardpage").length > 0 ? ei() : ui()
        }
        function et() {
            fi()
        }
        function pt() {
            var n = l();
            $(n).find('.sfFormSubmit input[type = "submit"]').trigger("click")
        }
        function wt(n) {
            var t = hi();
            t ? (ut = !0,
            f && f.length > 0 && f.addClass("disabled")) : n.preventDefault()
        }
        function ot(n) {
            var t = $(n.currentTarget).index();
            s(t)
        }
        function bt(n) {
            var t = $(n.currentTarget).data("pageIndex".toLowerCase());
            s(t)
        }
        function kt() {
            i.addClass("formWizard");
            u.addClass("formWizardPage");
            o.addClass("formWizardPage optionalFormWizardPage");
            i.attr("data-totalPages", e);
            dt();
            i.on("click", ".previousButton", ft);
            i.on("click", ".nextButton", et);
            i.on("click", ".formWizardPreviousButton", ft);
            i.on("click", ".formWizardNextButton", et);
            i.on("click", ".optionalPagesLink", bt);
            i.on("click", ".progressBar li.active", ot);
            i.on("click", ".sfFormSubmit input", wt);
            i.on("click", ".submitButton", pt);
            i.on("click", ".progressBar li.active", ot);
            i.find(".sfFormBox.placeholderInstructions").each(function(n, t) {
                var i = $.trim($(t).find(".sfExample").text());
                $(t).find('input[type="text"]').attr("placeholder", i)
            })
        }
        function dt() {
            typeof Telerik != "undefined" && Telerik && Telerik.Sitefinity && Telerik.Sitefinity.Web && Telerik.Sitefinity.Web.UI && Telerik.Sitefinity.Web.UI.Fields && Telerik.Sitefinity.Web.UI.Fields.FormManager && Telerik.Sitefinity.Web.UI.Fields.FormManager._validationGroupMappings && u.each(function(n, t) {
                var i = "formWizardValidationGroup_" + n
                  , r = [];
                $(t).find(".sfFormBox, .sfFormDropdown, .sfFormRadiolist, .sfFormCheckboxlist, .sfFormBlock, .sfFormFile").each(function(n, t) {
                    if (!$(t).hasClass("non-validate")) {
                        var i = $(t).attr("id");
                        $.trim(i).length > 0 && r.push(i)
                    }
                });
                Telerik.Sitefinity.Web.UI.Fields.FormManager._validationGroupMappings[i] = r;
                tt.push(i)
            })
        }
        function gt() {
            for (var t, n, i = 0; i < o.length; i++)
                t = o[i],
                n = $(t).find(".wizardPageTitle").text(),
                $.trim(n).length == 0 && (n = $(t).find("legend:visible").eq(0).text()),
                $.trim(n).length == 0 && (n = $(t).find("h1, h2, h3, h4, h5, h6").eq(0).text()),
                $.trim(n).length == 0 && (n = $(t).find("label").eq(0).text()),
                $.trim(n).length == 0 && (n = $(t).find("legend").eq(0).text()),
                $.trim(n).length == 0 && (n = c({
                    en: "Optional information",
                    ar: "Optional information"
                })),
                it[i] = n
        }
        function ni() {
            var n = [], t;
            for (n.push('<ul class="progressBar">'),
            t = 0; t < e; t++)
                n.push("<li><\/li>");
            n.push('<\/ul">');
            i.prepend(n.join(""))
        }
        function ti() {
            var n = setInterval(function() {
                (ii(),
                a) || yt()
            }, 100, this)
        }
        function ii() {
            var t = l(), n;
            if (ut) {
                f.addClass("disabled");
                return
            }
            v && v.length > 0 && f && f.length > 0 && (n = v.attr("disabled"),
            n == "disabled" ? f.hasClass("disabled") || f.addClass("disabled") : f.hasClass("disabled") && f.removeClass("disabled"))
        }
        function c(n) {
            var t = $.trim(document.location.pathname).toLowerCase();
            return t == "ar" || t.startsWith("ar/") || t.startsWith("/ar/") ? n.ar : n.en
        }
        function st(n) {
            var t, u, i;
            if (n) {
                if (t = [],
                t.push('<div class="formsWizardNavigation">'),
                b() && t.push('<a class="btn btn-default previousButton" href="javascript: void(0);"><span>' + c({
                    en: "Previous",
                    ar: "السابق"
                }) + "<\/span><\/a>"),
                !y() || n.find(".sfFormSubmit input").is(":visible") ? (u = $(n).find(".sfFormSubmit input").val(),
                $.trim(u).length == 0 && (u = c({
                    en: "Submit",
                    ar: "تقديم الطلب"
                })),
                t.push('<a class="btn btn-default submitButton" href="javascript: void(0);"><span>' + u + "<\/span><\/a>")) : t.push('<a class="btn btn-default nextButton" href="javascript: void(0);"><span>' + c({
                    en: "Next",
                    ar: "التالي"
                }) + "<\/span><\/a>"),
                !y() && r < e && o.length > 0) {
                    for (t.push('<div class="formsWizardOptionalPagesLinks">'),
                    i = 0; i < o.length; i++)
                        t.push('<a class="optionalPagesLink" href="javascript: void(0);" data-pageIndex="' + (e + i).toString() + '"><span>' + it[i] + "<\/span><\/a>");
                    t.push("<\/div>")
                }
                t.push("<\/div>");
                n.append(t.join(""));
                v = $(n).find('.sfFormSubmit input[type = "submit"]').eq(0);
                f = $(n).find(".submitButton").eq(0)
            }
        }
        function ri(n) {
            n && (n.find(".formsWizardNavigation").remove(),
            n.find(".formsWizardOptionalPagesLinks").remove())
        }
        function b() {
            return r > 0
        }
        function y() {
            return r < e - 1
        }
        function ht(n) {
            for (var t = 0; t < u.length; t++)
                if (u[t] == $(n)[0])
                    return t;
            return -1
        }
        function l() {
            return $(u[r])
        }
        function p(n) {
            var t = u[n];
            return t ? $(t) : null
        }
        function ui() {
            b() && s(r - 1)
        }
        function fi() {
            if (y()) {
                var n = ct();
                if (!n) {
                    li();
                    return
                }
                s(r + 1)
            }
        }
        function ei() {
            s(e - 1)
        }
        function ct() {
            var n = !0, i = Math.max(0, Math.min(r, u.length - 1)), t = p(i), f;
            if (t.find(".customError").remove(),
            $(t).find("input, textarea, select").each(function(t, i) {
                var o = d(i), e = $(i).data("validators"), r, u, f;
                if (e && (r = e.validators,
                r && r.length > 0))
                    for (u = 0; u < r.length; u++)
                        if (f = r[u],
                        typeof f.validate == "function" && !f.validate(o)) {
                            g(i, f.message);
                            n = !1;
                            break
                        }
            }),
            $(t).find("input, textarea, select").filter("[data-regexp]").each(function(t, i) {
                var r, u, f, e;
                if ((!$(this).is(":hidden") || $(this).attr("data-validate-if-invisible") != "false") && (r = $.trim($(this).attr("data-regexp")),
                r.length != 0) && (u = $.trim(d(i)),
                u.length != 0))
                    try {
                        f = new RegExp(r,"i");
                        f.test(u) || (e = $.trim($(this).attr("data-regexp-msg")),
                        g(i, e),
                        n = !1)
                    } catch (o) {
                        console.log('Invalid regexp validation "' + r + '"')
                    }
            }),
            $(t).find("input, textarea, select").filter("[required], [data-required]").each(function(t, i) {
                var r, u;
                $(this).is(":hidden") && $(this).attr("data-validate-if-invisible") == "false" || (r = $.trim(d(i)),
                r.length == 0 && (u = $.trim($(this).attr("data-required-msg")),
                g(i, u),
                n = !1))
            }),
            typeof Telerik == "undefined" || !Telerik || !Telerik.Sitefinity || !Telerik.Sitefinity.Web || !Telerik.Sitefinity.Web.UI || !Telerik.Sitefinity.Web.UI.Fields || !Telerik.Sitefinity.Web.UI.Fields.FormManager || typeof Telerik.Sitefinity.Web.UI.Fields.FormManager.validateGroup != "function")
                return n;
            f = tt[i];
            try {
                Telerik.Sitefinity.Web.UI.Fields.FormManager.validateGroup(f) || (n = !1)
            } catch (e) {
                return console.log("Error encountered while executing Sitefinity validations."),
                n
            }
            return n
        }
        function k() {
            var n = i.data("formWizardSettings");
            return typeof n != "object" && (n = {}),
            n
        }
        function oi(n, t) {
            var i = k();
            typeof i.PageNavigating == "function" && i.PageNavigating(r, n, t)
        }
        function lt() {
            var n = k();
            typeof n.PageNavigated == "function" && n.PageNavigated(r, rt)
        }
        function si() {
            var n = k();
            typeof n.FormSubmit == "function" && n.FormSubmit(r)
        }
        function at(n) {
            var t, u;
            typeof n != "number" && (n = r);
            t = i.find(".progressBar li");
            t.removeClass("focus");
            u = t.eq(n);
            u.addClass("active focus");
            u.prevAll().addClass("active")
        }
        function hi() {
            var n;
            if (ct(),
            n = i.find(".sfError, .customError").filter(function(n, t) {
                return !$(t).parent().hasClass("non-validate") && $.trim($(t).text()).length > 0
            }).eq(0),
            n.length == 0)
                return si(),
                !0;
            var r = n.parent().find("input, select, textarea").eq(0)
              , u = n.closest(h)
              , t = ht(u);
            return t >= 0 && s(t, r),
            !1
        }
        function vt() {
            r == 0 ? i.attr("data-wizardPageState", "first") : r == e - 1 ? i.attr("data-wizardPageState", "last") : r > e - 1 ? i.attr("data-wizardPageState", "optional") : i.attr("data-wizardPageState", "ongoing");
            i.attr("data-wizardPageIndex", r)
        }
        function yt() {
            var n = l();
            $(".formWizard .formControls").height() !== n.height() && $(".formWizard .formControls").height(n.height())
        }
        function d(n) {
            return $(n).val()
        }
        function g(n, t) {
            var i = "", u, r;
            typeof t == "object" ? i = c(t) : typeof t == "string" && (i = t);
            $.trim(i).length == 0 && (i = c({
                en: "Required",
                ar: "مطلوب"
            }));
            u = $(n).parent().parent();
            r = u.find(".sfError");
            r.length > 0 && r.is(":visible") ? r.text(i) : u.append('<div class="sfError customError">' + i + "<\/div>")
        }
        function ci() {
            var i = Math.max(0, Math.min(r, u.length - 1)), f = p(i), n = f.find("label:visible, input:visible, select:visible, textarea:visible").eq(0), t;
            n && n.length <= 0 || (t = n.offset().top - $(".eitc-breadcrumbs-wrapper").outerHeight(),
            $(window).scrollTop() < t) || $("html, body").stop(!0).animate({
                scrollTop: t
            }, 300)
        }
        function li() {
            var t = Math.max(0, Math.min(r, u.length - 1))
              , i = p(t)
              , n = i.find(".sfError, .customError").filter(function(n, t) {
                return $.trim($(t).text()).length > 0
            }).eq(0);
            n.length !== 0 && setTimeout(function() {
                n.parent().find("input, select, textarea").eq(0).focus()
            }, 0)
        }
        var i = $(n), h = t.pageSelector, nt = t.direction, u = [], tt = [], o = [], e = 0, it = [], rt = 0, r = 0, w = 0, a = !1, v = null, f = null, ut = !1, s;
        this.init = function() {
            var n, t;
            $.trim(h).length == 0 && (h = "fieldset");
            n = [];
            i.data("formWizard", this);
            i.find(h).filter("[data-disabled=true]").hide();
            i.find(h).not("[data-disabled=true]").filter(function(n, t) {
                return $(t).closest(".optionalWizardpage").length == 0
            }).each(function(t, i) {
                n.push(i)
            });
            e = n.length;
            o = i.find(h).filter(function(n, t) {
                return $(t).closest(".optionalWizardpage").length > 0
            });
            o.each(function(t, i) {
                n.push(i)
            });
            i.attr("data-hasOptionalPages", o.length > 0);
            u = $(n);
            gt();
            kt();
            ni();
            ti();
            t = l();
            at();
            t.css({
                left: 0,
                opacity: 1,
                transform: ""
            });
            t.show();
            st(t);
            vt();
            lt();
            window.pubsub.publish("formWizard/initialized", [])
        }
        ;
        s = this.GoToPageByIndex = function(n, t) {
            var o, e, h;
            if (rt = r,
            o = r,
            e = Math.max(0, Math.min(n, u.length - 1)),
            o != e) {
                var i = l()
                  , f = p(n)
                  , s = e > o
                  , c = w < n;
                if (w = Math.max(w, n),
                a)
                    return !1;
                a = !0;
                oi(n, c);
                r = e;
                f.show();
                st(f);
                at();
                vt();
                yt();
                h = 200;
                i.animate({
                    opacity: 0
                }, {
                    step: function(n) {
                        var r = 1 - (1 - n) * .2, t;
                        i.css({
                            transform: "scale(" + r + ")"
                        });
                        t = {
                            opacity: 1 - n,
                            left: n * 20 + "%"
                        };
                        (nt == "ltr" && !s || nt == "rtl" && s) && (t.left = -(n * 10) + "%");
                        f.css(t)
                    },
                    duration: h,
                    complete: function() {
                        i.css({
                            left: 0,
                            opacity: 0,
                            transform: ""
                        });
                        i.hide();
                        ri(i);
                        f.css({
                            left: 0,
                            opacity: 1,
                            transform: ""
                        });
                        t ? $(t).focus() : f.find("input, select, textarea").eq(0).focus();
                        lt();
                        a = !1
                    }
                });
                ci();
                window.pubsub.publish("formWizard/movedToPage", [n, t])
            }
        }
    }
    ,
    window.FormWizardManager = t,
    i = $(".enableFormWizard").filter(function() {
        return $(this).find(".formControls").length > 0
    }).toArray(),
    window.isDebugMode && console.log("formWizard.js: Found " + i.length + " form wizards."),
    n = $(".enableFormWizard").filter(function() {
        return $(this).find(".sfSuccess").length > 0
    }).show(),
    setTimeout(function() {
        n.length > 0 && $("html, body").scrollTop(n.offset().top)
    }, 100, n),
    window.isDebugMode && console.log("formWizard.js: Showing " + n.length + " form wizards."),
    r = window.FormWizardManagers = $.map(i, function(n) {
        var i = new t(n,{
            pageSelector: ".formControls fieldset",
            direction: $("html").attr("dir")
        });
        i.init();
        $(n).data("formWizard", i)
    }),
    window.isDebugMode && console.log("formWizard.js: Initialized"),
    t
});
