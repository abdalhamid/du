﻿/*
 *  jQuery Validator - v1.0.0
 *
 *  Made by Prototype Interactive
 */
; (function ($, window, document, undefined) {

    // Create the defaults once
    var pluginName = "protoValidator",
        defaults = {
            control: ".control",
            group: ".group",
            errorTemplate: "<span class='help'>{{msg}}</span>"
        };


    var validators = {
        "credit-card-number": /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|3[47][0-9]{13})$/,
        "cvv-number": /^[0-9]{3}$/,
        "number": /^[0-9]*$/,
        "alphabetic": /^[a-zA-Z\u0600-\u06FF]*$/,
        //old
//        "amount": /(^[0-9]*[1-9]+[0-9]*\.[0-9]*$)|(^[0-9]*\.[0-9]*[1-9]+[0-9]*$)|(^[0-9]*[1-9]+[0-9]*$)/,

        "amount": /^(([1-9][0-9]{0,3}(\.[0-9][0-9]?)?)|([1-2][0-9][0-9][0-9][0-9](\.[0-9][0-9]?)?|[1-3][0][0][0][0](\.[0][0]?)?))$/,
 
        //"amount": /^(([1-9][0-9]{0,3}(\.[0-9][0-9]?)?)|([1-2][0-9][0-9][0-9][0-9](\.[0-9][0-9]?)?|[1-3][0][0][0][0](\.[0][0]?)?))$/,
        "account": /^(([0-9][.]([0-9]|[0-9][.][0-9])+)|(((971[2|3|4|6|7|9]))[0-9]{7})|((9715)|(05))[0-9]{8})$/,
        "email": /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$|(^$)/,
       // "account": /^(([0-9][.]([0-9]|[0-9][.][0-9])+)|(((971[2|3|4|6|7|9]))[0-9]{7})|(((9715)|(05))[0-9]{8}))$/,

        "username": /^\S*$/,
        "password": /(?=.*[0-9])(?=.*[a-zA-Z])^([a-zA-Z0-9]){8,12}$/,
        "invoice": /^([0-9]{8,13})$/,
        "percent": /^(\d?[1-9]|[1-9]0)$/,
        "phoneNumber": /^((((971[2|3|4|6|7|9]))[0-9]{7})|(((9715))[0-9]{8}))$|(^$)/,
        "nickname": /^([a-zA-Z0-9]{2,12})$/,
        "mobile": /^(((9715)|(05))[0-9]{8})$/,
        "mobile-all-formats": /^(((\+9715)|(009715)|(9715)|(05))+([0-9]{8}))$/,
        "landline": /^(((971[2|3|4|6|7|9]))[0-9]{7})$/,
        "voucher": /^([0-9]{15})$/,
        "recharge" : /^([1-4][0-9][0-9]|(500)|[2-9][0-9])$/
    };

    function validator(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }


    $.extend(validator.prototype, {
        init: function () {
            var _this = this;
            //attaches itself on the form submit
            $(_this.element).on("submit", function () {
             
                var formID=$(this).attr('id'); 
                var foundError = false;
                $("#"+formID+" "+_this.settings.control).each(function () {
                    var $this = $(this);
                    if (!$this.is(":hidden") || !(!$this.closest(".selectricWrapper").length || $this.closest(".selectricWrapper").is(":hidden"))) {
                        if ($this.data("required") && $this.val() == "") {
                            _this.addError($this, $this.data("required-msg"));
                            foundError = true;
                        }
                        else {
                            $this.closest(_this.settings.group).removeClass("error").find(".help").remove();
                            switch ($this.data("valid")) {
                                default:
                                    if (!$this.val() == "" && !($this.val().trim()).match(validators[$this.data("valid")])) {
                                        // credit card with spaces in between
                                        if ($this.is('.txt-CC') && ($this.val().replace(/\s+/g,'')).match(validators[$this.data("valid")])) {
                                            break;
                                        } else {
                                            _this.addError($this, $this.data("valid-msg"));
                                            foundError = true;
                                        }
                                    }
                                    break;
                                case "match":
                                    if ($this.val() != $($this.data("match-with")).val()) {
                                        _this.addError($this, $this.data("valid-msg"));
                                        foundError = true;
                                    }
                                    break;
                            }
                        }
                        if($('.usage-alert').length && $('.usage-alert input.control:eq(0)').val() == $('.usage-alert input.control:eq(1)').val()&&$this.attr('id')!="selectMsisdn"){
                            _this.addError($this, $this.data("unique-msg"));
                            foundError = true;
                        }
                    }
                });
                if (foundError) {
                    // jsc: added invalid form handler
                    if (_this.settings.onInvalidForm) {
                        _this.settings.onInvalidForm();
                    }
                    return false;
                }
            });
        },
        //removes the error handler
        removeError: function ($this) {
            $this.closest(this.settings.group).removeClass("error").find(".help").remove();
        },
        //adds the error handler
        addError: function ($this, msg) {
            var _this = this;
            $this.closest(this.settings.group).addClass("error").find(".help").remove();
            //support for Selectric
            if ($this.closest(".selectricWrapper").not(".inline .selectricWrapper").length) {
                $this.closest(".selectricWrapper").after(this.settings.errorTemplate.replace("{{msg}}", msg));
            } else if ($this.closest(".inline .selectricWrapper").length) {
                $this.closest(".selectricWrapper").parent().append(this.settings.errorTemplate.replace("{{msg}}", msg));
            } else {
                $this.parent().append(this.settings.errorTemplate.replace("{{msg}}", msg));
            }
            $this.one("change", function () { _this.removeError($(this)); });
        }
    });

    $.fn[pluginName] = function (options) {
        this.each(function () {
            if (!$.data(this, "validator_" + pluginName)) {
                $.data(this, "validator_" + pluginName, new validator(this, options));
            }
        });
        return this;
    };

})(jQuery, window, document);