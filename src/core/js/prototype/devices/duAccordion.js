﻿'use strict';

$(function () {
    require(['jquery'], function () {
        var closeAllAccordions = function closeAllAccordions(accordionWidget) {
            accordionWidget.find(".block.open").removeClass("open");
        };

        // Unbind previous attached click events to avoid conflicts when this js file is included more than once
     //   $('.device-details .du-accordion .block-header').off("click");

        $('.device-details .du-accordion .block-header').click(function () {
        
            var accordionWidget = $(this).closest(".device-details .du-accordion");
            var accordionBlock = $(this).closest(".device-details .block");
            var accordionBlockWasOpen = accordionBlock.hasClass("open");

          //  closeAllAccordions(accordionWidget);

            if (!accordionBlockWasOpen) {
             //   accordionBlock.addClass("open");

                var dataLayerTag = $(this).attr('data-datalayer-opened');
                if ($.trim(dataLayerTag).length > 0) {
                    dataLayerTag = dataLayerTag.replace(/'/g, "\"");
                    try {
                        var dataLayerTagValue = JSON.parse(dataLayerTag);
                        dataLayer.push(dataLayerTagValue);
                    } catch (ex) {
                        console.log('Could not parse datalayer from attributes');
                    }
                }

                var blockVerticalPosition = accordionBlock.position().top;
                var menuHeight = $('#topBar').outerHeight();
                var scrollTarget = blockVerticalPosition - menuHeight;
                var currentScroll = $('body').scrollTop();

                $('html, body').stop(true).animate({
                    scrollTop: scrollTarget
                }, 300);
            } else {
                var dataLayerTag = $(this).attr('data-datalayer-closed');
                if ($.trim(dataLayerTag).length > 0) {
                    dataLayerTag = dataLayerTag.replace(/'/g, "\"");
                    try {
                        var dataLayerTagValue = JSON.parse(dataLayerTag);
                        dataLayer.push(dataLayerTagValue);
                    } catch (ex) {
                        console.log('Could not parse datalayer from attributes');
                    }
                }
            }
        });
    });
});

