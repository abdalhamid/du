'use strict';

$(function () {

    if (window.PlanCardsController) {
        return;
    }

    var PlanCardsController = window.PlanCardsController = function (container, Isotope, noUiSlider) {

        this.Isotope = Isotope;
        this.noUiSlider = noUiSlider;
        this.container = $(container);
        this.planCards = null;
        this.isotopeContainer = null;
        this.debounceResize = false;
        this.onFilteredHandlers = [];
        this.onPricesRescaledHandlers = [];
        this.onSelectionChangedHandlers = [];
        this.filters = [];
        this.isAr = false;
        this.isotope = null;
        this.isInitialized = false;

        this.priceSliderFormat = null;

        this.filtersContainer = null;
        this.filterStatusContainer = null;
        this.navigationHeaderContainer = null;
        this.filterStatusAppearanceMargin = 0;

        this.staticScrollMeasurements = 10000;
        this.staticScrollMeasurementDebounceId = null;
    };

    PlanCardsController.prototype.initialize = function () {

        this.container.data('planCardsController', this);
        this.planCards = $(this.container).find('.plan-cards');
        this.isotopeContainer = $(this.container).find('.plansSelection-cardItems');

        this.isAr = $('html').attr('dir') == 'rtl';

        this.filtersContainer = $('.plansSelection-filters');
        this.filterStatusContainer = $('.plansSelection-filterStatus');
        this.navigationHeaderContainer = $('#topBar');

        this.priceSliderFormat = {
            to: function to(value) {
                var isAr = $('html').attr('dir') == 'rtl';
                var prefix = isAr ? 'درهم ' : 'AED ';

                return prefix + value;
            },
            from: function from(value) {
                var numericValue = parseInt(value.match(/[0-9,.]+/)[0]);
                return numericValue;
            }
        };

        this.initializeControls();

        this.isInitialized = true;
    };

    PlanCardsController.prototype.initializeControls = function () {

        var _this = this;

        //Store controller to each card's data for faster retrieval during filtering
        this.isotopeContainer.find('.plansSelection-cardItem').data('controller', this);

        //Highlight adjacent features when hovered
        this.container.on('mouseover', '.plansSelection-featureHighlight, .plansSelection-feature', { controller: this }, function (e) {
            var controller = e.data.controller;
            var element = $(this);
            var sectionIndex = element.index() + 1;

            var adjacentSections = controller.isotopeContainer.find('.plansSelection-cardContent:visible > :nth-child(' + sectionIndex + ')');
            adjacentSections.addClass('hover');
        });

        this.container.on('mouseout', '.plansSelection-featureHighlight, .plansSelection-feature', { controller: this }, function (e) {
            var controller = e.data.controller;
            var element = $(this);
            var sectionIndex = element.index() + 1;

            var adjacentSections = controller.isotopeContainer.find('.plansSelection-cardContent:visible > :nth-child(' + sectionIndex + ')');
            adjacentSections.removeClass('hover');
        });

        //Handle events for plan selection
        this.container.on('click', '.plansSelection-cardItem .plansSelection-buyPlanButton', { controller: this }, function (e) {

            var controller = e.data.controller;
            var element = $(this);
            var cardItem = element.closest('.plansSelection-cardItem');

            controller.isotopeContainer.find('.plansSelection-cardItem').removeClass('active');
            cardItem.addClass('active');

            controller.container.find('.plansSelection-filterStatus').addClass('mobileHidden');
            $('.orderSummaryStatus').addClass('mobileVisible');
            $('#PublicWrapper #MainWrapper').addClass('orderSummaryStatus-open');

            _this.onSelectionChangedHandlers.forEach(function (handler) {
                if (typeof handler == 'function') {
                    handler(element);
                }
            });
        });

        this.container.on('click', '.plansSelection-cardItem .plansSelection-deselectPlanButton', { controller: this }, function (e) {

            var controller = e.data.controller;
            var element = $(this);
            var cardItem = element.closest('.plansSelection-cardItem');

            cardItem.removeClass('active');

            controller.container.find('.plansSelection-filterStatus').removeClass('mobileHidden');
            $('.orderSummaryStatus').removeClass('mobileVisible');
            $('#PublicWrapper #MainWrapper').removeClass('orderSummaryStatus-open');

            _this.onSelectionChangedHandlers.forEach(function (handler) {
                if (typeof handler == 'function') {
                    handler(element);
                }
            });
        });

        //Scroll up to filters when filter summary is clicked
        this.container.on('click', '.plansSelection-filterStatus', function (e) {

            _this.scrollToFilters();
        });

        //Handle click events for GTM in generic plan cards
        $('.orderSummaryStatus-orderButton').on('click', function () {

            var dataLayerTag = $(this).attr('data-datalayer');
            var monthlyPrice = $('.orderSummaryStatus-monthlyPriceValue').attr('data-value');

            if ($.trim(dataLayerTag).length > 0) {
                dataLayerTag = dataLayerTag.replace(/'/g, "\"");
                dataLayerTag = dataLayerTag.replace(/\[price]/g, monthlyPrice);
                try {
                    var dataLayerTagValue = JSON.parse(dataLayerTag);
                    dataLayer.push(dataLayerTagValue);
                } catch (ex) {
                    console.log('Could not parse datalayer from attributes');
                }
            }
        });

        this.initializeIsotope();
        this.initializeFilterStatusVisibilityBehavior();
        this.initializeFilterControls();
        this.initializeDefaultFilters();
        this.initializePopup();

        //Trigger click event of currently selected plan
        this.container.find('.plansSelection-cardItem.active .plansSelection-buyPlanButton').trigger('click');
    };

    PlanCardsController.prototype.initializeIsotope = function () {

        var _this = this;
        var Isotope = this.Isotope;

        var orderAttribute = $.trim(this.isotopeContainer.attr('data-order-attribute'));
        var orderDirection = $.trim(this.isotopeContainer.attr('data-order-direction'));


        this.isotope = new Isotope(this.isotopeContainer[0], {
            isOriginLeft: !this.isAr,
            itemSelector: '.plansSelection-cardItem',
            layoutMode: 'fitRows',
            getSortData: {
                order: '[' + orderAttribute + '] parseInt'
            },
            sortBy: 'order',
            sortAscending: orderDirection == 'desc' ? false : true
        });

        this.isotope.on('arrangeComplete', function (filteredItems) {

            if (filteredItems.length == 0) {
                _this.isotopeContainer.find('.plansSelection-emptyResultSet').show();
            } else {
                _this.isotopeContainer.find('.plansSelection-emptyResultSet').hide();
            }
        });

        this.isotope.on('layoutComplete', function (filteredItems) {

            if (filteredItems.length == 0) {
                return;
            }

            var items = filteredItems.map(function (x) {
                return x.element;
            });
            _this.matchCardSectionHeights(items);
        });
    };

    PlanCardsController.prototype.initializeFilterStatusVisibilityBehavior = function () {

        var _this = this;

        //Refresh static measurements on resize and orientation changed to prevent expensive operations on the setInterval() function below
        $(window).on('resize, orientationchanged', function () {
            if (!this.staticScrollMeasurementDebounceId) {
                return;
            }

            this.staticScrollMeasurementDebounceId = setTimeout(function () {

                this.staticScrollMeasurements = _this.filtersContainer.offset().top + _this.filtersContainer.outerHeight() - _this.filterStatusContainer.outerHeight() - _this.navigationHeaderContainer.outerHeight() - _this.filterStatusAppearanceMargin;
                this.staticScrollMeasurementDebounceId = null;
            }, 1000);
        });

        this.staticScrollMeasurements = this.filtersContainer.offset().top + this.filtersContainer.outerHeight() - this.filterStatusContainer.outerHeight() - this.navigationHeaderContainer.outerHeight() - this.filterStatusAppearanceMargin;

        //Periodically check if the user has scrolled below the filter area. setInterval will be faster and more cross-platform than listening to scroll event
        setInterval(function (context) {
            var scroll = context.staticScrollMeasurements - $(window).scrollTop();
            if (scroll > 0) {
                context.filterStatusContainer.removeClass('show');
                return;
            }

            context.filterStatusContainer.addClass('show');
        }, 500, this);
    };

    PlanCardsController.prototype.matchCardSectionHeights = function (filteredItems) {

        var items = $(filteredItems);

        var matchHeight = function matchHeight(selectedElements) {
            if (selectedElements.length == 0) return;
            var elements = selectedElements;
            var largest = 0;

            var i = elements.length;
            while (i--) {
                elements[i].style.height = "auto";

                var height = elements[i].offsetHeight;
                if (height > largest) {

                    largest = height;
                }
            }
            var j = elements.length;
            while (j--) {
                elements[j].style.height = largest + "px";
            }
        };

        var index = 0;
        while (true) {
            index++;

            var cardSections = items.find('.plansSelection-cardContent > :nth-child(' + index + ')');
            if (cardSections.length <= 0) {
                break;
            }

            var allSectionsEmpty = true;
            cardSections.each(function () {
                allSectionsEmpty = allSectionsEmpty && $(this).hasClass('empty');
            });

            if (allSectionsEmpty) {
                cardSections.addClass('hide');
            } else {
                cardSections.removeClass('hide');
                matchHeight(cardSections);
            }
        }
    };

    PlanCardsController.prototype.performPriceSliderRescaleAfterFilter = function () {

        this.isotope.once('arrangeComplete', function (filteredItems) {
            var items = filteredItems.map(function (x) {
                return x.element;
            });

            var controller = $(this.element).closest('.plansSelection').data('planCardsController');
            controller.rescalePriceSliders(items);
        });
    };

    PlanCardsController.prototype.initializeFilterControls = function () {

        var _this = this;

        this.container.find('.plansSelection-radioButtons input[type="radio"]').on('change', function () {

            var element = $(this);
            if (element.prop('checked') == false) {
                return;
            }

            var name = $.trim(element.attr('name'));
            var value = $.trim(element.val());
            var filter = '.' + name + value;

            _this.container.find('.plansSelection-priceFilter input[type="text"]').val('');

            _this.clearFilters('.' + name);
            _this.filters.push(filter);
            _this.filter();
            _this.performPriceSliderRescaleAfterFilter();
            _this.scrollToFilters();
        });

        this.container.find('.plansSelection-sliderFilter input[type="text"]').on('change', function () {

            var element = $(this);
            var name = $.trim(element.attr('name'));
            var value = $.trim(element.val());
            var filter = '.' + name + value;

            _this.container.find('.plansSelection-priceFilter input[type="text"]').val('');

            _this.clearFilters('.' + name);
            if (value.length > 0) {
                _this.filters.push(filter);
            }

            _this.filter();
            _this.performPriceSliderRescaleAfterFilter();
            _this.scrollToFilters();
        });

        var priceFilter = function priceFilter(item) {

            var element = $(item);
            var controller = element.data('controller');
            var isValid = true;

            controller.filtersContainer.find('.plansSelection-priceFilter input[type="text"]').each(function () {
                if (isValid == false) {
                    return;
                }

                var filterElement = $(this);
                var filterName = $.trim(filterElement.attr('name'));
                var filterValue = $.trim(filterElement.val());

                if (filterName.length == 0 || filterValue.length == 0) {
                    return;
                }

                var elementValue = $.trim(element.attr('data-' + filterName + '-filterValue'));
                if (elementValue.length == 0) {
                    return;
                }

                if (parseInt(elementValue) > parseInt(filterValue)) {
                    isValid = false;
                }
            });

            return isValid;
        };
        _this.filters.push(priceFilter);

        this.container.find('.plansSelection-priceFilter input[type="text"]').on('change', function (e) {

            if (_this.isInitialized) {
                var name = $(this).attr('name');
                var value = $(this).val();
                var dataLayerTag = $(this).attr('data-datalayer');

                if ($.trim(dataLayerTag).length > 0) {
                    dataLayerTag = dataLayerTag.replace(/'/g, "\"");
                    dataLayerTag = dataLayerTag.replace(new RegExp('\\[' + name + ']', 'g'), value);
                    try {
                        var dataLayerTagValue = JSON.parse(dataLayerTag);
                        dataLayer.push(dataLayerTagValue);
                    } catch (ex) {
                        console.log('Could not parse datalayer from attributes');
                    }
                }
            }

            _this.filter();
            _this.scrollToFilters();
        });

        this.initializeSliders();
    };

    PlanCardsController.prototype.initializeSliders = function () {

        var _this = this;
        var noUiSlider = this.noUiSlider;

        this.container.find('.plansSelection-sliderFilter .plansSelection-slider, .plansSelection-priceFilter .plansSelection-slider').each(function () {

            var element = this;

            var options = {
                start: [1000],
                snap: true,
                direction: _this.isAr ? 'rtl' : 'ltr',
                animate: true,
                range: {
                    'min': [150],
                    '33.33%': [300],
                    '66.66%': [450],
                    'max': [1000]
                },
                pips: {
                    mode: 'steps',
                    density: 1000,
                    format: _this.priceSliderFormat
                }
            };

            if ($(element).closest('.plansSelection-priceFilter').length > 0) {
                options.connect = 'lower';
            }

            noUiSlider.create(element, options);

            function onValueChanged() {
                var slider = this;
                var target = $(slider.target);

                var isUpdating = target.data('isUpdating');
                if (isUpdating) {
                    target.data('isUpdating', false);
                    return;
                }

                var range = getOrderedRange(slider.options.range);
                var value = parseInt(slider.get());
                var index = range.indexOf(value);

                if (index >= 0) {
                    var valueMarkers = $(this.target).find('.noUi-value');
                    valueMarkers.removeClass('active');
                    valueMarkers.eq(index).addClass('active');
                }

                $(this.target).closest('.plansSelection-sliderFilter, .plansSelection-priceFilter').find('input[type="text"]').val(value).trigger('change');
            }

            function getOrderedRange(range) {
                var values = [];
                var maxValue = null;
                for (var key in range) {
                    if (!range.hasOwnProperty(key)) {
                        continue;
                    }

                    var value = range[key][0];
                    if (key == 'min') {
                        values.unshift(value);
                    } else if (key == 'max') {
                        maxValue = value;
                    } else {
                        values.push(value);
                    }
                }

                values.push(maxValue);

                return values;
            }

            element.noUiSlider.on('update', onValueChanged);

            $(element).on('click', '.noUi-value', { element: element }, function (e) {
                var element = e.data.element;
                var text = $(this).text().match(/[0-9,.]+/)[0];
                var value = parseInt(text);

                element.noUiSlider.set(value);
            });
        });
    };

    PlanCardsController.prototype.initializeDefaultFilters = function () {

        var _this = this;

        this.container.find('.plansSelection-radioButtons input[type="radio"]:checked').each(function () {

            var element = $(this);
            var name = $.trim(element.attr('name'));
            var value = $.trim(element.val());
            var filter = '.' + name + value;

            _this.clearFilters('.' + name);
            _this.filters.push(filter);
        });
        this.container.find('.plansSelection-sliderFilter input[type="text"]').each(function () {

            var element = $(this);
            var name = $.trim(element.attr('name'));
            var value = $.trim(element.val());
            var filter = '.' + name + value;

            _this.clearFilters('.' + name);
            if (value.length > 0) {
                _this.filters.push(filter);
            }
        });

        this.container.find('.plansSelection-priceFilter input[type="text"]').val('');

        this.filter(this.filters);
        _this.performPriceSliderRescaleAfterFilter();
    };

    PlanCardsController.prototype.rescalePriceSliders = function (filteredItems) {

        var _this = this;
        var elements = $(filteredItems);

        this.container.find('.plansSelection-priceFilter').each(function () {

            var filterContainer = $(this);
            var slider = filterContainer.find('.plansSelection-slider');
            var filter = filterContainer.find('input[type="text"]');
            var filterName = filter.attr('name');

            var values = [];
            elements.each(function () {
                var value = $.trim($(this).attr('data-' + filterName + '-filterValue'));
                if (value.length == 0) {
                    return;
                }

                values.push(parseInt(value));
            });

            values = values.sort(function (a, b) {
                return a - b;
            });

            var range = {
                min: 0,
                max: 0
            };
            if (values.length == 1) {
                range.min = [values[0]];
                range.max = [values[0]];
            }
            values.forEach(function (item, index) {

                if (index == 0) {
                    range.min = [item];
                } else if (index == values.length - 1) {
                    range.max = [item];
                } else {
                    var position = index * (100 / (values.length - 1));
                    range[position + '%'] = [item];
                }
            });

            var options = {
                start: range.max[0],
                range: range
            };

            filterContainer.find('input[type="text"]').val(range.max[0]);
            slider.data('isUpdating', true);
            slider[0].noUiSlider.updateOptions(options, false);
            slider.find('.noUi-pips').remove();
            slider[0].noUiSlider.pips({
                mode: 'steps',
                density: range.max[0],
                format: _this.priceSliderFormat
            });
        });

        this.onPricesRescaledHandlers.forEach(function (handler) {
            handler();
        });
    };

    PlanCardsController.prototype.initializePopup = function () {

        var _this = this;
        var infoPopupCardItem = null;

        this.container.find('a[data-reveal-id="PlanDetailsPopup"]').on('click', function (e) {
            e.preventDefault();
            infoPopupCardItem = $(this).closest('.plansSelection-cardItem');
            var planInformationUrl = $(this).attr('href');
            var planInformationPrintUrl = planInformationUrl;
            var planInformationDocumentUrl = infoPopupCardItem.attr('data-documentUrl');
            var purchasePlanUrl = infoPopupCardItem.find('.plansSelection-buyPlanButton').attr('href');

            if (planInformationPrintUrl.indexOf('?') == -1) {
                planInformationPrintUrl += '?print=true';
            } else {
                planInformationPrintUrl += '&print=true';
            }

            var planDetailsPopup = $('#PlanDetailsPopup');

            planDetailsPopup.find('.planDetailsPopup-loadingOverlay').show();

            planDetailsPopup.find('.planDetailsPopup-frame').attr('src', planInformationUrl);
            planDetailsPopup.find('.planDetailsPopup-printButton').attr('href', planInformationPrintUrl).attr('target', '_blank');

            var planDetailsPopupBuyButton = planDetailsPopup.find('.planDetailsPopup-buyButton');
            planDetailsPopupBuyButton.attr('href', purchasePlanUrl);
            planDetailsPopupBuyButton.attr('data-id', infoPopupCardItem.attr('data-id'));

            planDetailsPopup.css('margin-left', -planDetailsPopup.outerWidth() / 2);

            var width = Math.round(planDetailsPopup.width());
            planDetailsPopup[0].style.setProperty('width', width + 'px', 'important');
            planDetailsPopup[0].style.setProperty('display','block', 'important');
        });

        $('#PlanDetailsPopup:first .planDetailsPopup-buyButton').on('click', function () {
            var id = $(this).attr('data-id');
            _this.isotopeContainer.find('.plansSelection-cardItem[data-id="' + id + '"] .plansSelection-buyPlanButton').trigger('click');

            $(this).removeAttr('data-id');
        });

        $('#PlanDetailsPopup:first .close-reveal-modal').on('click', function () {
            infoPopupCardItem = null;

            var planDetailsPopup = $('#PlanDetailsPopup');

            planDetailsPopup.find('.planDetailsPopup-frame').attr('src', 'about:blank');
            planDetailsPopup.find('.planDetailsPopup-printButton').attr('href', 'javascript:void(0);').attr('target', null);
            planDetailsPopup.find('.planDetailsPopup-buyButton').attr('href', 'javascript:void(0);');
            planDetailsPopup[0].style.removeProperty('width');
        });

        //Platform-specific implementation of print functionality
        $('#PlanDetailsPopup:first .printButton').on('click', function (e) {
            e.preventDefault();

            if ($('body.android').length > 0 || $('.firefox .desktop').length > 0) {
                var printUrl = $(this).attr('href');
                window.open(printUrl);
            } else {
                //document.PlanDetailsFrame.printDocument();
                $('#PlanDetailsFrame:visible')[0].contentWindow.printDocument();
            }
        });

        this.applyPopupFixes();
    };

    PlanCardsController.prototype.applyPopupFixes = function () {

        var recenterPopup = function recenterPopup(popupContainer) {
            var popup = $(popupContainer);
            if (popup.length == 0) {
                return;
            }

            popup.css("margin-left", -popup.outerWidth() / 2 + 'px');
            popup.each(function () {
                this.style.removeProperty('width');
            });

            if ($('body.mobile').length > 0) {
                var viewportFromTop = $(window).scrollTop();
                if (viewportFromTop === 0) {
                    popup.css("position", "fixed");

                    if ($(window).width() < 479) {
                        popup.css("top", "10px");
                    } else {
                        popup.css("top", "60px");
                    }
                } else if ($(window).offset()) {
                    popup.css("position", "absolute");
                    popup.css("top", $(window).offset().top + "px");
                }
            }
        };

        $(window).on('orientationchange', function () {
            var popup = $('#PlanDetailsPopup:first:visible');
            if (popup.length > 0) {
                recenterPopup(popup);
            }
        });
    };

    PlanCardsController.prototype.scrollToFilters = function () {

        if (!this.isInitialized) {
            return;
        }

        var filters = this.container.find('.plansSelection-filters');
        if (filters.length > 0) {
            var scrollPosition = filters.offset().top - $('#topBar').outerHeight();

            $('html, body').stop(true).animate({
                scrollTop: scrollPosition
            }, 300);
        }
    };

    PlanCardsController.prototype.generateFilterFunction = function (filter) {

        var filter = function filter() {
            var element = $(this);
            var controller = element.data('controller');
            var filters = controller.filters;

            var stringFilters = [];
            var methodFilters = [];

            filters.forEach(function (item) {
                if (typeof item == 'string') {
                    stringFilters.push(item);
                } else if (typeof item == 'function') {
                    methodFilters.push(item);
                } else {
                    throw 'Unsupported filter type';
                }
            });

            var isValid = element.is(stringFilters.join(''));
            if (!isValid) {
                return false;
            }

            methodFilters.forEach(function (method) {
                if (!isValid) {
                    return;
                }

                isValid = method(element);
            });

            return isValid;
        };

        return filter;
    };

    PlanCardsController.prototype.filter = function (filters) {

        var filtersValue = filters || this.filters;
        this.filters = filtersValue;

        var filter = this.generateFilterFunction();

        this.isotope.arrange({ filter: filter });

        //if (navigator.userAgent.indexOf('iPad') >= 0) {
        //    var filteredItems = this.isotopeContainer.find('.plansSelection-cardItem:visible');
        //    this.matchCardSectionHeights(filteredItems);
        //}

        this.onFilteredHandlers.forEach(function (handler) {
            handler();
        });
    };

    PlanCardsController.prototype.clearFilters = function (prefix) {

        var prefixValue = $.trim(prefix);

        if (prefixValue.length == 0) {
            this.filters = [];
            return;
        }

        var filters = this.filters;
        var newFilters = [];

        filters.forEach(function (item) {
            if (typeof item == 'function') {
                newFilters.push(item);
                return;
            }

            var itemValue = $.trim(item);
            if (!itemValue.startsWith(prefixValue)) {
                newFilters.push(itemValue);
            }
        });

        this.filters = newFilters;
    };

    PlanCardsController.prototype.onSelectionChanged = function (handler) {

        if (typeof handler == 'function') {
            this.onSelectionChangedHandlers.push(handler);
        }
    };

    PlanCardsController.prototype.onFiltered = function (handler) {

        if (typeof handler == 'function') {
            this.onFilteredHandlers.push(handler);
        }
    };

    PlanCardsController.prototype.onPricesRescaled = function (handler) {

        if (typeof handler == 'function') {
            this.onPricesRescaledHandlers.push(handler);
        }
    };
});

window.printPopupCallback = function (window) {
    setTimeout(function () {
        window.close();
    }, 250);
};

window.frameReadyCallback = function (window) {
    $('#PlanDetailsPopup .planDetailsPopup-loadingOverlay').hide();
};
