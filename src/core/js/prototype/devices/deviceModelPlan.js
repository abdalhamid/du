///<reference path="planCards.js" />

'use strict';

$(function () {

    if (window.DeviceModelPlansController) {
        return;
    }

    var DeviceModelPlansController = window.DeviceModelPlansController = function (container, Isotope, noUiSlider) {

        this.Isotope = Isotope;
        this.noUiSlider = noUiSlider;

        this.container = $(container);
        this.deviceModelId = null;
        this.deviceColorVariantId = null;
        this.data = null;
        this.planCardsController = null;
    };

    DeviceModelPlansController.prototype.initialize = function () {

        this.container.data('deviceModelPlansController', this);
        this.deviceModelId = this.container.attr('data-device-model-id');
        this.deviceColorVariantId = this.container.attr('data-device-color-variant-id');

        this.data = window.deviceModelPlans[this.deviceModelId];

        this.initializeControls();
    };

    DeviceModelPlansController.prototype.initializeControls = function () {
        var Isotope = this.Isotope;
        var noUiSlider = this.noUiSlider;
        var _this = this;

        var parameters = Utilities.getUriParameters();
        var contractPeriod = $.trim(parameters.contractPeriod);
        var callingMinutes = $.trim(parameters.callingMinutes);

        if (contractPeriod.length > 0) {
            $('.deviceModelPlans-contractPeriodFilter input[name="contractPeriod"]').removeAttr('checked');
            $('.deviceModelPlans-contractPeriodFilter input[name="contractPeriod"][value="' + contractPeriod + '"]').attr('checked', 'checked');
        }
        if (callingMinutes.length > 0) {
            $('.deviceModelPlans-callingMinutesFilter input[name="callingMinutes"]').removeAttr('checked');
            $('.deviceModelPlans-callingMinutesFilter input[name="callingMinutes"][value="' + callingMinutes + '"]').attr('checked', 'checked');
        }

        $('.orderSummaryStatus-orderButton').attr('data-default-href', $('.orderSummaryStatus-orderButton').attr('href'));
        $('.deviceModelPlans-newCustomerButton').attr('data-default-href', $('.deviceModelPlans-newCustomerButton').attr('href'));
        $('.deviceModelPlans-migratingCustomerButton').attr('data-default-href', $('.deviceModelPlans-migratingCustomerButton').attr('href'));
        $('.deviceModelPlans-existingCustomerButton').attr('data-default-href', $('.deviceModelPlans-existingCustomerButton').attr('href'));

        $('.orderSummaryStatus-orderButton').on('click', function () {
            $('#CustomerTypeConfirmationPopup').addClass('show');
        });
        $('#CustomerTypeConfirmationPopup + .popupSlider-loadingOverlay').on('click', function () {
            $(this).prev().removeClass('show');
        });
        $('.popupSlider').on('click', '.popupSlider-closeButton', function () {
            $(this).closest('.popupSlider').removeClass('show');
        });

        //Handle device-specific GTM tags
        $('.du-accordion .faqs-block').on('click', function () {
            dataLayer.push({ 'event': 'click', 'category': 'phones_and_tablets', 'action': _this.data.Model.DeviceType.Title, 'label': 'info_faqs' });
        });

        $('.du-accordion .terms-and-conditions-block').on('click', function () {
            dataLayer.push({ 'event': 'click', 'category': 'phones_and_tablets', 'action': _this.data.Model.DeviceType.Title, 'label': 'info_termsandconditions' });
        });

        if (this.data && this.data.Model && this.data.Model.DeviceType) {
            var deviceTitle = $.trim(this.data.Model.DeviceType.Title);
            var deviceTypeFilter = '.deviceType-' + deviceTitle.toLowerCase().replace(/\s/g, '_');
            var deviceTypeElements = $(deviceTypeFilter);

            if (deviceTypeElements.length > 0) {
                $('.deviceType-default').hide();
                deviceTypeElements.show();
            }
        }

        this.planCardsController = new PlanCardsController(this.container, Isotope, noUiSlider);

        this.planCardsController.onFiltered(function () {
            _this.updateFilterStatusValues();
            _this.updateOrderStatusValues();
        });
        this.planCardsController.onPricesRescaled(function () {
            _this.updateFilterStatusValues();
            _this.updateOrderStatusValues();
        });
        this.planCardsController.onSelectionChanged(function () {
            _this.updateOrderStatusValues();
        });

        this.planCardsController.initialize();
        this.updateFilterStatusValues();
        this.updateOrderStatusValues();
    };

    DeviceModelPlansController.prototype.updateOrderStatusValues = function () {

        var parameters = Utilities.getUriParameters();
        var deviceModelId = $.trim(parameters.deviceModelId);
        var deviceColorVariantId = $.trim(parameters.deviceColorVariantId);
        var section = $.trim(parameters.section);
        var deviceUrl = $.trim(parameters.deviceUrl);
        var selectedCard = this.container.find('.plansSelection-cardItem.active:first');

        var contractPeriod = $.trim(this.container.find('.deviceModelPlans-contractPeriodFilter input[name="contractPeriod"]:checked').val());
        var callingMinutes = $.trim(this.container.find('.deviceModelPlans-callingMinutesFilter input[name="callingMinutes"]:checked').val());
        var deviceToPlanId = $.trim(selectedCard.attr('data-device-to-plan-id'));

        //Create URL parameters for the modify device button
        var deviceDetailsParameters = {
            deviceModelId: deviceModelId,
            deviceColorVariantId: deviceColorVariantId,
            contractPeriod: contractPeriod,
            callingMinutes: callingMinutes
        };

        if (deviceToPlanId.length > 0) {
            deviceDetailsParameters.deviceToPlanId = deviceToPlanId;
        }

        var deviceUrlParameters = Utilities.getUriParameters(deviceUrl);
        var newParameters = $.extend({}, deviceUrlParameters, deviceDetailsParameters);
        delete newParameters.deviceUrl;
        delete newParameters.section;

        if (selectedCard.length == 0) {
            delete newParameters.deviceToPlanId;
        }

        //Set URL of modify device button
        var anchor = $('<a href="' + deviceUrl + '"></a>');
        var baseUrl = deviceUrl.replace(anchor[0].search, '');
        var modifyDeviceButton = $('.orderSummaryStatus-deviceContainer > a');
        var url = Utilities.appendParametersToUrl(baseUrl, newParameters);
        modifyDeviceButton.attr('href', url);

        if (selectedCard.length > 0) {
            var monthlyFee = parseInt(selectedCard.attr('data-monthly-fee'));
            var monthlyFeeStatus = $('.orderSummaryStatus-monthlyPriceValue');
            monthlyFeeStatus.attr('data-value', monthlyFee);
            monthlyFeeStatus.text(monthlyFee);

            var upfrontPrice = parseInt(selectedCard.attr('data-upfront-price'));
            var upfrontPriceStatus = $('.orderSummaryStatus-upfrontPriceValue');
            upfrontPriceStatus.attr('data-value', upfrontPrice);
            upfrontPriceStatus.text(upfrontPrice);

            $('.orderSummaryStatus-planContainer').removeClass('active').addClass('checked');
        } else {
            var monthlyFeeStatus = $('.orderSummaryStatus-monthlyPriceValue');
            monthlyFeeStatus.attr('data-value', 0);
            monthlyFeeStatus.text('-');

            var upfrontPriceStatus = $('.orderSummaryStatus-upfrontPriceValue');
            upfrontPriceStatus.attr('data-value', 0);
            upfrontPriceStatus.text(0);

            $('.orderSummaryStatus-planContainer').removeClass('checked').addClass('active');
        }

        if (deviceToPlanId.length > 0 && deviceColorVariantId.length > 0) {
            $('.orderSummaryStatus-orderButton').removeClass('disabled');

            var orderParameters = $.extend({}, deviceUrlParameters, deviceDetailsParameters);
            $('.deviceModelPlans-newCustomerButton').attr('href', Utilities.appendParametersToUrl($('.deviceModelPlans-newCustomerButton').attr('data-default-href'), orderParameters));
            $('.deviceModelPlans-migratingCustomerButton').attr('href', Utilities.appendParametersToUrl($('.deviceModelPlans-migratingCustomerButton').attr('data-default-href'), orderParameters));
            $('.deviceModelPlans-existingCustomerButton').attr('href', Utilities.appendParametersToUrl($('.deviceModelPlans-existingCustomerButton').attr('data-default-href'), orderParameters));
        } else {
            $('.orderSummaryStatus-orderButton').addClass('disabled');
        }
    };

    DeviceModelPlansController.prototype.updateFilterStatusValues = function () {

        var contractPeriod = $('.deviceModelPlans-contractPeriodFilter input[name="contractPeriod"]:checked').val();
        var callingMinutes = $('.deviceModelPlans-callingMinutesFilter input[name="callingMinutes"]:checked').attr('data-localized-value');
        var price = $('.deviceModelPlans-monthlyFeeFilter input[name="price"]').val();

        $('.deviceModelPlans-contractPeriodFilterStatusValue').text(contractPeriod);
        $('.deviceModelPlans-callingMinutesFilterStatusValue').text(callingMinutes);
        $('.deviceModelPlans-monthlyFeeFilterStatusValue').text(price);
    };

    require(['require', 'isotope', 'nouislider.min', 'jquery.sticky-kit.min', 'bootstrap'], function (require, Isotope, noUiSlider) {
        $('.deviceModelPlans').each(function () {
            var controller = new DeviceModelPlansController(this, Isotope, noUiSlider);
            controller.initialize();
        });
    });
});

