﻿"use strict";

$(function () {
    require(['bootstrap'], function (bootstrap) {

        /*
        var anchors = [];
         $('.plan-information h4[id]').each(function () {
             var id = $(this).attr('id');
            var title = $.trim($(this).text());
             anchors.push({ id: id, title: title });
        });
         var navHtml = [];
         navHtml.push('<ul class="nav navbar-nav" role="tablist">');
         for (var i = 0; i < anchors.length; i++) {
             var anchor = anchors[i];
            navHtml.push('<li><a href="#' + anchor.id + '">' + anchor.title + '</a></li>');
        }
         navHtml.push('</ul>');
         $('#ScrollNavContainer').addClass('navbar navbar-default');
        $('#ScrollNavContainer').html(navHtml.join(''));
         if ($('#ScrollNavContainer').is(':visible')) {
            $('#MainWrapper').css('padding-top', $('#ScrollNavContainer').outerHeight() + 'px');
        }
         $('#ScrollNavContainer a, .headered-content-layout a[href^=#]').on('click', function () {
             if ($('.iphone, .ipad').length <= 0) {
                var scrollPosition = $('.plan-information').find($(this).attr('href')).offset().top - (64 - 1);
                 $('body,html').animate({
                    scrollTop: scrollPosition
                }, 500, function () {
                    $(".btn-navbar").click();
                });
                return false;
            }
        });
         $('body').css('position', 'relative');
        $('body').scrollspy({ target: '#ScrollNavContainer', offset: 75 });
        */
    });
});

function printDocument() {

    window.focus();
    window.print();

    return false;
};

$(function () {

    if (/[?&]print=true/.test(document.location.search)) {
        printDocument();

        if (window.opener && typeof window.opener.printPopupCallback == 'function') {
            window.opener.printPopupCallback(window);
        }
    }

    if (window.opener && typeof window.opener.popupReadyCallback == 'function') {
        window.opener.popupReadyCallback(window);
    }

    if (window.parent && typeof window.parent.frameReadyCallback == 'function') {
        window.parent.frameReadyCallback(window);
    }
});

