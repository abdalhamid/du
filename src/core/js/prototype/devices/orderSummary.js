﻿'use strict';

$(function () {

    function initialize() {

        var orderSummary = $('.orderSummary');
        if (!orderSummary || orderSummary.length === 0) {
            return;
        }

        initializeControls();
        updateTotals();
    }

    function initializeControls() {
        $('.orderSummaryStatus-orderButton').attr('data-default-href', $('.orderSummaryStatus-orderButton').attr('href'));

        var parameters = Utilities.getUriParameters();

        if ($.trim(parameters.customerType).toLowerCase() == 'existing') {
            $('.orderSummary-oneTimeActivationFee').hide();
        }
    }

    function updateTotals() {

        var orderSummary = $('.orderSummary');

        var totalUpfrontPrice = 0;
        var totalOneTimePrice = 0;
        var totalMonthlyPrice = 0;

        orderSummary.find('[data-upfront-price]:visible').each(function () {
            var upfrontPrice = parseInt($.trim($(this).attr('data-upfront-price')));
            if (!isNaN(upfrontPrice)) {
                totalUpfrontPrice += upfrontPrice;
            }
        });

        orderSummary.find('[data-one-time-price]:visible').each(function () {
            var oneTimePrice = parseInt($.trim($(this).attr('data-one-time-price')));
            if (!isNaN(oneTimePrice)) {
                totalOneTimePrice += oneTimePrice;
            }
        });

        orderSummary.find('[data-monthly-price]:visible').each(function () {
            var monthlyPrice = parseInt($.trim($(this).attr('data-monthly-price')));
            if (!isNaN(monthlyPrice)) {
                totalMonthlyPrice += monthlyPrice;
            }
        });

        $('.orderSummaryStatus-upfrontPriceValue').attr('data-total-upfront-price', totalUpfrontPrice).text(totalUpfrontPrice);
        $('.orderSummaryStatus-oneTimePriceValue').attr('data-total-one-time-price', totalOneTimePrice).text(totalOneTimePrice);
        $('.orderSummaryStatus-monthlyPriceValue').attr('data-total-monthly-price', totalMonthlyPrice).text(totalMonthlyPrice);

        updateOrderButtonUrl();
    }

    function updateOrderButtonUrl() {

        var referenceUrl = $('.orderSummaryStatus-orderButton').attr('data-default-href');

        var parameters = Utilities.getUriParameters();
        var orderParameters = {
            customerType: parameters.customerType,
            deviceToPlanId: parameters.deviceToPlanId,
            deviceColorVariantId: parameters.deviceColorVariantId
        };

        if (orderSummaryData) {
            var title = [];

            if (orderSummaryData.DeviceToPlan && orderSummaryData.DeviceToPlan.RatePlan && $.trim(orderSummaryData.DeviceToPlan.RatePlan.Heading).length > 0) {
                title.push($.trim(orderSummaryData.DeviceToPlan.RatePlan.Heading));
            }

            if (orderSummaryData.DeviceType && $.trim(orderSummaryData.DeviceType.Title).length > 0) {
                title.push($.trim(orderSummaryData.DeviceType.Title));
            }

            orderParameters.title = title.join(' + ');
        }

        var url = Utilities.appendParametersToUrl(referenceUrl, orderParameters);
        $('.orderSummaryStatus-orderButton').attr('href', url);
    }

    require(['bootstrap'], function () {
        initialize();
    });
});