﻿'use strict';

$(function () {

    if (window.DeviceDetailsController) {
        return;
    }

    var DeviceDetailsController = window.DeviceDetailsController = function (container) {

        this.container = $(container);
        this.defaultStockQuantityThreshold = 20;
        this.deviceTypeId = null;
        this.data = null;
        this.enableBackToTopPopup = true;
        this.sectionScrollHandle = null;

        this.selectedRatePlanId = null;
    };

    DeviceDetailsController.prototype.initialize = function () {

        this.container.data('deviceDetailsController', this);

        var defaultStockQuantityThreshold = parseInt(this.container.attr('data-default-stock-quantity-threshold'));
        if (!isNaN(defaultStockQuantityThreshold)) {
            this.defaultStockQuantityThreshold = parseInt(defaultStockQuantityThreshold);
        }

        this.deviceTypeId = this.container.attr('data-device-type-id');
        this.data = window.deviceDetails[this.deviceTypeId];

        this.initializeControls();
    };

    DeviceDetailsController.prototype.initializeControls = function () {

        var orderButton = $('.orderSummaryStatus-orderButton');
        orderButton.attr('data-default-href', orderButton.attr('href'));

        this.container.on('click', '.deviceDetails-colorsList > li > a', function () {
            var controller = $(this).closest('.deviceDetails').data('deviceDetailsController');
            controller.deviceColorVariant_click(this);
        });

        this.container.on('click', '.deviceDetails-modelsList > li > a', function () {
            var controller = $(this).closest('.deviceDetails').data('deviceDetailsController');
            controller.deviceModel_click(this);
        });

        var parameters = Utilities.getUriParameters();
        var deviceToPlanId = $.trim(parameters.deviceToPlanId);
        if (deviceToPlanId.length > 0) {
            $('.orderSummaryStatus-planContainer').removeClass('disabled').addClass('checked');
        }

        //Initialize state
        this.container.find('.deviceDetails-modelsList > li.active > a').trigger('click');

        //Set initial order summary values
        var defaultDeviceModelId = $.trim(parameters.deviceModelId);
        var deviceToPlanId = $.trim(parameters.deviceToPlanId);
        if (defaultDeviceModelId.length > 0 && deviceToPlanId.length > 0) {

            var defaultDeviceModels = this.data.DeviceModels.filter(function (deviceModel) {
                return deviceModel.Id == defaultDeviceModelId;
            });
            if (defaultDeviceModels == null || defaultDeviceModels.length == 0) {
                return;
            }

            var plans = defaultDeviceModels[0].DeviceModelPlans.filter(function (plan) {
                return plan.Id == deviceToPlanId;
            });
            if (plans == null || plans.length == 0 || plans[0].RatePlan == null) {
                return;
            }

            var plan = plans[0];

            var monthlyFee = $.trim(plan.MonthlyFee);
            var monthlyFeeStatus = $('.orderSummaryStatus-monthlyPriceValue');
            monthlyFeeStatus.attr('data-value', monthlyFee);
            monthlyFeeStatus.text(monthlyFee == '0' ? '-' : monthlyFee);

            if (monthlyFee == '0') {
                $('.orderSummaryStatus-monthlyPriceCurrency').hide();
            } else {
                $('.orderSummaryStatus-monthlyPriceCurrency').show();
            }

            var upfrontPrice = $.trim(plan.UpfrontPrice) || 0;
            var upfrontPriceStatus = $('.orderSummaryStatus-upfrontPriceValue');
            upfrontPriceStatus.attr('data-value', upfrontPrice);
            upfrontPriceStatus.text(upfrontPrice);

            this.selectedRatePlanId = plan.RatePlan.Id;
        }

        //iPad hack for taking a long time to rearrange layout without triggering the change through css
        $(window).on('orientationchange', function () {

            if ($('body.ipad').length <= 0) {
                return;
            }

            setTimeout(function () {
                $('.eitc-main-content').css('width', '100%');

                setTimeout(function () {
                    $('.eitc-main-content').css('width', '');
                }, 10);
            }, 10);
        });

        this.initializeSectionToggleButtons();
        this.initializeBackToTop();
    };

    DeviceDetailsController.prototype.initializeSectionToggleButtons = function () {
        var _this = this;

        $('body').on('click', '[data-section-toggle-selector]', function () {

            var element = $(this);
            var sectionToggleSelector = $.trim(element.attr('data-section-toggle-selector'));
            
            if (sectionToggleSelector.length <= 0) {
                console.log('empty')
                return
            }

            var section = $(sectionToggleSelector);
            
            if (!section || section.length <= 0) {
                console.log('somt')
                return;
            }

            if (section.is(':hidden')) {
                console.log('hidden')
                section.find('.deviceDetails-sectionCloseButton').one('click', function () {
                    section.hide();
                    element.removeClass('active');

                    $('html, body').stop(true).animate({
                        scrollTop: 0
                    }, 300);
                });

                section.show();
                element.addClass('active');

                $('html, body').stop(true).animate({
                    scrollTop: section.offset().top - $('#topBar').outerHeight()
                }, 300);
            } else {
                $('html, body').stop(true).animate({
                    scrollTop: section.offset().top - $('#topBar').outerHeight()
                }, 300);
            }
        });

        $(window).on('scroll', function () {

            if (_this.sectionScrollHandle != null) {
                return;
            }

            _this.sectionScrollHandle = setTimeout(function (data) {

                $('.deviceDetails-section:visible .deviceDetails-sectionCloseButton').each(function (e) {

                    var closeButton = $(this);
                    var section = closeButton.parent();
                    var offset = $('#topBar').outerHeight();
                    var minPosition = section.offset().top;
                    var maxPosition = section.offset().top + section.outerHeight(true);
                    var currentScrollPosition = $(window).scrollTop();

                    if (minPosition - offset < currentScrollPosition && currentScrollPosition < maxPosition - offset) {
                        closeButton.addClass('fixed');
                    } else {
                        closeButton.removeClass('fixed');
                    }
                });

                _this.sectionScrollHandle = null;
            }, 500, _this);
        });
    };

    DeviceDetailsController.prototype.initializeBackToTop = function () {

        $('.deviceDetails-backToTopButton').on('click', function () {

            $('html, body').stop(true).animate({
                scrollTop: 0
            }, 300);
        });

        setInterval(function (controller) {

            if (!controller.enableBackToTopPopup) {
                return;
            }

            var deviceDetails = $('.deviceDetails:visible').eq(0);
            if (deviceDetails.length == 0) {
                return;
            }

            var backToTopPopup = $('.deviceDetails-backToTopPopup');

            var scrollOffset = deviceDetails.offset().top + deviceDetails.outerHeight() - $(window).scrollTop();
            //var scrollOffset = 300 - $(window).scrollTop();
            if (scrollOffset <= 0) {
                controller.hidePriorityStatus();
                backToTopPopup.addClass('show');
            } else {
                controller.showPriorityStatus();
                backToTopPopup.removeClass('show');
            }
        }, 500, this);
    };

    DeviceDetailsController.prototype.deviceColorVariant_click = function (target) {

        var listItem = $(target).closest('li');
        var colorContainer = $(target).closest('.deviceDetails-colorContainer');
        var selectedColorText = $(target).find('.deviceDetails-colorText').text();
        var stockQuantity = parseInt(listItem.attr('data-stock-quantity'));
        var limitedStockThreshold = parseInt(listItem.attr('data-limited-stock-threshold'));

        colorContainer.find('.deviceDetails-selectedColorText').text(selectedColorText);
        colorContainer.find('.deviceDetails-colorsList li.active').removeClass('active');
        listItem.addClass('active');

        var deviceImageUrl = listItem.attr('data-device-image-url');
        this.container.find('.deviceDetails-image').attr('src', deviceImageUrl);

        var limitedAvailabilityWarning = colorContainer.find('.deviceDetails-availability-limited');
        var outOfStockWarning = colorContainer.find('.deviceDetails-availability-none');

        limitedAvailabilityWarning.hide();
        outOfStockWarning.hide();

        if (stockQuantity < 0) {
            outOfStockWarning.show();
            this.showAvailabilityStatus();
        } else {
            var limitedStockThresholdValue = this.defaultStockQuantityThreshold;
            if (!isNaN(limitedStockThreshold)) {
                limitedStockThresholdValue = limitedStockThreshold;
            }

            if (stockQuantity > 0 && stockQuantity <= limitedStockThresholdValue) {
                limitedAvailabilityWarning.show();
            }

            this.hideAvailabilityStatus();
        }

        this.persistOrderInformation();
    };

    DeviceDetailsController.prototype.deviceModel_click = function (target) {

        var listItem = $(target).closest('li');
        var modelContainer = $(target).closest('.deviceDetails-modelContainer');
        var selectedDeviceModelId = listItem.attr('data-device-model-id');
        var selectedColorId = this.container.find('.deviceDetails-colorsList li.active').attr('data-color-id');

        modelContainer.find('.deviceDetails-modelsList > li').removeClass('active');
        $(target).closest('li').addClass('active');

        this.refreshColors(selectedDeviceModelId, selectedColorId);

        this.setValuesFromDeviceModel(selectedDeviceModelId);
    };

    DeviceDetailsController.prototype.setValuesFromDeviceModel = function (deviceModelId) {

        var deviceModel = this.data.DeviceModels.filter(function (model) {
            return $.trim(model.Id).toLowerCase() == $.trim(deviceModelId).toLowerCase();
        })[0];

        var minimumUpfrontValue = 999999999;
        for (var i = 0; i < deviceModel.DeviceModelPlans.length; i++) {
            var modelPlan = deviceModel.DeviceModelPlans[i];
            var upfrontPrice = modelPlan.UpfrontPrice;

            if (typeof upfrontPrice != 'number') {
                upfrontPrice = 0;
            }

            minimumUpfrontValue = Math.min(minimumUpfrontValue, upfrontPrice);
            if (minimumUpfrontValue === 0) {
                break;
            }
        }

        var lowestUpfrontPrice = $('.deviceDetails-lowestUpfrontPrice');
        lowestUpfrontPrice.attr('data-value', minimumUpfrontValue);
        lowestUpfrontPrice.text(minimumUpfrontValue);

        var upfrontPriceValue = $('.orderSummaryStatus-upfrontPriceValue');
        var upfrontPriceValueOriginal = parseInt(upfrontPriceValue.attr('data-original-value'));
        upfrontPriceValue.attr('data-value', upfrontPriceValueOriginal + minimumUpfrontValue);
        upfrontPriceValue.text(upfrontPriceValueOriginal + minimumUpfrontValue);

        this.persistOrderInformation();
    };

    DeviceDetailsController.prototype.persistOrderInformation = function () {

        var deviceColorVariantId = $.trim(this.container.find('.deviceDetails-colorsList li.active').attr('data-device-color-variant-id'));
        var deviceModelId = $.trim(this.container.find('.deviceDetails-modelsList li.active').attr('data-device-model-id'));
        var sectionName = $.trim(this.data.DeviceModels[0].DeviceModelPlans[0].Section);
        var section = '';

        if (sectionName.toLowerCase() === 'personal') {
            section = '1';
        } else if (sectionName.toLowerCase() === 'enterprise') {
            section = '2';
        }

        var plansParameters = {
            deviceColorVariantId: deviceColorVariantId,
            deviceModelId: deviceModelId,
            section: section,
            deviceUrl: document.location.href.replace(document.location.search, '')
        };

        //Update order summary values by finding the device model plan based on the preselected rate plan
        var selectedRatePlanId = $.trim(this.selectedRatePlanId);
        if (deviceModelId.length > 0 && selectedRatePlanId.length > 0) {
            var defaultDeviceModels = this.data.DeviceModels.filter(function (deviceModel) {
                return deviceModel.Id == deviceModelId;
            });
            if (defaultDeviceModels == null || defaultDeviceModels.length == 0) {
                return;
            }

            var plans = defaultDeviceModels[0].DeviceModelPlans.filter(function (plan) {
                return plan.RatePlan != null && plan.RatePlan.Id == selectedRatePlanId;
            });
            if (plans == null || plans.length == 0) {
                return;
            }

            var plan = plans[0];

            plansParameters.deviceToPlanId = plan.Id;

            var monthlyFee = $.trim(plan.MonthlyFee);
            var monthlyFeeStatus = $('.orderSummaryStatus-monthlyPriceValue');
            monthlyFeeStatus.attr('data-value', monthlyFee);
            monthlyFeeStatus.text(monthlyFee == '0' ? '-' : monthlyFee);

            if (monthlyFee == '0') {
                $('.orderSummaryStatus-monthlyPriceCurrency').hide();
            } else {
                $('.orderSummaryStatus-monthlyPriceCurrency').show();
            }

            var upfrontPrice = $.trim(plan.UpfrontPrice) || 0;
            var upfrontPriceStatus = $('.orderSummaryStatus-upfrontPriceValue');
            upfrontPriceStatus.attr('data-value', upfrontPrice);
            upfrontPriceStatus.text(upfrontPrice);
        }

        var currentParameters = Utilities.getUriParameters();
        var newParameters = $.extend({}, currentParameters, plansParameters);

        var orderButton = $('.orderSummaryStatus-orderButton');
        var baseUrl = orderButton.attr('data-default-href');
        var url = Utilities.appendParametersToUrl(baseUrl, newParameters);

        orderButton.attr('href', url);
        $('.orderSummaryStatus-planContainer.checked > a').attr('href', url);
    };

    DeviceDetailsController.prototype.showPriorityStatus = function () {

        var orderSummaryStatus = $('.orderSummaryStatus');
        var availabilityStatus = $('.deviceDetails-availabilityStatus');

        if (availabilityStatus.attr('data-visible')) {
            orderSummaryStatus.addClass('hide');
            availabilityStatus.addClass('show');
        } else {
            orderSummaryStatus.removeClass('hide');
            availabilityStatus.removeClass('show');
        }
    };

    DeviceDetailsController.prototype.hidePriorityStatus = function () {

        var orderSummaryStatus = $('.orderSummaryStatus');
        var availabilityStatus = $('.deviceDetails-availabilityStatus');

        orderSummaryStatus.addClass('hide');
        availabilityStatus.removeClass('show');
    };

    DeviceDetailsController.prototype.showAvailabilityStatus = function () {

        $('.deviceDetails-availabilityStatus').attr('data-visible', 'visible');
        this.showPriorityStatus();
    };

    DeviceDetailsController.prototype.hideAvailabilityStatus = function () {

        $('.deviceDetails-availabilityStatus').removeAttr('data-visible');
        this.showPriorityStatus();
    };

    DeviceDetailsController.prototype.refreshColors = function (deviceModelId, colorId) {

        if ($.trim(deviceModelId).length == 0) {
            return;
        }

        var _this = this;
        var deviceType = this.data.DeviceType;

        var deviceModel = this.data.DeviceModels.filter(function (item) {
            return $.trim(item.Id).toLowerCase() === $.trim(deviceModelId).toLowerCase();
        })[0];

        var deviceDetailsColorListItems = [];
        var deviceDetailsColorListItemTemplate = $('#DeviceDetailsColorListItem').html();
        deviceModel.DeviceColorVariants.forEach(function (deviceColorVariant) {

            var colorImage = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
            var colorValue = 'transparent';
            var color = { Id: '', Title: '' };
            if (deviceColorVariant.Color) {
                color = deviceColorVariant.Color;

                if ($.trim(color.HexValue).length > 0) {
                    colorValue = '#' + color.HexValue;
                }

                if (color.ColorThumb && $.trim(color.ColorThumb.Url).length > 0) {
                    colorImage = color.ColorThumb.Url;
                }
            }
            var deviceImage = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
            if (deviceColorVariant.Image != null && $.trim(deviceColorVariant.Image.Url).length > 0) {
                deviceImage = deviceColorVariant.Image.Url;
            }

            var deviceDetailsColorListItem = deviceDetailsColorListItemTemplate;
            deviceDetailsColorListItem = deviceDetailsColorListItem.replace('[data-device-color-variant-id]', deviceColorVariant.Id);
            deviceDetailsColorListItem = deviceDetailsColorListItem.replace('[data-color-id]', color.Id);
            deviceDetailsColorListItem = deviceDetailsColorListItem.replace('[data-stock-quantity]', deviceColorVariant.StockQty);
            deviceDetailsColorListItem = deviceDetailsColorListItem.replace('[device-title]', deviceType.Title);
            deviceDetailsColorListItem = deviceDetailsColorListItem.replace(/\[color-title]/g, color.Title);
            deviceDetailsColorListItem = deviceDetailsColorListItem.replace('[color-hex-value]', colorValue);
            deviceDetailsColorListItem = deviceDetailsColorListItem.replace('[color-image]', colorImage);
            deviceDetailsColorListItem = deviceDetailsColorListItem.replace('[data-device-image-url]', deviceImage);
            deviceDetailsColorListItem = deviceDetailsColorListItem.replace('[data-limited-stock-threshold]', deviceColorVariant.LimitedStockThreshold);

            deviceDetailsColorListItems.push(deviceDetailsColorListItem);
        });

        var colorsList = this.container.find('.deviceDetails-colorsList');
        // colorsList.empty();
        colorsList.append(deviceDetailsColorListItems.join(''));

        colorsList.find('li[data-color-hex-value]').each(function () {
            var hexValue = $(this).attr('data-color-hex-value');
            $(this).find('.deviceDetails-colorImage').css('background-color', hexValue);
        });

        var selectedColorListItem = colorsList.find('li[data-color-id="' + colorId + '"]:first').eq(0);
        if (selectedColorListItem.length == 0) {
            selectedColorListItem = colorsList.find('li:first');
        }

        selectedColorListItem.find('a:first').trigger('click');
    };

    function initializeWidgets(require) {

        $('.deviceDetails[data-device-type-id]').each(function () {

            var controller = new DeviceDetailsController(this);
            controller.initialize();
        });
    }

    require(['require', 'bootstrap'], function (require) {
        initializeWidgets(require);
    });
});

