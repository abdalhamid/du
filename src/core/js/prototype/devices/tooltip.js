﻿'use strict';

$(function () {
    
    require(['bootstrap'], function () {

        $("[data-toggle=popover]").popover({
            trigger: 'focus',
            container: 'body',
            placement: 'auto top'
        });

        //Mobile safari fixes
        if (navigator.userAgent.indexOf('iPad') >= 0 || navigator.userAgent.indexOf('iPhone') >= 0) {
            $("[data-toggle=popover]").on("shown.bs.popover", function () {
                $('body').css('cursor', 'pointer');
            });

            $("[data-toggle=popover]").on("hide.bs.popover", function () {
                $('body').css('cursor', 'auto');
            });
        }

    });
});

