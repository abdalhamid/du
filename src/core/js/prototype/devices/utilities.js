﻿'use strict';

$(function () {
    
    window.Utilities = {

        getUriParameters: function () {

            var parameters = {};
            var search = decodeURI(document.location.search);
            var pattern = /[&?]([^=^&^#]+)=([^&^#]+)/g;
            var match = null;

            while (match = pattern.exec(search)) {
                var parameter = match[1];
                var value = match[2];

                parameters[parameter] = decodeURIComponent(value.replace(/\+/g, ' '));
            }

            return parameters;
        },

        appendParametersToUrl: function (url, parameters) {

            var urlValue = $.trim(url);

            if (urlValue.indexOf('?') >= 0) {
                urlValue += '&' + $.param(parameters);
            }
            else {
                urlValue += '?' + $.param(parameters);
            }

            return urlValue;

        },

        getLanguage: function () {

            var language = $('html').attr('lang');
            if ($.trim(language).length == 0) {
                language = 'en';
            }

            return language;
        },

        localizeText: function (cultureTexts) {
            var language = Utilities.getLanguage();

            return cultureTexts[language];
        },

        createCookie: function (name, value, days) {
            var expires = "";
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toUTCString();
            }
            document.cookie = name + "=" + value + expires + "; path=/";
        },

        readCookie: function (name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        },

        eraseCookie: function (name) {
            createCookie(name, "", -1);
        }

    };

});

