require(['jquery', 'lib/youtube_channel', 'lib/jquery.classyscroll.min', 'lib/jquery.fitvids.min', 'lib/twitter', 'lib/jquery.mousewheel.min'], function () {

    $(document).ready(function () {

        if ($('.media-center').length <= 0) {
            return;
        }

        $('.fluid-video').fitVids();

        $('.ucPopularVideos li a').click(function (e) {
            e.preventDefault();
            var URL = $(this).attr('href');
            var htm = '<iframe width="710" height="350" src="' + URL + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
            $('.ucVideo .fluid-video').html(htm);
            $('.fluid-video').fitVids();

            return false;
        });

        $(".ucscrollbar").ClassyScroll();

        $('.ucPopularVideos .content').youTubeChannel({
            userName: 'theduchannel',
            hideNumberOfRatings: false,
            videoBox: 'youtubeplay'
        });
    });
});