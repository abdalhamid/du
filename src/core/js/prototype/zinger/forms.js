var FormController = (function() {

    var init = function() {
        var lang = "/" + $('html').attr('lang') + "/";
        var ADD_ERROR_MESSAGE = true;
        var NO_ERROR_MESSAGE = false;
        this.creditCardNumberLength = 0;
        this.creditCardExpiryLength = 0;

        /////////////// Register Form Validation and Formatting ///////////////
        this.validateMobileNumber = function() {
            var mobileNumberField = $('input[name="MobileNumber"]');
            var mobileNumberValue = mobileNumberField.val();
            if (mobileNumberValue.length == 0 || !mobileNumberValue.match(/[0-9]{8}/g)) {
                $('.form-register-error').text(mobileNumberField.data('required-error-message')).parent('label').addClass('error');
            } else {
                $('.form-register-error').text('').parent('label').removeClass('error');
            
                $('.form-register-error').closest('form').submit();
            }
        }
        $('input[name="MobileNumber"]').on('keyup', function(e) {
            var cardCvvInput = $(this);
            if (cardCvvInput.val().length >= 8)
                cardCvvInput.val(cardCvvInput.val().substr(0, 8));
        });
        /////////////////////////////////////////////////////////////////////////



        /////////////// Credit Card Form Validation and Formatting ///////////////
        this.validateCreditCardHolderName = function(errorFlag) {
            var cardHolderField = $('input[name="cardName"]');
            var cardHolderName = cardHolderField.val();
            if (cardHolderName.length == 0) {
                if (!errorFlag) {
                    $('.form-card-error-name').text(cardHolderField.data('required-error-message')).parent('label').addClass('error');
                }
                return false;
            } else {
                $('.form-card-error-name').text('').parent('label').removeClass('error');
                return true;
            }
        }

        $('input[name="cardName"]').blur(function(e) {
            this.validateCreditCardHolderName();
            this.validateAllCreditCardFields();
        }.bind(this));

        this.validateCreditCardNumber = function(errorFlag) {
            var VISA_PATTERN = /^4[0-9]{15}$/g;
            var MASTER_CARD_PATTERN = /^5[1-5][0-9]{14}$/g;
             var cardHolderField = $('input[name="cardNumber"]');
            var cardHolderNumber = cardHolderField.val().split(' ').join('');
            if (cardHolderNumber.length == 0 || (!cardHolderNumber.match(VISA_PATTERN) && !cardHolderNumber.match(MASTER_CARD_PATTERN))) {
                if (!errorFlag) {
                    $('.form-card-error-number').text(cardHolderField.data('required-error-message')).parent('label').addClass('error');
                    $('.credit-card-img').removeClass('show');
                }
                return false;
            } else {
                $('.form-card-error-number').text('').parent('label').removeClass('error');
                return true;
            }
        }

        this.showCreditCardType = function() {
            var creditCardNumber = $('input[name="cardNumber"]').val().split(' ').join('');
            if (creditCardNumber.length === 16 && creditCardNumber.match(/^4[0-9]{15}$/g)) {
                $('.credit-card-img.visa').addClass('show');
                $('.form-card-error-number').text('').parent('label').removeClass('error');

            } else if (creditCardNumber.length === 16 && creditCardNumber.match(/^5[1-5][0-9]{14}$/g)) {
                $('.credit-card-img.mastercard').addClass('show');
                $('.form-card-error-number').text('').parent('label').removeClass('error');
            } else {
                $('.credit-card-img').removeClass('show');
            }
        }

        $('input[name="cardNumber"]').blur(function(e) {
            this.validateCreditCardNumber();
            this.validateAllCreditCardFields();

        }.bind(this));

        $('input[name="cardNumber"]').on('keyup', function(e) {
            var cardInput = $('input[name="cardNumber"]');
            if (cardInput.val().length != this.creditCardNumberLength) {
                var spacedValue = this.formateCreditCard(cardInput.val());
                cardInput.val(spacedValue);
                this.showCreditCardType();
                this.creditCardNumberLength = cardInput.val().length;
            }
        }.bind(this));

        this.validateCreditCardExpiry = function(errorFlag) {
            var cardExpiryField = $('input[name="cardExpire"]');
            var cardExpiry = cardExpiryField.val();
            if (cardExpiry.length == 0 || !cardExpiry.match(/^(1[0-2]|0*[1-9])\/([0-9]{2})$/g)) {
                if (!errorFlag) {
                    $('.form-card-error-expire').text(cardExpiryField.data('required-error-message')).parent('label').addClass('error');
                }
                return false;
            } else {
                $('.form-card-error-expire').text('').parent('label').removeClass('error');
                return true;
            }

        }

        $('input[name="cardExpire"]').blur(function(e) {
            this.validateCreditCardExpiry();
            this.validateAllCreditCardFields();
        }.bind(this));

        this.validateCreditCardCvv = function(errorFlag) {
            var cardCvvField = $('input[name="cardCVV"]');
            var cardCvv = cardCvvField.val();
            if (cardCvv.length == 0 || !cardCvv.match(/^[0-9]{3}$/g)) {
                if (!errorFlag) {
                    $('.form-card-error-cvv').text(cardCvvField.data('required-error-message')).parent('label').addClass('error');
                }
                return false;
            } else {
                $('.form-card-error-cvv').text('').parent('label').removeClass('error')
                return true;
            }

        }
        $('input[name="cardCVV"]').blur(function(e) {
            this.validateCreditCardCvv();
            this.validateAllCreditCardFields();
        }.bind(this));

        this.validateAllCreditCardFields = function(errorFlag) {
            var holderNameValid = this.validateCreditCardHolderName(!errorFlag);
            var numberValid = this.validateCreditCardNumber(!errorFlag);
            var expiryValid = this.validateCreditCardExpiry(!errorFlag);
            var cvvValid = this.validateCreditCardCvv(!errorFlag);
            if (holderNameValid && numberValid && expiryValid && cvvValid) {
                $('.form-card-error-name').text('').parent('label').removeClass('error');

                var topPosition = $('.zinger-container').offset().top + $('.zinger-container').height() + parseInt($('.zinger-container').css('padding-bottom').split("px")[0]) - $(window).height();
                $('body').animate({ scrollTop: topPosition }, '500', 'swing', function() {});
                return true;
            } else
                return false;
        }

        $('.form-card[data-card="1"] form button[type="submit"]').click(function(e) {
            e.preventDefault();
            if (this.validateAllCreditCardFields(true))
            {
                $('.form-card[data-card="1"] form').submit();
         
            }
                //window.location = lang + "zinger/credit-card-du.ae_mobile_plans_zinger_confirmation.html";

        }.bind(this));

        this.formateCreditCard = function(value) {
            var v = value.replace(/\s*/g, '').replace(/[^0-9]/gi, '')
            var matches = v.match(/\d{4,16}/g);
            var match = matches && matches[0] || ''
            var parts = []
            for (i = 0, len = match.length; i < len; i += 4) {
                parts.push(match.substring(i, i + 4))
            }
            if (parts.length) {
                return parts.join(' ')
            } else {
                return v;
            }
        }

        this.formateExpiryDate = function(value) {
            var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
            var matches = v.match(/\d{2,4}/g);
            var match = matches && matches[0] || ''
            var parts = []
            for (i = 0, len = match.length; i < len; i += 2) {
                parts.push(match.substring(i, i + 2))
            }
            if (parts.length) {
                return parts.join('/')
            } else {
                return v
            }
        }



        $('input[name="cardExpire"]').on('keyup', function(e) {
            var expiryDateInput = $('input[name="cardExpire"]');
            var ValueWidthSlash = this.formateExpiryDate(expiryDateInput.val());
            expiryDateInput.val(ValueWidthSlash);

            if ((expiryDateInput.val().length == 2 && expiryDateInput.val().length >= this.creditCardExpiryLength)) {
                expiryDateInput.val(expiryDateInput.val() + "/");
            }
            this.creditCardExpiryLength = expiryDateInput.val().length;


        }.bind(this));

        $('input[name="cardCVV"]').on('keyup', function(e) {
            var cardCvvInput = $('input[name="cardCVV"]');
            if (cardCvvInput.val().length >= 3)
                cardCvvInput.val(cardCvvInput.val().substr(0, 3));
        }.bind(this));
        //////////////////////////////////////////////////////////////////




        /////////////////////// Login Form Validation   //////////////////
        this.validateLoginPassword = function() {
            var passwordField = $('.form-login input[name="password"]');
            var passwordValue = passwordField.val();
            if (passwordValue.length < 4) {
                $('.form-login-error-password').text(passwordField.data('required-error-message')).parent('label').addClass('error');
                return false;
            } else {
                $('.form-login-error-password').text('').parent('label').removeClass('error');
                return true;
            }
        }

        this.validateLoginUsername = function() {
            var usernameField = $('.form-login input[name="username"]');
            var usernameValue = usernameField.val();
            if (usernameValue.length == 0) {
                $('.form-login-error-username').text(usernameField.data('required-error-message')).parent('label').addClass('error');
                return false;
            } else {
                $('.form-login-error-username').text('').parent('label').removeClass('error');
                return true;
            }
        }
        $('.form-login input[name="username"]').blur(function() {
            this.validateLoginUsername();
        }.bind(this));

        $('.form-login input[name="password"]').blur(function() {
            this.validateLoginPassword();
        }.bind(this));

        $('.form-login button[type="submit"]').click(function(e) {
            e.preventDefault();
            var usernameValid = this.validateLoginUsername();
            var passwordValid = this.validateLoginPassword();
            if (usernameValid && passwordValid){
                 $('.form-login form').submit();
            }
              //  window.location = lang + "zinger/du.ae_mobile_plans_zinger_credit-card.html";
        }.bind(this));


        //////////////////////////////////////////////////////////////////

        this.validatePassword = function(passwordField) {
            var passwordValue = passwordField.val();
            if (passwordValue.length < 4) {
                $('.form-login-error').text(passwordField.data('incorrect-error-message')).parent('label').addClass('error');
            } else {
                $('.form-login-error').text('').parent('label').removeClass('error');
                //window.location = lang + "zinger/du.ae_mobile_plans_zinger_credit-card.html";
                $('.form-login-error').closest('form').submit();
            }
        }

        this.resetPassword = function() {
            $('.alert-message').addClass('show');
          //  $('.title-password').text('Enter the new password sent to you via SMS & email');
            $('body').animate({ scrollTop: 0 }, '500', 'swing', function() {});
        }

        $('.resend-password').on('click', function(e) {
            e.preventDefault();
            this.resetPassword();
        }.bind(this));

        $('.alert-message-close').on('click', function() {
            $('.alert-message').removeClass('show');
        })

        $('.form-register[data-register="1"]:not(".hidden") form button[type="submit"]').click(function(e) {
            e.preventDefault();
            this.validateMobileNumber();
        }.bind(this));


        $('.form-register[data-register="2"]:not(".hidden") form button[type="submit"] , .form-register[data-register="3"]:not(".hidden") form button[type="submit"]').click(function(e) {
            e.preventDefault();
            var passwordField = $('.form-register[data-register="2"] input[name="password"]');
            this.validatePassword(passwordField);
        }.bind(this));


        /*$('.form-card[data-card="2"] button[type="submit"]').click(function(e) {
            e.preventDefault();
            window.location = lang + "zinger/credit-card-du.ae_mobile_plans_zinger_confirmation.html";
        });*/

        $('.form-card[data-card="3"] button[type="submit"]').click(function(e) {
            e.preventDefault();

            if (!$('.form-card[data-card="3"] input[type="checkbox"]').prop('checked')) {
                $('.locator-map-filter-list').addClass('error');
            } else {

                $(this).closest('form').submit();

            }
                //window.location = lang + "zinger/du.ae_mobile_plans_zinger_confirmation.html";

        });

        $('input[type="checkbox"]').on('change', function() {
            if (this.checked) {
                $('button.pink').prop('disabled', false)
                if ($(window).width() < 768) {
                    $('html, body').animate({
                        scrollTop: $('.button.pink').offset().top - ($(window).height() - $('.button.pink').height()) + 25
                    }, 500);
                }
            } else {
                $('button.pink').prop('disabled', true);
            }
        });

        $('input[type="checkbox"]').on('click', function() {
            $('.locator-map-filter-list').removeClass('error');
        })

    };
    return {
        init: init
    };
})();
$(function() {
    FormController.init();
});
