var Popup = (function() {


    var init = function() {
        //close popup from btn
        this.closePopupDelayed = function($btn) {
            var popupId = $btn.data('close-for-popup-delayed');
            var popupToClose = $("#" + popupId);
            if (popupToClose.hasClass('template'))
                popupToClose.children('.popup-container').empty();

            popupToClose.removeClass('active');
        }

        //open popup for any element with data-for-popup
        $('body').on('click', '[data-for-popup]', function(e) {
            e.preventDefault();
            var currentPopupType = $(this).data('for-popup');

            if (currentPopupType == 'subscribe-popup') {
                var originalPrice = $(this).data('old-price');
                if (parseInt(originalPrice) <= 10) {
                    $('.monthly-daily').text('daily');
                    $('.month-day').text('day');
                } else {
                    $('.monthly-daily').text('monthly');
                    $('.month-day').text('month');
                }
                var oldPrice = $(this).data('new-price') ? $(this).data('old-price') : "";
                var newPrice = $(this).data('new-price') ? $(this).data('new-price') : $(this).data('old-price');

                $('#subscribe-popup').find('.continue a').attr('data-original-price', originalPrice);

                $('#subscribe-popup').find('.old-price').text(oldPrice);
                $('#subscribe-popup').find('.new-price').text(newPrice);
                $('#subscribe-popup').find('.original-price').text(originalPrice);
            } else if (currentPopupType == 'phonebalance-subscribe-popup') {

            }

            requestAnimationFrame(function() {
                $('#' + currentPopupType).addClass('active');
            });
            $('body').addClass('no-scroll');
        });

        //close popup from any button that has data-close-for-popup
        $('body').on('click', '[data-close-for-popup]', function(e) {
            var popupId = $(this).data('close-for-popup');
            var popupToClose = $("#" + popupId);
            if (popupToClose.hasClass('template'))
                popupToClose.children('.popup-container').empty();

            popupToClose.removeClass('active');
        });

        //close popup from default popup X button
        $('body ').on('click', '.close-popup', function() {
            $('body').removeClass('no-scroll');
            $(this).closest('.popup').removeClass('active');
            setTimeout(function() {
                $('#phonebalance-subscribe-popup').addClass('show-right');
            }, 350);
        });


        $('#subscribe-popup a').click(function(e) {
            e.preventDefault();
            if ($('input[name="subscribe-by"]:checked').val() == "card") {
                var lang = $('html').attr('lang');
                if ($(window).width() < 768)
                    window.location = "/" + lang + "/zinger/du.ae_mobile_plans_zinger_register.html";
                else
                    window.location = "/" + lang + "/zinger/du.ae_mobile_plans_zinger_login.html";
            } else {
                $('#subscribe-popup').addClass('hide-left');
                $('#subscribe-popup').removeClass('active');


                $('#phonebalance-subscribe-popup').removeClass('show-right');
                $('#phonebalance-subscribe-popup').addClass('active');
                $('body').addClass('no-scroll');

                var originalPrice = $(e.target).attr('data-original-price');
                $('#phonebalance-subscribe-popup').find('.original-price').text(originalPrice);
                setTimeout(function() {
                    $('#subscribe-popup').removeClass('hide-left');

                    $('input[name="subscribe-by"]').prop('checked', false);
                    $('input[name="subscribe-by"][value="card"]').prop('checked', true);
                    $('input[name="subscribe-by"]').parent('label').removeClass('active');
                    $('input[name="subscribe-by"][value="card"]').parent('label').addClass('active');
                }, 350);

            }
        });


        if ($(window).width() < 768) {
            if ($('#subscription-offer-popup').length && window.location.hash.substr(1) != 'no-offer') {
                setTimeout(function() {
                    $('#subscription-offer-popup').addClass('active');
                    $('body').addClass('no-scroll');
                }, 2500);
            }
        }

    };
    return {
        init: init
    };

})();
$(function() {
    Popup.init();
});
