var ZingerController = (function() {

    var ButtonsInit = function() {
        $('.buttons-container input[type="radio"]').change(function(e) {
            $('.buttons-container label').removeClass('active');
            if ($(this).prop('checked'))
                $(this).closest('label').addClass('active');
        });
    }

    var ScrollOnMobile = function() {
        var url = window.location.href;
        var pathName = window.location.pathname;
        if ((pathName === "/en/du.ae_mobile_plans_zinger.html" || pathName === "/ar/du.ae_mobile_plans_zinger.html") && (url.lastIndexOf('#') != -1 && window.location.hash.indexOf("-bundle") != -1)) {
            var urlSplit = url.split('#')[1];
            var bundleValue = urlSplit.split('-')[0];
            if (bundleValue <= 10) {
                $("[data-filter='dailybundle']")[0].click();
                setTimeout(function() {
                    $('body, html').animate({
                        scrollTop: $('#' + urlSplit).offset().top - 120
                    })
                }, 1000)
            } else {
                $('body, html').animate({
                    scrollTop: $('#' + urlSplit).offset().top - 140
                })
            }
        }
    }

    var init = function() {
        if ($(window).width() < 768) {
            setTimeout(function() {
                ScrollOnMobile();
            }, 2000)
        }
        ButtonsInit();
    };

    return {
        init: init,
    }
})();
$(function() {
    ZingerController.init();
});
