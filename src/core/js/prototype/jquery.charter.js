﻿/*
 *  jQuery Charter - v1.0.0
 *
 *  Made by Prototype Interactive
 */
; (function ($, window, document, undefined) {

    var pluginName = "protoCharter",
        defaults = {
            height: 400,
            margin: { top: 100, right: 120, bottom: 0, left: 120 },
            topTitle: "Bill history",
            bottomTitle: "Last 6 months",
            dir: "ltr",
            type: "bar"
        };

    function charter(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        return this;
    }

    $.extend(charter.prototype, {
        attachData: function (data) {
            this.data = data;
        },
        draw: function () {
            switch (this.settings.type) {
                default:
                case "bar":
                    this.drawBar();
                    break;
                case "line":
                    this.drawLine();
                    break;
            }
        },

        drawLine: function () {
            var _this = this;

            var maxWidth = $(_this.element).width();
            var isSmallScreen = maxWidth < 500;

            if (isSmallScreen) {
                _this.settings.margin.right = 20;
                _this.settings.margin.left = 20;
            }

            var width = maxWidth,
                height = _this.settings.height;

            var x = d3.scale.ordinal().rangeRoundBands([_this.settings.margin.right, width - _this.settings.margin.left], .1);

            var y = d3.scale.linear().range([height, 0]);

            var svg = d3.select(".graph").append("svg")
                .attr("class", "chart")
                .attr("preserveAspectRatio", "xMidYMid")
                .attr("viewBox", "0 0 " + width + " " + height);

            x.domain(_this.data.map(function (d) { return d.label; }));
            y.domain([0, d3.max(_this.data, function (d) { return +d.value + (+d.value / 2.5); })]);

            svg
                .append("g")
                .attr("class", "grid")
                .call(d3.svg.axis().scale(y)
                    .orient("left")
                    .ticks(4)
                    .tickSize(-width, 0, 0)
                    .tickFormat(function (d) { return "AED " + d; })
                )
                .selectAll("text")
                .attr("x", 0)
                .attr("y", -10)
                .style("text-anchor", _this.settings.dir == "rtl" ? "end" : "start");

            var line = d3.svg.line()
                 .x(function (d) { return x(d.label); })
                 .y(function (d) { return y(+d.value); })
                    .interpolate("monotone");

            svg.append("path")
                    .attr("class", "path")
                    .datum(_this.data)
                    .attr("d", line);

            svg.selectAll("circle")
                .data(_this.data)
                .enter()
                .append("circle")
                .attr("class", function (d) { return d.unbilled ? "circle unbilled" : "circle"; })
                .attr("cx", function (d) { return x(d.label); })
                .attr("cy", function (d) { return y(+d.value); })
                .attr("r", 5);

            svg
                .append("text")
                .attr("class", "topTitle")
                .attr("x", (width / 2))
                .attr("y", 10)
                .text(_this.settings.topTitle);

            svg
                .append("text")
                .attr("class", "bottomTitle")
                .attr("x", (width / 2))
                .attr("y", 30)
                .text(_this.settings.bottomTitle);

            var yTextPadding = -20;

            svg.selectAll(".linetext")
                .data(_this.data)
                .enter()
                .append("text")
                .attr("class", "linetext")
                .attr("x", function (d) {
                    return x(d.label);
                })
                .attr("y", function () {
                    return height - yTextPadding;
                })
                .text(function (d) {
                    return d.label;
                });

            var yTextPadding = 10;
            
            svg.selectAll(".bartoptext")
                .data(_this.data)
                .enter()
                .append("text")
                .attr("class", "bartoptext")
                .attr("x", function (d) {
                    return x(d.label) ;
                })
                .attr("y", function (d) {
                    return y(+d.value) - yTextPadding;
                })
                .text(function (d) {
                    return "AED " + d.value;
                });

            yTextPadding = 30;


            svg
                .selectAll(".unbilled")
                .data(_this.data)
                .enter()
                .append("text")
                .attr("class", "bartoptext")
                .attr("x", function (d) {
                    return x(d.label) ;
                })
                .attr("y", function (d) {
                    return y(+d.value) - yTextPadding;
                })
                .filter(function (d) {
                    return d.unbilled;
                })
                .text(function () {
                    return "Unbilled Amount";
                });
        },
        drawBar: function () {
            var _this = this;

            var maxWidth = $(_this.element).width();
            var isSmallScreen = maxWidth < 500;
            if (isSmallScreen) {
                _this.settings.margin.right = 20;
                _this.settings.margin.left = 20;
            }

            var width = maxWidth,
                height = _this.settings.height;

            var x = d3.scale.ordinal().rangeRoundBands([_this.settings.margin.right, width - _this.settings.margin.left], .1);

            var y = d3.scale.linear().range([height, 0]);

            var svg = d3.select(".graph").append("svg")
                .attr("class", "chart")
                .attr("preserveAspectRatio", "xMidYMid")
                .attr("viewBox", "0 0 " + width + " " + height);

            x.domain(_this.data.map(function (d) { return d.label; }));
            y.domain([0, d3.max(_this.data, function (d) { return +d.value + (+d.value / 2.5); })]);//25% extra

            svg
                .append("g")
                .attr("class", "grid")
                .call(d3.svg.axis().scale(y)
                    .orient("left")
                    .ticks(4)
                    .tickSize(-width, 0, 0)
                    .tickFormat(function (d) { return "AED " + d; })
            )
                .selectAll("text")
                .attr("x", 0)
                .attr("y", -10)
                .style("text-anchor", _this.settings.dir == "rtl" ? "end" : "start");

            svg
                 .append("text")
                 .attr("class", "topTitle")
                 .attr("x", (width / 2))
                 .attr("y", 10)
                 .text(_this.settings.topTitle);

            svg
                 .append("text")
                 .attr("class", "bottomTitle")
                 .attr("x", (width / 2))
                 .attr("y", 30)
                 .text(_this.settings.bottomTitle);

            svg
               .selectAll(".bar")
               .data(_this.data)
               .enter()
               .append("rect")
               .attr("class", function (d) { return d.unbilled ? "bar unbilled" : "bar"; })
               .attr("x", function (d) { return x(d.label); })
               .attr("width", x.rangeBand())
               .attr("height", 0)
               .attr("y", height)
               .transition()
                .duration(450)
                .ease("ease-out")
               .attr("y", function (d) { return y(+d.value); })
               .attr("height", function (d) { return height - y(+d.value); });

            var yTextPadding = 20;

            svg.selectAll(".bartext")
                .data(_this.data)
                .enter()
                .append("text")
                .attr("class", "bartext")
                .attr("x", function (d) {
                    return x(d.label) + x.rangeBand() / 2;
                })
                .attr("y", function () {
                    return height + 20;
                })
                .text(function (d) {
                    return d.label;
                })
                .style("opacity", "0")
                //.attr("y", height)
                .transition()
                .duration(1000)
                .ease("ease-out")
                .delay(500)
                .style("opacity", "1");


            yTextPadding = 10;

            svg.selectAll(".bartoptext")
                .data(_this.data)
                .enter()
                .append("text")
                .attr("class", "bartoptext")
                .attr("x", function (d) {
                    return x(d.label) + x.rangeBand() / 2;
                })
                .text(function (d) {
                    //alert("AED " + d.value)
                    return "AED " + d.value;
                })
                //.style("opacity", "0")
                .attr("y", height)
                .transition()
                .duration(560)
                .ease("ease-in")
                .attr("y", function (d) {
                    if ((y(+d.value) - yTextPadding) > 390) return 390;
                    else return y(+d.value) - yTextPadding;
                });
                //.style("opacity", "1");


            yTextPadding = 30;

            svg
                .selectAll(".unbilled")
                .data(_this.data)
                .enter()
                .append("text")
                .attr("class", "bartoptext")
                .attr("x", function (d) {
                    return x(d.label) + x.rangeBand() / 2;
                })
                .filter(function (d) {
                    return d.unbilled;
                })
                .text(function () {
                    return "Unbilled Amount";
                })
                .attr("y", height)
                .transition()
                .duration(560)
                .ease("ease-in")
                .attr("y", function (d) {
                    return y(+d.value) - yTextPadding;
                });
        }
    });

    $.fn[pluginName] = function (options) {
        return new charter(this, options);
    };


})(jQuery, window, document);