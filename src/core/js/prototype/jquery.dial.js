﻿/*
 *  jQuery Dial - v1.0.0
 *
 *  Made by Prototype Interactive
 */
;
(function ($, window, document, undefined) {

    // Create the defaults once

    var templates = {
        "available": "<div class='dataTextWrapper'>" +
        "<div class='data'>" +
        "<div class='value available-value'>" +
        "<span>{{available}}</span>" +
        "<span class='measure'>{{measure}}</span>" +
        "</div>" +
        "<span class='underline'>" +
        "Available " +
        "</span>" +
        "<span class='out-of'>" +
        "Out of" +
        "</span>" +
        "<div class='value max-value'>" +
        "<span>{{max}}</span>" +
        "<span class='measure'>{{measure}}</span>" +
        "</div>" +
        "</div>" +
        "</div>",

        "show": "<div class='dataTextWrapper short'>" +
        "<div class='data'>" +
        "<div class='value'>" +
        "<span>{{available}}</span>" +
        "<span class='measure'>{{measure}}</span>" +
        "</div>" +
        "</div>" +
        "</div>",

        "only-available": "<div class='dataTextWrapper short'>" +
        "<div class='data'>" +
        "<div class='value'>" +
        "<span>{{available}}</span>" +
        "<span class='measure'>{{measure}}</span>" +
        "</div>" +
        "<span>" +
        "Available " +
        "</span>" +
        "</div>" +
        "</div>"
    }
    var pluginName = "protoDial",
        defaults = {
            fgcolor: '#66CA39', //Color of dial
            available: 100, //Use to calculate arc of dial
            max: 100, //Used to calculate arc of dial
            animateFrom: 0,
            measure: ' MB',
            toptext: '',
            bottomtext: '',
            responsive: false,
            innertemplate: "available" //which innet template to show
        };

    function dial(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, $(this.element).data());
        this.settings = $.extend({}, this.settings, options);
        if (this.settings.measure != "") {
            this.settings.measure = " " + this.settings.measure;
        }
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    $.extend(dial.prototype, {
        replaceTemplate: function () {
            var _this = this;
            var template = templates[_this.settings.innertemplate];
            for (var property in _this.settings) {
                if (_this.settings.hasOwnProperty(property)) {
                    template = template.replace(new RegExp("{{" + property + "}}", 'g'), _this.settings[property]);
                }
            }
            return template;
        },
        update: function (options) {
            this.settings = $.extend({}, this.settings, options);
            var _this = this;
            $(_this.element).closest(".knob")
                .find(".dataTextWrapper").remove()
                .end()
                .append(_this.replaceTemplate());
            _this.animate();
        },
        init: function () {
            var _this = this;

            if (_this.settings.available <= 0) {
                _this.settings.fgcolor = "";
            }

            if (_this.settings.max <= 0) {
                _this.settings.max = _this.settings.available;
                _this.settings.innertemplate = "only-available";
            }
            if(_this.settings.measure == ' MB'){
                if(_this.settings.max >= 1024){
                    _this.settings.max = parseFloat((_this.settings.max/1024).toFixed(2));
                    _this.settings.available = parseFloat((_this.settings.available/1024).toFixed(2));
                    _this.settings.measure = ' GB'
                }
            }
            if(_this.settings.max < _this.settings.available){
                var $bonus = _this.settings.available - _this.settings.max;
                if(_this.settings.measure == ' MB' || _this.settings.measure == ' GB'){
                    if(_this.settings.max >= 1024){
                        $bonus = parseFloat(($bonus/1024).toFixed(2))
                        _this.settings.bottomtext = "<span class='bonus' style='color:"+_this.settings.fgcolor+"'>+" + $bonus + " " +  _this.settings.measure + " Bonus!</span>" + _this.settings.bottomtext;
                    }
                    else _this.settings.bottomtext = "<span class='bonus' style='color:"+_this.settings.fgcolor+"'>+" + $bonus + " " +  _this.settings.measure + " Bonus!</span>" + _this.settings.bottomtext;
                }
                else _this.settings.bottomtext = "<span class='bonus' style='color:"+_this.settings.fgcolor+"'>+" + $bonus + /*" " +  _this.settings.measure + */" Bonus!</span>" + _this.settings.bottomtext;
            }
            var mobile = this.settings.responsive && $(window).width() <= 768;
            var newDashboard = $('.new-dashboard').length;
            //alert(newDashboard);
            $(_this.element).knob({
                'displayPrevious': true,
                //'lineCap': 'round',
                'width': mobile ? 90 : 200,
                'height': mobile ? 90 : 200,
                'fgColor': _this.settings.fgcolor,
                'readOnly': true,
                'max': _this.settings.max * 1000,
                'thickness': newDashboard ? .04 : .2,
                'displayInput': false
            })
                .after(_this.settings.bottomtext == "" ? "" : "<div class='bottomText'>" + _this.settings.bottomtext + "</div>")
                .after("<div class='topText regular'>" + _this.settings.toptext + "</div>")
                .after(_this.replaceTemplate());
            this.animate();
        },
        animate: function (from, to) {
            var _this = this;
            from = (from || _this.settings.animateFrom) * 1000;
            to = (to || _this.settings.available) * 1000;
            $(_this.element).val(from).trigger("change");
            $({animatedVal: from}).animate({animatedVal: to}, {
                duration: 1000,
                easing: "swing",
                progress: function () {
                    $(_this.element).val(Math.ceil(this.animatedVal)).trigger("change");
                }
            });
            return _this;
        }
    });

    $.fn[pluginName] = function (options) {
        this.each(function () {
            if (!$.data(this, "dial_" + pluginName)) {
                $.data(this, "dial_" + pluginName, new dial(this, options));
            }
        });
        return this;
    };


})(jQuery, window, document);