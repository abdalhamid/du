'use strict';

require(['jquery', 'lib/galleria/galleria-1.5.6.min'], function () {

    $(function () {

        function initialize() {

            if ($('.sfTmbStripAndImageOnSamePage').length <= 0) {
                return;
            }

            if (!Galleria) {
                throw 'Galleria is not loaded.';
            }

            Galleria.loadTheme('/common/scripts/lib/galleria/galleria.classic.min.js');
            Galleria.run('.sfTmbStripAndImageOnSamePage');

            Galleria.on('image', ImageGallerySelection_Changed);
        }

        function ImageGallerySelection_Changed(e) {

            var index = e.index;

            var contentBlocks = $(e.currentTarget).closest('.row').children('.sf_colsOut').eq(1).find('.sfContentBlock');

            if (contentBlocks.length > 1) {
                contentBlocks.hide();
                contentBlocks.eq(index).show();
            } else {
                contentBlocks.show();
            }

            if (e.scope && typeof e.scope.rescale == 'function') {
                e.scope.rescale();
            }
        }

        initialize();
    });

});