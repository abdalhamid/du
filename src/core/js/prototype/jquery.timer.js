﻿/*
 *  jQuery Timer - v1.0.0
 *
 *  Made by Prototype Interactive
 */
; (function ($, window, document, undefined) {

    // Create the defaults once
    var pluginName = "protoTimer",
        defaults = {
            //number of seconds for the timer to count down
            timerSeconds: 40,
            //function to run when the timer ends
            callback: null,
            //true is the mer should use jquery.knob instead of proprietary code
            useKnob: false,
            //text to show above the timer
            topText: "Time Limit",
            //small text to show below the timer
            bottomText: "This is a fine print"
        };

    function timer(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }


    $.extend(timer.prototype, {
        init: function () {
            var _this = this;
            var timerFunc, timerFinish, timerSeconds = _this.settings.timerSeconds;

            function formatSecondsAsTime(secs) {
                var hr = Math.floor(secs / 3600);
                var min = Math.floor((secs - (hr * 3600)) / 60);
                var sec = Math.floor(secs - (hr * 3600) - (min * 60));
                if (min < 10) { min = "0" + min; }
                if (sec < 10) { sec = "0" + sec; }
                return min + ':' + sec;
            }

            if (_this.settings.useKnob) {
                $(_this.element).html('<div id="counter">' + _this.settings.topText + '<span>' + formatSecondsAsTime(seconds) + '</span></div><input class="knob second" data-min="0" data-max="' + _this.settings.timerSeconds + '" data-bgcolor="#f8f8f8" data-fgcolor="#0c8d12"data-displayinput=false data-width="200" data-height="200" data-thickness="1"><div id="fineprint">' + _this.settings.bottomText + '</div>');
                $(".second").knob({ "readOnly": true });
                var seconds = _this.settings.timerSeconds;

                function clock() {
                    seconds--;
                    $(".second").val(seconds).trigger("change");
                    $("#counter>span").html(formatSecondsAsTime(seconds));
                    if (seconds > 0) {
                        setTimeout(function () { clock(); }, 1000);
                    }
                    else
                    {
                        if (_this.settings.callback && typeof _this.settings.callback == "function") {
                            _this.settings.callback();
                        }
                    }
                }
                clock();
            } else {
                function drawTimer(percent, seconds) {
                    $(_this.element).html('<div id="counter">' + _this.settings.topText + '<span>' + formatSecondsAsTime(seconds) + '</span></div><div class="filler"></div><div id="slice"' + (percent > 50 ? ' class="gt50"' : '') + '><div class="pie"></div>' + (percent > 50 ? '<div class="pie fill"></div>' : '') + '</div><div id="fineprint">' + _this.settings.bottomText + '</div>');
                    var deg = 360 / 100 * percent;
                    $('#slice .pie').css({
                        '-moz-transform': 'rotate(' + deg + 'deg)',
                        '-webkit-transform': 'rotate(' + deg + 'deg)',
                        '-o-transform': 'rotate(' + deg + 'deg)',
                        'transform': 'rotate(' + deg + 'deg)'
                    });
                }
                function stopWatch() {
                    var seconds = (timerFinish - (new Date().getTime())) / 1000;
                    if (seconds <= 0) {
                        drawTimer(100, 0);
                        clearInterval(timerFunc);
                        if (_this.settings.callback && typeof _this.settings.callback == "function") {
                            _this.settings.callback();
                        }
                    } else {
                        var percent = 100 - ((seconds / timerSeconds) * 100);
                        drawTimer(percent, seconds);
                    }
                }
                timerFinish = new Date().getTime() + (timerSeconds * 1000);
                timerFunc = setInterval(function () { stopWatch(); }, 50);
            }
        },
    });

    $.fn[pluginName] = function (options) {
        this.each(function () {
            if (!$.data(this, "timer_" + pluginName)) {
                $.data(this, "timer_" + pluginName, new timer(this, options));
            }
        });
        return this;
    };

})(jQuery, window, document);