﻿/*
 *  Alert View - v1.0.0
 *
 *  Made by Prototype Interactive
 */
;
var alertView = function () {
    var template =
    [
        '<div class="alert {{css}}">',
        '{{message}}',
        '<div class="action">',
        '<a href="{{link}}">{{action}}</a>',
        '</div>',
        '<a href="" class="close"></a>',
        '</div>'
    ].join('');
    function set(options, css) {
        var temp = $(template
            .replace("{{css}}", css)
            .replace("{{message}}", options.message)
            .replace("{{action}}", options.action)
            .replace("{{link}}", options.link));

        $(".contentIn").prepend(temp);

        setTimeout(function() {
            temp.addClass("active");
        }, 0);

        temp.find(".close").one("click", function (event) {
            event.preventDefault();
            var $this = $(this).closest(".alert");
            $this.removeClass("active");
            setTimeout(function () {
                $this.remove();
            }, 400);
            
        });
    }
    return {
        alert: function (options) { set(options, options._class); },
        warning: function (options) { set(options, "warning"); },
        info: function (options) { set(options, "info"); },
        error: function (options) { set(options, "error"); },
        success: function (options) { set(options, "success"); },
    }
}();