'use strict';

require(['jquery'], function () {

    $(function () {

        if ($('.fixed-plans').length === 0) {
            return;
        }

        var $container;

        var isTouch;

        var isAr = $('html').attr('dir') === "rtl";

        var firstTimeLayoutComplete = true;

        var isInZone, selectedEmirate, selectedArea, map, marker;

        var $searchInput = $('#search-city');

        var $mapSearchInput = $('#map-search');

        var tvNotAvailableMessage = $(".tv-not-available");

        var polygons = [];

        var infoWindow = new google.maps.InfoWindow();

        var geocoder = new google.maps.Geocoder();

        var isInZone = false;

        var $mapBtn = $('#show-map');

        var $mapCont = $('#map-container');

        var $externalMapInput = $('#search-city');

        var $mapCloseBtn = $('#map-close');

        var $filterPlansBtn = $('#filter-plans');

        var userSwiping = false;

        var updateFiltersSummary = function updateFiltersSummary(area) {
            if (area) {
                $("[data-plan-filters-summary-area] h5").text(area);
            } else {
                $("[data-plan-filters-summary-area] h5").text("-");
            }
        };

        var showHideFaqsTc = function showHideFaqsTc(isInZone) {
            if (isInZone) {
                $(".faqs-tc-with-tv").addClass("active");
                $(".faqs-tc-without-tv").removeClass("active");
            } else {
                $(".faqs-tc-with-tv").removeClass("active");
                $(".faqs-tc-without-tv").addClass("active");
            }
        };

        var filterBasedOnQueryStrings = function filterBasedOnQueryStrings() {
            var zoneQueryString = getUrlParameterByName('zone');
            var cityQueryString = getUrlParameterByName('city');
            var emirateQueryString = getUrlParameterByName('emirate');
            var city = cityQueryString || 'Dubai';

            selectedEmirate = emirateQueryString || 'Dubai';

            isInZone = zoneQueryString == 'in' || zoneQueryString == null;

            if (isInZone) {
                $searchInput.val(city);
                $mapSearchInput.val(city);
            } else {
                $searchInput.val(cityQueryString);
                $mapSearchInput.val(cityQueryString);
            }

            updateFiltersSummary(city);
            showHideFaqsTc(isInZone);
            showHideTvNotAvailableMessage(isInZone);

            //call the isotope to filter wether it's is zone or not
        };

        var initGoogleMaps = function initGoogleMaps() {
            var placeMarker = function placeMarker(location) {
                if (marker) {
                    marker.setMap(null);
                }
                marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                marker.setIcon('/common/images/map-icon.png');
                $('#map-sticky').addClass('active');
                $mapCont.addClass('map-sticky-active');
            };

            function initMap() {
                var options = {
                    center: {
                        lat: 25.0537936,
                        lng: 55.1572327
                    },
                    zoom: 10,
                    mapTypeControl: false,
                    scrollwheel: false
                };
                map = new google.maps.Map(document.getElementById('map'), options);

                var getPotentialEmirateArea = function getPotentialEmirateArea(latlng, callback) {
                    var getPotentialArea = function getPotentialArea(results) {
                        var potentialAreas = [];

                        results.forEach(function (result) {
                            result.address_components.filter(function (item) {
                                if (item.types.indexOf('sublocality') > -1) {
                                    potentialAreas.push(item.short_name);
                                }
                            });
                        });

                        return potentialAreas.length > 0 ? potentialAreas[0] : '';
                    };

                    var getPotentialEmirate = function getPotentialEmirate(results) {
                        var potentialEmirates = [];

                        results.forEach(function (result) {
                            result.address_components.filter(function (item) {
                                if (item.types.indexOf('administrative_area_level_1') > -1) {
                                    potentialEmirates.push(item.short_name);
                                }
                            });
                        });

                        return potentialEmirates.length > 0 ? potentialEmirates[0] : '';
                    };

                    geocoder.geocode({
                        'location': latlng
                    }, function (results, status) {
                        if (status == 'OK') {

                            selectedEmirate = getPotentialEmirate(results);
                            selectedArea = getPotentialArea(results);

                            if (callback) {
                                callback(selectedEmirate, selectedArea);
                            }
                        }
                    });
                };
                $.getJSON('/common/scripts/prototype/fixed-plans/inzone-areas.json', function (data) {

                    data.features.forEach(function (item) {
                        var coordinates = item.geometry.coordinates[0].map(function (item) {
                            return {
                                lng: parseFloat(item[0]),
                                lat: parseFloat(item[1])
                            };
                        });

                        var featurePolygon = new google.maps.Polygon({
                            paths: coordinates,
                            name: item.properties.Name,
                            fillColor: 'transparent',
                            strokeColor: 'transparent',
                            strokeOpacity: 0.8
                        });

                        featurePolygon.setMap(map);

                        polygons.push(featurePolygon);

                        google.maps.event.addListener(featurePolygon, 'click', function (event) {
                            if (!userSwiping) {
                                var location = event.latLng;

                                placeMarker(location);

                                infoWindow.setOptions({
                                    content: '<div class="info-window-name">' + this.name + '</div>',
                                    pixelOffset: new google.maps.Size(0, -45),
                                    position: event.latLng
                                });

                                infoWindow.open(map);

                                isInZone = true;

                                updateFiltersSummary(this.name);

                                var polygonAreaName = this.name;

                                if (!event.triggered) {
                                    $searchInput.val(this.name);
                                    $mapSearchInput.val(this.name);
                                }

                                getPotentialEmirateArea(location, function (selectedEmirate) {
                                    selectedArea = polygonAreaName;
                                });

                                console.log(isInZone);
                            }
                        });
                    });
                });

                google.maps.event.addListener(map, 'click', function (event) {
                    if (!userSwiping) {
                        var location = event.latLng;
                        placeMarker(event.latLng);
                        infoWindow.close();
                        getPotentialEmirateArea(event.latLng, function (selectedEmirate, selectedArea) {

                            var area = selectedArea;
                            if ($.trim(area).length <= 0) {
                                area = selectedEmirate;
                            }

                            updateFiltersSummary(area);
                            if (!event.triggered) {
                                $searchInput.val(area);
                                $mapSearchInput.val(area);
                            }

                            if (event.callback) {
                                event.callback();
                            }
                        });

                        var isPointOfInterest = event.placeId;

                        if (isPointOfInterest) {
                            //stop the infoWindow from appearing
                            event.stop();
                        }

                        isInZone = false;

                        console.log(isInZone);
                    }
                });

                $mapCont.on('touchmove', function () {
                    userSwiping = true;
                });

                $mapCont.on('touchend', function () {
                    userSwiping = false;
                });

                google.maps.event.addListener(infoWindow, 'domready', function () {
                    $('#map .gm-style-iw').parent().addClass('info-window');
                });
            }

            var alreadyLoaded = document.readyState == 'complete';

            if (alreadyLoaded) {
                initMap();
            } else {
                google.maps.event.addDomListener(window, "load", initMap);
            }

            var placeChangedCallBack = function placeChangedCallBack(place, $input) {
                $input.trigger('change');
                if (place.geometry) {
                    map.setCenter(place.geometry.location);
                    map.setZoom(10);

                    var isPolygonClicked = false;

                    for (var i = 0; i < polygons.length; i++) {
                        if (google.maps.geometry.poly.containsLocation(place.geometry.location, polygons[i])) {

                            google.maps.event.trigger(polygons[i], 'click', {
                                latLng: place.geometry.location,
                                triggered: true
                            });
                            isPolygonClicked = true;
                            break;
                        }
                    }

                    var callback = function callback() {

                        var isMapOpened = $mapCont.hasClass('active');
                        if (!isMapOpened) {
                            execFilters();
                            showHideTvNotAvailableMessage(isInZone);
                            showHideFaqsTc(isInZone);
                            pushContextAwareDataLayer('Emirates', selectedEmirate);
                            pushContextAwareDataLayer('Area', selectedArea);
                            pushContextAwareDataLayer('Service', isInZone ? 'TV, Internet & landline' : 'Internet & landline');
                            scrollToFilters();
                        }
                    };

                    if (!isPolygonClicked) {
                        google.maps.event.trigger(map, 'click', {
                            latLng: place.geometry.location,
                            triggered: true,
                            callback: callback
                        });
                    } else {
                        callback();
                    }
                }
            };

            new google.maps.places.Autocomplete($searchInput[0], {
                componentRestrictions: {
                    country: "AE"
                }
            }).addListener('place_changed', function () {
                placeChangedCallBack(this.getPlace(), $searchInput);
            });
            new google.maps.places.Autocomplete($mapSearchInput[0], {
                componentRestrictions: {
                    country: "AE"
                }
            }).addListener('place_changed', function () {
                placeChangedCallBack(this.getPlace(), $mapSearchInput);
            });

            $searchInput.on('change', function () {
                $mapSearchInput.val($(this).val());
            });
            $mapSearchInput.on('change', function () {
                $searchInput.val($(this).val());
            });

            var $autoCompleteCont = $('.pac-container');

            var adjustAutocompleteDirection = function adjustAutocompleteDirection() {
                if (this.value.length > 0) {
                    if ($autoCompleteCont.length == 0) {
                        $autoCompleteCont = $('.pac-container');
                    }

                    var hasEnglishCharacters = $(this).val().match(/[a-zA-Z]{1,}/g);

                    if (hasEnglishCharacters) {
                        $autoCompleteCont.removeClass('rtl');
                    } else {
                        $autoCompleteCont.addClass('rtl');
                    }
                }
            };

            var debounce = function debounce(func, wait, immediate) {
                var timeout;
                return function () {
                    var context = this,
                        args = arguments;
                    var later = function later() {
                        timeout = null;
                        if (!immediate) func.apply(context, args);
                    };
                    var callNow = immediate && !timeout;
                    clearTimeout(timeout);
                    timeout = setTimeout(later, wait);
                    if (callNow) func.apply(context, args);
                };
            };

            var debouncedAdjustAutocompleteDirection = debounce(adjustAutocompleteDirection, 200);

            $searchInput.on('keyup', debouncedAdjustAutocompleteDirection);
            $mapSearchInput.on('keyup', debouncedAdjustAutocompleteDirection);

            $searchInput.one('change', debouncedAdjustAutocompleteDirection);
            $mapSearchInput.one('change', debouncedAdjustAutocompleteDirection);
        };

        var showHideTvNotAvailableMessage = function showHideTvNotAvailableMessage(isInZone) {
            var tvNotAvailableMessage = $(".tv-not-available");

            if (isInZone) {
                tvNotAvailableMessage.removeClass("is-visible");
            } else {
                tvNotAvailableMessage.addClass("is-visible");
            }
        };

        var createSelectButtonLink = function createSelectButtonLink(button) {
            var url = $(button).attr("data-href-pattern");
            var journey = $.trim($(button).closest(".offers").find("li").attr("data-plan-title"));
            var isVariationAdded = $(button).closest(".offers").find("li [data-offers-add]").css("display") === "none";
            var variationId = isVariationAdded ? $(button).closest(".offers").find("li").attr("data-variation-id") : "";
            var emirate = selectedEmirate || '';
            var area = selectedArea || '';
            var planTitle = $(button).closest(".card-item").attr("data-plantitle");
            var monthlyFee = $(button).closest(".card-item").attr("data-monthlyfee");
            var oneTimeFee = $(button).closest(".card-item").attr("data-onetimefee");
            var contractPeriod = $(button).closest(".card-item").attr("data-contractperiod");
            var downloadSpeed = $(button).closest(".card-item").attr("data-downloadspeed");

            var monthlyFeeValue = $.trim(monthlyFee).match(/[0-9.,]+/);
            var subFilter = isInZone ? '.with-tv' : '.without-tv';
            var planCategory = subFilter === '.with-tv' ? 'TV, Internet & landline' : 'Internet & landline';
            var packageType = subFilter.substring(1);

            url = url.replace("[journey]", encodeURIComponent(journey));
            url = url.replace("[selectedOfferVariation]", encodeURIComponent(variationId));
            url = url.replace("[emirate]", encodeURIComponent(emirate));
            url = url.replace("[area]", encodeURIComponent(area));
            url = url.replace("[planTitle]", encodeURIComponent(planTitle));
            url = url.replace("[monthlyFee]", encodeURIComponent(monthlyFee));
            url = url.replace("[oneTimeFee]", encodeURIComponent(oneTimeFee));
            url = url.replace("[contractPeriod]", encodeURIComponent(contractPeriod));
            url = url.replace("[downloadSpeed]", encodeURIComponent(downloadSpeed));
            url = url.replace("[packageType]", encodeURIComponent(packageType));

            $(button).attr("href", url);

            var selectTags = pushContextAwareDataLayer(planCategory, 'AED ' + monthlyFee + '_select', true);
            var infoTags = pushContextAwareDataLayer(planCategory, 'AED ' + monthlyFee + '_info', true);

            $(button).attr('onclick', 'dataLayer.push(' + JSON.stringify(selectTags) + ');');
            $(button).parent().find('.showPlanDetailsButton').attr('onclick', 'dataLayer.push(' + JSON.stringify(infoTags) + ');');
        };

        var createSelectButtonLinkAll = function createSelectButtonLinkAll() {
            $("a.buyPlanButton").each(function () {
                createSelectButtonLink(this);
            });
        };

        var handleSelectButtonLinks = function handleSelectButtonLinks() {
            $("a.buyPlanButton").each(function () {
                if (!$(this).attr("data-href-pattern")) {
                    $(this).attr("data-href-pattern", $(this).attr("href"));
                }
                createSelectButtonLink(this);
            });
        };

        var displayMessageIfNoCards = function displayMessageIfNoCards(filtersApplied) {
            if (!$container) {
                return;
            }

            var isotopeHasCards = $container.data("isotope").filteredItems.length;

            if (!isotopeHasCards) {
                if (filtersApplied) {
                    $(".no-cards-message.no-results").addClass("show-message");
                } else {
                    $(".no-cards-message.no-area-selected").addClass("show-message");
                }
            } else {
                $(".no-cards-message").removeClass("show-message");
            }
        };

        var execFilters = function execFilters() {
            var parameters = getUriParameters();
            var packageType = $.trim(parameters.packageType);

            if (packageType == 'with-tv') {
                isInZone = true;
            } else if (packageType == 'without-tv') {
                isInZone = false;
            }

            execSubFilters(isInZone);
        };

        var execSubFilters = function execSubFilters(isInZone) {
            var filterValue = isInZone ? '.with-tv' : '.without-tv';
            if ($container) {
                firstTimeLayoutComplete = true;
                $container.isotope({
                    filter: filterValue
                });
            }

            displayMessageIfNoCards(true);

            createSelectButtonLinkAll();
        };

        var getUrlParameterByName = function getUrlParameterByName(name, url) {
            if (!url) {
                url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        };

        var handleShowOnJourneyBindings = function handleShowOnJourneyBindings() {
            var journeyUrl = getUrlParameterByName('journey');
            if (!journeyUrl) {
                journeyUrl = "";
            }

            $("[data-show-on-journey]").each(function () {
                var journeyFilter = $(this).attr("data-show-on-journey");

                if (journeyFilter === journeyUrl) {
                    $(this).removeClass("hide");
                } else {
                    $(this).addClass("hide");
                }
            });
        };

        var handleStickyHeader = function handleStickyHeader() {
            var $planFilters = $('.plan-filters');
            var $stickyFiltersSummary = $(".sticky-filters-summary");

            var appendStickyClass = function appendStickyClass() {
                var breakpoint = $planFilters.position().top + $planFilters.height() + 100;
                var windowScrollTop = $(window).scrollTop();

                if (windowScrollTop >= breakpoint && !$stickyFiltersSummary.hasClass("sticky")) {
                    $stickyFiltersSummary.addClass("sticky");
                } else if (windowScrollTop < breakpoint && $stickyFiltersSummary.hasClass("sticky")) {
                    $stickyFiltersSummary.removeClass("sticky");
                }
            };

            appendStickyClass();

            $(window).scroll($.throttle(250, appendStickyClass));
            $(window).on("orientationchange", appendStickyClass);
        };

        function getUriParameters() {

            var parameters = {};
            var search = decodeURI(document.location.search);
            var pattern = /[&?]([^=^&^#]+)=([^&^#]+)/g;
            var match = null;

            while (match = pattern.exec(search)) {
                var parameter = match[1];
                var value = match[2];

                parameters[parameter] = decodeURIComponent(value.replace(/\+/g, ' '));
            }

            return parameters;
        }

        function formatDataLayerCategory(category) {
            var fallbackCategory = 'Homeservices_plan';
            var result = fallbackCategory;
            if ($.trim(category).length > 2) {
                result = category[0].toUpperCase() + category.toLowerCase().replace(/\s/g, '_').substring(1);
            } else if ($.trim(category).length > 1) {
                result = journey[0].toUpperCase();
            }

            return result;
        }

        function pushContextAwareDataLayer(action, label, deferPush) {
            var parameters = getUriParameters();
            var journey = parameters.journey;
            var category = formatDataLayerCategory(journey);

            return pushDataLayer(category, action, label, deferPush);
        }

        function pushDataLayer(category, action, label, deferPush) {
            var tags = {
                event: 'click',
                category: category,
                action: action,
                label: label
            };

            if (!deferPush) {
                dataLayer.push(tags);
            }

            return tags;
        }

        var getTopPositionFilters = function getTopPositionFilters() {
            var subTitleTop = $('.sub-title').offset().top;
            var headerHeight = $(".eitc-top-pane").outerHeight();

            return subTitleTop - headerHeight;
        };

        var getTopPositionMap = function getTopPositionFilters() {
            var subTitleTop = $('#map-container').offset().top;
            var headerAndSummaryHeight = $(".eitc-top-pane").outerHeight() + 20;

            return subTitleTop - headerAndSummaryHeight;
        };

        var scrollToFilters = function scrollToFilters() {
            $('html, body').stop(true).animate({
                scrollTop: getTopPositionFilters()
            }, 300);
        };

        var scrollToMap = function scrollToFilters() {
            $('html, body').stop(true).animate({
                scrollTop: getTopPositionMap()
            }, 300);
        };

        var handlePopup = function handlePopup() {
            var closePpopup = function closePpopup() {
                $("html").removeClass("product-page-add-popup-open");
            };

            $("[data-open-popup]").on("click", function (e) {
                e.preventDefault();
                var cardLink = $(this).attr('href');

                if (!/[&|?]emirate=/.test($(this).attr('href'))) {
                    cardLink += '&emirate=' + selectedEmirate;
                }

                var existingLink = cardLink + '&planType=existing';

                $('[data-existing-plan]').attr('href', existingLink);
                $('[data-new-plan]').attr('href', cardLink);

                $("html").addClass("product-page-add-popup-open");
            });

            $(".product-page-add-popup .close-button").on("click", closePpopup);
            $(".product-page-add-popup-overlay").on("click", closePpopup);
        };

        var initializeMapFocus = function initializeMapFocus() {

            var container = $('body');

            container.on('focus', '.map-container .map-search-input', function () {

                var searchField = $(this);
                var mapContainer = searchField.closest('.map-container');

                var scrollPosition = mapContainer.offset().top - $('.eitc-breadcrumbs-wrapper').outerHeight();

                $('html, body').stop(true).animate({
                    scrollTop: scrollPosition
                }, 300);
            });
        };

        require(['require', 'isotope', 'jquery.sticky-kit.min', 'lib/jquery.matchheight.min', /*"bootstrapselect",*/ "lib/modernizr-2.6.2.min", /*'bootstrap',*/ 'lib/jquery.observer.min', "lib/jquery.ba-throttle-debounce.min"], function (require, Isotope) {
            isTouch = $("html").hasClass("touch");

            handleShowOnJourneyBindings();
            handleStickyHeader();

            initGoogleMaps();
            initializeMapFocus();

            filterBasedOnQueryStrings();

            $mapBtn.on('click', function () {
                $mapCont.addClass('active');
                $mapBtn.attr('disabled', 'disabled');
                $externalMapInput.attr('disabled', 'disabled');
                scrollToMap();
            });

            $mapCloseBtn.on('click', function (e) {
                e.preventDefault();
                $mapCont.removeClass('active');
                $mapBtn.removeAttr('disabled');
                $externalMapInput.removeAttr('disabled');
            });

            $filterPlansBtn.on('click', function (e) {
                e.preventDefault();
                $mapCloseBtn.trigger('click');
                execFilters();
                showHideTvNotAvailableMessage(isInZone);
                showHideFaqsTc(isInZone);
                pushContextAwareDataLayer('Emirates', selectedEmirate);
                pushContextAwareDataLayer('Area', selectedArea);
                pushContextAwareDataLayer('Service', isInZone ? 'TV, Internet & landline' : 'Internet & landline');
                scrollToFilters();
            });

            require(['bridget'], function (jQueryBridget) {

                $('.plans .hidden').removeClass('hidden').addClass('softHidden');

                var addGtmDataLayer = function addGtmDataLayer(eventAction, eventLayer) {
                    dataLayer.push({
                        EventCategory: "fixed_plans",
                        EventAction: eventAction,
                        EventLabel: eventLayer
                    });
                };

                //Initialize matchHeight
                var matchRowHeights = function matchRowHeights(visibleOnly) {
                    var matchHeight = function matchHeight(selectedElements) {
                        if (selectedElements.length == 0) return;
                        var elements = selectedElements;
                        var largest = 0;

                        var i = elements.length;
                        while (i--) {
                            elements[i].style.height = "auto";

                            var height = elements[i].offsetHeight;
                            if (height > largest) {

                                largest = height;
                            }
                        }
                        var j = elements.length;
                        while (j--) {
                            elements[j].style.height = largest + "px";
                        }
                    };

                    matchHeight($(".filter-catg > h4"), {});

                    var visible = '';
                    if (visibleOnly) {
                        visible = ':visible';
                    }
                    matchHeight($('.card-item:visible > .card-content > .you-pay > h4' + visible), {});
                    matchHeight($('.card-item:visible > .card-content > .you-pay > h5' + visible), {});
                    matchHeight($('.card-item:visible > .card-content > .you-get > h4' + visible), {});

                    var firstCard = $('.card-item:visible').eq(0);

                    firstCard.find('.you-get > div:not(".benefits")').each(function (index, element) {
                        var elementIndex = $(element).index() + 1;
                        matchHeight($('.card-item:visible > .card-content > .you-get > div:not(".benefits"):nth-child(' + elementIndex + ')' + visible), {});
                    });

                    firstCard.find('.you-get > div.benefits > div').each(function (index, element) {
                        var elementIndex = $(element).index() + 1;
                        matchHeight($('.card-item:visible > .card-content > .you-get > div.benefits > div:nth-child(' + elementIndex + ') > h5' + visible), {});
                        matchHeight($('.card-item:visible > .card-content > .you-get > div.benefits > div:nth-child(' + elementIndex + ') > h6' + visible), {});
                    });

                    matchHeight($('.card-item.without-tv > .card-content > .you-get > .item0:visible > h5' + visible + ', ' + '.card-item.without-tv > .card-content > .you-get > .item1:visible > h5' + visible), {});
                    matchHeight($('.card-item.without-tv > .card-content > .you-get > .item0:visible' + visible + ', ' + '.card-item.without-tv > .card-content > .you-get > .item1:visible'), {});
                    matchHeight($('.card-item.with-tv > .card-content > .you-get > .item0 > h5' + visible + ', ' + '.card-item.with-tv > .card-content > .you-get > .item1 > h5' + visible), {});
                    matchHeight($('.card-item.with-tv > .card-content > .you-get > .item0' + visible + ', ' + '.card-item.with-tv > .card-content > .you-get > .item1' + visible), {});

                    matchHeight($('.card-item:visible > .card-content > .you-get > div:not(".benefits")' + visible), {});

                    //$('.card-item.with-tv > .card-content > .you-get > div.item0').each(function () {
                    //    var elemHeight = $(this).height();
                    //    $(this).closest(".you-get").find(".benefits").css("padding-top", elemHeight + "px");
                    //});
                };

                $("[data-toggle=popover]").popover({
                    trigger: 'focus',
                    container: 'body',
                    html: true,
                    placement: function placement(context, source) {
                        var position = $(source).offset().left;
                        var windowWidth = $(window).width();

                        if (position >= windowWidth - 100) {
                            return "left";
                        }
                        if (position <= 100) {
                            return "right";
                        }

                        return "auto top";
                    }
                });

                // Fixes to iPhone and iPad
                if (navigator.userAgent.indexOf('iPad') >= 0 || navigator.userAgent.indexOf('iPhone') >= 0) {
                    // isolate the fix to apply only when a tooltip is shown
                    $("[data-toggle=popover]").on("shown.bs.popover", function () {
                        $('body').css('cursor', 'pointer');
                    });

                    // Return cursor to default value when tooltip hidden.
                    // Use 'hide' event (as opposed to "hidden") to ensure .css() fires before
                    // 'shown' event for another tooltip.
                    $("[data-toggle=popover]").on("hide.bs.popover", function () {
                        $('body').css('cursor', 'auto');
                    });

                    //Fixes to iPad only
                    if (navigator.userAgent.indexOf('iPad') >= 0) {
                        //Layout change doesn't work on iPad, so initiate matchHeight here.
                        setTimeout(function () {
                            matchRowHeights(true);
                        }, 500);
                    }
                }

                matchRowHeights();

                //Initialize isotope
                jQueryBridget('isotope', Isotope, $);
                if ($('.plan-cards').length > 0) {
                    $container = $('.isotope').isotope({
                        isOriginLeft: isAr ? false : true,
                        itemSelector: '.card-item',
                        layoutMode: 'fitRows',
                        getSortData: {
                            price: '[data-monthlyFee] parseInt',
                            planindex: '[data-planindex] parseInt'
                        },
                        sortBy: 'price',
                        sortAscending: false,
                        isInitLayout: false
                    });
                }

                window.debounceMatchHeight = false;
                $container.isotope('on', 'arrangeComplete', function () {
                    if (window.debounceMatchHeight == false) {
                        window.debounceMatchHeight = true;

                        $container.isotope('layout');
                    } else {
                        window.debounceMatchHeight = false;
                    }
                });

                var forceCardsRepositioning = function forceCardsRepositioning() {
                    try {
                        $container.isotope();
                    } catch (e) {}
                };

                var fixPopovers = function fixPopovers() {
                    //initializePopover();
                };

                var updateHeightAndPosition = function updateHeightAndPosition() {
                    setTimeout(function () {
                        matchRowHeights(true);
                        setTimeout(forceCardsRepositioning, 200);
                    }, 500);
                };

                var recenterPopup = function recenterPopup(popupId) {
                    var popup = $("#" + popupId);
                    popup.css("margin-left", -popup.outerWidth() / 2);

                    if ($('body.mobile').length > 0) {
                        var viewportFromTop = $(window).scrollTop();
                        if (viewportFromTop === 0) {
                            popup.css("position", "fixed");
                            popup.css("top", "10px");
                        } else if ($(window).offset()) {
                            popup.css("position", "absolute");
                            popup.css("top", $(window).offset().top + "px");
                        }
                    }
                };

                var fixPopup = function fixPopup() {
                    if ($("#PlanDetailsPopup").css("display") !== "none") {
                        recenterPopup("PlanDetailsPopup");
                    }
                };

                $(window).on("orientationchange", function () {
                    var setTimeoutRifle = function setTimeoutRifle(callback, timeout, numTimes) {
                        var idx = 1;

                        var setTimeoutRifleElab = function setTimeoutRifleElab() {
                            setTimeout(function () {
                                try {
                                    callback();
                                } catch (e) {}

                                idx++;

                                if (idx <= numTimes) {
                                    setTimeoutRifleElab();
                                }
                            }, timeout);
                        };

                        setTimeoutRifleElab();
                    };

                    // updateHeightAndPosition();
                    setTimeoutRifle(function () {
                        matchRowHeights(true);
                        setTimeoutRifle(forceCardsRepositioning, 100, 2);
                    }, 100, 5);

                    setTimeoutRifle(function () {
                        fixPopup();

                        if (window.pubsub) {
                            window.pubsub.publish("autoMatchheight/refresh", []);
                        }
                    }, 100, 5);

                    $('.planDetailsPopup:visible').each(function () {

                        var popupWidth = $(this).width();
                        $(this).css('margin-left', -(popupWidth / 2) + 'px');

                        this.style.removeProperty('width');
                    });
                });

                $container.isotope("on", "layoutComplete", function () {
                    // Mark the page as fully rendered
                    setTimeout(function () {
                        $('.plan-cards').addClass('loaded');
                    }, 500);

                    fixPopup();

                    setTimeout(function () {
                        matchRowHeights(true);
                        if (firstTimeLayoutComplete) {
                            // It shouldn't run every time because it slows down the site
                            setTimeout(forceCardsRepositioning, 200);
                            firstTimeLayoutComplete = false;
                        }

                        if (window.pubsub) {
                            window.pubsub.publish("autoMatchheight/refresh", []);
                        }
                    }, 500);
                });

                // Fix to close Popover on iPad
                if ($("html").hasClass("ipad")) {
                    $(document).on("click touchstart", function (e) {
                        if ($(e.target).data("toggle") !== "popover" && $(e.target).parents("[data-toggle='popover']").length === 0 && $(e.target).parents(".popover.in").length === 0) {
                            $("[data-toggle='popover']").popover("hide");
                        }
                    });
                }

                // init plans table filter on pageload
                setTimeout(function () {
                    $('.plans-master > .brandFilter > a:eq(0)').trigger('click');

                    $('.sticky-content .lined-buttons:first a:eq(0)').trigger('click');
                }, 50);

                //Initialize controls
                if ($('.you-get').length > 0) {
                    $('a[href="#ContentPlaceHolderMain"]').click(scrollToFilters);

                    $('.you-get').each(function () {
                        $('> div:not(".benefits")', $(this)).each(function (i) {
                            $(this).addClass('item' + i);
                        });
                        $('> div > div', $(this)).each(function (i) {
                            $(this).addClass('benefit' + i);
                        });
                    });

                    $('.you-get > div:not(".benefits"), .you-get > div > div').hover(function () {
                        $('.' + $(this).attr('class')).addClass('highlight');
                    }, function () {
                        $('.you-get > div:not(".benefits"), .you-get > div > div').removeClass('highlight');
                    });
                }

                //Plan information popup
                $('a[data-reveal-id="PlanDetailsPopup"]').on('click', function (e) {
                    e.preventDefault();

                    var cardItem = $(this).closest('.card-item');
                    var planInformationUrl = $(this).attr('href');
                    var planInformationPrintUrl = planInformationUrl;
                    var planInformationDocumentUrl = cardItem.attr('data-documentUrl');
                    var purchasePlanUrl = cardItem.find('.buyPlanButton').attr('href');

                    if (planInformationPrintUrl.indexOf('?') == -1) {
                        planInformationPrintUrl += '?print=true';
                    } else {
                        planInformationPrintUrl += '&print=true';
                    }

                    var planDetailsPopup = $('#PlanDetailsPopup');

                    planDetailsPopup.find('.loadingOverlay').show();

                    planDetailsPopup.find('.planDetailsFrame').attr('src', planInformationUrl);
                    planDetailsPopup.find('.printButton').attr('href', planInformationPrintUrl).attr('target', '_blank');
                    planDetailsPopup.find('.downloadButton').attr('href', planInformationDocumentUrl).attr('target', '_blank');
                    planDetailsPopup.find('.buyButton').attr('href', purchasePlanUrl);

                    recenterPopup("PlanDetailsPopup");

                    var width = Math.round(planDetailsPopup.width());
                    planDetailsPopup[0].style.setProperty('width', width + 'px', 'important');
                });

                $('#PlanDetailsPopup .close-reveal-modal').on('click', function () {

                    var planDetailsPopup = $('#PlanDetailsPopup');

                    planDetailsPopup.find('.planDetailsFrame').attr('src', 'about:blank');
                    planDetailsPopup.find('.printButton').attr('href', 'javascript:void(0);').attr('target', null);
                    planDetailsPopup.find('.downloadButton').attr('href', 'javascript:void(0);').attr('target', null);
                    planDetailsPopup.find('.buyButton').attr('href', 'javascript:void(0);');
                    planDetailsPopup[0].style.removeProperty('width');
                });

                $('#PlanDetailsPopup .printButton').on('click', function (e) {

                    e.preventDefault();

                    if ($('body.android').length > 0 || $('.firefox .desktop').length > 0) {
                        var printUrl = $(this).attr('href');
                        window.open(printUrl);
                    } else {
                        document.PlanDetailsFrame.printDocument();
                    }
                });

                $('.plan-sub-filters a').on('click', function () {
                    execSubFilters(isInZone);

                    var subFilter = isInZone ? '.with-tv' : '.without-tv';
                    var packageType = subFilter === '.with-tv' ? 'TV, Internet & landline' : 'Internet & landline';

                    pushContextAwareDataLayer('Service', packageType);

                    return false;
                });

                $('#filter-emirate').on('change', function (e, data) {
                    var emirate = $('#filter-emirate').val();

                    if (!data || !data.isSystemInitiated) {
                        pushContextAwareDataLayer('Emirates', emirate);
                    }
                });

                $('#filter-area').on('change', function () {
                    var area = $('#filter-area').val();
                    pushContextAwareDataLayer('Area', area);
                });

                $('.du-accordion .block-header').on('click', function () {

                    var block = $(this).closest('.block');

                    if (!block.hasClass('open')) {
                        return;
                    }

                    if (block.hasClass('how-to-get-a-sim-block')) {
                        addGtmDataLayer("details", 'howtogetasim');
                    } else if (block.hasClass('how-to-recharge-block')) {
                        addGtmDataLayer("details", 'howtorecharge');
                    } else if (block.hasClass('faqs-block')) {
                        addGtmDataLayer("details", 'faqs');
                    } else if (block.hasClass('terms-and-conditions-block')) {
                        addGtmDataLayer("details", 'termsandconditions');
                    }
                });

                var handleOfferButtons = function handleOfferButtons(selectedOfferVariation) {
                    if (selectedOfferVariation) {
                        $("[data-offers-add]").hide();
                        $("[data-offers-remove]").show();
                        $(".offer-text-added").show();
                    } else {
                        $("[data-offers-add]").show();
                        $("[data-offers-remove]").hide();
                        $(".offer-text-added").hide();
                    }

                    $("[data-offers-remove], [data-offers-add]").unbind("click");
                    $("[data-offers-remove], [data-offers-add]").on("click", function () {
                        var id = $(this).closest("li").attr("data-offers-id");
                        var variationId = $(this).closest("li").attr("data-variation-id");
                        var selectedOfferVariationId = getUrlParameterByName('selectedOfferVariation', document.location.href);
                        var applyOffersGlobally = variationId == selectedOfferVariation ? true : false;

                        applyOffersGlobally = false; //Always apply locally

                        var parent = $(this).closest('.card-item');

                        if (applyOffersGlobally) {
                            parent = $('body');
                        }

                        parent.find("li[data-offers-id='" + id + "']").find("[data-offers-add]").toggle();
                        parent.find("li[data-offers-id='" + id + "']").find("[data-offers-remove]").toggle();
                        parent.find("li[data-offers-id='" + id + "']").find(".offer-text-added").toggle();

                        if (applyOffersGlobally) {
                            createSelectButtonLinkAll();
                        } else {
                            var buyPlanButon = parent.find('.buyPlanButton').eq(0);
                            createSelectButtonLink(buyPlanButon[0]);
                        }
                    });
                };

                var retrieveOffers = function retrieveOffers() {
                    var selectedOfferVariation = getUrlParameterByName('selectedOfferVariation');
                    var journeyUrl = getUrlParameterByName('journey');
                    journeyUrl = !journeyUrl ? "" : journeyUrl;
                    var journey = !journeyUrl ? "" : journeyUrl;
                    if (!journey) {
                        journey = "Apple TV";
                    }
                    var currentLanguage = $("html").attr("lang");
                    var url = "/common/scripts/prototype/fixed-plans/fixedPlanOffers." + currentLanguage + ".json";

                    var offerIdsInvolved = [];
                    $("li[data-offers-id]").each(function (index, elem) {
                        var offerId = $(elem).attr("data-offers-id");
                        if (offerId && offerIdsInvolved.indexOf(offerId) < 0) {
                            offerIdsInvolved.push(offerId);
                        }
                    });

                    var createOfferComponent = function createOfferComponent(data, offerId) {
                        var planOffer, offerVariation;
                        var templateHtml = $("#offersTemplate").html();

                        /*
                        planOffer = data.FixedPlanOffers.filter(function (offer) {
                            return offer.Title === journey;
                        })[0];
                        */

                        planOffer = data.FixedPlanOffers.filter(function (offer) {
                            return offer.Id === offerId;
                        })[0];
                        planOffer.Installments.forEach(function (installment) {
                            var offerVariations = installment.Variations.filter(function (variation) {
                                return variation.Id === selectedOfferVariation;
                            });

                            if (offerVariations.length > 0) {
                                offerVariation = offerVariations[0];
                            }
                        });

                        // Add plan title parameter
                        $(".card-content .offers ul li[data-offers-id='" + planOffer.Id + "']").attr("data-plan-title", journeyUrl);

                        // If none are chosen take the lowest
                        if (!offerVariation) {
                            offerVariation = planOffer.Installments[0].Variations[0];
                        }

                        // Add selected variation id parameter
                        $(".card-content .offers ul li[data-offers-id='" + planOffer.Id + "']").attr("data-variation-id", offerVariation.Id);

                        var title = planOffer.Title;
                        var monthlyFee = planOffer.Installments[0].Variations[0].MonthlyFee;
                        if (selectedOfferVariation) {
                            title = planOffer.Title + (!isAr ? " - " : " سعة ") + offerVariation.Title;
                            monthlyFee = offerVariation.MonthlyFee;
                            templateHtml = templateHtml.replace("[NonSelectedTextClass]", "hide");
                            templateHtml = templateHtml.replace("[SelectedTextClass]", "");
                        } else {
                            templateHtml = templateHtml.replace("[NonSelectedTextClass]", "");
                            templateHtml = templateHtml.replace("[SelectedTextClass]", "hide");
                        }

                        templateHtml = templateHtml.replace("[ImageUrl]", planOffer.ImageUrl);
                        templateHtml = templateHtml.replace(new RegExp("[Title]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), title);
                        templateHtml = templateHtml.replace(new RegExp("[MonthlyFee]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), monthlyFee);

                        if (templateHtml.indexOf("[StartingFrom]") >= 0) {
                            var idxStart = templateHtml.indexOf("[StartingFrom]");
                            var idxEnd = templateHtml.indexOf("[/StartingFrom]") + "[/StartingFrom]".length;
                            var startingFromTextTags = templateHtml.substring(idxStart, idxEnd);
                            var startingFromText = startingFromTextTags.replace("[StartingFrom]", "").replace("[/StartingFrom]", "");

                            if (planOffer.Installments[0].Variations.length > 1) {
                                templateHtml = templateHtml.replace(startingFromTextTags, startingFromText);
                            } else {
                                templateHtml = templateHtml.replace(startingFromTextTags, "");
                            }
                        }

                        $(".card-content .offers ul li[data-offers-id='" + planOffer.Id + "']").append(templateHtml);
                        if (selectedOfferVariation) {
                            $(".card-content .offers ul li[data-offers-id='" + planOffer.Id + "']").addClass("from-page");
                        }

                        handleOfferButtons(selectedOfferVariation);
                    };

                    $.getJSON(url, function (data) {
                        $(offerIdsInvolved).each(function (index, offerId) {
                            createOfferComponent(data, offerId);
                        });

                        execFilters();
                    });
                };
                retrieveOffers();
            });

            handleSelectButtonLinks();

            handlePopup();
        });

        window.printPopupCallback = function (window) {

            setTimeout(function () {
                window.close();
            }, 250);
        };

        window.frameReadyCallback = function (window) {

            $('#PlanDetailsPopup .loadingOverlay').hide();
        };
    });

});