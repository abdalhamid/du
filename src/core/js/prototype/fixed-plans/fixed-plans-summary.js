'use strict';

require(['jquery', /*'bootstrap',*/ "lib/odometer"], function (jQuery, Odometer) {

    $(function () {

        if ($('.fixed-plans-summary').length === 0) {
            return;
        }

        var getContractPeriodNum = function getContractPeriodNum(contractPeriod) {
            return parseInt(contractPeriod.split(' ')[0]);
        };

        function initialize() {

            initializeControls();
            initializeData();
        }

        function initializeControls() {

            Odometer.init();

            $('.fixed-plans-summary .offers').empty();

            $('.fixed-plans-summary').on('click', '.toggle-offer-button', toggleOfferButton_clicked);
            $('.fixed-plans-summary .change-plan-button').on('click', changePlanButton_clicked);
            $('.fixed-plans-summary').on('change', '.offer .variation input[type="radio"]', offerVariation_changed);

            //Popup events
            $(".page-popup-overlay").on("click", hideAllPopups);
            $(".page-popup").on("click", '.close-button', hideAllPopups);
            $(".page-popup").on("click", '.cancel-btn', hideAllPopups);

            $('body').on('click', '.change-plan-confirmation-popup .confirm-btn', changePlanConfirmationButton_clicked);

            var parameters = getUriParameters();
            if (parameters.packageType === 'without-tv') {
                $('.set-top-box-fee').hide();
                $('.set-top-box-fee').find('[data-one-time-cost]').removeAttr('data-one-time-cost');
            }

            //Initialize popovers
            try {
                $("[data-toggle=popover]").popover({
                    trigger: 'focus',
                    container: 'body',
                    placement: 'auto top',
                    html: true
                });
            } catch (ex) {
                console.log('popover initialization failed.');
            }
        }

        function showChangePlanConfirmationPopup() {
            $('.change-plan-confirmation-popup').addClass('popup-visible');
            showPopupOverlay();
        }

        function hideAllPopups() {
            $('.page-popup').removeClass('popup-visible');
            hidePopupOverlay();
        }

        function showPopupOverlay() {
            $("html").addClass("page-popup-open");
        }

        function hidePopupOverlay() {
            $("html").removeClass("page-popup-open");
        }

        function changePlanButton_clicked(e) {
            e.preventDefault();

            pushContextAwareDataLayer('Order_summary', 'change_plan');

            showChangePlanConfirmationPopup();
        }

        function changePlanConfirmationButton_clicked() {

            var parameters = getUriParameters();
            var planParameters = {
                journey: parameters.journey //,
                //selectedOfferVariation: parameters.selectedOfferVariation
            };

            var url = './?' + $.param(planParameters);
            document.location.href = url;
        }

        function toggleOfferButton_clicked() {

            var offer = $(this).closest('.offer');
            var isAdded = $.trim(offer.attr('data-state')) == 'added';
            var offerTitle = offer.attr('data-title');
            var variationTitle = offer.find('input:checked').closest('.variation').attr('data-title');
            var category = formatDataLayerCategory(offerTitle);
            var label = $.trim(variationTitle).replace(/\s/g, '');

            if (isAdded) {
                offer.removeAttr('data-state');
                offer.find('.price .header').removeAttr('data-monthly-cost');

                if ($.trim(label).length > 0) {
                    pushDataLayer(category, 'Interested', label + '_delete');
                }
            } else {
                offer.attr('data-state', 'added');
                var monthlyFee = offer.find('.variation input:checked').closest('.variation').attr('data-monthly-fee');
                offer.find('.price .header').attr('data-monthly-cost', monthlyFee);

                if ($.trim(label).length > 0) {
                    pushDataLayer(category, 'Interested', label + '_add');
                }
            }

            recalculateFees();
            refreshControls();
        }

        function offerVariation_changed() {

            var groupName = $(this).attr('name');
            var selectedItem = $('.offer .variation input[name="' + groupName + '"]:checked').eq(0);

            if (selectedItem.length > 0) {
                var variation = selectedItem.closest('.variation');
                var monthlyFee = variation.attr('data-monthly-fee');

                var offer = selectedItem.closest('.offer');
                offer.find('.price .header .value').text(monthlyFee);

                var state = $.trim(offer.attr('data-state'));
                if (state == 'added') {
                    offer.find('.price .header').attr('data-monthly-cost', monthlyFee);
                }
            }

            recalculateFees();
            refreshControls();
        }

        function initializeData() {

            var parameters = getUriParameters();

            $('.fixed-plans-summary .breakdown .plan-title').text(parameters.planTitle);
            $('.fixed-plans-summary .breakdown .details .contract-period').text(getContractPeriodNum(parameters.contractPeriod));

            var monthlyFeeNumber = parameters.monthlyFee.match(/[0-9.,]+/);
            if (monthlyFeeNumber.length > 0) {
                var monthlyFee = parseFloat(monthlyFeeNumber);

                $('.fixed-plans-summary .plan-monthly-fee').attr('data-monthly-cost', monthlyFee).find('.value').text(monthlyFee);
            }

            var lang = getCulture();

            $.get('/common/scripts/prototype/fixed-plans/fixedPlanOffers.' + lang + '.json').done(renderData).fail(function () {
                alert('Failed to load offers');
            });
        }

        var offerIsVisibleForThisContract = function offerIsVisibleForThisContract(offer, contractPeriod) {
            return offer.Installments.some(function (installment) {
                if (!installment.FixedPlanMinPeriod) {
                    return true;
                }

                if (contractPeriod >= installment.FixedPlanMinPeriod) {
                    return true;
                }

                return false;
            });
        };

        function renderData(data) {

            if (!data || !data.FixedPlanOffers) {
                alert('Failed to load offers');
                return;
            }

            var parameters = getUriParameters();
            var selectedOfferVariation = $.trim(parameters.selectedOfferVariation);

            var offersView = $('.fixed-plans-summary .offers');

            var offerTemplate = $('#FixedPlan_OfferTemplate').html();
            var offerVariationTemplate = $('#FixedPlan_OfferVariationTemplate').html();

            for (var offerIndex = 0; offerIndex < data.FixedPlanOffers.length; offerIndex++) {
                var offer = data.FixedPlanOffers[offerIndex];

                if (!offerIsVisibleForThisContract(offer, getContractPeriodNum(parameters.contractPeriod))) {
                    continue;
                }

                var offerView = $(offerTemplate);

                offerView.attr('data-id', offer.Id).attr('data-title', offer.Title).removeAttr('data-state');

                offerView.find('.image img').attr('src', offer.ImageUrl);
                offerView.find('.description .header .header-text').text(offer.Title);
                offerView.find('.description .header [title]').attr('title', offer.Title);
                offerView.find('.description .header [data-content]').attr('data-content', offer.Details);
                offerView.find('.price .header .value').text('');
                //offerView.find('.price .details').text('');

                if ($.trim(offer.Details).length == 0) {
                    offerView.find('.description .header [data-content]').hide();
                }

                var variationsView = offerView.find('.variations');
                variationsView.empty();

                for (var installmentIndex = 0; installmentIndex < offer.Installments.length; installmentIndex++) {
                    var installment = offer.Installments[installmentIndex];

                    offerView.find('.description .details .contract-period').text(installment.ContractPeriod);

                    for (var variationIndex = 0; variationIndex < installment.Variations.length; variationIndex++) {
                        var variation = installment.Variations[variationIndex];
                        var variationId = $.trim(variation.Id);

                        var variationView = $(offerVariationTemplate);
                        variationView.attr('data-id', variation.Id).attr('data-title', variation.Title).attr('data-monthly-fee', variation.MonthlyFee).attr('data-contract-period', installment.ContractPeriod);

                        variationView.find('input').attr('id', offer.Title.replace(/\s/g, '') + '_Variations_' + variationIndex).attr('name', offer.Title.replace(/\s/g, '') + '_Variations').attr('value', variation.Id);

                        variationView.find('label').attr('for', offer.Title.replace(/\s/g, '') + '_Variations_' + variationIndex).text(variation.Title);

                        if (variation.DetailsOverride) {
                            offerView.find('.description .details').text(variation.DetailsOverride);
                            offerView.find('.price .details').text('');
                        }

                        variationsView.append(variationView);
                    }

                    if (installment.Variations.length < 2) {
                        variationsView.hide();
                    }
                }

                offersView.append(offerView);

                offerView.find("[data-toggle=popover]").popover({
                    trigger: 'focus',
                    container: 'body',
                    placement: 'auto top',
                    html: true
                });

                //Initialize selected variation
                offerView.find('.variations input').removeAttr('checked');

                var hasSelectedVariation = false;
                var selectedOfferVariation = $.trim(parameters.selectedOfferVariation);
                if (selectedOfferVariation.length > 0) {
                    var selectedOfferVariation = offerView.find('.variations .variation[data-id="' + selectedOfferVariation + '"]');
                    if (selectedOfferVariation.length > 0) {
                        selectedOfferVariation.closest('.offer').find('.toggle-offer-button').trigger('click');
                        selectedOfferVariation.find('input').attr('checked', 'checked').trigger('change');
                        hasSelectedVariation = true;
                    }
                }

                if (!hasSelectedVariation) {
                    offerView.find('.variations input').eq(0).attr('checked', 'checked').trigger('change');
                }
            }

            var packageType = $.trim(parameters.packageType);
            var customerType = $.trim(parameters.planType);
            if (customerType.length <= 0) {
                customerType = 'new';
            }
            $('[data-package-type-filter], [data-customer-type-filter]').each(function () {

                var element = $(this);
                var showElement = true;

                var packageTypeFilter = $.trim(element.attr('data-package-type-filter'));
                if (packageTypeFilter.length > 0 && packageType.length > 0 && packageTypeFilter !== packageType) {
                    showElement = showElement && false;
                }

                var customerTypeFilter = $.trim(element.attr('data-customer-type-filter'));
                if (customerTypeFilter.length > 0 && customerType.length > 0 && customerTypeFilter !== customerType) {
                    showElement = showElement && false;
                }

                if (showElement) {
                    element.show();
                } else {
                    element.hide();
                }
            });;

            recalculateFees();
            refreshControls();
        }

        function refreshControls() {
            var parameters = getUriParameters();
            //delete parameters.selectedOfferVariation;

            var planOffers = [];

            $('.fixed-plans-summary .offers .offer[data-state="added"]').each(function () {

                var title = $(this).attr('data-title');
                var variation = $(this).find('.variation input:checked').eq(0).closest('.variation').attr('data-title');

                if ($.trim(title) == $.trim(variation)) {
                    variation = '';
                }

                if ($.trim(variation).length > 0) {
                    planOffers.push(title + ' ' + variation);
                } else {
                    planOffers.push(title);
                }
            });

            parameters.planOffers = planOffers.join(',');

            var url = $('.fixed-plans-summary .breakdown-totals .buy-button').attr('href').match(/^[^?^#]+/)[0];
            if (!parameters.journey) {
                url = $('.fixed-plans-summary .breakdown-totals .buy-button').attr('data-href-no-journey').match(/^[^?^#]+/)[0];
            }
            var newUrl = url + '?' + $.param(parameters);

            $('.fixed-plans-summary .breakdown-totals .buy-button').attr('href', newUrl);

            var monthlyFee = $('.monthly-payment-total .value').text();
            var tags = pushContextAwareDataLayer('Total', 'Order now_AED ' + monthlyFee, true);
            $('.fixed-plans-summary .breakdown-totals .buy-button').attr('onclick', 'dataLayer.push(' + JSON.stringify(tags) + ');');
        }

        function recalculateFees() {

            var totalMonthlyCost = 0;
            $('.fixed-plans-summary').find('[data-monthly-cost]:visible').each(function (index, element) {
                var monthlyCost = parseInt($(element).attr('data-monthly-cost'));
                totalMonthlyCost += monthlyCost;
            });

            var totalOneTimeCostCost = 0;
            $('.fixed-plans-summary').find('[data-one-time-cost]:visible').each(function (index, element) {
                var oneTimeCost = parseInt($(element).attr('data-one-time-cost'));
                totalOneTimeCostCost += oneTimeCost;
            });

            $('.breakdown-totals .monthly-payment-total .heading .value').text(totalMonthlyCost);
            $('.breakdown-totals .one-time-payment-total .heading .value').text(totalOneTimeCostCost);
        }

        //Utility methods
        function getCulture() {
            var path = $.trim(document.location.pathname).toLowerCase();
            if (path === "ar" || path.startsWith("ar/") || path.startsWith("/ar/")) {
                return 'ar';
            } else {
                return 'en';
            }
        };

        function getUriParameters() {

            var parameters = {};
            var search = decodeURI(document.location.search);
            var pattern = /[&?]([^=^&^#]+)=([^&^#]+)/g;
            var match = null;

            while (match = pattern.exec(search)) {
                var parameter = match[1];
                var value = match[2];

                parameters[parameter] = decodeURIComponent(value.replace(/\+/g, ' '));
            }

            return parameters;
        }

        function formatDataLayerCategory(category) {
            var fallbackCategory = 'Homeservices_plan';
            var result = fallbackCategory;
            if ($.trim(category).length > 2) {
                result = category[0].toUpperCase() + category.toLowerCase().replace(/\s/g, '_').substring(1);
            } else if ($.trim(category).length > 1) {
                result = journey[0].toUpperCase();
            }

            return result;
        }

        function pushContextAwareDataLayer(action, label, deferPush) {
            var parameters = getUriParameters();
            var journey = parameters.journey;
            var category = formatDataLayerCategory(journey);

            return pushDataLayer(category, action, label, deferPush);
        }

        function pushDataLayer(category, action, label, deferPush) {
            var tags = {
                event: 'click',
                category: category,
                action: action,
                label: label
            };

            if (!deferPush) {
                dataLayer.push(tags);
            }

            return tags;
        }

        initialize();
    });
});