require(['jquery'], function () {

    $(function () {

        if ($('.apple-tv-details').length === 0) {
            return;
        }

        var isAr = $('html').attr('dir') === "rtl";
        var lang = isAr ? 'ar' : 'en';

        var recenterPopup = function recenterPopup(popupId) {
            var popup = $("#" + popupId);
            popup.css("margin-left", -popup.outerWidth() / 2);

            if ($('body.mobile').length > 0) {
                var viewportFromTop = $(window).scrollTop();
                if (viewportFromTop === 0) {
                    popup.css("position", "fixed");

                    if ($(window).width() < 479) {
                        popup.css("top", "10px");
                    } else {
                        popup.css("top", "60px");
                    }
                } else if ($(window).offset()) {
                    popup.css("position", "absolute");
                    popup.css("top", $(window).offset().top + "px");
                }
            }
        };

        var fixPopup = function fixPopup() {
            if ($("#PlanDetailsPopup").css("display") !== "none") {
                recenterPopup("PlanDetailsPopup");
            }
        };

        $(window).on("orientationchange", function () {
            var setTimeoutRifle = function setTimeoutRifle(callback, timeout, numTimes) {
                var idx = 1;

                var setTimeoutRifleElab = function setTimeoutRifleElab() {
                    setTimeout(function () {
                        try {
                            callback();
                        } catch (e) {}

                        idx++;

                        if (idx <= numTimes) {
                            setTimeoutRifleElab();
                        }
                    }, timeout);
                };

                setTimeoutRifleElab();
            };

            setTimeoutRifle(function () {
                fixPopup();

                if (window.pubsub) {
                    window.pubsub.publish("autoMatchheight/refresh", []);
                }
            }, 100, 5);

            $('.planDetailsPopup:visible').each(function () {

                var popupWidth = $(this).width();
                $(this).css('margin-left', -(popupWidth / 2) + 'px');

                this.style.removeProperty('width');
            });
        });

        //Plan information popup
        $('a[data-reveal-id="PlanDetailsPopup"]').on('click', function (e) {
            e.preventDefault();

            var isTouch = $('body').hasClass('mobile');
            var planDetailsPopup = $('#PlanDetailsPopup');
            var popupImage = planDetailsPopup.find('[data-popup-img]');

            var suffix = (isTouch ? '-mobile' : '') + '-' + lang;
            var imgSrc = popupImage.data('popupImg').replace('[suffix]', suffix);

            popupImage.attr('src', imgSrc);

            planDetailsPopup.css('margin-left', -Math.round(planDetailsPopup.outerWidth() / 2));

            var width = Math.floor(planDetailsPopup.width());
            planDetailsPopup[0].style.setProperty('width', width + 'px', 'important');
        });

        $('#PlanDetailsPopup .close-reveal-modal').on('click', function () {
            infoPopupCardItem = null;

            var planDetailsPopup = $('#PlanDetailsPopup');

            planDetailsPopup[0].style.removeProperty('width');
        });
        // Map Code
        //TODO: move to a separate file to be used by both appletv and fixedplans
        var isInZone, selectedEmirate, selectedArea, map, marker;

        var $mapSearchInput = $('#map-search');

        var infoWindow = new google.maps.InfoWindow();

        var geocoder = new google.maps.Geocoder;

        var $mapCont = $('#map-container');

        var $filterPlansBtn = $('#filter-plans');

        var polygons = [];

        var userSwiping = false;

        var initGoogleMaps = function () {
            var placeMarker = function placeMarker(location) {
                if (marker) {
                    marker.setMap(null);
                }
                marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                marker.setIcon('/common/images/map-icon.png');
                $('#map-sticky').addClass('active');
                $mapCont.addClass('map-sticky-active');
            };

            function initMap() {
                var options = {
                    center: {
                        lat: 25.0537936,
                        lng: 55.1572327
                    },
                    zoom: 10,
                    mapTypeControl: false,
                    scrollwheel: false
                };
                map = new google.maps.Map(document.getElementById('map'), options);

                var getPotentialEmirateArea = function (latlng, callback) {
                    var getPotentialArea = function (results) {
                        var potentialAreas = [];

                        results.forEach(function (result) {
                            result.address_components.filter(function (item) {
                                if (item.types.indexOf('sublocality') > -1) {
                                    potentialAreas.push(item.short_name);
                                }
                            });
                        });

                        return potentialAreas.length > 0 ? potentialAreas[0] : '';
                    }

                    var getPotentialEmirate = function (results) {
                        var potentialEmirates = [];

                        results.forEach(function (result) {
                            result.address_components.filter(function (item) {
                                if (item.types.indexOf('administrative_area_level_1') > -1) {
                                    potentialEmirates.push(item.short_name);
                                }
                            });
                        });

                        return potentialEmirates.length > 0 ? potentialEmirates[0] : '';
                    }

                    geocoder.geocode({
                        'location': latlng
                    }, function (results, status) {
                        if (status == 'OK') {

                            selectedEmirate = getPotentialEmirate(results);
                            selectedArea = getPotentialArea(results);

                            if (callback) {
                                callback(selectedEmirate, selectedArea);
                            }
                        }
                    });
                }
                $.getJSON('/common/scripts/prototype/fixed-plans/inzone-areas.json', function (data) {

                    data.features.forEach(function (item) {
                        var coordinates = item.geometry.coordinates[0].map(function (item) {
                            return {
                                lng: parseFloat(item[0]),
                                lat: parseFloat(item[1])
                            };
                        });

                        var featurePolygon = new google.maps.Polygon({
                            paths: coordinates,
                            name: item.properties.Name,
                            fillColor: 'transparent',
                            strokeColor: 'transparent',
                            strokeOpacity: 0.8
                        });

                        featurePolygon.setMap(map);

                        polygons.push(featurePolygon);

                        google.maps.event.addListener(featurePolygon, 'click', function (event) {
                            if (!userSwiping) {
                                var location = event.latLng;

                                placeMarker(location);

                                infoWindow.setOptions({
                                    content: '<div class="info-window-name">' + this.name + '</div>',
                                    pixelOffset: new google.maps.Size(0, -45),
                                    position: event.latLng
                                });

                                infoWindow.open(map);

                                isInZone = true;

                                var polygonAreaName = this.name;

                                if (!event.triggered) {
                                    $mapSearchInput.val(this.name);
                                }

                                getPotentialEmirateArea(location, function (selectedEmirate) {
                                    selectedArea = polygonAreaName;
                                })

                                console.log(isInZone);
                            }
                        });
                    });
                });

                google.maps.event.addListener(map, 'click', function (event) {
                    if (!userSwiping) {
                        var location = event.latLng;
                        placeMarker(event.latLng);
                        infoWindow.close();
                        getPotentialEmirateArea(event.latLng, function (selectedEmirate, selectedArea) {
                            if (!event.triggered) {
                                $mapSearchInput.val(selectedArea);
                            }
                        });

                        var isPointOfInterest = event.placeId;

                        if (isPointOfInterest) {
                            //stop the infoWindow from appearing
                            event.stop();
                        }

                        isInZone = false;

                        console.log(isInZone);
                    }
                });

                $mapCont.on('touchmove', function () {
                    userSwiping = true;
                });

                $mapCont.on('touchend', function () {
                    userSwiping = false;
                });

                google.maps.event.addListener(infoWindow, 'domready', function () {
                    $('#map .gm-style-iw').parent().addClass('info-window');
                });
            }

            var alreadyLoaded = document.readyState == 'complete';

            if (alreadyLoaded) {
                initMap();
            } else {
                google.maps.event.addDomListener(window, "load", initMap);
            }

            var placeChangedCallBack = function () {
                var place = this.getPlace();
                if (place.geometry) {
                    map.setCenter(place.geometry.location);
                    map.setZoom(10);

                    var isPolygonClicked = false;

                    for (var i = 0; i < polygons.length; i++) {
                        if (google.maps.geometry.poly.containsLocation(place.geometry.location, polygons[i])) {

                            google.maps.event.trigger(polygons[i], 'click', {
                                latLng: place.geometry.location,
                                triggered: true
                            });
                            isPolygonClicked = true;
                            break;
                        }
                    }

                    if (!isPolygonClicked) {
                        google.maps.event.trigger(map, 'click', {
                            latLng: place.geometry.location,
                            triggered: true
                        });
                    }
                }

            };

            new google.maps.places.Autocomplete($mapSearchInput[0], {
                componentRestrictions: {
                    country: "AE"
                }
            }).addListener('place_changed', placeChangedCallBack);

            var $autoCompleteCont = $('.pac-container');

            var adjustAutocompleteDirection = function () {
                if (this.value.length > 0) {
                    if ($autoCompleteCont.length == 0) {
                        $autoCompleteCont = $('.pac-container');
                    }

                    var hasEnglishCharacters = $(this).val().match(/[a-zA-Z]{1,}/g);

                    if (hasEnglishCharacters) {
                        $autoCompleteCont.removeClass('rtl');
                    } else {
                        $autoCompleteCont.addClass('rtl');
                    }
                }
            }

            var debounce = function (func, wait, immediate) {
                var timeout;
                return function () {
                    var context = this,
                        args = arguments;
                    var later = function () {
                        timeout = null;
                        if (!immediate) func.apply(context, args);
                    };
                    var callNow = immediate && !timeout;
                    clearTimeout(timeout);
                    timeout = setTimeout(later, wait);
                    if (callNow) func.apply(context, args);
                };
            };

            var debouncedAdjustAutocompleteDirection = debounce(adjustAutocompleteDirection, 200);

            $mapSearchInput.on('keyup', debouncedAdjustAutocompleteDirection);

            $mapSearchInput.one('change', debouncedAdjustAutocompleteDirection);
        }

        var initializeMapFocus = function () {

            var container = $('body');

            container.on('focus', '.map-container .map-search-input', function () {

                var searchField = $(this);
                var referenceElement = $('.choose-package-title').eq(0);

                var scrollPosition = referenceElement.offset().top - 40;

                $('html, body').stop(true).animate({
                    scrollTop: scrollPosition
                }, 300);
            });
        };

        initGoogleMaps();
        initializeMapFocus();

        $filterPlansBtn.on('click', function (e) {
            e.preventDefault();
            window.location = (isAr ? '/ar' : '/en') + '/fixed-plans/du.ae_athome_fixed-plans.html?zone=' + (isInZone ? 'in' : 'out') + '&city=' + (selectedArea) + '&emirate=' + (selectedEmirate);
        });
        // End Map Code
    });

});