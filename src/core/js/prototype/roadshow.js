'use strict';

require(['jquery', 'lib/underscore.min', 'lib/jquery.bootpag', 'lib/jquery.typeahead', 'lib/chosen.jquery.min'], function () {

    $(function () {

        if ($('.roadshow-container').length === 0) {
            return;
        }

        var Map = (function () {
            var map,
                mapOptions,
                markers = [],
                bounds,
                trafficLayer,
                directionsDisplay,
                directionsService = new google.maps.DirectionsService(),
                initZoom = 8,
                maxZoom = 14;

            function clearMarkers() {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(null);
                }
                markers = [];
            }

            function TrafficControl(controlDiv, map) {
                controlDiv.className = "btn-map";
                controlDiv.innerHTML = 'Traffic';
                google.maps.event.addDomListener(controlDiv, 'click', function () {
                    var $controlDiv = $(controlDiv);
                    if (!$controlDiv.hasClass("active")) {
                        $controlDiv.addClass("active");
                        trafficLayer.setMap(map);
                    } else {
                        $controlDiv.removeClass("active");
                        trafficLayer.setMap(null);
                    }
                });
            }

            function init() {

                //init map
                mapOptions = {
                    center: new google.maps.LatLng(25.2092, 55.2495),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    disableDefaultUI: true,
                    trafficControl: true,
                    zoom: initZoom
                };

                map = new google.maps.Map(document.getElementById("map"), mapOptions);

                //set directions
                directionsDisplay = new google.maps.DirectionsRenderer();
                directionsDisplay.setMap(map);

                //set traffic
                trafficLayer = new google.maps.TrafficLayer();

                var trafficControlDiv = document.createElement('div');
                var trafficControl = new TrafficControl(trafficControlDiv, map);

                trafficControlDiv.index = 1;
                map.controls[google.maps.ControlPosition.TOP_RIGHT].push(trafficControlDiv);

                //set places autocomplete
                var input = document.getElementById('location');
                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.bindTo('bounds', map);
            }

            function calcRoute(end) {
                clearMarkers();
                var request = {
                    origin: $("#location").val(),
                    destination: new google.maps.LatLng(end.lat, end.lng),
                    travelMode: google.maps.TravelMode[$("input:radio[name=travelMode]:checked").val()]
                };
                directionsService.route(request, function (response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setDirections(response);
                        drawDirections(response);
                    }
                });
            }

            function resetRoute() {
                $("#directions-panel").hide();
                clearRoute();
            }

            function clearRoute() {
                directionsDisplay.setDirections({
                    routes: []
                });
                $("#google-directions-panel").html("").hide();
            }

            function fetchAddress(callback, error) {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (p) {
                        var Position = new google.maps.LatLng(p.coords.latitude, p.coords.longitude),
                            Locater = new google.maps.Geocoder();

                        Locater.geocode({
                            'latLng': Position
                        }, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                var _r = results[0];
                                callback(_r.formatted_address);
                            }
                        });
                    });
                } else {
                    error();
                }
            }

            function drawDirections(response) {
                var StepTemplate = _.template($("#step").html());
                $("#google-directions-panel").html(StepTemplate({
                    route: response.routes[0].legs[0],
                    mode: $("input:radio[name=travelMode]:checked").data("display"),
                    modeclass: $("input:radio[name=travelMode]:checked").data("class"),
                })).show();
            }

            function drawMap(roadshows) {
                clearMarkers();
                resetRoute();

                bounds = new google.maps.LatLngBounds();

                //draw Map
                var iconsPath = '/common/images/';
                if (roadshows.length > 0) {
                    //TODO:invest in infoBox
                    var infowindow = new google.maps.InfoWindow();
                    var marker, i;

                    var previousMarker;
                    var icons = {
                        "icon-roadshow": iconsPath + 'icon-gmap-roadshow.png'
                    };
                    var hovers = {
                        "icon-roadshow": iconsPath + 'icon-gmap-roadshow-hover.png'
                    };

                    function resetIcon(imarker) {
                        if (previousMarker) {
                            previousMarker.setIcon(imarker.icon);
                            previousMarker = imarker;
                        } else {
                            previousMarker = imarker;
                        }
                        imarker.setIcon(imarker.hover);
                    }

                    for (var i = 0, l = roadshows.length; i < l; i++) {
                        var roadshow = roadshows[i];
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(roadshow.lat, roadshow.lng),
                            map: map,
                            icon: icons["icon-roadshow"],
                            hover: hovers["icon-roadshow"],
                            id: roadshow.id
                        });

                        bounds.extend(marker.position);
                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            marker.click = function () {
                                infowindow.setContent(roadshows[i].html);
                                infowindow.open(map, marker);

                                google.maps.event.addListener(infowindow, 'domready', function () {
                                    $('.infoWindow').parent().parent().parent().parent().addClass("infoBox");
                                });

                                resetIcon(marker);
                            }
                            return marker.click;
                        })(marker, i));
                        markers.push(marker);
                    }
                    map.fitBounds(bounds);
                    if (map.getZoom() > maxZoom || map.getZoom() == 0) map.setZoom(maxZoom);
                    var listener = google.maps.event.addListener(map, "idle", function () {
                        if (map.getZoom() > maxZoom || map.getZoom() == 0) map.setZoom(maxZoom);
                        google.maps.event.removeListener(listener);
                    });
                }
            }

            function findMarker(id) {
                var i = markers.length;
                while (i--) {
                    if (markers[i].id == id) {
                        return markers[i];
                    }
                }
            }

            function catchMarker(id) {
                findMarker(id).click();
            }

            try {
                init();
            }
            catch (ex) {
                console.log('Error: ' + ex);
            }

            return {
                drawMap: drawMap,
                calcRoute: calcRoute,
                fetchAddress: fetchAddress,
                resetRoute: resetRoute,
                clearRoute: clearRoute,
                catchMarker: catchMarker
            }
        })();

        var MapModule = {
            duRoadshows: [{
                "Id": "f5183a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "DIFC",
                "Timing": "9:00 AM to 4:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "DIFC Main Buidling, Ground Floor",
                "Latitude": 25.214301,
                "Longitude": 55.282133
            }, {
                "Id": "ff183a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "Emirates HQ",
                "Timing": "8:00 AM - 10:00 PM. Friday: 10:00 AM to 6:00 PM.",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "Emirates HQ, Al Garhoud",
                "Latitude": 25.241476,
                "Longitude": 55.365999999999985
            }, {
                "Id": "09193a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "ADNOC",
                "Timing": "8:00 AM - 2:30 PM",
                "EndDate": null,
                "LocationAreaIds": ["07410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Abu Dhabi",
                "Street": "ADNOC HQ, Corniche Road",
                "Latitude": 24.46147,
                "Longitude": 54.32353
            }, {
                "Id": "13193a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "du Hili Mall",
                "Timing": "Saturday to Wednesday: 10:00 AM -10:00 PM Thursday to Friday: 10:00 AM to 11:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["4e410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Al Ain",
                "Street": "du Retail Shop",
                "Latitude": 24.27385,
                "Longitude": 55.778810000000021
            }, {
                "Id": "28193a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "du Sahara Center",
                "Timing": "Sunday to Wednesday: 10:00 AM -11:00 PM Thursday to Saturday: 10:00 AM - 12:00 AM",
                "EndDate": null,
                "LocationAreaIds": ["3c410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Sharjah",
                "Street": "du Retail Shop",
                "Latitude": 25.295189,
                "Longitude": 55.373572999999965
            }, {
                "Id": "33193a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "du Sharjah City Center",
                "Timing": "Saturday to Wednesday: 10:00 AM -10:00 PM Thursday: 10:00 AM - 12:00 AM Friday: 2:00 PM - 12:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["3c410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Sharjah",
                "Street": "du Retail Shop",
                "Latitude": 25.325325,
                "Longitude": 55.393066999999974
            }, {
                "Id": "48193a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "du UAQ Shop",
                "Timing": "Saturday to Wednesday: 9:00 AM -10:00 PM Thursday: 9:00 AM - 5:00 PM Friday Closed",
                "EndDate": null,
                "LocationAreaIds": [],
                "CountryCode": "AE",
                "City": "Umm Al Quwain",
                "Street": "du Retail Shop",
                "Latitude": 25.494544,
                "Longitude": 55.551948000000039
            }, {
                "Id": "52193a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "du Manar Mall",
                "Timing": "Saturday to Thursday: 9:00 AM -10:00 PM Friday: 2:00 PM - 10:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["48410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Ras Al Khaimah",
                "Street": "du Retail Shop",
                "Latitude": 25.785125,
                "Longitude": 55.965632000000028
            }, {
                "Id": "5d193a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "du Mirdiff City Center",
                "Timing": "Sunday to Wednesday: 10:00 AM -10:00 PM Thursday to Saturday: 10:00 AM - 12:00 AM",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "du Retail Shop",
                "Latitude": 25.216314,
                "Longitude": 55.407786999999985
            }, {
                "Id": "67193a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "UAE Army",
                "Timing": "7:30 AM - 2:30 PM",
                "EndDate": null,
                "LocationAreaIds": ["07410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Abu Dhabi",
                "Street": "UAE Army, Sweihan Camp",
                "Latitude": 24.444145,
                "Longitude": 54.841628
            }, {
                "Id": "71193a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "du Fujairah City Center",
                "Timing": "Sunday to Wednesday: 9:00 AM -11:00 PM Thursday: 9:00 AM - 12:00 AM Friday: 2:00 PM - 12:00 AM Saturday: 9:00 AM - 12:00 AM",
                "EndDate": null,
                "LocationAreaIds": ["3c410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Sharjah",
                "Street": "du Retail Shop",
                "Latitude": 25.125688,
                "Longitude": 56.302149999999983
            }, {
                "Id": "7b193a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "Regus office",
                "Timing": "9:00 AM -6:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["3c410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Sharjah",
                "Street": "Regus Buikding, next to Sharjah Megamall",
                "Latitude": 25.345519,
                "Longitude": 55.398914999999988
            }, {
                "Id": "85193a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "Sharjah Police",
                "Timing": "7:30 AM - 2:30 PM",
                "EndDate": null,
                "LocationAreaIds": ["3c410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Sharjah",
                "Street": "Sharjah Police GHQ",
                "Latitude": 25.358149,
                "Longitude": 55.443772
            }, {
                "Id": "8f193a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "du Salam Shop",
                "Timing": "Saturday to Thursday: 8:00 AM -8:00 PM Friday Closed",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "du Retail Shop",
                "Latitude": 25.110489,
                "Longitude": 55.204104000000029
            }, {
                "Id": "99193a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "du Hamdan Shop",
                "Timing": "Saturday to Thursday: 9:00 AM -10:00 PM Friday: 2:00 PM - 10:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["07410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Abu Dhabi",
                "Street": "du Retail Shop",
                "Latitude": 24.487389,
                "Longitude": 54.359312999999929
            }, {
                "Id": "a3193a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "Abu Dhabi Police & Ministry of Interior",
                "Timing": "7:30 AM - 2:30 PM",
                "EndDate": null,
                "LocationAreaIds": ["07410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Abu Dhabi",
                "Street": "Abu Dhabi Police IT Office, Opp. Mushrif Mall",
                "Latitude": 24.435269,
                "Longitude": 54.418406
            }, {
                "Id": "ad193a28-3c49-6e48-96d2-ff5a00702077",
                "Title": "Al Ain Police",
                "Timing": "7:30 AM - 2:30 PM",
                "EndDate": null,
                "LocationAreaIds": ["4e410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Al Ain",
                "Street": "Al Ain Police Officers Club",
                "Latitude": 24.23519,
                "Longitude": 55.741261
            }, {
                "Id": "53b33c28-3c49-6e48-96d2-ff5a00702077",
                "Title": "du Bawadi Mall",
                "Timing": "Saturday to Wednesday: 10:00 AM -10:00 PM Thursday to Friday: 10:00 AM to 11:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["4e410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "",
                "Street": "",
                "Latitude": 24.160883,
                "Longitude": 55.807123999999931
            }, {
                "Id": "faf14028-3c49-6e48-96d2-ff5a00702077",
                "Title": "Mall of the Emirates",
                "Timing": "Sunday to Wednesday: 10:00 AM -10:00 PM Thursday to Saturday: 10:00 AM - 12:00 AM",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077", "6c410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "",
                "Latitude": 25.11834,
                "Longitude": 55.200618999999961
            }, {
                "Id": "71f24028-3c49-6e48-96d2-ff5a00702077",
                "Title": "Dubai Mall",
                "Timing": "Sunday to Wednesday: 10:00 AM -10:00 PM Thursday to Saturday: 10:00 AM - 12:00 AM",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "",
                "Latitude": 25.197859,
                "Longitude": 55.278923999999961
            }, {
                "Id": "99f24028-3c49-6e48-96d2-ff5a00702077",
                "Title": "DAFZA",
                "Timing": "9:00 AM – 5:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "",
                "Latitude": 25.26062,
                "Longitude": 55.37275
            }, {
                "Id": "b7f24028-3c49-6e48-96d2-ff5a00702077",
                "Title": "Al Wahda Mall",
                "Timing": "Sunday to Wednesday: 10:00 AM -10:00 PM Thursday to Saturday: 10:00 AM - 11:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["07410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "",
                "Latitude": 24.470601,
                "Longitude": 54.372714999999971
            }, {
                "Id": "52a74828-3c49-6e48-96d2-ff5a00702077",
                "Title": "Bawabat Al Sharq Mall",
                "Timing": "Saturday to Thursday: 10:00 AM -10:00 PM Friday: 3:00 PM - 11:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["07410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Abudhabi",
                "Street": "",
                "Latitude": 24.2991738,
                "Longitude": 54.697277399999962
            }, {
                "Id": "69a74828-3c49-6e48-96d2-ff5a00702077",
                "Title": "Abu Dhabi Marina Mall",
                "Timing": "Sunday to Wednesday: 10:00 AM -10:00 PM Thursday: 10:00 AM - 11:00 PM Friday: 2:00 PM - 11:00 PM Saturday: 10:00 AM - 11:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["07410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Abudhabi",
                "Street": "",
                "Latitude": 24.476352,
                "Longitude": 54.321461
            }, {
                "Id": "8aa74828-3c49-6e48-96d2-ff5a00702077",
                "Title": "Dana Mall",
                "Timing": "Saturday to Wednesday: 10:00 AM -10:00 PM Thursday: 10:00 AM - 12:00 AM Friday: 2:00 PM - 12:00 AM",
                "EndDate": null,
                "LocationAreaIds": ["39410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Ajman",
                "Street": "",
                "Latitude": 25.389888,
                "Longitude": 55.458588
            }, {
                "Id": "a1a74828-3c49-6e48-96d2-ff5a00702077",
                "Title": "Dubai Airport - Terminal 3",
                "Timing": "12:00 PM - 8:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "",
                "Latitude": 25.245264,
                "Longitude": 55.360541000000012
            }, {
                "Id": "b8a74828-3c49-6e48-96d2-ff5a00702077",
                "Title": "Arabian Center",
                "Timing": "Saturday to Wednesday: 10:00 AM -10:00 PM Thursday to Friday: 10:00 AM - 12:00 AM",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "",
                "Latitude": 25.235695,
                "Longitude": 55.436455
            }, {
                "Id": "cfa74828-3c49-6e48-96d2-ff5a00702077",
                "Title": "RAK Safeer Mall",
                "Timing": "Sunday to Wednesday: 10:00 AM -10:00 PM Thursday to Saturday: 10:00 AM - 11:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["48410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Ras Al Khaimah",
                "Street": "",
                "Latitude": 25.749045,
                "Longitude": 55.92508
            }, {
                "Id": "03664e28-3c49-6e48-96d2-ff5a00702077",
                "Title": "Abu Dhabi Dalma Mall",
                "Timing": "Saturday to Wednesday: 10:00AM -10:00PM,  Thursday to Friday: 10:00 AM - 12:00 AM",
                "EndDate": null,
                "LocationAreaIds": ["07410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Abu Dhabi",
                "Street": "du Retail Shop, Dalma Mall",
                "Latitude": 24.333436,
                "Longitude": 54.523837
            }, {
                "Id": "1a664e28-3c49-6e48-96d2-ff5a00702077",
                "Title": "Al Barsha Mall",
                "Timing": "10:00AM - 10:00PM",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "du Retail Shop, Al Barsha Mall",
                "Latitude": 25.099168,
                "Longitude": 55.204664
            }, {
                "Id": "31664e28-3c49-6e48-96d2-ff5a00702077",
                "Title": "Century Mall",
                "Timing": "Saturday to Thursday: 10:00AM -10:00PM, Friday: 10:00AM - 11:00PM",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "du Retail Shop, Century Mall",
                "Latitude": 25.290184,
                "Longitude": 55.347048
            }, {
                "Id": "48664e28-3c49-6e48-96d2-ff5a00702077",
                "Title": "Abu Dhabi Airport - Terminal 1",
                "Timing": "12:00PM - 8:00PM",
                "EndDate": null,
                "LocationAreaIds": ["07410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Abu Dhabi",
                "Street": "Abu Dhabi Terminal 1, Arrivals",
                "Latitude": 24.430974,
                "Longitude": 54.639956
            }, {
                "Id": "5f664e28-3c49-6e48-96d2-ff5a00702077",
                "Title": "Twar Center",
                "Timing": "Saturday to Thursday: 8:00AM -10:00PM, Friday: Closed",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "Twar Center, Al Nahda Road, Al Twar",
                "Latitude": 25.263089,
                "Longitude": 55.38484
            }, {
                "Id": "76664e28-3c49-6e48-96d2-ff5a00702077",
                "Title": "Wafi Mall",
                "Timing": "Saturday to Wednesday: 10:00AM -10:00PM, Thursday to Friday: 10:00AM - 12:00AM",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "du Retail Shop, Wafi Mall",
                "Latitude": 25.229935,
                "Longitude": 55.319424
            }, {
                "Id": "8d664e28-3c49-6e48-96d2-ff5a00702077",
                "Title": "Sharjah Mega Mall",
                "Timing": "Saturday to Thursday: 10:00AM -11:00PM, Friday: 2:00PM - 11:00PM",
                "EndDate": null,
                "LocationAreaIds": ["3c410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Sharjah",
                "Street": "du Retail Shop, Sharjah Mega Mall",
                "Latitude": 25.344806,
                "Longitude": 55.398678
            }, {
                "Id": "7fc35028-3c49-6e48-96d2-ff5a00702077",
                "Title": "Al Hamra Mall",
                "Timing": "Sunday to Wednesday: 10:00 AM -10:00 PM Thursday to Saturday: 10:00 AM - 11:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["48410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "",
                "Street": "1st Floor, Al Hamra Mall",
                "Latitude": 25.68309,
                "Longitude": 55.7816368
            }, {
                "Id": "b6c35028-3c49-6e48-96d2-ff5a00702077",
                "Title": "Abu Dhabi Municipality",
                "Timing": "Sunday to Thursday: 9:00 AM -3:00 PM Friday to Saturday: Closed",
                "EndDate": null,
                "LocationAreaIds": ["07410628-3c49-6e48-96d2-ff5a00702077", "e5410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Abu Dhabi",
                "Street": "Customer Care Centre - Salam Street",
                "Latitude": 24.487749,
                "Longitude": 54.379612
            }, {
                "Id": "eac35028-3c49-6e48-96d2-ff5a00702077",
                "Title": "Al Ghurair Center ",
                "Timing": "Saturday to Wednesday: 10:00 AM -10:00 PM Thursday: 10:00 AM - 12:00 AM Friday: 2:00 PM - 12:00 AM",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "du Retail Shop, Al Ghurair Center",
                "Latitude": 25.267212,
                "Longitude": 55.317313
            }, {
                "Id": "05c45028-3c49-6e48-96d2-ff5a00702077",
                "Title": "Dubai Airport - Terminal 1",
                "Timing": "12:00 PM - 8:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "Dubai Airport Terminal 1, Arrivals",
                "Latitude": 25.249417,
                "Longitude": 55.350675
            }, {
                "Id": "23c45028-3c49-6e48-96d2-ff5a00702077",
                "Title": "Al Khaleej Center",
                "Timing": "Saturday to Thursday: 10:00 AM -10:00 PM Friday: 5:00 PM -10:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "1st Floor, Al Khaleej Center",
                "Latitude": 25.257158,
                "Longitude": 55.29575
            }, {
                "Id": "3fc45028-3c49-6e48-96d2-ff5a00702077",
                "Title": "Dibba Lulu Hypermarket",
                "Timing": "Saturday to Thursday: 9:00 AM -10:00 PM Friday: 2:00 PM -11:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["4b410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Fujairah",
                "Street": "Ground Floor, Lulu Hypermarket",
                "Latitude": 25.588835,
                "Longitude": 56.272841
            }, {
                "Id": "842e5228-3c49-6e48-96d2-ff5a00702077",
                "Title": "Ibn Battuta Mall",
                "Timing": "Sunday to Wednesday: 10:00 AM -10:00 PM Thursday to Saturday: 10:00 AM - 12:00 AM",
                "EndDate": null,
                "LocationAreaIds": ["f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "",
                "Latitude": 25.04483,
                "Longitude": 55.120286
            }, {
                "Id": "c92e5228-3c49-6e48-96d2-ff5a00702077",
                "Title": "Jumeirah Center",
                "Timing": "Saturday to Thursday: 9:00 AM -9:00 PM Friday: 2:00 PM -9:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["ae410628-3c49-6e48-96d2-ff5a00702077", "f8400628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Dubai",
                "Street": "",
                "Latitude": 25.231043,
                "Longitude": 55.262777
            }, {
                "Id": "e42e5228-3c49-6e48-96d2-ff5a00702077",
                "Title": "Sharjah Airport",
                "Timing": "12:00 PM - 8:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["ff410628-3c49-6e48-96d2-ff5a00702077", "3c410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Sharjah",
                "Street": "",
                "Latitude": 25.32606,
                "Longitude": 55.510514
            }, {
                "Id": "fe2e5228-3c49-6e48-96d2-ff5a00702077",
                "Title": "Al Ain Remal Mall",
                "Timing": "10:00 AM - 10:00 PM",
                "EndDate": null,
                "LocationAreaIds": ["4e410628-3c49-6e48-96d2-ff5a00702077"],
                "CountryCode": "AE",
                "City": "Al Ain",
                "Street": "",
                "Latitude": 24.204371,
                "Longitude": 55.762172
            }] || [],
            cities: [{
                "Id": "07410628-3c49-6e48-96d2-ff5a00702077",
                "Name": "Abu Dhabi",
                "Areas": [{
                    "Id": "de410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Airport Road"
                }, {
                    "Id": "f3410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Al Khalifa Street"
                }, {
                    "Id": "e5410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Salam Street"
                }, {
                    "Id": "e1410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Tourist Club"
                }]
            }, {
                "Id": "39410628-3c49-6e48-96d2-ff5a00702077",
                "Name": "Ajman",
                "Areas": [{
                    "Id": "fc410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Al Jarf"
                }]
            }, {
                "Id": "4e410628-3c49-6e48-96d2-ff5a00702077",
                "Name": "Al Ain",
                "Areas": [{
                    "Id": "0d420628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Al Jimi"
                }]
            }, {
                "Id": "f8400628-3c49-6e48-96d2-ff5a00702077",
                "Name": "Dubai",
                "Areas": [{
                    "Id": "6c410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Al Barsha"
                }, {
                    "Id": "db410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Al Ittehad Roundabout"
                }, {
                    "Id": "d8410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Al Mizhar"
                }, {
                    "Id": "d5410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Al Nakheel"
                }, {
                    "Id": "d2410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Al Rigga"
                }, {
                    "Id": "cf410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Bur Dubai"
                }, {
                    "Id": "73410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Deira"
                }, {
                    "Id": "c8410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Dubai Media City"
                }, {
                    "Id": "be410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "International City"
                }, {
                    "Id": "b7410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Jebel Ali"
                }, {
                    "Id": "ae410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Jumeirah"
                }, {
                    "Id": "9f410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Jumeirah Beach Residence"
                }, {
                    "Id": "91410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Karama"
                }, {
                    "Id": "83410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Oud Metha Road"
                }, {
                    "Id": "80410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Sheikh Zayed Road"
                }]
            }, {
                "Id": "4b410628-3c49-6e48-96d2-ff5a00702077",
                "Name": "Fujairah",
                "Areas": []
            }, {
                "Id": "48410628-3c49-6e48-96d2-ff5a00702077",
                "Name": "Ras Al Khaimah",
                "Areas": []
            }, {
                "Id": "3c410628-3c49-6e48-96d2-ff5a00702077",
                "Name": "Sharjah",
                "Areas": [{
                    "Id": "02420628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Al Sour"
                }, {
                    "Id": "05420628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Corniche"
                }, {
                    "Id": "ff410628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Sharjah Airport"
                }]
            }, {
                "Id": "3f410628-3c49-6e48-96d2-ff5a00702077",
                "Name": "Umm AL Quwaim",
                "Areas": [{
                    "Id": "0a420628-3c49-6e48-96d2-ff5a00702077",
                    "Name": "Al Ittihad Roundabout"
                }]
            }] || [],
            mapPinTemplate: null,
            listRowTemplate: null,

            init: function () {

                _.templateSettings = {
                    interpolate: /\<\@\=(.+?)\@\>/g,
                    evaluate: /\<\@(.+?)\@\>/g
                };
                this.mapPinTemplate = _.template($("#pin").html());
                this.listRowTemplate = _.template($("#listTemplate").html());

                var self = this;

                //append icons to duRoadshows
                var i = this.duRoadshows.length,
                    roadshow;
                while (i--) {
                    roadshow = this.duRoadshows[i];
                    switch (roadshow.LocationType) {
                        case "1":
                            roadshow["icon"] = "icon-roadshow";
                            break;
                    }
                }

                var substringMatcher = function (strs) {
                    return function findMatches(q, cb) {
                        var matches, substrRegex;
                        matches = [];
                        substrRegex = new RegExp(q, 'i');
                        $.each(strs, function (i, str) {
                            if (substrRegex.test(str.Title)) {
                                matches.push({
                                    value: str.Title
                                });
                                if (matches.length > 10) {
                                    return false;
                                }
                            }
                        });
                        cb(matches);
                    };
                };

                $('#mm-search').typeahead({
                    minLength: 3,
                    highlight: true,
                }, {
                    name: 'roadshows',
                    source: substringMatcher(self.duRoadshows)
                }).on('typeahead:selected', function (obj, datum) {
                    self.getData();
                });

                $('#mm-search').on("keypress", function (event) {
                    if ($(this).val().length > 3) {
                        self.getData();
                    }
                });

                $(".close").on("click", function (event) {
                    event.preventDefault();
                    $(this).closest(".panel").hide();
                    self.getData();
                });

                $(".btn-bump").on("click", function (event) {
                    event.preventDefault();
                    var $this = $(this);
                    $this.toggleClass("active");
                    if ($this.data("show")) {
                        $($this.data("show")).toggle();
                    }
                    self.getData();
                });
                var timeout;
                $(".panel-heading, #toggle-filters").on("click", function (event) {
                    event.preventDefault();
                    var $this = $("#mobile-filter").find(".panel-heading");
                    var body = $("#mobile-filter").find(".panel");
                    var toggler = $("#toggle-filters");

                    if (!body.hasClass("open")) {
                        body.addClass("open");
                        $this.find("i").removeClass("fa-minus").addClass("fa-plus");
                        toggler.html(toggler.data("less"));
                        timeout = setTimeout(function () {
                            body.css("overflow", 'visible');
                            timeout = null;
                        }, 600);
                    } else {
                        if (timeout) {
                            timeout.cancel();
                        }
                        body.css("overflow", 'hidden').removeClass("open");
                        $this.find("i").removeClass("fa-plus").addClass("fa-minus");
                        toggler.html(toggler.data("more"));
                    }
                });

                $(".filter").on("change", function (event) {
                    self.getData();
                });

                $("body").on("click", ".getDirection", function (event) {
                    event.preventDefault();
                    self.handleTabs(true);
                    $("#directions-panel").show();
                    var row = $(event.target).closest("p");
                    $("#destination")
                        .html(row.data("displayaddress"))
                        .data("lat", row.data("lat"))
                        .data("lng", row.data("lng"));
                });

                $("body").on("click", ".viewOnMap", function (event) {
                    event.preventDefault();
                    self.handleTabs(true);
                    Map.catchMarker($(this).closest("p").data("guid"));
                });


                $("#calcRoute").on("click", function (event) {
                    event.preventDefault();
                    var dest = $("#destination");
                    Map.calcRoute({
                        lat: dest.data("lat"),
                        lng: dest.data("lng")
                    });
                });

                $(".detect").on("click", function (event) {
                    event.preventDefault();
                    $(".location").hide();
                    $("#detecting-location").show();
                    Map.fetchAddress(function (result) {
                        $("#location").val(result);
                        $(".location").hide();
                        $("#location-detected").show();

                        $("#calcRoute").removeAttr("disabled");
                        $("#reset").show();
                    }, function () {
                        $(".location").hide();
                        $("#error-location").show();
                    });
                });

                $("#location").on("blur", function (event) {
                    if ($(this).val() != "") {
                        $("#calcRoute").removeAttr("disabled");
                        $("#reset").show();
                    }
                });

                $("#reset").on("click", function (event) {
                    $("#location").val("");
                    $("#calcRoute").attr("disabled", "disabled");
                    $("#reset").hide();
                    Map.clearRoute();
                });

                $("#list-view").on("click", function (event) {
                    event.preventDefault();
                    this.handleTabs(false);
                    Map.resetRoute();
                    this.getData();
                }.bind(this));

                $("#map-view").on("click", function (event) {
                    event.preventDefault();
                    this.handleTabs(true);
                    this.getData();
                }.bind(this));

                $("#reset-search").on("click", function (event) {
                    event.preventDefault();
                    $(".filter").val("").trigger("chosen:updated");
                    $(".btn-bump.active").click();
                    $(".mma-stores").click();
                    $("#mm-search").val("");
                    self.getData();
                });

                this.loadEmirates();
                this.loadAreas();

                this.getData();
            },

            handleTabs: function (map) {
                if (map) {
                    $("#list, #list-view").removeClass("active");
                    $("#map, #map-view").addClass("active");
                } else {
                    $("#map, #map-view").removeClass("active");
                    $("#list, #list-view").addClass("active");
                }
            },

            getData: function () {
                var self = this;

                function stringContains(item, keyw) {
                    keyw = ($.trim(keyw) || "").toLowerCase();
                    if (keyw) {
                        return item.toLowerCase().indexOf(keyw) > -1;
                    }
                    return true;
                }

                function arrayContains(array, item) {
                    if (item) {
                        if (item == "All" || item == "") {
                            return true;
                        }
                        return array.indexOf(item) > -1;
                    }
                    return true;
                }

                var surMobile = !$("#mobile-filter").is(":hidden");

                var keyword = $("#mm-search").val();
                var emirate = $("#mms-emirates").val();
                var area = $("#mms-areas").val();
                var service = $("#mms-services").val();
                var wifiType = $("#mms-type").val();
                var list = !$("#list").is(":hidden");
                var locationType = $("#mm-location button.active").map(function () {
                    return $(this).data("type") + '';
                }).get();

                var results = [],
                    Lengths = {
                        "1": 0
                    };

                $.each(this.duRoadshows, function () {
                    var check = (keyword != "" && stringContains(this.Title, keyword)) ||
                        (stringContains(this.Title, keyword) &&
                            arrayContains(this.LocationAreaIds, emirate) &&
                            arrayContains(this.LocationAreaIds, area));
                    if (check) {
                        //Lengths[this.LocationType]++;
                        Lengths["1"]++;
                        results.push(this);
                    }
                });

                var roadshows = [];

                var l = results.length,
                    value;

                if (results.length > 0) {
                    $("#chosen-text").show();
                    $("#found-nothing").hide();
                } else {
                    $("#chosen-text").hide();
                    $("#found-nothing").show();
                }

                //TODO: make this a template
                var roadshowsText = 'Roadshows';
                if (Lengths[1] <= 1) {
                    roadshowsText = 'Roadshow';
                }



                $('#result').html("").append((Lengths[1] > 0 ? (Lengths[1] + ' ' + roadshowsText + ' ') : ''));
                var chosen = $("#mms-emirates").find("option:selected").text();
                if (chosen == "") {
                    chosen = "All";
                }
                $('#chosen-emirate').html("'" + chosen + (chosen == "All" ? " Emirates" : "") + "'");
                chosen = $("#mms-areas").find("option:selected").text();
                if (chosen == "") {
                    chosen = "All";
                }
                $('#chosen-area').html("'" + chosen + (chosen == "All" ? " Area" : "") + "'");
                chosen = $("#mms-services").find("option:selected").text();
                if (chosen == "") {
                    chosen = "All";
                }
                //$('#chosen-type').html("'" + chosen + (chosen == "All" ? " Services Offered" : "") + "'");

                //always draw the map (for the case when user switchs back to map without invoking getData (getting directions or viewing on map)
                var mapLocations = [];
                l = results.length;
                for (var i = 0; i < l; i++) {
                    value = results[i];
                    //value["OfferedServicesList"] = value["OfferedServices"].join(", "); 
                    mapLocations.push({
                        html: self.mapPinTemplate(value),
                        id: value.Id,
                        lat: value.Latitude,
                        lng: value.Longitude,
                        icon: value.icon
                    });
                }
                Map.drawMap(mapLocations);

                //only draw the list when needed

                var pageLength = $(window).width() < 768 ? 3 : 6;
                if (list) {
                    l = results.length;
                    if (l == 0) {
                        $("#list").html($("#noitemsTemplate").html());
                        return false;
                    }
                    var current = 0;
                    var top = Math.min(l, pageLength);

                    for (var i = current; i < top; i++) {
                        value = results[i];
                        roadshows.push(self.listRowTemplate(value));
                    }

                    $("#list").html(roadshows.join(''));
                    //var pages = parseInt(results.length / pageLength) + 1;
                    var pages = parseInt(Math.ceil(results.length / pageLength));
                    $('#list').bootpag({
                        total: pages,
                        page: 1,
                        maxVisible: 5,
                        leaps: true,
                        next: '&gt;&gt;',
                        prev: '&lt;&lt;'
                    }).on("page", function (event, num) {
                        roadshows = [];
                        l = results.length;
                        var current = pageLength * (num - 1);
                        var top = Math.min(l, current + pageLength);
                        for (var i = current; i < top; i++) {
                            value = results[i];
                            roadshows.push(self.listRowTemplate(value));
                        }
                        $("#list").html(roadshows.join(''));
                        $(this).bootpag({
                            total: pages,
                            maxVisible: 5,
                            page: num
                        });
                    });

                }
            },

            loadEmirates: function () {
                var self = this;
                var html = [],
                    i = this.cities.length,
                    l = i - 1,
                    item;
                html.push('<option value></option>');
                while (i--) {
                    item = this.cities[l - i];
                    html.push("<option value='" + item.Id + "'>" + item.Name + "</option>");
                }
                $('#mms-emirates').html(html.join('')).on("change", function () {
                    var $this = $(this);
                    self.loadAreas($this.data("where"), $this.val());
                }).chosen();
                $('#mms-areas').chosen();
            },

            loadAreas: function (where, emirate) {
                var areas = $(where).attr("disabled", "disabled");
                var city;
                var i = this.cities.length;
                while (i--) {
                    city = this.cities[i];
                    if (city.Id == emirate) {
                        break;
                    }
                }

                var html = [],
                    i = city.Areas.length,
                    l = i - 1,
                    item;
                html.push('<option value></option>');
                while (i--) {
                    item = city.Areas[l - i];
                    html.push("<option value='" + item.Id + "'>" + item.Name + "</option>");
                }
                areas.html(html.join('')).removeAttr("disabled").trigger("chosen:updated");
            }
        }

        $(function () {
            MapModule.init();
        });

    });

});