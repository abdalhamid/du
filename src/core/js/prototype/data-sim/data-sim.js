'use strict';

require(['jquery'], function () {

    $(function () {

        if ($('.data-sim').length === 0) {
            return;
        }

        var $container;

        var dataContractSlider;

        var isTouch;

        var isAr = $('html').attr('dir') === "rtl";

        var firstTimeLayoutComplete = true;

        var emirateObs, areaObs, homeservicesObs;

        var addGtmDataLayer = function addGtmDataLayer(eventAction, eventLayer) {
            dataLayer.push({
                EventCategory: "fixed_plans",
                EventAction: eventAction,
                EventLabel: eventLayer
            });
        };

        //Initialize matchHeight
        var matchRowHeights = function matchRowHeights(visibleOnly) {

            var matchHeight = function (selectedElements) {
                if (selectedElements.length == 0) return;
                var elements = selectedElements;
                var largest = 0;

                var i = elements.length;
                while (i--) {
                    elements[i].style.height = "auto";

                    var height = elements[i].offsetHeight;
                    if (height > largest) {

                        largest = height;
                    }
                }
                var j = elements.length;
                while (j--) {
                    elements[j].style.height = largest + "px";
                }
            };


            matchHeight($(".filter-catg > h4"), {});

            var visible = '';
            if (visibleOnly) {
                visible = ':visible';
            }
            matchHeight($('.card-item:visible > .card-content > .you-pay > h4' + visible), {});
            matchHeight($('.card-item:visible > .card-content > .you-pay > h5' + visible), {});
            matchHeight($('.card-item:visible > .card-content > .you-get > h4' + visible), {});
            matchHeight($('.card-item:visible > .card-content > .you-get > div:not(".benefits")'), {});
            matchHeight($('.card-item:visible > .card-content > .offers > ul > li > .offer-text-container > .offer-text' + visible), {});

            var firstCard = $('.card-item:visible').eq(0);

            firstCard.find('.you-get > div:not(".benefits")').each(function (index, element) {
                var elementIndex = $(element).index() + 1;
                matchHeight($('.card-item:visible > .card-content > .you-get > div:not(".benefits"):nth-child(' + elementIndex + ')' + visible), {});
                matchHeight($('.card-item:visible > .card-content > .offers  > ul > li > .offer-text-container > .offer-text' + visible), {});

            });

            firstCard.find('.you-get > div.benefits > div').each(function (index, element) {
                var elementIndex = $(element).index() + 1;
                matchHeight($('.card-item:visible > .card-content > .you-get > div.benefits > div:nth-child(' + elementIndex + ') > h5' + visible), {});
                matchHeight($('.card-item:visible > .card-content > .you-get > div.benefits > div:nth-child(' + elementIndex + ') > h6' + visible), {});
                matchHeight($('.card-item:visible > .card-content > .offers  > ul > li > .offer-text-container > .offer-text' + visible), {});

            });
        };

        var updateFilters = function () {
            var filterValue;
            var $count = parseInt(dataContractSlider.noUiSlider.get());
            var $value = $('.dataContract .value');
            var $minutes = $.trim($('.plan-sub-filters a.selected').attr('data-filter'));
            var isAr = $('html').attr('dir') == 'rtl';

            if ($minutes.length > 0) {
                $minutes = '.' + $minutes;
            }

            if ($count == 3) {
                filterValue = '.3gb' + $minutes;
                $value.text('3GB');
                $('.dataContract .labels li').removeClass('active');
                $('.dataContract .labels li:eq(0)').addClass('active');
                $('.selection .data h5').text(isAr ? 'بسيط' : 'Light');
            } else if ($count == 10) {
                filterValue = '.10gb' + $minutes;
                $value.text('10GB');
                $('.dataContract .labels li').removeClass('active');
                $('.dataContract .labels li:eq(1)').addClass('active');
                $('.selection .data h5').text(isAr ? 'متوسط' : 'Medium');
            } else if ($count == 50) {
                filterValue = '.50gb' + $minutes;
                $value.text('50GB');
                $('.dataContract .labels li').removeClass('active');
                $('.dataContract .labels li:eq(2)').addClass('active');
                $('.selection .data h5').text(isAr ? 'ثقيل' : 'Heavy');
            }

            $('.dataContract .catg-content').attr('rel', filterValue);

            if ($container) {
                $container.isotope({
                    filter: filterValue
                });
            }
            updateFiltersSummary();
            matchRowHeights(true);
        };

        var updateFiltersSummary = function updateFiltersSummary() {
            var customer = $('.plan-sub-filters').find('a.selected');
            var data = $('.labels').find('li.active');

            if (customer.text()) {
                $("[data-plan-filters-summary-customer] h5").text(customer.text());
            } else {
                $("[data-plan-filters-summary-customer] h5").text("-");
            }

            if (data.text()) {
                $("[data-plan-filters-summary-data] h5").text(data.text());
            } else {
                $("[data-plan-filters-summary-data] h5").text("-");
            }

            if ($(window).width() < 992) {
                $('[data-plan-filters-summary-customer], [data-plan-filters-summary-data]').removeClass('hide');
            }

        };

        updateFiltersSummary();

        //Sliders for plans pages
        if ($("[id*='slider']").length) {
            requirejs(["nouislider.min"], function (noUiSlider) {
                $(function () {
                    var dataSlider = document.getElementById('data-slider');
                    if (dataSlider != undefined) {
                        noUiSlider.create(dataSlider, {
                            start: [100], // Handle start position
                            step: 1, // Slider moves in increments of '10'
                            snap: true,
                            //margin: 2, // Handles must be more than '20' apart
                            connect: 'lower', // Display a colored bar between the handles
                            direction: isAr ? 'rtl' : 'ltr', // Put '0' at the bottom of the slider
                            //orientation: 'vertical', // Orient the slider vertically
                            //behaviour: 'tap-drag', // Move handle on tap, bar is draggable
                            range: { // Slider can select '0' to '100'
                                'min': [1],
                                '14.28%': [2, 6],
                                '28.57%': [6, 10],
                                '42.85%': [10, 18],
                                '57.13%': [18, 25],
                                '71.41%': [25, 50],
                                '85.69%': [50],
                                'max': [100]
                            },
                            pips: { // Show a scale with the slider
                                mode: 'steps',
                                density: 2
                            }
                        });
                    }

                    var dataSliderChangeValue = function dataSliderChangeValue() {
                        // scrollUp();

                        var selectedValue = "";
                        switch (parseInt(dataContractSlider.noUiSlider.get())) {
                            case 3:
                                selectedValue = "3gb";
                                break;
                            case 10:
                                selectedValue = "10gb";
                                break;
                            case 50:
                                selectedValue = "50gb";
                                break;
                        }
                        addGtmDataLayer("type_data", selectedValue);
                    };

                    var dataUpdate = function dataUpdate() {
                        dataSlider.noUiSlider.on('update', function () {
                            var filterValue = "";
                            var $count = parseInt(dataSlider.noUiSlider.get());
                            var $value = $('.data .value');
                            var $minutes = $('.plan-sub-filters a.selected').attr('data-filter');
                            $value.text($count);

                            for (i = 1; i <= $count; i++) {
                                if (i == $count) filterValue += '.data' + i + $minutes;
                                else filterValue += '.data' + i + $minutes + ', ';
                            }

                            $('.data .catg-content').attr('rel', filterValue);

                            // use filterFn if matches value
                            runFilters(filterValue);
                            updateFiltersSummary();
                        });

                        //alert(parseInt($('.you-get h5:eq(0) span').text()));
                    };

                    var initialData = 50;
                    var dataMatches = document.location.search.match(/[?&]data=([^&^#]+)/);
                    if (dataMatches != null && dataMatches.length > 1) {
                        var dataText = $.trim(dataMatches[1]).toLocaleLowerCase();
                        if (dataText == '3gb') {
                            initialData = 0;
                        } else if (dataText == '10gb') {
                            initialData = 10;
                        } else if (dataText == '50gb') {
                            initialData = 50;
                        }
                    }

                    dataContractSlider = document.getElementById('data-contract-slider');
                    if (dataContractSlider != undefined) {
                        noUiSlider.create(dataContractSlider, {
                            start: [initialData], // Handle start position
                            //step: 1, // Slider moves in increments of '10'
                            snap: true,
                            //margin: 2, // Handles must be more than '20' apart
                            //connect: 'lower', // Display a colored bar between the handles
                            direction: isAr ? 'rtl' : 'ltr', // Put '0' at the bottom of the slider
                            //orientation: 'vertical', // Orient the slider vertically
                            //behaviour: 'drag', // Move handle on tap, bar is draggable
                            range: { // Slider can select '0' to '100'
                                'min': 3,
                                '50%': 10,
                                'max': 50
                            }
                            /*,
                                               pips: { // Show a scale with the slider
                                               mode: 'values',
                                               values: ['Small', 'Medium', 'Large'],
                                               density: 3
                                               }*/
                        });
                    }

                    var dataContractUpdate = function dataContractUpdate() {
                        dataContractSlider.noUiSlider.on('update', updateFilters);
                    };

                    dataContractUpdate();

                    dataContractSlider.noUiSlider.on('change', dataSliderChangeValue);

                    $('.du-slider .labels li').on('click', function () {
                        $('.du-slider .labels li').removeClass('active');
                        $(this).addClass('active');
                        dataContractSlider.noUiSlider.set($(this).attr('rel'));
                        dataSliderChangeValue();
                    });

                    var contractSlider = document.getElementById('contract-slider'); //TODO: Element does not exist. Consider removing.
                    if (contractSlider != undefined) {
                        noUiSlider.create(contractSlider, {
                            start: [24], // Handle start position
                            //step: 1, // Slider moves in increments of '10'
                            snap: true,
                            //margin: 2, // Handles must be more than '20' apart
                            //connect: 'lower', // Display a colored bar between the handles
                            direction: isAr ? 'rtl' : 'ltr', // Put '0' at the bottom of the slider
                            //orientation: 'vertical', // Orient the slider vertically
                            //behaviour: 'drag', // Move handle on tap, bar is draggable
                            range: { // Slider can select '0' to '100'
                                'min': 3,
                                '50%': 10,
                                'max': 50
                            },
                            pips: { // Show a scale with the slider
                                mode: 'steps',
                                density: 100,
                                format: wNumb({
                                    postfix: ' mnth'
                                })
                            }
                        });
                    }

                    var contractUpdate = function contractUpdate() {
                        contractSlider.noUiSlider.on('update', function () {
                            var filterValue;
                            var $count = parseInt(contractSlider.noUiSlider.get());
                            var $value = $('.contract .value');
                            var $measure = $('.contract .measure');

                            if ($count == 3) {
                                filterValue = '.3gb';
                                $value.text('No');
                                $measure.text('Contract');
                            } else if ($count == 10) {
                                filterValue = '.10gb';
                                $value.text('12');
                                $measure.text('Months');
                            } else if ($count == 50) {
                                filterValue = '.50gb';
                                $value.text('24');
                                $measure.text('Months');
                            }

                            $('.contract .catg-content').attr('rel', filterValue);

                            runFilters(filterValue);
                        });
                        filterByData();
                    };

                    $('.catg-content .round-box a').click(function () {
                        var elem = $(this).parent().parent();
                        $('.catg-content').removeClass('selected');

                        setTimeout(function () {
                            elem.addClass('selected');
                        }, 400);

                        if (elem.parent().hasClass('data')) dataUpdate();
                        else if (elem.parent().hasClass('price')) priceUpdate();
                        else if (elem.parent().hasClass('contract')) contractUpdate();

                        return false;
                    });

                    $('.plan-sub-filters').each(function () {
                        var $this = $(this);
                        $(this).on('click', 'a', function () {
                            $('a', $this).removeClass('selected');
                            $('.selection .mins h5').text($(this).attr('rel'));
                            $('a .icon', $this).removeClass('icon-ok-circled').addClass('icon-ok-circled2');
                            $(this).addClass('selected');
                            $('.icon', $(this)).removeClass('icon-ok-circled2').addClass('icon-ok-circled');

                            // scrollUp();
                            //updateFilters();
                            return false;
                        });
                    });

                    var toTitleCase = function toTitleCase(str) {
                        return str.replace(/\w\S*/g, function (txt) {
                            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                        });
                    };
                });
            });
        }

        var createSelectButtonLink = function createSelectButtonLink(button) {
            var url = $(button).attr("data-href-pattern");
            var journey = $(button).closest(".offers").find("li").attr("data-plan-title");
            var isVariationAdded = $(button).closest(".offers").find("li [data-offers-add]").css("display") === "none";
            var variationId = isVariationAdded ? $(button).closest(".offers").find("li").attr("data-offers-id") : "";

            var planTitle = $(button).closest(".card-item").attr("data-plantitle");
            var smsCode = $(button).closest(".card-item").attr("data-smscode");
            var dataSize = $(button).closest(".card-item").attr("data-datasize");
            var contractPeriod = $(button).closest(".card-item").attr("data-contractperiod");
            var dataPlan = $(button).closest(".card-item").attr("data-dataplan");
            var monthlyFee = $(button).closest(".card-item").attr("data-monthlyfee");
            var minutes = $(button).closest(".card-item").attr("data-minutes");

            var monthlyFeeValue = $.trim(monthlyFee).match(/[0-9.,]+/);
            var subFilter = $('.plan-sub-filters .selected').attr('data-filter');
            var planCategory = subFilter === '.with-tv' ? 'TV, Internet & landline' : 'Internet & landline';
            var packageType = subFilter.substring(1);

            var customerType = '';
            var customerTypeFilter = $('.plan-filters-mobile-sim .plan-sub-filters a.selected[data-filter]').attr('data-filter');
            if (customerTypeFilter === 'customer') {
                customerType = 'existing'
            } else if (customerTypeFilter === 'not-costumer' || customerTypeFilter === 'not-customer') {
                customerType = 'new'
            }

            var customizeStorage = $(button).closest(".card-item").find("[data-storage-product]").text();
            var customizeColor = $(button).closest(".card-item").find("[data-color-product]").text();
            var customizePrice = $(button).closest(".card-item").find("[data-price-product]").text();

            url = url.replace("[selectedOfferVariation]", encodeURIComponent($.trim(variationId)));
            url = url.replace("[plantitle]", encodeURIComponent($.trim(planTitle)));
            url = url.replace("[monthlyfee]", encodeURIComponent($.trim(monthlyFee)));
            url = url.replace("[contractperiod]", encodeURIComponent($.trim(contractPeriod)));
            url = url.replace("[dataplan]", encodeURIComponent($.trim(dataPlan)));
            url = url.replace("[datasize]", encodeURIComponent($.trim(dataSize)));
            url = url.replace("[minutes]", encodeURIComponent($.trim(minutes)));
            url = url.replace("[smscode]", encodeURIComponent($.trim(smsCode)));
            url = url.replace("[customer]", encodeURIComponent($.trim(customerType)));
            url = url.replace("[prodColor]", encodeURIComponent($.trim(customizeColor)));
            url = url.replace("[prodStorage]", encodeURIComponent($.trim(customizeStorage)));
            url = url.replace("[prodPrice]", encodeURIComponent($.trim(customizePrice)));

            $(button).attr("href", url);

            var selectTags = pushContextAwareDataLayer(planCategory, 'AED ' + monthlyFee + '_select', true);
            var infoTags = pushContextAwareDataLayer(planCategory, 'AED ' + monthlyFee + '_info', true);

            $(button).attr('onclick', 'dataLayer.push(' + JSON.stringify(selectTags) + ');');
            $(button).parent().find('.showPlanDetailsButton').attr('onclick', 'dataLayer.push(' + JSON.stringify(infoTags) + ');');
        };

        var createSelectButtonLinkAll = function createSelectButtonLinkAll() {
            $("a.buyPlanButton").each(function () {
                createSelectButtonLink(this);
            });
        };

        var handleSelectButtonLinks = function handleSelectButtonLinks() {
            $("a.buyPlanButton").each(function () {
                if (!$(this).attr("data-href-pattern")) {
                    $(this).attr("data-href-pattern", $(this).attr("href"));
                }
                createSelectButtonLink(this);
            });
        };

        var displayMessageIfNoCards = function displayMessageIfNoCards(filtersApplied) {
            if (!$container) {
                return;
            }

            var isotopeHasCards = $container.data("isotope").filteredItems.length;

            if (!isotopeHasCards) {
                if (filtersApplied) {
                    $(".no-cards-message.no-results").addClass("show-message");
                } else {
                    $(".no-cards-message.no-area-selected").addClass("show-message");
                }
            } else {
                $(".no-cards-message").removeClass("show-message");
            }
        };

        var execSubFilters = function execSubFilters(selectedElem) {
            if (!!selectedElem) {
                $('.plan-sub-filters a').removeClass('selected');
                $(selectedElem).addClass('selected');
            }


            updateFiltersSummary();
            createSelectButtonLinkAll();
        };

        var retrieveAreas = function retrieveAreas(callback) {
            var currentLanguage = $("html").attr("lang");
            var url = "../common/scripts/prototype/data-sim/fixedPlanAreas." + currentLanguage + ".json";

            $.getJSON(url, callback);
        };

        var styleDefaultHtmlSelect = function styleDefaultHtmlSelect(dom) {
            if (!isTouch) {
                return;
            }

            if ($(".calculate-text-length").length <= 0) {
                $("body").append("<span class='calculate-text-length'></span>");
            }

            if ($(dom).parent().find(".filter-dropdown-caret").length <= 0) {
                $(dom).after("<span class='filter-dropdown-caret'></span>");
            }

            var text = $(dom).find("option:selected").text();
            $(".calculate-text-length").text(text);
            var textIndent = $(".calculate-text-length").width() / 2;

            $(dom).css("text-indent", "calc(50% - " + textIndent + "px)");
        };

        var getUrlParameterByName = function getUrlParameterByName(name, url) {
            if (!url) {
                url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        };

        var handleShowOnJourneyBindings = function handleShowOnJourneyBindings() {
            var journeyUrl = getUrlParameterByName('journey');
            if (!journeyUrl) {
                journeyUrl = "";
            }

            $("[data-show-on-journey]").each(function () {
                var journeyFilter = $(this).attr("data-show-on-journey");

                if (journeyFilter === journeyUrl) {
                    $(this).removeClass("hide");
                } else {
                    $(this).addClass("hide");
                }
            });
        };

        var handleStickyHeader = function handleStickyHeader() {
            var appendStickyClass = function appendStickyClass() {
                var breakpoint = $('.plan-sub-filters').position().top + $('.plan-sub-filters').height() + 200;
                if ($(window).scrollTop() >= breakpoint && !$(".sticky-filters-summary").hasClass("sticky")) {
                    $(".sticky-filters-summary").addClass("sticky");
                } else if ($(window).scrollTop() < breakpoint && $(".sticky-filters-summary").hasClass("sticky")) {
                    $(".sticky-filters-summary").removeClass("sticky");
                }
            };

            appendStickyClass();

            $(window).scroll($.throttle(250, appendStickyClass));
            $(window).on("orientationchange", appendStickyClass);
        };

        function getUriParameters() {

            var parameters = {};
            var search = decodeURI(document.location.search);
            var pattern = /[&?]([^=^&^#]+)=([^&^#]+)/g;
            var match = null;

            while (match = pattern.exec(search)) {
                var parameter = match[1];
                var value = match[2];

                parameters[parameter] = decodeURIComponent(value.replace(/\+/g, ' '));
            }

            return parameters;
        }

        function formatDataLayerCategory(category) {
            var fallbackCategory = 'Homeservices_plan';
            var result = fallbackCategory;
            if ($.trim(category).length > 2) {
                result = category[0].toUpperCase() + category.toLowerCase().replace(/\s/g, '_').substring(1);
            } else if ($.trim(category).length > 1) {
                result = journey[0].toUpperCase();
            }

            return result;
        }

        function pushContextAwareDataLayer(action, label, deferPush) {
            var parameters = getUriParameters();
            var journey = parameters.journey;
            var category = formatDataLayerCategory(journey);

            return pushDataLayer(category, action, label, deferPush);
        }

        function pushDataLayer(category, action, label, deferPush) {
            var tags = {
                event: 'click',
                category: category,
                action: action,
                label: label
            };

            if (!deferPush) {
                dataLayer.push(tags);
            }

            return tags;
        }

        require(['require', 'isotope', 'jquery.sticky-kit.min', 'lib/jquery.matchheight.min', /*"bootstrapselect",*/ "lib/modernizr-2.6.2.min", /*'bootstrap',*/ 'lib/jquery.observer.min', "lib/jquery.ba-throttle-debounce.min"], function (require, Isotope) {
            isTouch = $("html").hasClass("touch");

            handleShowOnJourneyBindings();
            handleStickyHeader();

            // define observables and subscribe to them
            emirateObs = $.observable("");
            areaObs = $.observable("");

            emirateObs.subscribe(updateFiltersSummary);
            areaObs.subscribe(updateFiltersSummary);

            require(['jquery-bridget/jquery.bridget'], function (jQueryBridget) {

                $('.plans .hidden').removeClass('hidden').addClass('softHidden');

                var addGtmDataLayer = function addGtmDataLayer(eventAction, eventLayer) {
                    dataLayer.push({
                        EventCategory: "fixed_plans",
                        EventAction: eventAction,
                        EventLabel: eventLayer
                    });
                };

                $("[data-toggle=popover]").popover({
                    trigger: 'focus',
                    container: 'body',
                    html: true,
                    placement: function placement(context, source) {
                        var position = $(source).offset().left;
                        var windowWidth = $(window).width();

                        if (position >= windowWidth - 100) {
                            return "left";
                        }
                        if (position <= 100) {
                            return "right";
                        }

                        return "auto top";
                    }
                });

                // Fixes to iPhone and iPad
                if (navigator.userAgent.indexOf('iPad') >= 0 || navigator.userAgent.indexOf('iPhone') >= 0) {
                    // isolate the fix to apply only when a tooltip is shown
                    $("[data-toggle=popover]").on("shown.bs.popover", function () {
                        $('body').css('cursor', 'pointer');
                    });

                    // Return cursor to default value when tooltip hidden.
                    // Use 'hide' event (as opposed to "hidden") to ensure .css() fires before
                    // 'shown' event for another tooltip.
                    $("[data-toggle=popover]").on("hide.bs.popover", function () {
                        $('body').css('cursor', 'auto');
                    });

                    //Fixes to iPad only
                    if (navigator.userAgent.indexOf('iPad') >= 0) {
                        //Layout change doesn't work on iPad, so initiate matchHeight here.
                        setTimeout(function () {
                            matchRowHeights(true);
                        }, 500);
                    }
                }

                matchRowHeights();

                //Initialize isotope
                jQueryBridget('isotope', Isotope, $);
                if ($('.plan-cards').length > 0) {
                    $container = $('.isotope').isotope({
                        isOriginLeft: isAr ? false : true,
                        itemSelector: '.card-item',
                        layoutMode: 'fitRows',
                        getSortData: {
                            planindex: '[data-planindex] parseInt'
                        },
                        sortBy: 'planindex',
                        sortAscending: true,
                        isInitLayout: false
                    });
                }

                window.debounceMatchHeight = false;
                $container.isotope('on', 'arrangeComplete', function () {
                    if (window.debounceMatchHeight == false) {
                        window.debounceMatchHeight = true;

                        $container.isotope('layout');
                    } else {
                        window.debounceMatchHeight = false;
                    }
                });

                var forceCardsRepositioning = function forceCardsRepositioning() {
                    try {
                        $container.isotope();
                    } catch (e) {}
                };

                var fixPopovers = function fixPopovers() {
                    //initializePopover();
                };

                var updateHeightAndPosition = function updateHeightAndPosition() {
                    setTimeout(function () {
                        matchRowHeights(true);
                        setTimeout(forceCardsRepositioning, 200);
                    }, 500);
                };

                var recenterPopup = function recenterPopup(popupId) {
                    var popup = $("#" + popupId);
                    popup.css("margin-left", -popup.outerWidth() / 2);

                    if ($('body.mobile').length > 0) {
                        var viewportFromTop = $(window).scrollTop();
                        if (viewportFromTop === 0) {
                            popup.css("position", "fixed");
                            popup.css("top", "10px");
                        } else if ($(window).offset()) {
                            popup.css("position", "absolute");
                            popup.css("top", $(window).offset().top + "px");
                        }
                    }
                };

                var fixPopup = function fixPopup() {
                    if ($("#PlanDetailsPopup").css("display") !== "none") {
                        recenterPopup("PlanDetailsPopup");
                    }
                };

                var getTopPositionFilters = function getTopPositionFilters() {
                    var subTitleTop = $('.plan-filters-mobile-sim').offset().top;
                    var headerHeight = $(".eitc-top-pane").outerHeight();
                    return subTitleTop - headerHeight;
                };

                var scrollUpToFilters = function scrollUpToFilters() {
                    $('html, body').stop(true).animate({
                        scrollTop: getTopPositionFilters()
                    }, 300);
                };

                $(window).on("orientationchange", function () {
                    var setTimeoutRifle = function setTimeoutRifle(callback, timeout, numTimes) {
                        var idx = 1;

                        var setTimeoutRifleElab = function setTimeoutRifleElab() {
                            setTimeout(function () {
                                try {
                                    callback();
                                } catch (e) {}

                                idx++;

                                if (idx <= numTimes) {
                                    setTimeoutRifleElab();
                                }
                            }, timeout);
                        };

                        setTimeoutRifleElab();
                    };

                    // updateHeightAndPosition();
                    setTimeoutRifle(function () {
                        matchRowHeights(true);
                        setTimeoutRifle(forceCardsRepositioning, 100, 2);
                    }, 100, 5);

                    setTimeoutRifle(function () {
                        fixPopup();

                        if (window.pubsub) {
                            window.pubsub.publish("autoMatchheight/refresh", []);
                        }
                    }, 100, 5);

                    $('.planDetailsPopup:visible').each(function () {

                        var popupWidth = $(this).width();
                        $(this).css('margin-left', -(popupWidth / 2) + 'px');

                        this.style.removeProperty('width');
                    });
                });


                var showCardsAfterReady = function showCardsAfterReady() {
                    // Mark the page as fully rendered
                    setTimeout(function () {
                        $('.plan-cards').addClass('loaded');
                    }, 500);

                    fixPopup();

                    setTimeout(function () {
                        matchRowHeights(true);
                        if (firstTimeLayoutComplete) {
                            // It shouldn't run every time because it slows down the site
                            setTimeout(forceCardsRepositioning, 200);
                            firstTimeLayoutComplete = false;
                        }

                        if (window.pubsub) {
                            window.pubsub.publish("autoMatchheight/refresh", []);
                        }
                    }, 500);
                }

                var triggerClick = function triggerClick() {
                    $('[data-sliderfilter="50g"]').trigger('click');
                    showCardsAfterReady();
                }
                triggerClick();


                // Fix to close Popover on iPad
                if ($("html").hasClass("ipad")) {
                    $(document).on("click touchstart", function (e) {
                        if ($(e.target).data("toggle") !== "popover" && $(e.target).parents("[data-toggle='popover']").length === 0 && $(e.target).parents(".popover.in").length === 0) {
                            $("[data-toggle='popover']").popover("hide");
                        }
                    });
                }

                // init plans table filter on pageload
                setTimeout(function () {
                    $('.plans-master > .brandFilter > a:eq(0)').trigger('click');

                    $('.sticky-content .lined-buttons:first a:eq(0)').trigger('click');
                }, 50);

                //Initialize controls
                if ($('.you-get').length > 0) {
                    $('a[href="#ContentPlaceHolderMain"]').click(scrollUpToFilters);

                    $('.you-get').each(function () {
                        $('> div:not(".benefits")', $(this)).each(function (i) {
                            $(this).addClass('item' + i);
                        });
                        $('> div > div', $(this)).each(function (i) {
                            $(this).addClass('benefit' + i);
                        });
                    });

                    $('.you-get > div:not(".benefits"), .you-get > div > div').hover(function () {
                        $('.' + $(this).attr('class')).addClass('highlight');
                    }, function () {
                        $('.you-get > div:not(".benefits"), .you-get > div > div').removeClass('highlight');
                    });
                }

                //Plan information popup
                $('a[data-reveal-id="PlanDetailsPopup"]').on('click', function (e) {
                    e.preventDefault();

                    var cardItem = $(this).closest('.card-item');
                    var planInformationUrl = $(this).attr('href');
                    var planInformationPrintUrl = planInformationUrl;
                    var planInformationDocumentUrl = cardItem.attr('data-documentUrl');
                    var purchasePlanUrl = cardItem.find('.buyPlanButton').attr('href');

                    if (planInformationPrintUrl.indexOf('?') == -1) {
                        planInformationPrintUrl += '?print=true';
                    } else {
                        planInformationPrintUrl += '&print=true';
                    }

                    var planDetailsPopup = $('#PlanDetailsPopup');

                    planDetailsPopup.find('.loadingOverlay').show();

                    planDetailsPopup.find('.planDetailsFrame').attr('src', planInformationUrl);
                    planDetailsPopup.find('.printButton').attr('href', planInformationPrintUrl).attr('target', '_blank');
                    planDetailsPopup.find('.downloadButton').attr('href', planInformationDocumentUrl).attr('target', '_blank');
                    planDetailsPopup.find('.buyButton').attr('href', purchasePlanUrl);

                    recenterPopup("PlanDetailsPopup");

                    var width = Math.round(planDetailsPopup.width());
                    planDetailsPopup[0].style.setProperty('width', width + 'px', 'important');
                });

                $('#PlanDetailsPopup .close-reveal-modal').on('click', function () {

                    var planDetailsPopup = $('#PlanDetailsPopup');

                    planDetailsPopup.find('.planDetailsFrame').attr('src', 'about:blank');
                    planDetailsPopup.find('.printButton').attr('href', 'javascript:void(0);').attr('target', null);
                    planDetailsPopup.find('.downloadButton').attr('href', 'javascript:void(0);').attr('target', null);
                    planDetailsPopup.find('.buyButton').attr('href', 'javascript:void(0);');
                    planDetailsPopup[0].style.removeProperty('width');
                });

                $('#PlanDetailsPopup .printButton').on('click', function (e) {

                    e.preventDefault();

                    if ($('body.android').length > 0 || $('.firefox .desktop').length > 0) {
                        var printUrl = $(this).attr('href');
                        window.open(printUrl);
                    } else {
                        document.PlanDetailsFrame.printDocument();
                    }
                });

                $('.plan-sub-filters a').on('click', function () {
                    execSubFilters(this);

                    var subFilter = $('.plan-sub-filters .selected').attr('data-filter');
                    var packageType = subFilter === '.with-tv' ? 'TV, Internet & landline' : 'Internet & landline';

                    pushContextAwareDataLayer('Service', packageType);

                    updateFilters();

                    return false;
                });

                $('.du-accordion .block-header').on('click', function () {

                    var block = $(this).closest('.block');

                    if (!block.hasClass('open')) {
                        return;
                    }

                    if (block.hasClass('how-to-get-a-sim-block')) {
                        addGtmDataLayer("details", 'howtogetasim');
                    } else if (block.hasClass('how-to-recharge-block')) {
                        addGtmDataLayer("details", 'howtorecharge');
                    } else if (block.hasClass('faqs-block')) {
                        addGtmDataLayer("details", 'faqs');
                    } else if (block.hasClass('terms-and-conditions-block')) {
                        addGtmDataLayer("details", 'termsandconditions');
                    }
                });

                function pushContextAwareDataLayer(action, label, deferPush) {
                    var parameters = getUriParameters();
                    var journey = parameters.journey;
                    var category = formatDataLayerCategory(journey);

                    return pushDataLayer(category, action, label, deferPush);
                }

                function getUriParameters() {

                    var parameters = {};
                    var search = decodeURI(document.location.search);
                    var pattern = /[&?]([^=^&^#]+)=([^&^#]+)/g;
                    var match = null;

                    while (match = pattern.exec(search)) {
                        var parameter = match[1];
                        var value = match[2];

                        parameters[parameter] = decodeURIComponent(value.replace(/\+/g, ' '));
                    }

                    return parameters;
                }

                function showChangePlanConfirmationPopup() {
                    $('.customize-popup').addClass('popup-visible');
                    $("html").addClass("page-popup-open");
                }

                function hideAllPopups() {
                    $('.page-popup').removeClass('popup-visible');
                    $("html").removeClass("page-popup-open");
                }

                var handleOfferButtons = function handleOfferButtons(selectedOfferVariation) {
                    if (selectedOfferVariation) {
                        $("[data-offers-add]").hide();
                        $("[data-offers-remove]").show();
                        $(".offer-text-added").show();
                    } else {
                        $("[data-offers-add]").show();
                        $("[data-offers-remove]").hide();
                        $(".offer-text-added").hide();
                    }

                    $("[data-offers-remove], [data-offers-add]").on("click", function () {
                        var id = $(this).closest("li").attr("data-offers-id");
                        var variationId = $(this).closest("li").attr("data-offers-id");
                        var selectedOfferVariationId = getUrlParameterByName('selectedOfferVariation', document.location.href);
                        var applyOffersGlobally = variationId == selectedOfferVariation ? true : false;

                        var parent = $(this).closest('.card-item');

                        if (applyOffersGlobally) {
                            parent = $('body');
                        }

                        parent.find("li[data-offers-id='" + id + "']").find("[data-offers-add]").toggle();
                        parent.find("li[data-offers-id='" + id + "']").find("[data-offers-remove]").toggle();
                        parent.find("li[data-offers-id='" + id + "']").find(".offer-text-added").toggle();

                        if (applyOffersGlobally) {
                            createSelectButtonLinkAll();
                        } else {
                            var buyPlanButon = parent.find('.buyPlanButton').eq(0);
                            createSelectButtonLink(buyPlanButon[0]);
                        }
                    });

                    var generateStorageButtons = function (variationTitles, variationStorages, variationPrices, selectedStorage) {
                        var templateHtml = $("#offersStorageButtonTemplate").html();

                        $(".storage .choose-storage").remove();

                        var variationTitlesArray = variationTitles.split(',');
                        var variationStoragesArray = variationStorages.split(',');
                        var variationPricesArray = variationPrices.split(',');

                        $(variationTitlesArray).each(function (index, title) {
                            var positionclass = index === 0 ? "first" : (index === variationTitlesArray.length - 1 ? "last" : "");

                            var newHtml = templateHtml.replace(new RegExp("[title]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), title)
                                .replace(new RegExp("[storage]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), variationStoragesArray[index])
                                .replace(new RegExp("[price]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), variationPricesArray[index])
                                .replace(new RegExp("[positionclass]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), positionclass);

                            $(".storage").append(newHtml);
                        });

                        $(".storage .choose-storage input[type='radio']:first").addClass('active').prop('checked', true);

                        getCustomizeStorage();

                        if (selectedStorage) {
                            $(".storage .choose-storage input[value='" + selectedStorage + "']").trigger("click");
                        } else {
                            $(".storage .choose-storage input[type='radio']:first").addClass('active').prop('checked', true);
                        }
                    };

                    var generateStorageButtons2 = function (variations) {

                        var storage = $('.storage');
                        var templateHtml = $("#offersStorageButtonTemplate").html();

                        storage.find('.choose-storage').remove();

                        for (var i = 0; i < variations.length; i++) {

                            var variation = variations[i];

                            var storageHtml = templateHtml;
                            storageHtml = storageHtml.replace(/\[title]/g, variation.Title);
                            storageHtml = storageHtml.replace(/\[storage]/g, variation.Storage);
                            storageHtml = storageHtml.replace(/\[price]/g, variation.MonthlyFee);
                            storageHtml = storageHtml.replace(/\[positionclass]/g, '');

                            var storageElement = $(storageHtml);
                            storageElement.data('variation', variation);

                            if (!variation.Visible) {
                                storageElement.hide();
                            }

                            storage.append(storageElement);
                        }

                        storage.closest('[data-product-storage]').show();
                        if (storage.find('.choose-storage:visible').length <= 0) {
                            storage.closest('[data-product-storage]').hide();
                        } else {
                            storage.find('.choose-storage:visible:first').addClass('first');
                            storage.find('.choose-storage:visible:last').addClass('last');

                            storage.closest('[data-product-storage]').show();
                        }

                        getCustomizeStorage();

                        storage.find(".choose-storage input[type='radio']:first").addClass('active').prop('checked', true);

                        var selectedVariation = storage.find('.choose-storage:first').data('variation');
                        generateColorButtons(selectedVariation.Colors);
                    };

                    var generateColorButtons = function (colors) {

                        var colorsContainer = $('[data-product-color]');
                        var templateHtml = $("#offersColorButtonTemplate").html();

                        colorsContainer.find('.choose-color').remove();

                        for (var i = 0; i < colors.length; i++) {

                            var color = colors[i];
                            var positionclass = i === 0 ? "first" : (i === colors.length - 1 ? "last" : "");

                            var colorHtml = templateHtml;
                            colorHtml = colorHtml.replace(/\[name]/g, color.Name);
                            colorHtml = colorHtml.replace(/\[color]/g, color.Value);

                            var colorElement = $(colorHtml);
                            colorElement.data('color', color);

                            colorsContainer.append(colorElement);
                        }

                        colorsContainer.find(".choose-color input[type='radio']:first").addClass('active').prop('checked', true);
                    };

                    $('[data-offers-customize]').on('click', function (e) {
                        e.preventDefault();

                        var offer = $(this).data('offer');
                        var selectedColor = $(this).closest("li").find("[data-color-product]").text().toLowerCase();
                        var selectedStorage = $(this).closest("li").find("[data-storage-product]").text();

                        pushContextAwareDataLayer('Order_summary', 'change_plan');

                        showChangePlanConfirmationPopup();

                        var cardId = $(this).closest('.card-item').attr('data-card-id');
                        var title = $(this).attr('data-title');
                        var price = $(this).attr('data-price');
                        var duration = $(this).attr('data-duration');
                        var storage = $(this).attr('data-storage');
                        var color = $(this).attr('data-color');
                        var offerID = $(this).attr('data-offer-id');
                        var description = $(this).attr('data-desc');
                        var image = $(this).attr('data-offer-img');
                        var brand = title.split(' ')[0];
                        var product = title.substr(title.indexOf(" ") + 1);
                        var customizable = $(this).attr('data-customizable') === "1";
                        var variationTitles = $(this).attr('data-variation-titles');
                        var variationStorages = $(this).attr('data-variation-storages');
                        var variationPrices = $(this).attr('data-variation-prices');

                        if (!customizable) {
                            storage = null;
                            color = null;
                        }

                        if (!storage) {
                            $('[data-product-storage]').addClass('hidden');
                        } else {
                            $('[data-product-storage]').removeClass('hidden');
                        }
                        if (!color) {
                            $('[data-product-color]').addClass('hidden');
                        } else {
                            $('[data-product-color]').removeClass('hidden');
                        }

                        $('[data-product-title] strong').text(product);
                        $('[data-color-selected]').text(color);
                        $('[data-product-title] span').text(brand + " ");
                        $('[data-product-description]').text(description);
                        $('[data-product-plan]').text(duration);
                        $('.image-desc').attr('src', image);
                        $('.customize-popup').attr('data-popup-id', offerID);
                        $('.customize-popup').attr('data-card-id', cardId);
                        $('[data-brand-image]').attr('src', offer.BrandImage);
                        if ($.trim(offer.BrandImage).length <= 0) {
                            $('[data-brand-image]').hide();
                        } else {
                            $('[data-brand-image]').show();
                        }

                        //generateStorageButtons(variationTitles, variationStorages, variationPrices, selectedStorage);
                        generateStorageButtons2(offer.Installments[0].Variations);

                        $(".monthly-payment-total .value").text(price);
                        $('.breakdown-totals .customize-button').attr('data-customize-price', price);

                        if (selectedColor) {
                            $('.choose-color input[value="' + selectedColor + '"]').trigger("click");
                        }
                        if (selectedStorage) {
                            $('.choose-storage input[value="' + selectedStorage + '"]').trigger("click");
                        }
                    });

                    $(".page-popup").on("click", '.close-button', hideAllPopups);
                    $(".page-popup-overlay").on("click", hideAllPopups);

                    $('.buy-button').on('click', function (e) {
                        e.preventDefault();
                        hideAllPopups();
                    })

                };

                function getCustomizeColor() {
                    $('[data-product-color]').on('click', '.choose-color input',
                        function () {
                            $('.choose-color input').removeClass('active');
                            $(this).addClass('active');
                            var id = $(this).parents('.customize-popup').attr('data-popup-id');
                            var selectedColor = $(this).val();
                            $('.breakdown-totals .customize-button').attr('data-customize-color', selectedColor);
                            $('.customize-popup[data-popup-id="' + id + '"] [data-color-selected]').text(selectedColor);

                            var color = $(this).closest('.choose-color').data('color');
                            $('.customize-popup[data-popup-id="' + id + '"] .image-desc').attr('src', color.ImageUrl);
                        });
                }

                getCustomizeColor();

                function getCustomizeStorage() {
                    $('[data-product-color]').off("click", '.choose-color input', getCustomizeStorageEventHandler);
                    $('[data-product-color]').on('click', '.choose-color input', getCustomizeStorageEventHandler);
                }

                function getCustomizeStorageEventHandler() {
                    $('.choose-storage input').removeClass('active');
                    $(this).addClass('active');
                    $('.customize-popup .monthly-payment-total .value').text($(this).attr('data-storage-price'));
                    $('.breakdown-totals .customize-button').attr('data-customize-storage', $(this).val());
                    $('.breakdown-totals .customize-button')
                        .attr('data-customize-price', $(this).attr('data-storage-price'));
                }

                getCustomizeStorage();


                function customizeAddonBox() {
                    $('.customize-button').on('click', function () {
                        var offerId = $(this).parents('.customize-popup').attr('data-popup-id');
                        var cardId = $(this).parents('.customize-popup').attr('data-card-id');
                        var color = $(this).closest('.customize-popup').find('.choose-color input:checked').val();
                        var storage = $(this).closest('.customize-popup').find('.choose-storage input:checked').next('.radio').text();
                        var price = $(this).closest('.customize-popup').find('.choose-storage input:checked').attr('data-storage-price');
                        var imageUrl = $(this).closest('.customize-popup').find('.image-desc').attr('src');

                        $('.card-item[data-card-id="' + cardId + '"] li[data-offers-id="' + offerId + '"]:visible [data-color-product]').text(color);
                        $('.card-item[data-card-id="' + cardId + '"] li[data-offers-id="' + offerId + '"]:visible [data-storage-product]').text(storage);
                        $('.card-item[data-card-id="' + cardId + '"] li[data-offers-id="' + offerId + '"]:visible [data-price-product]').text(price);
                        $('.card-item[data-card-id="' + cardId + '"] li[data-offers-id="' + offerId + '"]:visible [add-offer]').attr('data-color', color).attr('data-price', price).attr('data-storage', storage);
                        $('.card-item[data-card-id="' + cardId + '"] li[data-offers-id="' + offerId + '"]:visible .image-prod img').attr('src', imageUrl);
                        $('.card-item[data-card-id="' + cardId + '"] li[data-offers-id="' + offerId + '"]:visible [data-offers-add]:visible').trigger('click');

                        createSelectButtonLinkAll();
                        hideAllPopups();

                    })
                }
                customizeAddonBox();

                var execFilters = function execFilters() {
                    var parameters = getUriParameters();
                    var packageType = $.trim(parameters.packageType);
                    if (packageType.length > 0) {
                        var subfilter = $('.plan-sub-filters [data-filter=".' + packageType + '"]');
                        if (subfilter.length > 0) {
                            execSubFilters(subfilter[0]);
                        }
                    } else {
                        execSubFilters();
                    }
                };

                var retrieveOffers = function retrieveOffers() {
                    var selectedOfferVariation = getUrlParameterByName('selectedOfferVariation');
                    var currentLanguage = $("html").attr("lang");
                    var url = "../common/scripts/prototype/data-sim/dataSimPlanOffer." + currentLanguage + ".json";
                    var templateHtml = $("#offersTemplate").html();

                    $.getJSON(url, function (data) {
                        var planOffer;

                        planOffer = data.FixedPlanOffers.map(function (offer) {
                            var newHtml = "";

                            //$(".card-item .card-content .offers ul li").attr("data-offers-id", offer.Id);

                            var cardOfferDom = $(".card-item .card-content .offers ul li[data-offers-id='" + offer.Id + "']");

                            var title = offer.Title;
                            var monthlyFee = offer.Installments[0].Variations[0].MonthlyFee;
                            var price = offer.Price;
                            var variationTitles = offer.Installments[0].Variations.map(function (variation) {
                                return variation.Title;
                            }).join(",");
                            var variationStorages = offer.Installments[0].Variations.map(function (variation) {
                                return variation.Storage.toString();
                            }).join(",");
                            var variationPrices = offer.Installments[0].Variations.map(function (variation) {
                                return variation.MonthlyFee.toString();
                            }).join(",");
                            var color = $("[data-product-color] .choose-color:first input").val();
                            var storage = offer.Installments[0].Variations[0].Title;

                            newHtml = templateHtml.replace("[ImageUrl]", offer.ImageUrl)
                                .replace(new RegExp("[Title]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), title)
                                .replace(new RegExp("[MonthlyFee]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), monthlyFee)
                                .replace(new RegExp("[dataId]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), offer.Id)
                                .replace(new RegExp("[Price]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), price)
                                .replace(new RegExp("[price]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), price)
                                .replace(new RegExp("[description]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), offer.Description)
                                .replace(new RegExp("[image]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), offer.ImageUrl)
                                .replace(new RegExp("[customizable]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'),
                                    offer.Customizable ? "1" : "0")
                                .replace(new RegExp("[variation-titles]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'),
                                    variationTitles)
                                .replace(new RegExp("[variation-storages]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'),
                                    variationStorages)
                                .replace(new RegExp("[variation-prices]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'),
                                    variationPrices)
                                .replace(new RegExp("[color]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), color)
                                .replace(new RegExp("[storage]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), storage);

                            cardOfferDom.append(newHtml);
                            if (offer.Customizable) {
                                cardOfferDom.find(".offer-non-selected-text").show();
                            } else {
                                cardOfferDom.find(".offer-non-selected-text").hide();
                            }

                            cardOfferDom.find('.buttons-prod .customize').data('offer', offer);
                        });

                        handleOfferButtons(selectedOfferVariation);

                        execFilters();
                    });
                };
                retrieveOffers();
            });

            retrieveAreas(function (areas) {

                // Refresh the layout the first time
                if ($container) {
                    $container.isotope("layout");
                }
            });
            handleSelectButtonLinks();
        });

        window.printPopupCallback = function (window) {

            setTimeout(function () {
                window.close();
            }, 250);
        };

        window.frameReadyCallback = function (window) {

            $('#PlanDetailsPopup .loadingOverlay').hide();
        };
    });

});