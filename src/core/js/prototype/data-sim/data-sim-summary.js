'use strict';

require(['jquery'], function () {

    $(function () {

        if ($('.data-sim-summary-root').length === 0) {
            return;
        }

        var getContractPeriodNum = function getContractPeriodNum(contractPeriod) {
            // return parseInt(contractPeriod.split(' ')[0]);
        };

        var retrieveOffers = function (callback) {
            var currentLanguage = $("html").attr("lang");
            var url = "/common/scripts/prototype/data-sim/dataSimPlanOffer." + currentLanguage + ".json";
            var templateHtml = $("#offersTemplate").html();

            $.getJSON(url, callback);
        };

        var createAvailableOffers = function (data) {
            var templateHtml = $("#OfferTemplate").html();
            var firstColor = $("[data-product-color] input").first().val().toLowerCase();

            $(".data-sim-summary-addons .full-box").remove();

            $(data.FixedPlanOffers).each(function (index, offer) {
                if (!offer.Visible) {
                    return;
                }

                var installment = offer.Installments[0];
                var variation = installment.Variations[0];

                var variationTitles = installment.Variations.map(function (variation) {
                    return variation.Title;
                }).join(",");
                var variationStorages = installment.Variations.map(function (variation) {
                    return variation.Storage.toString();
                }).join(",");
                var variationPrices = installment.Variations.map(function (variation) {
                    return variation.MonthlyFee.toString();
                }).join(",");

                var newHtml = templateHtml.replace(new RegExp("[offerId]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), offer.Id)
                    .replace(new RegExp("[imgUrl]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), offer.ImageUrl)
                    .replace(new RegExp("[title]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), offer.Title)
                    .replace(new RegExp("[price]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), variation.MonthlyFee)
                    .replace(new RegExp("[duration]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), installment.ContractPeriod)
                    .replace(new RegExp("[storage]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), variation.Storage)
                    .replace(new RegExp("[color]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), firstColor)
                    .replace(new RegExp("[description]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), offer.Description)
                    .replace(new RegExp("[customizable]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), offer.Customizable ? "1" : "0")
                    .replace(new RegExp("[variation-titles]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), variationTitles)
                    .replace(new RegExp("[variation-storages]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), variationStorages)
                    .replace(new RegExp("[variation-prices]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), variationPrices);

                var element = $(newHtml);
                element.data('offerData', offer);

                if (installment.ContractPeriod <= 0) {
                    element.find('.duration').hide();
                }

                if ($.trim(offer.ImageUrl).length <= 0) {
                    element.find('.image').hide();
                }

                $(".data-sim-summary-addons").append(element);
                // $('[data-brand-image]').attr('src', offer.BrandImage);
                var offerCreated = $(".data-sim-summary-addons .full-box[data-offer='" + offer.Id + "']");

                if (offer.Customizable) {
                    offerCreated.find("[data-offers-storage-color]").show();
                } else {
                    offerCreated.find("[data-offers-storage-color]").hide();
                }

                element.find('.image img').on('load', function() {
                    if (navigator.userAgent.indexOf('iPhone') <= 0) {
                        setTimeout(matchAddonHeights, 1000);
                    }
                });
            });

            if (navigator.userAgent.indexOf('iPhone') <= 0) {
                matchAddonHeights();
            }

            addOfferToSummary();
            bindOffersCustomizeButtons();
        };

        var matchAddonHeights = function () {

            var matchHeight = function (selectedElements) {
                if (selectedElements.length == 0) return;
                var elements = selectedElements;
                var largest = 0;

                var i = elements.length;
                while (i--) {
                    elements[i].style.height = "auto";

                    var height = elements[i].offsetHeight;
                    if (height > largest) {

                        largest = height;
                    }
                }
                var j = elements.length;
                while (j--) {
                    elements[j].style.height = largest + "px";
                }
            };

            matchHeight($('.full-box .add-on-box .image'));
            matchHeight($('.full-box .add-on-box .text'));
            matchHeight($('.full-box .add-on-box .totals'));

        };

        function initialize() {
            initializeControls();
            retrieveOffers(function (data) {
                createAvailableOffers(data);
                initializeData();
                refreshControls();
                getCustomizeColor(data);
            });
            editSummary();
        }

        function initializeControls() {

            $('.fixed-plans-summary .offers').empty();

            $('.fixed-plans-summary').on('click', '.change-plan-button', changePlanButton_clicked);
            $('.fixed-plans-summary').on('click', '.toggle-offer-button', toggleOfferButton_clicked);
            $('.fixed-plans-summary').on('change', '.offer .variation input[type="radio"]', offerVariation_changed);

            //Popup events
            $(".page-popup-overlay").on("click", hideAllPopups);
            $(".page-popup").on("click", '.close-button', hideAllPopups);
            $(".page-popup").on("click", '.cancel-btn', hideAllPopups);

            $('body').on('click', '.change-plan-confirmation-popup .confirm-btn', changePlanConfirmationButton_clicked);

            var parameters = getUriParameters();
            if (parameters.packageType === 'without-tv') {
                $('.set-top-box-fee').hide();
                $('.set-top-box-fee').find('[data-one-time-cost]').removeAttr('data-one-time-cost');
            }

            //Initialize popovers
            try {
                $("[data-toggle=popover]").popover({
                    trigger: 'focus',
                    container: 'body',
                    placement: 'auto top',
                    html: true
                });
            } catch (ex) {
                console.log('popover initialization failed.');
            }
        }



        function showChangePlanConfirmationPopup() {
            $('.change-plan-confirmation-popup').removeClass('hidden').addClass('popup-visible');
            $('.customize-popup').addClass('hidden');
            showPopupOverlay();
        }

        function showCustomizeItem() {
            $('.customize-popup').removeClass('hidden').addClass('popup-visible');
            $('.change-plan-confirmation-popup').addClass('hidden');
            showPopupOverlay();
        }

        function hideAllPopups() {
            $('.page-popup').removeClass('popup-visible');
            hidePopupOverlay();
        }

        function showPopupOverlay() {
            $("html").addClass("page-popup-open");
        }

        function hidePopupOverlay() {
            $("html").removeClass("page-popup-open");
        }

        var generateStorageButtons = function (variationTitles, variationStorages, variationPrices, selectedStorage) {
            var templateHtml = $("#offersStorageButtonTemplate").html();

            $(".storage .choose-storage").remove();

            var variationTitlesArray = variationTitles.split(',');
            var variationStoragesArray = variationStorages.split(',');
            var variationPricesArray = variationPrices.split(',');

            $(variationTitlesArray).each(function (index, title) {
                var positionclass = index === 0 ? "first" : (index === variationTitlesArray.length - 1 ? "last" : "");

                var newHtml = templateHtml.replace(new RegExp("[title]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), title)
                    .replace(new RegExp("[storage]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), variationStoragesArray[index])
                    .replace(new RegExp("[price]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), variationPricesArray[index])
                    .replace(new RegExp("[positionclass]".replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), positionclass);

                $(".storage").append(newHtml);
            });

            getCustomizeStorage();

            if (selectedStorage) {
                $(".storage .choose-storage input[value='" + selectedStorage + "']").trigger("click");
            }
        };

        var generateStorageButtons2 = function (variations) {

            var storage = $('.storage');
            var templateHtml = $("#offersStorageButtonTemplate").html();

            storage.find('.choose-storage').remove();

            for (var i = 0; i < variations.length; i++) {

                var variation = variations[i];

                var storageHtml = templateHtml;
                storageHtml = storageHtml.replace(/\[title]/g, variation.Title);
                storageHtml = storageHtml.replace(/\[storage]/g, variation.Storage);
                storageHtml = storageHtml.replace(/\[price]/g, variation.MonthlyFee);
                storageHtml = storageHtml.replace(/\[positionclass]/g, '');

                var storageElement = $(storageHtml);
                storageElement.data('variation', variation);

                if (!variation.Visible) {
                    storageElement.hide();
                }

                storage.append(storageElement);
            }

            if (storage.find('.choose-storage:visible').length <= 0) {
                storage.closest('[data-product-storage]').hide();
            } else {
                storage.find('.choose-storage:visible:first').addClass('first');
                storage.find('.choose-storage:visible:last').addClass('last');

                storage.closest('[data-product-storage]').show();
            }

            getCustomizeStorage();

            storage.find(".choose-storage input[type='radio']:first").addClass('active').prop('checked', true);

            var selectedVariation = storage.find('.choose-storage:first').data('variation');
            generateColorButtons(selectedVariation.Colors);
        };

        var generateColorButtons = function (colors) {

            var colorsContainer = $('[data-product-color]');
            var templateHtml = $("#offersColorButtonTemplate").html();

            colorsContainer.find('.choose-color').remove();

            for (var i = 0; i < colors.length; i++) {

                var color = colors[i];
                var positionclass = i === 0 ? "first" : (i === colors.length - 1 ? "last" : "");

                var colorHtml = templateHtml;
                colorHtml = colorHtml.replace(/\[name]/g, color.Name);
                colorHtml = colorHtml.replace(/\[color]/g, color.Value);

                var colorElement = $(colorHtml);
                colorElement.data('color', color);

                colorsContainer.append(colorElement);
            }

            colorsContainer.find(".choose-color input[type='radio']:first").addClass('active').prop('checked', true);
        };

        function bindOffersCustomizeButtons() {
            $('[data-offers-customize]').off('click');

            $('[data-offers-customize]').on('click', function (e) {
                e.preventDefault();

                var offer = $(this).closest('.full-box, .plan').data('offerData');

                var title = $(this).attr('data-title');
                var price = $(this).attr('data-price');
                var duration = $(this).attr('data-duration');
                var storage = $(this).attr('data-storage');
                var color = $(this).attr('data-color').toUpperCase();
                var offerID = $(this).attr('data-offer-id');
                var description = $(this).attr('data-desc');
                var image = $(this).attr('data-offer-img');
                var brand = title.split(' ')[0];
                var product = title.substr(title.indexOf(" ") + 1);
                var customizable = $(this).attr("data-customizable") === "1";
                var variationTitles = $(this).attr('data-variation-titles');
                var variationStorages = $(this).attr('data-variation-storages');
                var variationPrices = $(this).attr('data-variation-prices');

                if (!customizable) {
                    storage = null;
                    color = null;
                }

                var selectedStorage, selectedColor;

                var offerBoxOrdered = $(".order-box .plan.line[data-offer-id='" + offerID + "']");
                var offerBoxAdditional = $(".add-on-box[data-offer-id='" + offerID + "']");
                if (offerBoxOrdered.length > 0) {
                    selectedStorage = offerBoxOrdered.find(".plan-storage").text().replace(/,/g, "").replace(/ /g, "");
                    selectedColor = offerBoxOrdered.find(".plan-color").text().replace(/,/g, "").replace(/ /g, "").toLowerCase();
                } else if (offerBoxAdditional.length > 0) {
                    selectedStorage = offerBoxAdditional.find(".text-storage").text().replace(/,/g, "").replace(/ /g, "");
                    selectedColor = offerBoxAdditional.find(".text-color").text().replace(/,/g, "").replace(/ /g, "").toLowerCase();
                }

                if (storage == 'undefined' || !storage) {
                    $('[data-product-storage]').addClass('hidden');

                } else {
                    $('[data-product-storage]').removeClass('hidden');
                }
                if (color == 'undefined' || !color) {
                    $('[data-product-color]').addClass('hidden');
                } else {
                    $('[data-product-color]').removeClass('hidden');
                }

                var numberMatch = duration.match('[0-9]+');
                if (numberMatch.length > 0 && numberMatch[0] == '0') {
                    $('[data-contract-period]').hide();
                } else {
                    $('[data-contract-period]').show();
                }

                $('.customize-popup[data-popup-id="' + offerID + '"] .choose-color input').removeClass("active");
                $('.customize-popup[data-popup-id="' + offerID + '"] .choose-storage input').removeClass("active");

                $('.customize-popup[data-popup-id="' + offerID + '"] .choose-color input[value="' + color + '"]').addClass("active");
                $('.customize-popup[data-popup-id="' + offerID + '"] .choose-storage input[value="' + storage + '"]').addClass("active");

                $('.customize-popup').attr('data-popup-id', offerID);
                $('.customize-popup[data-popup-id="' + offerID + '"] [data-product-title] strong').text(product);
                $('.customize-popup[data-popup-id="' + offerID + '"] [data-color-selected]').text(color);
                $('.customize-popup[data-popup-id="' + offerID + '"] [data-product-title] span').text(brand + ' ');
                $('.customize-popup[data-popup-id="' + offerID + '"] [data-product-description]').text(description);
                $('.customize-popup[data-popup-id="' + offerID + '"] [data-product-plan]').text(duration);
                $('.customize-popup[data-popup-id="' + offerID + '"] .customize-popup .monthly-payment-total .value').text(price);
                $('.customize-popup[data-popup-id="' + offerID + '"] .image-desc').attr('src', image);

                if ($.trim(image).length <= 0) {
                    $('.customize-popup[data-popup-id="' + offerID + '"] .image-desc').hide();
                } else {
                    $('.customize-popup[data-popup-id="' + offerID + '"] .image-desc').show();
                }

                showCustomizeItem();

                $('[data-brand-image]').attr('src', offer.BrandImage);
                if ($.trim(offer.BrandImage).length <= 0) {
                    $('[data-brand-image]').hide();
                } else {
                    $('[data-brand-image]').show();
                }

                //generateStorageButtons(variationTitles, variationStorages, variationPrices, selectedStorage);
                generateStorageButtons2(offer.Installments[0].Variations);

                $(".monthly-payment-total .value").text(price);
                $('.breakdown-totals .customize-button').attr('data-customize-price', price);

                if (selectedColor) {
                    $('.choose-color input[value="' + selectedColor + '"]').trigger("click");
                }
                if (selectedStorage) {
                    $('.choose-storage input[value="' + selectedStorage + '"]').trigger("click");
                }
            });
        }

        function showPopups() {
            $('.close-button.plan').on('click',
                function (e) {
                    e.preventDefault();
                    showChangePlanConfirmationPopup();
                });

            bindOffersCustomizeButtons();

            $(".page-popup").on("click", '.close-button', hideAllPopups);
            $(".page-popup-overlay").on("click", hideAllPopups);
            customizeAddonBox();
        }



        function changePlanButton_clicked(e) {
            e.preventDefault();

            pushContextAwareDataLayer('Order_summary', 'change_plan');

            showChangePlanConfirmationPopup();
        }

        function changePlanConfirmationButton_clicked() {

            var parameters = getUriParameters();
            var planParameters = {
                journey: parameters.journey
            };

            var url = './';
            document.location.href = url;
        }

        function toggleOfferButton_clicked() {

            var offer = $(this).closest('.offer');
            var isAdded = $.trim(offer.attr('data-state')) == 'added';
            var offerTitle = offer.attr('data-title');
            var variationTitle = offer.find('input:checked').closest('.variation').attr('data-title');
            var category = formatDataLayerCategory(offerTitle);
            var label = $.trim(variationTitle).replace(/\s/g, '');

            if (isAdded) {
                offer.removeAttr('data-state');
                offer.find('.price .header').removeAttr('data-monthly-cost');

                if ($.trim(label).length > 0) {
                    pushDataLayer(category, 'Interested', label + '_delete');
                }
            } else {
                offer.attr('data-state', 'added');
                var monthlyFee = offer.find('.variation input:checked').closest('.variation').attr('data-monthly-fee');
                offer.find('.price .header').attr('data-monthly-cost', monthlyFee);

                if ($.trim(label).length > 0) {
                    pushDataLayer(category, 'Interested', label + '_add');
                }
            }

            recalculateFees();
            refreshControls();
        }

        function offerVariation_changed() {

            var groupName = $(this).attr('name');
            var selectedItem = $('.offer .variation input[name="' + groupName + '"]:checked').eq(0);

            if (selectedItem.length > 0) {
                var variation = selectedItem.closest('.variation');
                var monthlyFee = variation.attr('data-monthly-fee');

                var offer = selectedItem.closest('.offer');
                offer.find('.price .header .value').text(monthlyFee);

                var state = $.trim(offer.attr('data-state'));
                if (state == 'added') {
                    offer.find('.price .header').attr('data-monthly-cost', monthlyFee);
                }
            }

            recalculateFees();
            refreshControls();
        }

        function initializeData() {

            var parameters = getUriParameters();
            var isAr = $('html').attr('dir') === "rtl";
            if (isAr) {
                $('.plan .text .plan-title').html(parameters.plan + '<span class="plan-price"><span class="price"  data-monthly-cost="' + parameters.price + '">' + parameters.price + 'درهماً</span></span>');
            } else {
                $('.plan .text .plan-title').html(parameters.plan + '<span class="plan-price">AED <span class="price"  data-monthly-cost="' + parameters.price + '">' + parameters.price + '</span></span>');
            }
            $('.plan .text .plan-period').html(parameters.period + '<span class="plan-duration"> ' + parameters.duration + '</span>');

            // var monthlyFeeNumber = parameters.monthlyFee.match(/[0-9.,]+/);
            // if (monthlyFeeNumber.length > 0) {
            //     var monthlyFee = parseFloat(monthlyFeeNumber);

            //     $('.fixed-plans-summary .plan-monthly-fee').attr('data-monthly-cost', monthlyFee).find('.value').text(monthlyFee);
            // }

            var lang = getCulture();
            recalculateFees();

            // $.get('/du/scripts/plans/fixedPlanOffers.' + lang + '.json').done(renderData).fail(function () {
            //     alert('Failed to load offers');
            // });

            if (!parameters.freeOfferVariation == '') {
                addFreeOffer();
            }
            if (!parameters.selectedOfferVariation == '') {
                addSelectedOffer();
            }
        }

        var offerIsVisibleForThisContract = function offerIsVisibleForThisContract(offer, contractPeriod) {
            return offer.Installments.some(function (installment) {
                if (!installment.FixedPlanMinPeriod) {
                    return true;
                }

                if (contractPeriod >= installment.FixedPlanMinPeriod) {
                    return true;
                }

                return false;
            });
        };

        function refreshControls() {
            var newParameters = {};
            var parameters = getUriParameters();

            var addons = [];
            $('.data-sim-summary .order-box .plan.line .btn[data-offer-id]:visible').each(function () {
                var button = $(this);
                var title = $.trim(button.attr('data-title'));
                var storage = $.trim(button.attr('data-storage'));
                var color = $.trim(button.attr('data-color'));

                var addon = title;
                if (storage.length > 0 && storage.toLowerCase() !== 'undefined') {
                    addon += ' ' + storage;
                }
                if (color.length > 0 && color.toLowerCase() !== 'undefined') {
                    addon += ' ' + color;
                }

                addons.push(addon);
            });
            newParameters.addons = addons.join(',');

            newParameters.category = 'Data Sim';



            var oneTimeFee = newParameters.oneTimeFee = $('.data-sim-summary .breakdown-totals .one-time-payment-total .value').text();
            var monthlyFee = $('.data-sim-summary .breakdown-totals .monthly-payment-total .value').text();
            var dataPlan = $.trim(parameters.plan).replace(/\s/g, '');
            var dataSize = $.trim(parameters.dataSize).replace(/\s/g, '');
            var minutes = '';
            var smsCode = '';
            var contractPeriod = '';

            if (parameters.period) {
                var matches = parameters.period.match(/[0-9,.]+/);
                if (matches.length > 0) {
                    contractPeriod = matches[0];
                }
            }

            var customerType = $.trim(parameters.customer);
            if (customerType.length > 0) {
                newParameters.customerType = customerType;
            }

            newParameters.p = $.trim(parameters.plan) + '-' + monthlyFee + '-' + contractPeriod + '-' + dataPlan + '-' + dataSize + '-' + minutes + '-' + smsCode;

            var url = $('.data-sim-summary .breakdown-totals .buy-button').attr('href').match(/^[^?^#]+/)[0];
            var newUrl = url + '?' + $.param(newParameters).replace(/\+/g, '%20');

            $('.data-sim-summary .breakdown-totals .buy-button').attr('href', newUrl);

            var tags = pushContextAwareDataLayer('Total', 'Order now_AED ' + monthlyFee, true);
            $('.fixed-plans-summary .breakdown-totals .buy-button').attr('onclick', 'dataLayer.push(' + JSON.stringify(tags) + ');');
        }

        function recalculateFees() {

            var totalMonthlyCost = 0;
            $('.data-sim-summary-description').find('[data-monthly-cost]:visible').each(function (index, element) {
                var monthlyCost = parseInt($(element).attr('data-monthly-cost'));
                totalMonthlyCost += monthlyCost;
            });

            var totalOneTimeCostCost = 0;
            $('.data-sim-summary-description').find('[data-one-time-cost]:visible').each(function (index, element) {
                var oneTimeCost = parseInt($(element).attr('data-one-time-cost'));
                totalOneTimeCostCost += oneTimeCost;
            });

            $('.plan .final-price').text(totalMonthlyCost);
            $('.breakdown-totals .monthly-payment-total .heading .value').text(totalMonthlyCost);
            $('.breakdown-totals .one-time-payment-total .heading .value').text(totalOneTimeCostCost);
        }

        //Utility methods
        function getCulture() {
            var path = $.trim(document.location.pathname).toLowerCase();
            if (path === "ar" || path.startsWith("ar/") || path.startsWith("/ar/")) {
                return 'ar';
            } else {
                return 'en';
            }
        };

        function getUriParameters() {

            var parameters = {};
            var search = decodeURI(document.location.search);
            var pattern = /[&?]([^=^&^#]+)=([^&^#]+)/g;
            var match = null;

            while (match = pattern.exec(search)) {
                var parameter = match[1];
                var value = match[2];

                parameters[parameter] = decodeURIComponent(value.replace(/\+/g, ' '));
            }

            return parameters;
        }

        function formatDataLayerCategory(category) {
            var fallbackCategory = 'Homeservices_plan';
            var result = fallbackCategory;
            if ($.trim(category).length > 2) {
                result = category[0].toUpperCase() + category.toLowerCase().replace(/\s/g, '_').substring(1);
            } else if ($.trim(category).length > 1) {
                result = journey[0].toUpperCase();
            }

            return result;
        }

        var bindRemoveOfferButtonClick = function () {
            $('.close-button.addon').unbind('click');

            $('.close-button.addon').on('click',
                function () {
                    var data = $(this).parents('.plan').attr('data-offer');
                    $(this).parents('.plan').remove();
                    $('.full-box[data-offer="' + data + '"]').removeClass('hidden');
                    recalculateFees();
                    refreshControls();
                });
        };

        function addSelectedOffer() {
            var parameters = getUriParameters();

            if ($('.add-on-box[data-offer-id="' + parameters.selectedOfferVariation + '"]')) {

                var box = $('.add-on-box[data-offer-id="' + parameters.selectedOfferVariation + '"]');
                box.parents('.full-box').addClass('hidden');
                var button = box.find('[add-offer]');
                var planName = button.attr('data-plan');
                var planStorage = button.attr('data-storage');
                var planColor = $.trim(button.attr('data-color' + ', ')).replace(/^(.)|\s(.)/g, function ($1) {
                    return $1.toUpperCase();
                });
                var planPrice = button.attr('data-price');
                var planPeriod = button.attr('data-period');
                var plan = button.attr('data-duration');
                var offer = box.parents('.full-box').attr('data-offer');
                var offerID = box.parents('.add-on-box').attr('data-offer-id');
                var storage = parameters.prodStorage;
                var color = parameters.prodColor;
                var planDesc = button.attr('data-desc');
                var planImage = button.attr('data-offer-img');
                var customizable = button.attr('data-customizable');
                var variationTitles = button.attr('data-variation-titles');
                var variationStorages = button.attr('data-variation-storages');
                var variationPrices = button.attr('data-variation-prices');

                if (customizable !== "1") {
                    storage = "";
                    color = "";
                } else {
                    try {
                        var storageValue = parseInt(storage);
                        if (!isNaN(storageValue)) {
                            storage = storageValue.toString();
                        }
                    } catch (ex) {}
                }

                var selectedOfferTemplate = $('#SelectedOfferTemplate').html();
                var data = {
                    planImage: planImage,
                    planDesc: planDesc,
                    planName: planName,
                    offerID: parameters.selectedOfferVariation,
                    planStorage: storage,
                    planColor: parameters.prodColor,
                    planPrice: parameters.prodPrice,
                    planPeriod: planPeriod,
                    plan: plan,
                    offer: offer,
                    storage: storage,
                    color: color,
                    customizable: customizable,
                    variationTitles: variationTitles,
                    variationStorages: variationStorages,
                    variationPrices: variationPrices
                };
                var final = replaceTemplate(selectedOfferTemplate, data);

                var finalElement = $(final);
                finalElement.data('offerData', box.closest('.full-box').data('offerData'));

                $(finalElement).appendTo('.order-box-product');

                if (customizable === "1") {
                    $("[data-plan-storage-color]").css("visibility", "");
                } else {
                    $("[data-plan-storage-color]").css("visibility", "hidden");
                }

                $('.order-box-product, .plan').removeClass('edit');
                $('.change-plan-button').removeClass('hidden');
                $('.change-confirm').addClass('hidden');

                bindRemoveOfferButtonClick();
                showPopups();
                recalculateFees();

            }
        }

        function addFreeOffer() {
            var parameters = getUriParameters();

            if ($('.add-on-box[data-offer-id="' + parameters.freeOfferVariation + '"]')) {

                var box = $('.add-on-box[data-offer-id="' + parameters.freeOfferVariation + '"]');
                box.parents('.full-box').addClass('hidden');
                var button = box.find('[add-offer]');
                var planName = button.attr('data-plan');
                var planStorage = button.attr('data-storage');
                var planColor = $.trim(button.attr('data-color' + ', ')).replace(/^(.)|\s(.)/g, function ($1) {
                    return $1.toUpperCase();
                });
                var planPrice = 0;
                var planPeriod = button.attr('data-period');
                var plan = button.attr('data-duration');
                var offer = box.parents('.full-box').attr('data-offer');
                var offerID = box.parents('.add-on-box').attr('data-offer-id');
                var storage = parameters.prodStorage;
                var color = parameters.prodColor;
                var planDesc = button.attr('data-desc');
                var planImage = button.attr('data-offer-img');
                var customizable = button.attr('data-customizable');
                var variationTitles = button.attr('data-variation-titles');
                var variationStorages = button.attr('data-variation-storages');
                var variationPrices = 0;

                if (customizable !== "1") {
                    storage = "";
                    color = "";
                }

                var selectedOfferTemplate = $('#SelectedOfferTemplate').html();
                var data = {
                    planImage: planImage,
                    planDesc: planDesc,
                    planName: planName,
                    offerID: parameters.freeOfferVariation,
                    planStorage: parameters.prodStorage,
                    planColor: parameters.prodColor,
                    planPrice: 0,
                    planPeriod: planPeriod,
                    plan: plan,
                    offer: offer,
                    storage: storage,
                    color: color,
                    customizable: customizable,
                    variationTitles: variationTitles,
                    variationStorages: variationStorages,
                    variationPrices: variationPrices
                };
                var final = replaceTemplate(selectedOfferTemplate, data);

                var finalElement = $(final);
                finalElement.data('offerData', box.closest('.full-box').data('offerData'));
                finalElement.find('.close-button').hide();
                finalElement.find('[data-offers-customize]').hide();

                $(finalElement).appendTo('.order-box-product');

                if (customizable === "1") {
                    $("[data-plan-storage-color]").css("visibility", "");
                } else {
                    $("[data-plan-storage-color]").css("visibility", "hidden");
                }

                $('.order-box-product, .plan').removeClass('edit');
                $('.change-plan-button').removeClass('hidden');
                $('.change-confirm').addClass('hidden');


                //bindRemoveOfferButtonClick();
                //showPopups();
                recalculateFees();

            }
        }

        function pushContextAwareDataLayer(action, label, deferPush) {
            var parameters = getUriParameters();
            var journey = parameters.journey;
            var category = formatDataLayerCategory(journey);

            return pushDataLayer(category, action, label, deferPush);
        }

        function pushDataLayer(category, action, label, deferPush) {
            var tags = {
                event: 'click',
                category: category,
                action: action,
                label: label
            };

            if (!deferPush) {
                dataLayer.push(tags);
            }

            return tags;
        }

        function editSummary() {
            var editSummaryButton = $('.change-plan-button');
            var confirmChangeButton = $('.change-confirm');

            editSummaryButton.on('click', function (e) {
                e.preventDefault();
                $('.plan').addClass('edit');
                $(this).addClass('hidden');
                confirmChangeButton.removeClass('hidden');
            })

            confirmChangeButton.on('click', function (e) {
                e.preventDefault();
                $('.plan').removeClass('edit');
                $(this).addClass('hidden');
                editSummaryButton.removeClass('hidden');
            })
        }

        function replaceTemplate(template, data) {
            var actualTemplate = $.trim(template);
            var actualData = data || {};

            var result = template.replace(/\[([^\]]+)]/g, function (match, capture) {
                var key = match;
                if ($.trim(capture).length > 0) {
                    key = capture;
                }

                var value = actualData[key];
                if (typeof value == 'undefined' || value == null) {
                    value = '';
                }

                if (typeof value == 'string') {
                    value = value.replace('"', '&quot;');
                }

                return value;
            });

            return result;
        }

        function addOfferToSummary() {
            $('[add-offer]').unbind('click');

            $('[add-offer]').on('click',
                function (e) {
                    e.preventDefault();
                    $(this).parents('.full-box').addClass('hidden');
                    var planName = $(this).attr('data-plan');
                    var planStorage = $(this).attr('data-storage');
                    var planColor = $.trim($(this).attr('data-color'));
                    var planPrice = $(this).attr('data-price');
                    var planPeriod = $(this).attr('data-period');
                    var plan = $(this).attr('data-duration');
                    var offer = $(this).parents('.full-box').attr('data-offer');
                    var offerID = $(this).parents('.add-on-box').attr('data-offer-id');
                    var planDesc = $(this).attr('data-desc');
                    var planImage = $(this).attr('data-offer-img');
                    var customizable = $(this).attr('data-customizable');
                    var variationTitles = $(this).attr('data-variation-titles');
                    var variationStorages = $(this).attr('data-variation-storages');
                    var variationPrices = $(this).attr('data-variation-prices');

                    var selectedOfferTemplate = $('#SelectedOfferTemplate').html();
                    var data = {
                        planImage: planImage,
                        planDesc: planDesc,
                        planName: planName,
                        offerID: offerID,
                        planStorage: planStorage,
                        planColor: planColor,
                        planPrice: planPrice,
                        planPeriod: planPeriod,
                        plan: plan,
                        offer: offer,
                        customizable: customizable,
                        variationTitles: variationTitles,
                        variationStorages: variationStorages,
                        variationPrices: variationPrices
                    };
                    var final = replaceTemplate(selectedOfferTemplate, data);

                    var finalElement = $(final);

                    var offer = $(this).closest('.full-box, .plan').data('offerData');
                    finalElement.data('offerData', offer);

                    finalElement.appendTo('.order-box-product');
                    $('.order-box-product, .plan').removeClass('edit');
                    $('.change-plan-button').removeClass('hidden');
                    $('.change-confirm').addClass('hidden');
                    $('.customize-popup').attr('data-popup-id', offerID);

                    bindRemoveOfferButtonClick();

                    if (planColor === undefined) {
                        $('.plan[data-offer-id="' + offerID + '"] .plan-color').addClass('hidden');

                    } else {
                        $('.plan[data-offer-id="' + offerID + '"] .plan-color').removeClass('hidden');
                    }


                    if (planStorage === undefined) {
                        $('.plan[data-offer-id="' + offerID + '"] .plan-storage').addClass('hidden');

                    } else {
                        $('.plan[data-offer-id="' + offerID + '"] .plan-storage').removeClass('hidden');
                    }

                    if (customizable === "1") {
                        $(".plan[data-offer-id='" + offerID + "'] [data-plan-storage-color]").css("visibility", "");
                    } else {
                        $(".plan[data-offer-id='" + offerID + "'] [data-plan-storage-color]").css("visibility", "hidden");
                    }

                    showPopups();
                    recalculateFees();
                    refreshControls();
                });
        }

        function getCustomizeColor(data) {
            $('[data-product-color]').on('click', '.choose-color input', function () {
                $('.choose-color input').removeClass('active');
                $(this).addClass('active');
                var id = $(this).parents('.customize-popup').attr('data-popup-id');
                var selectedColor = $(this).val();
                $('.customize-popup[data-popup-id="' + id + '"] .breakdown-totals .customize-button').attr('data-customize-color', selectedColor)
                $('.customize-popup[data-popup-id="' + id + '"] [data-color-selected]').text(selectedColor);

                var color = $(this).closest('.choose-color').data('color');
                $('.customize-popup[data-popup-id="' + id + '"] .image-desc').attr('src', color.ImageUrl);
            });
        }

        function getCustomizeStorage() {
            $('.choose-storage input').unbind("click");

            $('.choose-storage input').on('click', function () {
                $('.choose-storage input').removeClass('active');
                $(this).addClass('active');
                var id = $(this).parents('.customize-popup').attr('data-popup-id');
                $('.customize-popup[data-popup-id="' + id + '"] .monthly-payment-total .value').text($(this).attr('data-storage-price'));
                $('.customize-popup[data-popup-id="' + id + '"] .breakdown-totals .customize-button').attr('data-customize-storage', $(this).val())
                $('.customize-popup[data-popup-id="' + id + '"] .breakdown-totals .customize-button').attr('data-customize-price', $(this).attr('data-storage-price'));

            })
        }

        function getCustomizeStorageEventHandler() {
            $('[data-product-color]').off("click", '.choose-color input', getCustomizeStorageEventHandler);
            $('[data-product-color]').on('click', '.choose-color input', getCustomizeStorageEventHandler);
        }

        function customizeAddonBox() {
            $('.customize-button').on('click', function () {
                var id = $(this).parents('.customize-popup').attr('data-popup-id');
                var color = $(this).attr('data-customize-color');
                var storage = $(this).attr('data-customize-storage');
                var price = $(this).attr('data-customize-price');
                var colorCap = $.trim($(this).attr('data-customize-color')).replace(/^(.)|\s(.)/g, function ($1) {
                    return $1.toUpperCase();
                });

                $('.add-on-box[data-offer-id="' + id + '"] .text-color').text(color);
                $('.add-on-box[data-offer-id="' + id + '"] .text-storage').text(storage);
                $('.add-on-box[data-offer-id="' + id + '"] .text-price').text(price);
                $('.add-on-box[data-offer-id="' + id + '"] [add-offer]').attr('data-color', color)
                    .attr('data-price', price).attr('data-storage', storage);
                $('.add-on-box[data-offer-id="' + id + '"] [data-offers-customize]').attr('data-color', color)
                    .attr('data-price', price).attr('data-storage', storage);

                $('.plan[data-offer-id="' + id + '"] .plan-storage').text(storage);
                $('.plan[data-offer-id="' + id + '"] .plan-color').text(colorCap);
                $('.plan[data-offer-id="' + id + '"] [data-monthly-cost]').text('AED ' + price);
                $('.plan[data-offer-id="' + id + '"] .price').attr('data-monthly-cost', price);
                $('.plan[data-offer-id="' + id + '"] [data-offers-customize]').attr('data-color', color);
                $('.plan[data-offer-id="' + id + '"] [data-offers-customize]').attr('data-storage', storage);

                hideAllPopups();
                recalculateFees();
            });
        }

        retrieveOffers(function (data) {
            getCustomizeColor(data);
        });


        customizeAddonBox();
        getCustomizeStorage();
        addOfferToSummary();
        hideAllPopups();
        showPopups();
        editSummary();
        initialize();
        refreshControls();
    });

});