$.fn.dataTableExt.afnFiltering.push(
    function(oSettings, aData, iDataIndex) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear(); //.toString().substring(2);

        if (dd < 10)
            dd = '0' + dd;

        if (mm < 10)
            mm = '0' + mm;

        today = dd + '/' + mm + '/' + yyyy;
 
        if (($('#startdate').length && $('#startdate').val() != '') || ($('#enddate').length && $('#enddate').val() != '')) {
            var iMin_temp = $('#startdate').val();
            if (iMin_temp == '') {
                iMin_temp = '01/01/1991';
            }

            var iMax_temp = $('#enddate').val();
            if (iMax_temp == '') {
                iMax_temp = today;
              //  console.log(iMax_temp);
            }

            if (iMin_temp === undefined || iMax_temp === undefined) return;

            var arr_min = iMin_temp.split("/");
            var arr_max = iMax_temp.split("/");
            var date_column = aData[0].substring(0, 10);
            var arr_date = date_column.split("/");
            //if (arr_min[2] > 90) {
            //    arr_min[2] = "19" + arr_min[2];
            //} else {
            //    arr_min[2] = "20" + arr_min[2];
            //}
            //if (arr_max[2] > 90) {
            //    arr_max[2] = "19" + arr_max[2];
            //} else {
            //    arr_max[2] = "20" + arr_max[2];
            //}
            //if (arr_date[2] > 90) {
            //    arr_date[2] = "19" + arr_date[2];
            //} else {
            //    arr_date[2] = "20" + arr_date[2];
            //}


           // console.log('arr_min:' + arr_min);
          //  console.log('arr_max:' + arr_max);
          //  console.log('arr_date:' + arr_date);

            //alert(arr_min[2]);
            var iMin = new Date(arr_min[2], arr_min[1] - 1, arr_min[0], 0, 0, 0, 0)
            var iMax = new Date(arr_max[2], arr_max[1] - 1, arr_max[0], 0, 0, 0, 0)
            var iDate = new Date(arr_date[2], arr_date[1] - 1, arr_date[0], 0, 0, 0, 0)


         //   console.log('iMin:' + iMin);
         //   console.log('iMax:' + iMax);
         //   console.log('iDate:' + iDate);

            if (iMin == "" && iMax == "") {
                return true;
            } else if (iMin == "" && iDate < iMax) {
                return true;
            } else if (iMin <= iDate && "" == iMax) {
                return true;
            } else if (iMin <= iDate && iDate <= iMax) {
                return true;
            }
            return false;
        } else {
            return true;
        }
    }
);
