//localStorage.clear();

requirejs.config({
    "baseUrl": "../common/scripts",
    "paths": {
        // the left side is the module ID,
        // the right side is the path to
        // the jQuery file, relative to baseUrl.
        // Also, the path should NOT include
        // the '.js' file extension. This example
        // is using jQuery 1.9.0 located at
        // js/lib/jquery-1.9.0.js, relative to
        // the HTML page.

        'jquery': 'jquery-3.0.0.min',
        'jquery-migrate': 'jquery-migrate-3.0.0.min',

       // 'jquery': 'jquery-1.11.2.min',

        'datatables': 'prototype/jquery.dataTables.min',
        'icheck':'prototype/icheck',
        isotope: 'jquery.isotope.min',
        bridget: 'jquery-bridget.min'
    },
    shim: {
       //'jquery-migrate': { deps: ['jquery'], exports: 'jQuery' },
        "icheck": ['jquery'],
        "isotope": ['jquery']
    }
});

// Load the main app module to start the app


//require(['jquery-migrate', 'datatables','icheck','isotope', 'bridget'], function ($,datatables, icheck,isotope,base) {
require(['jquery', 'datatables','icheck','isotope', 'bridget'], function ($,datatables, icheck,isotope,base) {
'use strict';

    // layout mode that does not position items
isotope.LayoutMode.create('none');

    require([
        //'jquery',
        'modernizr-2.6.2',
        'jquery.imagesloaded.min',
        'hammer',
        'jquery.hammer',
        //'jquery.isotope.min',
        'tabulous.min',
        'select2.min',
        'jquery.reject',
        'respond.min',
        'jquery.barrating',
        'jquery.cookie.min',
        'motion-ui.min',
        'jquery.sticky-kit.min',
        'jquery.auto-complete.min',
        //'jquery.stickymenu.min.js',
        //'nouislider.min',
        //'isInViewport.min',
        //'jquery.masonry.min',
        //'maps-api-v3',
        //'jquery.magnific-popup.min',
        'countrySelect.min',
        'jquery.bxslider',
        'swiper.min',
        'du/duTabs',
        'du/duDetect',
        'du/duColumns',
        'du/duScroll',
        'du/duSlide',
        'du/duLoginTabs',
        'du/duBuy',
        'du/duProductScroll',
        'du/duScrollToTop',
        'du/duTabAccordion',
        // 'du/contentflow',
        'du/duCoverflow',
        //'du/isotopeFIlter',
        'du/layoutRpr',
        'prototype/jquery.reveal',
        'prototype/jquery.alert',
        'prototype/bootstrap.popover',
        'prototype/chosen.jquery.min',
        'prototype/jquery.knob',
        //'prototype/dropzone',
        'prototype/jquery.dial',
        'prototype/jquery.timer',
        'prototype/jquery.validator',
        'prototype/protoSlide',
        'prototype/jquery.selectric',
        'prototype/d3.min',
        'prototype/jquery.center.min',
        'prototype/jquery.charter',
        //'prototype/icheck',
        //'prototype/jquery.dataTables.min',
        'prototype/jquery.datetimepicker',
        'prototype/jquery.dateRangeFilter',
        'prototype/jquery.dateSort',
        'prototype/jquery.matchHeight.min',
        'prototype/jquery.msg.min',
        'prototype/jquery.printElement.min',
        'prototype/jquery.tools.min',
        'prototype/list.min',
        'du/duQuickRecharge.Autocomplete',
        'prototype/list.pagination',
        'prototype/jScrollPane5152',
        'prototype/jScrollUI',
        'du/duSupportTickets',
        'prototype/zinger/popup',
        'prototype/zinger/zinger',
        'prototype/zinger/forms',
        'prototype/accessibility',
        'prototype/devices/utilities',
        'prototype/devices/tooltip',
        'prototype/devices/deviceDetails',
        'prototype/devices/planCards',
        'prototype/devices/deviceModelPlan',
        'prototype/devices/orderSummary',
        'prototype/devices/duAccordion',
        'prototype/devices/planInformation',
        'prototype/productShowcase',
        'prototype/media-center',
        'prototype/fixed-plans/apple-tv',
        'prototype/fixed-plans/fixed-plans',
        'prototype/fixed-plans/fixed-plans-summary',
        'emirati-plan',
        'business-employee-offers',
        'prototype/our-network',
        'prototype/formWizard',
        'prototype/data-sim/data-sim',
        'prototype/data-sim/data-sim-summary',
        'prototype/roadshow',
        'du/mod-onboarding'
  
    ], function () {
        require([
            /*'all.du.min'*/
            'du/init_global',
            'du/ie_en',
            'du/init_en',
            'custom/custom.min'
        ])
    });
});