 

//promise shim
import Es6Promise from 'es6-promise';
Es6Promise.polyfill();
 

function component () {
  var element = document.createElement('div');

  /* lodash is required for the next line to work */
  //element.innerHTML = _.join(['Hello from selfcare','webpack',moment().format()], ' ');

  return element;
}

document.body.appendChild(component());
 