

//promise shim
import Es6Promise from 'es6-promise';
Es6Promise.polyfill();
 import 'core/js/jquery.hammer';

function component () {
  var element = document.createElement('div');

  /* lodash is required for the next line to work */
  //element.innerHTML = _.join(['Hello from selfcare','webpack',moment().format()], ' ');

  return element;
}

document.body.appendChild(component());

/*
function determineDate() {
  import('moment')
    .then(moment => moment().format('LLLL'))
    .then(str => console.log(str))
    .catch(err => console.log('Failed to load moment', err));
}

determineDate();*/