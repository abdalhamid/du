//import _ from 'lodash';
//import moment from 'moment';

//promise shim
import Es6Promise from 'es6-promise';
Es6Promise.polyfill();



import 'core/js/jquery.hammer';
import 'core/js/prototype/jquery.dial';
import 'core/js/prototype/jquery.validator';
import 'core/js/prototype/jquery.timer';
import 'core/js/prototype/jquery.datetimepicker';
import 'core/js/prototype/jquery.datatables.min';
import 'core/js/prototype/bootstrap.popover';
import 'core/js/prototype/jquery.matchHeight.min';
import 'isotope-layout';
import 'core/js/prototype/icheck';


//var Isotope = require('isotope-layout');
//Isotope.LayoutMode.create('none');
import 'core/styles/du_en.less';
// load assets
function requireAll(r) { r.keys().forEach(r); }
requireAll(require.context('core/images/', true));

jQuery.fn.isotope = function(options)
{
    return this.each(function()
    {
        var Isotope = require('isotope-layout');
        var isotope = new Isotope(this, options);

        var $el = jQuery(this);
        $el.data("isotope", isotope);

        var events = ['arrangeComplete','layoutComplete','removeComplete'];

        for(var e=0; e<events.length; e++) {
          
            isotope[events[e]] = (function(el, eventName) {
           
                return function(ev) {
                    el.trigger(jQuery.Event(eventName, ev));
                };
            })($el, events[e]);
        }
    });
};
$('.grid').isotope({
  itemSelector: '.grid-item',
  masonry: {
    columnWidth: 100
  }
});

$('.grid2').isotope({
  itemSelector: '.grid2-item',
  percentPosition: true,
  masonry: {
    columnWidth: '.grid2-sizer'
  }
});

//import test from './common/common/js/util';




 

//console.log(jQuery)
 // return $div;



//$(document.body).append(component());

